namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("_MWV_subcontract_orders")]
    public partial class C_MWV_subcontract_orders
    {
        [Key]
        [Column(Order = 0)]
        public DateTime created_date { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long created_by { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(20)]
        public string customer_po { get; set; }

        public DateTime? processed_date { get; set; }

        [StringLength(50)]
        public string processed_filename { get; set; }

        public int? building_id { get; set; }
    }
}
