namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("_message_config")]
    public partial class C_message_config
    {
        [Key]
        public int message_config_id { get; set; }

        [StringLength(50)]
        public string Language { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        public int? FontSize { get; set; }

        public int? Delay { get; set; }

        public int? Bold { get; set; }

        public int? Width { get; set; }

        public int? Gap { get; set; }

        public int? Height { get; set; }

        public int? Pixel { get; set; }

        [StringLength(50)]
        public string PixelMode { get; set; }

        [StringLength(50)]
        public string Orientation { get; set; }

        public bool? Alternate0 { get; set; }

        public bool? Alternate1 { get; set; }

        public bool? Alternate7 { get; set; }

        public bool? RepeatPrint { get; set; }

        public bool? RepeatPitch { get; set; }

        public int? Year { get; set; }

        public int? Month { get; set; }

        public int? Day { get; set; }

        public int? Hour { get; set; }

        public int? Minute { get; set; }
    }
}
