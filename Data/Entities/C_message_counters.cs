namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("_message_counters")]
    public partial class C_message_counters
    {
        [Key]
        public int message_counter_id { get; set; }

        public int message_config_id { get; set; }

        public int counter_no { get; set; }

        public int? Start { get; set; }

        public int? Upper { get; set; }

        public int? Lower { get; set; }

        public int? Increment { get; set; }

        public int? Repeat { get; set; }

        public int? RepeatStart { get; set; }

        public int? Digits { get; set; }

        public bool? LeadZero { get; set; }

        public int? Value { get; set; }

        public bool? WrapAround { get; set; }
    }
}
