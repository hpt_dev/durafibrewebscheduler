namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("_message_exp_dates")]
    public partial class C_message_exp_dates
    {
        [Key]
        public int message_exp_date_id { get; set; }

        public int message_config_id { get; set; }

        public int exp_date_no { get; set; }

        public int? Day { get; set; }

        public int? Week { get; set; }

        public int? Month { get; set; }

        public int? Year { get; set; }
    }
}
