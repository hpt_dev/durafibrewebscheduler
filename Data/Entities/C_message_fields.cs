namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("_message_fields")]
    public partial class C_message_fields
    {
        [Key]
        public int message_field_id { get; set; }

        public int message_config_id { get; set; }

        [StringLength(50)]
        public string Type { get; set; }

        [StringLength(50)]
        public string DateType { get; set; }

        [StringLength(50)]
        public string Format { get; set; }

        [StringLength(50)]
        public string Language { get; set; }

        public string Text { get; set; }

        public int? FontSize { get; set; }

        public int? X { get; set; }

        public int? Y { get; set; }

        public int? Bold { get; set; }

        public int? Gap { get; set; }

        [StringLength(50)]
        public string Orientation { get; set; }

        public bool? UserDefined { get; set; }

        public bool? ReversedDate { get; set; }

        public bool? EncodedDate { get; set; }

        [StringLength(1)]
        public string DateDelimiter { get; set; }

        [StringLength(1)]
        public string TimeDelimiter { get; set; }
    }
}
