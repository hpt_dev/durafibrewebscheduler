namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("_message_shifts")]
    public partial class C_message_shifts
    {
        [Key]
        public int message_shift_id { get; set; }

        public int message_config_id { get; set; }

        public int? Hour { get; set; }

        public int? Minute { get; set; }

        public string Text { get; set; }
    }
}
