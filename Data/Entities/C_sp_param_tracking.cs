namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("_sp_param_tracking")]
    public partial class C_sp_param_tracking
    {
        [Key]
        public long param_track_id { get; set; }

        public long batch_id { get; set; }

        [Required]
        [StringLength(500)]
        public string sp_name { get; set; }

        public int parm_num { get; set; }

        [Required]
        [StringLength(500)]
        public string parm_name { get; set; }

        [Required]
        public string parm_value { get; set; }

        public DateTime? created_date { get; set; }
    }
}
