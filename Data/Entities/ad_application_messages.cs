namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_application_messages
    {
        [Key]
        public long application_message_id { get; set; }

        [Required]
        [StringLength(100)]
        public string application_id { get; set; }

        public long message_type_id { get; set; }

        [Required]
        public string message { get; set; }

        public DateTime message_time { get; set; }

        public long? user_id { get; set; }

        public Guid? instance_guid { get; set; }

        public virtual ad_application_message_type ad_application_message_type { get; set; }
    }
}
