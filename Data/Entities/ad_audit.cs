namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_audit
    {
        [Key]
        public long audit_id { get; set; }

        [StringLength(30)]
        public string app_name { get; set; }

        [StringLength(50)]
        public string node_name { get; set; }

        [StringLength(30)]
        public string user_name { get; set; }

        [Required]
        [StringLength(30)]
        public string table_name { get; set; }

        public long table_changed_id { get; set; }

        [Required]
        [StringLength(30)]
        public string field_name { get; set; }

        [Required]
        [StringLength(50)]
        public string old_value { get; set; }

        [Required]
        [StringLength(50)]
        public string new_value { get; set; }

        public DateTime timestamp_of_change { get; set; }
    }
}
