namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_comments
    {
        [Key]
        public long comment_id { get; set; }

        [Required]
        [StringLength(50)]
        public string inventory_id { get; set; }

        public string comment { get; set; }

        public int comment_type_id { get; set; }

        public int created_by { get; set; }

        public DateTime created_date { get; set; }

        public long? on_hand_inventory_id { get; set; }

        public virtual ad_comment_types ad_comment_types { get; set; }

        public virtual ic_on_hand_inventory ic_on_hand_inventory { get; set; }
    }
}
