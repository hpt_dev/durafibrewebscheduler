namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_joined_work_centers
    {
        [Key]
        public int joined_work_centers_id { get; set; }

        public int work_center_id { get; set; }

        public int joined_work_center_id { get; set; }

        public bool is_active { get; set; }

        public int created_by { get; set; }

        public DateTime created_date { get; set; }

        public int last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public int? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual sf_work_centers sf_work_centers { get; set; }
    }
}
