﻿namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ad_page_security")]
    public partial class ad_page_security
    {
        [Key]
        public string path { get; set; }
        public string json { get; set; }
    }
}
