namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_predefined_comments
    {
        [Key]
        public int predefined_comment_id { get; set; }

        [Required]
        [StringLength(50)]
        public string predefined_comment_description { get; set; }

        public int comment_type_id { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public virtual ad_comment_types ad_comment_types { get; set; }
    }
}
