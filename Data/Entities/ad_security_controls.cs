namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_security_controls
    {
        [Key]
        public long security_control_id { get; set; }

        public long security_screen_id { get; set; }

        public long control_id { get; set; }

        public bool visibility_prop { get; set; }

        public bool isreadonly_prop { get; set; }

        public bool? is_active { get; set; }

        public virtual ad_security_screens ad_security_screens { get; set; }

        public virtual ps_controls ps_controls { get; set; }
    }
}
