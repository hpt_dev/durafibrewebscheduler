namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_security_modules
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ad_security_modules()
        {
            ad_security_screens = new HashSet<ad_security_screens>();
        }

        [Key]
        public long security_module_id { get; set; }

        public long security_group_id { get; set; }

        public long module_id { get; set; }

        public bool accessible { get; set; }

        public virtual ad_security_groups ad_security_groups { get; set; }

        public virtual ps_modules ps_modules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_security_screens> ad_security_screens { get; set; }
    }
}
