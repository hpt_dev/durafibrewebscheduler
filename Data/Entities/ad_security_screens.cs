namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_security_screens
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ad_security_screens()
        {
            ad_security_controls = new HashSet<ad_security_controls>();
        }

        [Key]
        public long security_screen_id { get; set; }

        public long security_module_id { get; set; }

        public long screen_id { get; set; }

        public bool accessible { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_security_controls> ad_security_controls { get; set; }

        public virtual ad_security_modules ad_security_modules { get; set; }

        public virtual ps_screens ps_screens { get; set; }
    }
}
