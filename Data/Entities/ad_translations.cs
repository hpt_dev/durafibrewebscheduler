namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_translations
    {
        [Key]
        public long translation_id { get; set; }

        public byte language_id { get; set; }

        public int text_id { get; set; }

        [Required]
        public string translation { get; set; }

        public virtual ad_languages ad_languages { get; set; }
    }
}
