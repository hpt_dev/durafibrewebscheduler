namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_user_work_center_associations
    {
        [Key]
        public long user_work_center_association_id { get; set; }

        public long user_id { get; set; }

        public int work_center_id { get; set; }

        public bool is_default { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual sf_work_centers sf_work_centers { get; set; }
    }
}
