namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ad_users()
        {
            ad_security_groups = new HashSet<ad_security_groups>();
            ad_security_groups1 = new HashSet<ad_security_groups>();
            ad_security_groups2 = new HashSet<ad_security_groups>();
            ad_user_buildings = new HashSet<ad_user_buildings>();
            ad_user_work_center_associations = new HashSet<ad_user_work_center_associations>();
            ad_users1 = new HashSet<ad_users>();
            ad_users11 = new HashSet<ad_users>();
            ad_users12 = new HashSet<ad_users>();
            ap_po_children = new HashSet<ap_po_children>();
            ap_po_children1 = new HashSet<ap_po_children>();
            ap_po_master = new HashSet<ap_po_master>();
            ap_po_master1 = new HashSet<ap_po_master>();
            cs_addresses = new HashSet<cs_addresses>();
            cs_addresses1 = new HashSet<cs_addresses>();
            cs_addresses2 = new HashSet<cs_addresses>();
            cs_buildings = new HashSet<cs_buildings>();
            cs_buildings1 = new HashSet<cs_buildings>();
            cs_buildings2 = new HashSet<cs_buildings>();
            cs_companies = new HashSet<cs_companies>();
            cs_companies1 = new HashSet<cs_companies>();
            cs_companies2 = new HashSet<cs_companies>();
            cs_custom_field_values = new HashSet<cs_custom_field_values>();
            cs_custom_field_values1 = new HashSet<cs_custom_field_values>();
            cs_custom_field_values2 = new HashSet<cs_custom_field_values>();
            cs_custom_fields = new HashSet<cs_custom_fields>();
            cs_custom_fields1 = new HashSet<cs_custom_fields>();
            cs_custom_fields2 = new HashSet<cs_custom_fields>();
            cs_customer_contacts = new HashSet<cs_customer_contacts>();
            cs_customer_contacts1 = new HashSet<cs_customer_contacts>();
            cs_customer_contacts2 = new HashSet<cs_customer_contacts>();
            cs_customer_manufacturing_notes = new HashSet<cs_customer_manufacturing_notes>();
            cs_customer_manufacturing_notes1 = new HashSet<cs_customer_manufacturing_notes>();
            cs_customer_notes = new HashSet<cs_customer_notes>();
            cs_customer_third_party_settings = new HashSet<cs_customer_third_party_settings>();
            cs_customer_third_party_settings1 = new HashSet<cs_customer_third_party_settings>();
            cs_customer_third_party_settings2 = new HashSet<cs_customer_third_party_settings>();
            cs_customers = new HashSet<cs_customers>();
            cs_customers1 = new HashSet<cs_customers>();
            cs_customers2 = new HashSet<cs_customers>();
            cs_grid_conditionals = new HashSet<cs_grid_conditionals>();
            cs_grid_conditionals1 = new HashSet<cs_grid_conditionals>();
            cs_grid_conditionals2 = new HashSet<cs_grid_conditionals>();
            cs_locations = new HashSet<cs_locations>();
            cs_locations1 = new HashSet<cs_locations>();
            cs_locations2 = new HashSet<cs_locations>();
            ic_customer_items = new HashSet<ic_customer_items>();
            ic_customer_items1 = new HashSet<ic_customer_items>();
            ic_dim_groups = new HashSet<ic_dim_groups>();
            ic_dim_groups1 = new HashSet<ic_dim_groups>();
            ic_item_categories = new HashSet<ic_item_categories>();
            ic_item_categories1 = new HashSet<ic_item_categories>();
            ic_item_children = new HashSet<ic_item_children>();
            ic_item_children1 = new HashSet<ic_item_children>();
            ic_item_children2 = new HashSet<ic_item_children>();
            ic_item_master = new HashSet<ic_item_master>();
            ic_item_master1 = new HashSet<ic_item_master>();
            ic_item_master2 = new HashSet<ic_item_master>();
            ic_physical_inventory_history = new HashSet<ic_physical_inventory_history>();
            pac_templates = new HashSet<pac_templates>();
            pac_templates1 = new HashSet<pac_templates>();
            ps_third_party_options = new HashSet<ps_third_party_options>();
            ps_third_party_options1 = new HashSet<ps_third_party_options>();
            ps_third_party_options2 = new HashSet<ps_third_party_options>();
            ps_third_party_services = new HashSet<ps_third_party_services>();
            ps_third_party_services1 = new HashSet<ps_third_party_services>();
            ps_third_party_services2 = new HashSet<ps_third_party_services>();
            sf_bom_substitutions = new HashSet<sf_bom_substitutions>();
            sf_bom_substitutions1 = new HashSet<sf_bom_substitutions>();
            sf_bom_substitutions2 = new HashSet<sf_bom_substitutions>();
            sf_nodes = new HashSet<sf_nodes>();
            sf_nodes1 = new HashSet<sf_nodes>();
            sf_nodes2 = new HashSet<sf_nodes>();
            sf_work_centers = new HashSet<sf_work_centers>();
            sf_work_centers1 = new HashSet<sf_work_centers>();
            sf_work_centers2 = new HashSet<sf_work_centers>();
            sh_carriers = new HashSet<sh_carriers>();
            sh_carriers1 = new HashSet<sh_carriers>();
            sh_carriers2 = new HashSet<sh_carriers>();
            sh_freight_loads = new HashSet<sh_freight_loads>();
            sh_freight_loads1 = new HashSet<sh_freight_loads>();
            sh_shipments = new HashSet<sh_shipments>();
            sh_shipments1 = new HashSet<sh_shipments>();
            so_associated_charges = new HashSet<so_associated_charges>();
            so_associated_charges1 = new HashSet<so_associated_charges>();
            so_associated_charges2 = new HashSet<so_associated_charges>();
            so_charges = new HashSet<so_charges>();
            so_charges1 = new HashSet<so_charges>();
            so_charges2 = new HashSet<so_charges>();
            so_children = new HashSet<so_children>();
            so_children1 = new HashSet<so_children>();
            so_children_service_settings = new HashSet<so_children_service_settings>();
            so_children_service_settings1 = new HashSet<so_children_service_settings>();
            so_children_service_settings2 = new HashSet<so_children_service_settings>();
            so_sales_orders = new HashSet<so_sales_orders>();
            so_sales_orders1 = new HashSet<so_sales_orders>();
            ut_SSRS_log = new HashSet<ut_SSRS_log>();
            wo_approval_groups = new HashSet<wo_approval_groups>();
            wo_approval_groups1 = new HashSet<wo_approval_groups>();
            wo_approval_groups2 = new HashSet<wo_approval_groups>();
            wo_approval_settings = new HashSet<wo_approval_settings>();
            wo_approval_settings1 = new HashSet<wo_approval_settings>();
            wo_approval_settings2 = new HashSet<wo_approval_settings>();
            wo_approval_statuses = new HashSet<wo_approval_statuses>();
            wo_approval_statuses1 = new HashSet<wo_approval_statuses>();
            wo_approval_statuses2 = new HashSet<wo_approval_statuses>();
            wo_approval_templates = new HashSet<wo_approval_templates>();
            wo_approval_templates1 = new HashSet<wo_approval_templates>();
            wo_approval_templates2 = new HashSet<wo_approval_templates>();
            wo_master = new HashSet<wo_master>();
            wo_master1 = new HashSet<wo_master>();
            wo_notes = new HashSet<wo_notes>();
            wo_side_by_sides = new HashSet<wo_side_by_sides>();
            wo_side_by_sides1 = new HashSet<wo_side_by_sides>();
            wo_side_by_sides2 = new HashSet<wo_side_by_sides>();
            ad_security_groups3 = new HashSet<ad_security_groups>();
        }

        [Key]
        public long USER_ID { get; set; }

        [Required]
        [StringLength(10)]
        public string alt_user_id { get; set; }

        [Required]
        [StringLength(20)]
        public string first_name { get; set; }

        [Required]
        [StringLength(20)]
        public string last_name { get; set; }

        [Required]
        [StringLength(12)]
        public string USER_NAME { get; set; }

        [Required]
        [StringLength(150)]
        public string password { get; set; }

        public DateTime pwd_exp_date { get; set; }

        public byte language_id { get; set; }

        [StringLength(150)]
        public string user_email { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] version_stamp { get; set; }

        public int? building_id { get; set; }

        public bool? global_user { get; set; }

        public bool? deny_or_access { get; set; }

        public bool? is_named_user { get; set; }

        public virtual ad_languages ad_languages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_security_groups> ad_security_groups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_security_groups> ad_security_groups1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_security_groups> ad_security_groups2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_user_buildings> ad_user_buildings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_user_work_center_associations> ad_user_work_center_associations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_users> ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_users> ad_users11 { get; set; }

        public virtual ad_users ad_users3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_users> ad_users12 { get; set; }

        public virtual ad_users ad_users4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ap_po_children> ap_po_children { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ap_po_children> ap_po_children1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ap_po_master> ap_po_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ap_po_master> ap_po_master1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_addresses> cs_addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_addresses> cs_addresses1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_addresses> cs_addresses2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_buildings> cs_buildings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_buildings> cs_buildings1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_buildings> cs_buildings2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_companies> cs_companies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_companies> cs_companies1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_companies> cs_companies2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_custom_field_values> cs_custom_field_values { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_custom_field_values> cs_custom_field_values1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_custom_field_values> cs_custom_field_values2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_custom_fields> cs_custom_fields { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_custom_fields> cs_custom_fields1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_custom_fields> cs_custom_fields2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_contacts> cs_customer_contacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_contacts> cs_customer_contacts1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_contacts> cs_customer_contacts2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_manufacturing_notes> cs_customer_manufacturing_notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_manufacturing_notes> cs_customer_manufacturing_notes1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_notes> cs_customer_notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_third_party_settings> cs_customer_third_party_settings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_third_party_settings> cs_customer_third_party_settings1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_third_party_settings> cs_customer_third_party_settings2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customers> cs_customers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customers> cs_customers1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customers> cs_customers2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_conditionals> cs_grid_conditionals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_conditionals> cs_grid_conditionals1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_conditionals> cs_grid_conditionals2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_locations> cs_locations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_locations> cs_locations1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_locations> cs_locations2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_customer_items> ic_customer_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_customer_items> ic_customer_items1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_dim_groups> ic_dim_groups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_dim_groups> ic_dim_groups1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_categories> ic_item_categories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_categories> ic_item_categories1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_children> ic_item_children { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_children> ic_item_children1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_children> ic_item_children2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_master> ic_item_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_master> ic_item_master1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_master> ic_item_master2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_history> ic_physical_inventory_history { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pac_templates> pac_templates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pac_templates> pac_templates1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_third_party_options> ps_third_party_options { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_third_party_options> ps_third_party_options1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_third_party_options> ps_third_party_options2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_third_party_services> ps_third_party_services { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_third_party_services> ps_third_party_services1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_third_party_services> ps_third_party_services2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_bom_substitutions> sf_bom_substitutions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_bom_substitutions> sf_bom_substitutions1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_bom_substitutions> sf_bom_substitutions2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_nodes> sf_nodes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_nodes> sf_nodes1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_nodes> sf_nodes2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_work_centers> sf_work_centers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_work_centers> sf_work_centers1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_work_centers> sf_work_centers2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_carriers> sh_carriers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_carriers> sh_carriers1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_carriers> sh_carriers2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_freight_loads> sh_freight_loads { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_freight_loads> sh_freight_loads1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_shipments> sh_shipments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_shipments> sh_shipments1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_associated_charges> so_associated_charges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_associated_charges> so_associated_charges1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_associated_charges> so_associated_charges2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_charges> so_charges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_charges> so_charges1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_charges> so_charges2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children> so_children { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children> so_children1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children_service_settings> so_children_service_settings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children_service_settings> so_children_service_settings1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children_service_settings> so_children_service_settings2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_sales_orders> so_sales_orders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_sales_orders> so_sales_orders1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ut_SSRS_log> ut_SSRS_log { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_groups> wo_approval_groups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_groups> wo_approval_groups1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_groups> wo_approval_groups2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_settings> wo_approval_settings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_settings> wo_approval_settings1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_settings> wo_approval_settings2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_statuses> wo_approval_statuses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_statuses> wo_approval_statuses1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_statuses> wo_approval_statuses2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_templates> wo_approval_templates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_templates> wo_approval_templates1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_templates> wo_approval_templates2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_master> wo_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_master> wo_master1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_notes> wo_notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_side_by_sides> wo_side_by_sides { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_side_by_sides> wo_side_by_sides1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_side_by_sides> wo_side_by_sides2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_security_groups> ad_security_groups3 { get; set; }
    }
}
