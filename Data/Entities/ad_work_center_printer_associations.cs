namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ad_work_center_printer_associations
    {
        [Key]
        public int work_center_printer_association_id { get; set; }

        public int work_center_id { get; set; }

        public int printer_id { get; set; }

        public bool is_default { get; set; }

        public virtual lbl_printers lbl_printers { get; set; }
    }
}
