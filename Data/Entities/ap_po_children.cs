namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ap_po_children
    {
        [Key]
        public long po_child_id { get; set; }

        public long po_master_id { get; set; }

        public long item_child_id { get; set; }

        public decimal price { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public bool is_taxable { get; set; }

        public decimal? order_qty { get; set; }

        [StringLength(20)]
        public string order_uom { get; set; }

        public decimal? stock_uom_1 { get; set; }

        public decimal? stock_uom_2 { get; set; }

        public decimal? stock_uom_weight { get; set; }

        public decimal? discount { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ap_po_master ap_po_master { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }
    }
}
