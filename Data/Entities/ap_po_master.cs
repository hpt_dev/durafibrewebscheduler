namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ap_po_master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ap_po_master()
        {
            ap_po_children = new HashSet<ap_po_children>();
        }

        [Key]
        public long po_master_id { get; set; }

        [Required]
        [StringLength(20)]
        public string po_number { get; set; }

        [StringLength(20)]
        public string work_order_no { get; set; }

        public long bill_to_customer_id { get; set; }

        public long bill_to_address_id { get; set; }

        public long ship_to_customer_id { get; set; }

        public long ship_to_address_id { get; set; }

        public int po_type_id { get; set; }

        public int? ship_type_id { get; set; }

        public DateTime? delivery_date { get; set; }

        public decimal tax_rate { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public decimal subtotal { get; set; }

        [Column(TypeName = "text")]
        public string notes { get; set; }

        public int delivery_term_id { get; set; }

        public int payment_term_id { get; set; }

        public DateTime? ship_date { get; set; }

        public decimal? total_tax { get; set; }

        public decimal? total_discount { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ap_delivery_terms ap_delivery_terms { get; set; }

        public virtual ap_payment_terms ap_payment_terms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ap_po_children> ap_po_children { get; set; }

        public virtual ap_ship_types ap_ship_types { get; set; }

        public virtual cs_customers cs_customers { get; set; }
    }
}
