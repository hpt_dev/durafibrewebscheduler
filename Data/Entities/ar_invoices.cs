namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ar_invoices
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ar_invoices()
        {
            ar_payments_received = new HashSet<ar_payments_received>();
        }

        [Key]
        public long invoice_id { get; set; }

        [Required]
        [StringLength(20)]
        public string invoice_no { get; set; }

        public long so_children_id { get; set; }

        public decimal amount { get; set; }

        public bool fully_paid { get; set; }

        public bool is_released { get; set; }

        [Column(TypeName = "text")]
        public string notes { get; set; }

        public long? shipment_id { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long last_modified_by { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ar_payments_received> ar_payments_received { get; set; }
    }
}
