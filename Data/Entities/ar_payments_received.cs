namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ar_payments_received
    {
        [Key]
        public long payment_received_id { get; set; }

        public long invoice_id { get; set; }

        public int payment_type_id { get; set; }

        public decimal amount { get; set; }

        [Column(TypeName = "text")]
        public string notes { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        [StringLength(30)]
        public string invoice_no { get; set; }

        public virtual ar_invoices ar_invoices { get; set; }

        public virtual ar_payment_types ar_payment_types { get; set; }
    }
}
