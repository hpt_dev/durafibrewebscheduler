namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_required_materials
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bom_required_materials()
        {
            bom_route_required_materials = new HashSet<bom_route_required_materials>();
            bom_subassemblies = new HashSet<bom_subassemblies>();
        }

        [Key]
        public long required_material_id { get; set; }

        public long step_id { get; set; }

        public long? item_master_id { get; set; }

        public long? item_child_id { get; set; }

        public int inventory_type_id { get; set; }

        [StringLength(50)]
        public string generic_type_item_name { get; set; }

        public bool is_generic_type { get; set; }

        public bool is_specific_item { get; set; }

        public decimal quantity { get; set; }

        [Required]
        [StringLength(20)]
        public string uom { get; set; }

        public decimal scrap { get; set; }

        public decimal cost { get; set; }

        public bool is_optional { get; set; }

        public bool is_backflush { get; set; }

        public bool is_subassembly { get; set; }

        public bool is_phantom { get; set; }

        public bool allocate_in_oe { get; set; }

        public long? based_on_produced_item_id { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual bom_produced_items bom_produced_items { get; set; }

        public virtual bom_steps bom_steps { get; set; }

        public virtual ic_inventory_types ic_inventory_types { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }

        public virtual ic_item_master ic_item_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_required_materials> bom_route_required_materials { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_subassemblies> bom_subassemblies { get; set; }
    }
}
