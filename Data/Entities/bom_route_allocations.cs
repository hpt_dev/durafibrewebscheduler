namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_route_allocations
    {
        [Key]
        public long route_allocation_id { get; set; }

        public long? route_step_id { get; set; }

        public long? route_required_material_id { get; set; }

        public long on_hand_inventory_id { get; set; }

        public long? unit_allocation_id { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual bom_route_required_materials bom_route_required_materials { get; set; }

        public virtual bom_route_steps bom_route_steps { get; set; }

        public virtual ic_on_hand_inventory ic_on_hand_inventory { get; set; }

        public virtual ic_unit_allocations ic_unit_allocations { get; set; }
    }
}
