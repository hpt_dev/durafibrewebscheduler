namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_route_item_transactions
    {
        [Key]
        public long route_item_transaction_id { get; set; }

        public long route_produced_item_id { get; set; }

        public decimal? qty_completed_change { get; set; }

        public decimal? qty_scrap_change { get; set; }

        public int source_operation_no { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual bom_route_produced_items bom_route_produced_items { get; set; }
    }
}
