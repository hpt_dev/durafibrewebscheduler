namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_route_required_materials
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bom_route_required_materials()
        {
            bom_route_allocations = new HashSet<bom_route_allocations>();
            bom_route_material_transactions = new HashSet<bom_route_material_transactions>();
            bom_subassembly_hierarchies = new HashSet<bom_subassembly_hierarchies>();
        }

        [Key]
        public long route_required_material_id { get; set; }

        public long required_material_id { get; set; }

        public long route_step_id { get; set; }

        public long item_master_id { get; set; }

        public long? item_child_id { get; set; }

        public int inventory_type_id { get; set; }

        [StringLength(50)]
        public string generic_type_item_name { get; set; }

        [Required]
        [StringLength(20)]
        public string ordered_uom { get; set; }

        public decimal expected_scrap { get; set; }

        public decimal quantity_ordered { get; set; }

        public decimal quantity_on_hand { get; set; }

        public decimal quantity_completed { get; set; }

        public decimal quantity_scrapped { get; set; }

        public DateTime? date_started { get; set; }

        public DateTime? date_completed { get; set; }

        public bool backflush { get; set; }

        public bool is_subassembly { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual bom_required_materials bom_required_materials { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_allocations> bom_route_allocations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_material_transactions> bom_route_material_transactions { get; set; }

        public virtual bom_route_steps bom_route_steps { get; set; }

        public virtual ic_inventory_types ic_inventory_types { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }

        public virtual ic_item_master ic_item_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_subassembly_hierarchies> bom_subassembly_hierarchies { get; set; }
    }
}
