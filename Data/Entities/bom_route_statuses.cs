namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_route_statuses
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bom_route_statuses()
        {
            bom_route_steps = new HashSet<bom_route_steps>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int route_status_id { get; set; }

        [Required]
        [StringLength(3)]
        public string status { get; set; }

        [Required]
        [StringLength(25)]
        public string status_description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_steps> bom_route_steps { get; set; }
    }
}
