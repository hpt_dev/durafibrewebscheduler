namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_route_steps
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bom_route_steps()
        {
            bom_route_allocations = new HashSet<bom_route_allocations>();
            bom_route_produced_items = new HashSet<bom_route_produced_items>();
            bom_route_required_materials = new HashSet<bom_route_required_materials>();
            bom_subassembly_hierarchies = new HashSet<bom_subassembly_hierarchies>();
            wo_step_attributes = new HashSet<wo_step_attributes>();
        }

        [Key]
        public long route_step_id { get; set; }

        public long wo_master_id { get; set; }

        public long step_id { get; set; }

        public int route_status_id { get; set; }

        public int operation_no { get; set; }

        public int? node_id { get; set; }

        public int work_center_group_id { get; set; }

        public int? work_center_id { get; set; }

        public DateTime? date_started { get; set; }

        public DateTime? date_completed { get; set; }

        public string step_note { get; set; }

        public bool user_saved { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_allocations> bom_route_allocations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_produced_items> bom_route_produced_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_required_materials> bom_route_required_materials { get; set; }

        public virtual bom_route_statuses bom_route_statuses { get; set; }

        public virtual bom_steps bom_steps { get; set; }

        public virtual sf_work_center_groups sf_work_center_groups { get; set; }

        public virtual sf_work_centers sf_work_centers { get; set; }

        public virtual wo_master wo_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_subassembly_hierarchies> bom_subassembly_hierarchies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_step_attributes> wo_step_attributes { get; set; }
    }
}
