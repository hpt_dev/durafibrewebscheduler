namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_rules
    {
        [Key]
        [Column(Order = 0)]
        public long id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string name { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(500)]
        public string rule_filter { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(500)]
        public string step_filter { get; set; }

        [StringLength(500)]
        public string source { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool is_active { get; set; }

        public int? control_type { get; set; }
    }
}
