namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_step_attribute_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bom_step_attribute_types()
        {
            sf_work_center_bom_attributes = new HashSet<sf_work_center_bom_attributes>();
        }

        [Key]
        public long bom_attribute_type_id { get; set; }

        [StringLength(500)]
        public string attribute_type_name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_work_center_bom_attributes> sf_work_center_bom_attributes { get; set; }
    }
}
