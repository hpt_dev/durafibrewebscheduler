namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_step_rules
    {
        public long id { get; set; }

        public long rule_id { get; set; }

        [Required]
        [StringLength(500)]
        public string pattern { get; set; }

        public long step_id { get; set; }

        [Required]
        [StringLength(50)]
        public string step { get; set; }

        public long? parent_id { get; set; }

        [Required]
        [StringLength(50)]
        public string selector { get; set; }

        [Required]
        [StringLength(500)]
        public string value { get; set; }

        [Required]
        [StringLength(500)]
        public string filter { get; set; }

        [StringLength(500)]
        public string selected_work_centers { get; set; }

        [StringLength(50)]
        public string work_center_selector { get; set; }

        public long created_by { get; set; }

        public bool is_active { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }
    }
}
