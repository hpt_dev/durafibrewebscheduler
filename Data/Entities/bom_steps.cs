namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_steps
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bom_steps()
        {
            bom_produced_items = new HashSet<bom_produced_items>();
            bom_required_materials = new HashSet<bom_required_materials>();
            bom_route_steps = new HashSet<bom_route_steps>();
        }

        [Key]
        public long step_id { get; set; }

        public long template_id { get; set; }

        public int step_option_id { get; set; }

        public int work_center_group_id { get; set; }

        public string description { get; set; }

        public string notes { get; set; }

        public bool? is_optional { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_produced_items> bom_produced_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_required_materials> bom_required_materials { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_steps> bom_route_steps { get; set; }

        public virtual bom_step_options bom_step_options { get; set; }

        public virtual bom_templates bom_templates { get; set; }

        public virtual sf_work_center_groups sf_work_center_groups { get; set; }
    }
}
