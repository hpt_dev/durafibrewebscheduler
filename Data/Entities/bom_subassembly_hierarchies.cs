namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_subassembly_hierarchies
    {
        [Key]
        public long hierarchy_id { get; set; }

        public long parent_wo_master_id { get; set; }

        public long parent_route_step_id { get; set; }

        public long parent_route_required_material_id { get; set; }

        public long child_wo_master_id { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual bom_route_required_materials bom_route_required_materials { get; set; }

        public virtual bom_route_steps bom_route_steps { get; set; }

        public virtual wo_master wo_master { get; set; }

        public virtual wo_master wo_master1 { get; set; }
    }
}
