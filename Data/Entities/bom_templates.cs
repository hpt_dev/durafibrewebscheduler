namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_templates
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bom_templates()
        {
            bom_steps = new HashSet<bom_steps>();
            bom_subassemblies = new HashSet<bom_subassemblies>();
            bom_templates1 = new HashSet<bom_templates>();
            wo_master = new HashSet<wo_master>();
        }

        [Key]
        public long template_id { get; set; }

        [Required]
        [StringLength(100)]
        public string bom_name { get; set; }

        public string bom_description { get; set; }

        [StringLength(100)]
        public string bom_code { get; set; }

        public long? item_master_id { get; set; }

        public long? item_child_id { get; set; }

        public int inventory_type_id { get; set; }

        [StringLength(50)]
        public string generic_type_item_name { get; set; }

        public bool is_generic_type { get; set; }

        public bool is_specific_item { get; set; }

        public decimal quantity { get; set; }

        [Required]
        [StringLength(20)]
        public string uom { get; set; }

        public long? inherits_from_template_id { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_steps> bom_steps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_subassemblies> bom_subassemblies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_templates> bom_templates1 { get; set; }

        public virtual bom_templates bom_templates2 { get; set; }

        public virtual ic_inventory_types ic_inventory_types { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }

        public virtual ic_item_master ic_item_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_master> wo_master { get; set; }
    }
}
