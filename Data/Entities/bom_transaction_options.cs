namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class bom_transaction_options
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bom_transaction_options()
        {
            bom_produced_items = new HashSet<bom_produced_items>();
        }

        [Key]
        public int transaction_option_id { get; set; }

        [Required]
        [StringLength(50)]
        public string transaction_option { get; set; }

        [StringLength(50)]
        public string display { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_modified_by { get; set; }

        public DateTime? deleted_modified_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_produced_items> bom_produced_items { get; set; }
    }
}
