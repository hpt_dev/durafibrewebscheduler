namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_addresses
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_addresses()
        {
            cs_buildings = new HashSet<cs_buildings>();
            cs_companies = new HashSet<cs_companies>();
            cs_customer_addresses = new HashSet<cs_customer_addresses>();
            cs_customer_manufacturing_notes = new HashSet<cs_customer_manufacturing_notes>();
            cs_customer_work_group_notes = new HashSet<cs_customer_work_group_notes>();
            sh_carriers = new HashSet<sh_carriers>();
            sh_transit_days = new HashSet<sh_transit_days>();
            so_children = new HashSet<so_children>();
        }

        [Key]
        public long address_id { get; set; }

        [StringLength(50)]
        public string address { get; set; }

        [StringLength(50)]
        public string address_2 { get; set; }

        [StringLength(50)]
        public string address_3 { get; set; }

        [StringLength(50)]
        public string attention { get; set; }

        [StringLength(30)]
        public string city { get; set; }

        [StringLength(3)]
        public string state { get; set; }

        [StringLength(10)]
        public string zip_code { get; set; }

        [StringLength(4)]
        public string zip_code_suffix { get; set; }

        public bool is_active { get; set; }

        public int? country_id { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_buildings> cs_buildings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_companies> cs_companies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_addresses> cs_customer_addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_manufacturing_notes> cs_customer_manufacturing_notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_work_group_notes> cs_customer_work_group_notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_carriers> sh_carriers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_transit_days> sh_transit_days { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children> so_children { get; set; }
    }
}
