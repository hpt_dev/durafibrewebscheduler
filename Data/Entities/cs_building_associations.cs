namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_building_associations
    {
        [Key]
        public long building_association_id { get; set; }

        public int type_id { get; set; }

        public int building_id_from { get; set; }

        public int building_id_to { get; set; }
    }
}
