namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_buildings
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_buildings()
        {
            cs_locations = new HashSet<cs_locations>();
            ic_item_quantities = new HashSet<ic_item_quantities>();
            ic_physical_inventory_history = new HashSet<ic_physical_inventory_history>();
            sf_work_centers = new HashSet<sf_work_centers>();
            sh_transit_days = new HashSet<sh_transit_days>();
            so_sales_orders = new HashSet<so_sales_orders>();
            wo_master = new HashSet<wo_master>();
        }

        [Key]
        public int building_id { get; set; }

        public byte company_id { get; set; }

        public int building_id_rk { get; set; }

        [Required]
        [StringLength(30)]
        public string building_name { get; set; }

        [StringLength(50)]
        public string building_desc { get; set; }

        public long? building_address_id { get; set; }

        public int building_disporder { get; set; }

        public bool building_visibility { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }

        public long? rk_level { get; set; }

        public int offset_minutes { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        public virtual cs_addresses cs_addresses { get; set; }

        public virtual cs_companies cs_companies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_locations> cs_locations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_quantities> ic_item_quantities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_history> ic_physical_inventory_history { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_work_centers> sf_work_centers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_transit_days> sh_transit_days { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_sales_orders> so_sales_orders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_master> wo_master { get; set; }
    }
}
