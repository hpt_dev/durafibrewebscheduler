namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_business_units
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_business_units()
        {
            ic_consumed_inventory = new HashSet<ic_consumed_inventory>();
            ic_on_hand_inventory = new HashSet<ic_on_hand_inventory>();
        }

        [Key]
        public int business_unit_id { get; set; }

        [Required]
        [StringLength(50)]
        public string business_unit { get; set; }

        [Required]
        [StringLength(2)]
        public string gl_acct_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_consumed_inventory> ic_consumed_inventory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_on_hand_inventory> ic_on_hand_inventory { get; set; }
    }
}
