namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_carrier_contacts
    {
        [Key]
        public long carrier_contact_id { get; set; }

        public long carrier_id { get; set; }

        [Required]
        [StringLength(200)]
        public string first_name { get; set; }

        [Required]
        [StringLength(200)]
        public string last_name { get; set; }

        [StringLength(19)]
        public string work_phone { get; set; }

        [StringLength(19)]
        public string mobile_phone { get; set; }

        [StringLength(19)]
        public string fax_phone { get; set; }

        [StringLength(500)]
        public string contact_email { get; set; }

        [StringLength(500)]
        public string contact_title { get; set; }

        [StringLength(500)]
        public string contact_dept { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual sh_carriers sh_carriers { get; set; }
    }
}
