namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_carrier_notes
    {
        [Key]
        public long carrier_note_id { get; set; }

        public long carrier_id { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string carrier_note { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public virtual sh_carriers sh_carriers { get; set; }
    }
}
