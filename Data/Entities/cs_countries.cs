namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_countries
    {
        [Key]
        public long country_id { get; set; }

        [Required]
        [StringLength(100)]
        public string country_name { get; set; }

        [Required]
        [StringLength(10)]
        public string country_abbr { get; set; }

        public byte language_id { get; set; }

        [StringLength(100)]
        public string postcode_scheme { get; set; }

        [StringLength(20)]
        public string postcode_example { get; set; }

        public int? region_id { get; set; }

        public virtual ad_languages ad_languages { get; set; }
    }
}
