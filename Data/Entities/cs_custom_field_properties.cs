namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_custom_field_properties
    {
        [Key]
        public long custom_field_property_id { get; set; }

        public int custom_field_id { get; set; }

        [Required]
        public string property_name { get; set; }

        [Required]
        public string property_type { get; set; }

        public string property_value { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }

        public virtual cs_custom_fields cs_custom_fields { get; set; }
    }
}
