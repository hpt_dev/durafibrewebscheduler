namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_custom_field_rules
    {
        [Key]
        public long custom_field_rule_id { get; set; }

        public long custom_field_id { get; set; }

        [Required]
        [StringLength(500)]
        public string object_field_name { get; set; }

        [Required]
        [StringLength(100)]
        public string object_field_value { get; set; }

        public long rule_id { get; set; }
    }
}
