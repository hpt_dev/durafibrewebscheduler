namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_custom_fields
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_custom_fields()
        {
            cs_custom_field_properties = new HashSet<cs_custom_field_properties>();
            cs_custom_field_values = new HashSet<cs_custom_field_values>();
            sf_work_center_bom_attributes = new HashSet<sf_work_center_bom_attributes>();
        }

        [Key]
        public int custom_field_id { get; set; }

        public int table_id { get; set; }

        [Required]
        [StringLength(50)]
        public string field_name { get; set; }

        public byte field_type_id { get; set; }

        public int text_id { get; set; }

        public bool is_required { get; set; }

        public bool is_active { get; set; }

        public int? disporder { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? formula_id { get; set; }

        public bool display_only { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_custom_field_properties> cs_custom_field_properties { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_custom_field_values> cs_custom_field_values { get; set; }

        public virtual ps_field_types ps_field_types { get; set; }

        public virtual ps_tables ps_tables { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_work_center_bom_attributes> sf_work_center_bom_attributes { get; set; }
    }
}
