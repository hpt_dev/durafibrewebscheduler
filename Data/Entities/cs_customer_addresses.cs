namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_customer_addresses
    {
        [Key]
        public long customer_address_id { get; set; }

        public long customer_id { get; set; }

        public long address_id { get; set; }

        public int address_type_id { get; set; }

        public bool default_address { get; set; }

        public virtual cs_address_types cs_address_types { get; set; }

        public virtual cs_addresses cs_addresses { get; set; }

        public virtual cs_customers cs_customers { get; set; }
    }
}
