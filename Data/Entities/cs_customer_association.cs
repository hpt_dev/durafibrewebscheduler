namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_customer_association
    {
        [Key]
        public long alt_customer_id { get; set; }

        public long customer_id { get; set; }

        [Required]
        [StringLength(50)]
        public string alt_name { get; set; }

        public bool is_active { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public long? modified_by { get; set; }

        public DateTime? modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual cs_customers cs_customers { get; set; }
    }
}
