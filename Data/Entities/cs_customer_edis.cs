namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_customer_edis
    {
        [Key]
        public long customer_edi_id { get; set; }

        public long customer_id { get; set; }

        public int edi_type { get; set; }

        public int file_type { get; set; }

        [Required]
        [StringLength(100)]
        public string file_desc { get; set; }

        public virtual cs_customers cs_customers { get; set; }
    }
}
