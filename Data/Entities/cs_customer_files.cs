namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_customer_files
    {
        [Key]
        public long customer_file_id { get; set; }

        public long customer_id { get; set; }

        public long file_id { get; set; }

        public string file_note { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual cs_customers cs_customers { get; set; }

        public virtual ps_files ps_files { get; set; }
    }
}
