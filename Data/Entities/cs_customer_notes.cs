namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_customer_notes
    {
        [Key]
        public long customer_note_id { get; set; }

        public long customer_id { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string customer_note { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public bool note_is_active { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual cs_customers cs_customers { get; set; }
    }
}
