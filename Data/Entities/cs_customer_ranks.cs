namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_customer_ranks
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_customer_ranks()
        {
            cs_customers = new HashSet<cs_customers>();
        }

        [Key]
        public int customer_rank_id { get; set; }

        public int customer_rank_value { get; set; }

        [Required]
        [StringLength(50)]
        public string customer_rank { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customers> cs_customers { get; set; }
    }
}
