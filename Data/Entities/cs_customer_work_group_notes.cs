namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_customer_work_group_notes
    {
        [Key]
        public long cutomer_work_group_note_id { get; set; }

        public long customer_id { get; set; }

        public long address_id { get; set; }

        public int work_center_group_id { get; set; }

        [Required]
        public string customer_work_group_note { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual cs_addresses cs_addresses { get; set; }

        public virtual sf_work_center_groups sf_work_center_groups { get; set; }
    }
}
