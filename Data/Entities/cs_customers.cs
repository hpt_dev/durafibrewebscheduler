namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_customers
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_customers()
        {
            ap_po_master = new HashSet<ap_po_master>();
            cs_customer_addresses = new HashSet<cs_customer_addresses>();
            cs_customer_association = new HashSet<cs_customer_association>();
            cs_customer_contacts = new HashSet<cs_customer_contacts>();
            cs_customer_edis = new HashSet<cs_customer_edis>();
            cs_customer_files = new HashSet<cs_customer_files>();
            cs_customer_manufacturing_notes = new HashSet<cs_customer_manufacturing_notes>();
            cs_customer_notes = new HashSet<cs_customer_notes>();
            cs_customer_third_party_settings = new HashSet<cs_customer_third_party_settings>();
            cs_customers1 = new HashSet<cs_customers>();
            edi_customer_file_formats = new HashSet<edi_customer_file_formats>();
            edi_customer_mappings = new HashSet<edi_customer_mappings>();
            edi_ftpserver_info = new HashSet<edi_ftpserver_info>();
            edi_mappings = new HashSet<edi_mappings>();
            edi_regex = new HashSet<edi_regex>();
            ic_customer_items = new HashSet<ic_customer_items>();
            ic_customer_items1 = new HashSet<ic_customer_items>();
            ic_physical_inventory_comparison_master = new HashSet<ic_physical_inventory_comparison_master>();
            ic_physical_inventory_history = new HashSet<ic_physical_inventory_history>();
            rv_history = new HashSet<rv_history>();
            rv_pending_items = new HashSet<rv_pending_items>();
            rv_pending_items1 = new HashSet<rv_pending_items>();
            so_children = new HashSet<so_children>();
            so_customer_blanketpos = new HashSet<so_customer_blanketpos>();
            so_sales_orders = new HashSet<so_sales_orders>();
        }

        [Key]
        public long customer_id { get; set; }

        [StringLength(12)]
        public string customer_code { get; set; }

        [Required]
        [StringLength(50)]
        public string customer_name { get; set; }

        public int customer_rank_id { get; set; }

        public int overrun_percent { get; set; }

        public int underrun_percent { get; set; }

        public bool is_customer { get; set; }

        public bool is_vendor { get; set; }

        public int payment_term_id { get; set; }

        public int delivery_term_id { get; set; }

        public decimal credit_limit { get; set; }

        public int credit_hold_id { get; set; }

        public int quality_hold_id { get; set; }

        public decimal? markup_percent { get; set; }

        public decimal? admin_fee { get; set; }

        public bool? needs_1099 { get; set; }

        [StringLength(10)]
        public string tax_id { get; set; }

        public long parent_id { get; set; }

        public bool? is_merged { get; set; }

        public bool is_active { get; set; }

        [StringLength(20)]
        public string dun { get; set; }

        [StringLength(500)]
        public string ftp_link { get; set; }

        [StringLength(50)]
        public string ftp_username { get; set; }

        [StringLength(50)]
        public string ftp_password { get; set; }

        [StringLength(19)]
        public string fax { get; set; }

        [StringLength(50)]
        public string email { get; set; }

        [StringLength(19)]
        public string phone { get; set; }

        public int? ship_type_id { get; set; }

        public int? payment_type_id { get; set; }

        public decimal? discount_percent { get; set; }

        public decimal? tax_percent { get; set; }

        public bool? tax_exempt { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public bool is_thirdparty { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        public virtual ap_payment_terms ap_payment_terms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ap_po_master> ap_po_master { get; set; }

        public virtual ap_ship_types ap_ship_types { get; set; }

        public virtual cs_credit_holds cs_credit_holds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_addresses> cs_customer_addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_association> cs_customer_association { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_contacts> cs_customer_contacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_edis> cs_customer_edis { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_files> cs_customer_files { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_manufacturing_notes> cs_customer_manufacturing_notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_notes> cs_customer_notes { get; set; }

        public virtual cs_customer_ranks cs_customer_ranks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_third_party_settings> cs_customer_third_party_settings { get; set; }

        public virtual cs_quality_holds cs_quality_holds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customers> cs_customers1 { get; set; }

        public virtual cs_customers cs_customers2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_customer_file_formats> edi_customer_file_formats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_customer_mappings> edi_customer_mappings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_ftpserver_info> edi_ftpserver_info { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_mappings> edi_mappings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_regex> edi_regex { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_customer_items> ic_customer_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_customer_items> ic_customer_items1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_comparison_master> ic_physical_inventory_comparison_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_history> ic_physical_inventory_history { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<rv_history> rv_history { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<rv_pending_items> rv_pending_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<rv_pending_items> rv_pending_items1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children> so_children { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_customer_blanketpos> so_customer_blanketpos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_sales_orders> so_sales_orders { get; set; }
    }
}
