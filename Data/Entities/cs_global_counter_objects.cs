namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_global_counter_objects
    {
        [Key]
        public long global_counter_object_id { get; set; }

        [StringLength(8000)]
        public string name { get; set; }

        [StringLength(500)]
        public string counter_type { get; set; }

        public long? counter_num { get; set; }

        [StringLength(500)]
        public string counter_num_prefix { get; set; }
    }
}
