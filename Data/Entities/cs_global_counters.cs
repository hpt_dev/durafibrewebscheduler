namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_global_counters
    {
        [Key]
        public long counter_id { get; set; }

        [Required]
        [StringLength(50)]
        public string counter_name { get; set; }

        [StringLength(50)]
        public string counter_prefix { get; set; }

        [StringLength(50)]
        public string counter_suffix { get; set; }

        [StringLength(50)]
        public string counter_number { get; set; }

        public int counter_step { get; set; }

        public int counter_type { get; set; }

        public int number_length { get; set; }

        [Required]
        [StringLength(1)]
        public string lpad_character { get; set; }

        [StringLength(250)]
        public string counter_structure { get; set; }
    }
}
