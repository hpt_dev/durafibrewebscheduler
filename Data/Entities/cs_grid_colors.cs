namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_grid_colors
    {
        [Key]
        public long grid_color_id { get; set; }

        public long grid_id { get; set; }

        public long grid_field_id { get; set; }

        [Required]
        public string value { get; set; }

        [Required]
        [StringLength(8)]
        public string argb_color { get; set; }

        public int match_order { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual cs_grid_fields cs_grid_fields { get; set; }

        public virtual cs_grids cs_grids { get; set; }
    }
}
