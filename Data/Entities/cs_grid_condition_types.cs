namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_grid_condition_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_grid_condition_types()
        {
            cs_grid_filter_fields = new HashSet<cs_grid_filter_fields>();
        }

        [Key]
        public int condition_type_id { get; set; }

        [Required]
        [StringLength(50)]
        public string condition_name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_filter_fields> cs_grid_filter_fields { get; set; }
    }
}
