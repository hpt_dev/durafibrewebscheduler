namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_grid_fields
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_grid_fields()
        {
            cs_grid_colors = new HashSet<cs_grid_colors>();
        }

        [Key]
        public long grid_field_id { get; set; }

        [Required]
        [StringLength(50)]
        public string field_name { get; set; }

        public bool custom_field { get; set; }

        public int text_id { get; set; }

        public int disporder { get; set; }

        [Required]
        [StringLength(50)]
        public string grid_name { get; set; }

        public int? table_id { get; set; }

        public bool is_visible { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? grid_id { get; set; }

        public bool include_summary { get; set; }

        public string custom_column_sql { get; set; }

        public bool required_by_code { get; set; }

        public bool groupby { get; set; }

        [StringLength(1000)]
        public string format_string { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_colors> cs_grid_colors { get; set; }

        public virtual cs_grids cs_grids { get; set; }
    }
}
