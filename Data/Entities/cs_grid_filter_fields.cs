namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_grid_filter_fields
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_grid_filter_fields()
        {
            cs_grid_conditionals = new HashSet<cs_grid_conditionals>();
        }

        [Key]
        public long grid_filter_field_id { get; set; }

        public long grid_id { get; set; }

        public int text_id { get; set; }

        public int disporder { get; set; }

        public int control_type_id { get; set; }

        public string control_source { get; set; }

        public string validation { get; set; }

        public string validation_text { get; set; }

        public bool is_required { get; set; }

        public string default_value { get; set; }

        [StringLength(200)]
        public string input_mask { get; set; }

        public int condition_type_id { get; set; }

        public bool? is_visible { get; set; }

        public bool? is_active { get; set; }

        public int table_id { get; set; }

        [Required]
        [StringLength(50)]
        public string field_name { get; set; }

        public string custom_filter { get; set; }

        public bool do_not_generate_where { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }

        public long last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public virtual cs_grid_condition_types cs_grid_condition_types { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_conditionals> cs_grid_conditionals { get; set; }

        public virtual cs_grid_control_types cs_grid_control_types { get; set; }

        public virtual cs_grids cs_grids { get; set; }

        public virtual ps_tables ps_tables { get; set; }
    }
}
