namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_grid_user_colors
    {
        public long id { get; set; }

        public long user_id { get; set; }

        public long grid_id { get; set; }

        public long grid_field_id { get; set; }

        [Required]
        public string value { get; set; }

        [Required]
        [StringLength(8)]
        public string argb_color { get; set; }

        public int match_order { get; set; }

        public DateTime created_date { get; set; }

        public DateTime modified_date { get; set; }

        public bool is_active { get; set; }
    }
}
