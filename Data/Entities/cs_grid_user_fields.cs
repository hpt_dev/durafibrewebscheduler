namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_grid_user_fields
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long user_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long grid_field_id { get; set; }

        public int disporder { get; set; }

        public bool is_active { get; set; }

        public bool is_visible { get; set; }

        public long grid_id { get; set; }

        public DateTime created_date { get; set; }

        public DateTime modified_date { get; set; }
    }
}
