namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_grid_user_filter_fields
    {
        public long id { get; set; }

        public long user_id { get; set; }

        public long grid_filter_field_id { get; set; }

        public int disporder { get; set; }

        public long grid_id { get; set; }

        public int condition_type_id { get; set; }

        public string default_value { get; set; }

        public DateTime created_date { get; set; }

        public DateTime modified_date { get; set; }

        public bool is_visible { get; set; }
    }
}
