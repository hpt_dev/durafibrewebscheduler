namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_grids
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_grids()
        {
            cs_grid_colors = new HashSet<cs_grid_colors>();
            cs_grid_conditionals = new HashSet<cs_grid_conditionals>();
            cs_grid_filter_fields = new HashSet<cs_grid_filter_fields>();
        }

        [Key]
        public long grid_id { get; set; }

        [Required]
        [StringLength(50)]
        public string grid_name { get; set; }

        public bool is_visible { get; set; }

        public bool is_active { get; set; }

        public bool is_distinct { get; set; }

        public string base_query_text { get; set; }

        public string base_sort_text { get; set; }

        public int max_rows { get; set; }

        public int page_size { get; set; }

        public bool use_paging { get; set; }

        public bool auto_select_first_row { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_colors> cs_grid_colors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_conditionals> cs_grid_conditionals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_filter_fields> cs_grid_filter_fields { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_fields> cs_grid_fields { get; set; }
    }
}
