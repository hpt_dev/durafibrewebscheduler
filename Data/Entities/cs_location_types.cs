namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_location_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_location_types()
        {
            cs_locations = new HashSet<cs_locations>();
        }

        [Key]
        public int location_type_id { get; set; }

        [StringLength(50)]
        public string location_type_name { get; set; }

        [StringLength(200)]
        public string location_type_description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_locations> cs_locations { get; set; }
    }
}
