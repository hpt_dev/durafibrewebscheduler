namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_locations
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_locations()
        {
            ic_physical_inventory = new HashSet<ic_physical_inventory>();
        }

        [Key]
        public int location_id { get; set; }

        public int building_id { get; set; }

        [Required]
        [StringLength(30)]
        public string location_name { get; set; }

        [StringLength(50)]
        public string location_desc { get; set; }

        public int location_disporder { get; set; }

        public bool location_visibility { get; set; }

        public bool is_active { get; set; }

        public bool? use_palletbuilder { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public int location_type_id { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        public virtual cs_buildings cs_buildings { get; set; }

        public virtual cs_location_types cs_location_types { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory> ic_physical_inventory { get; set; }
    }
}
