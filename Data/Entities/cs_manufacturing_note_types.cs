namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_manufacturing_note_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs_manufacturing_note_types()
        {
            cs_customer_manufacturing_notes = new HashSet<cs_customer_manufacturing_notes>();
        }

        [Key]
        public int manufacturing_note_type_id { get; set; }

        [Required]
        [StringLength(20)]
        public string manufacturing_note_type { get; set; }

        [Required]
        [StringLength(100)]
        public string manufacturing_note_type_description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_manufacturing_notes> cs_customer_manufacturing_notes { get; set; }
    }
}
