namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_rules
    {
        [Key]
        public long rule_id { get; set; }

        [Required]
        [StringLength(100)]
        public string rule_name { get; set; }

        [Required]
        [StringLength(100)]
        public string rule_field_name { get; set; }

        [Required]
        [StringLength(100)]
        public string rule_value { get; set; }

        public int field_type_id { get; set; }

        public string comments { get; set; }
    }
}
