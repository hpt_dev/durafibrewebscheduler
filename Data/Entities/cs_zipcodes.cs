namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_zipcodes
    {
        [StringLength(50)]
        public string zipcode { get; set; }

        [StringLength(50)]
        public string lat { get; set; }

        [Column("long")]
        [StringLength(50)]
        public string _long { get; set; }

        [StringLength(50)]
        public string city { get; set; }

        [StringLength(50)]
        public string state { get; set; }

        [StringLength(50)]
        public string county { get; set; }

        [StringLength(50)]
        public string type { get; set; }

        [StringLength(50)]
        public string preferred { get; set; }

        [StringLength(50)]
        public string world_region { get; set; }

        [StringLength(50)]
        public string country { get; set; }

        [StringLength(50)]
        public string location_text { get; set; }

        [Key]
        public long zipcode_id { get; set; }
    }
}
