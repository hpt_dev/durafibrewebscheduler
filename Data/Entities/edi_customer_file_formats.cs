namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_customer_file_formats
    {
        [Key]
        public long customer_file_format_id { get; set; }

        public long customer_id { get; set; }

        public long document_type_id { get; set; }

        public int file_naming_scheme_id { get; set; }

        [Required]
        [StringLength(250)]
        public string format_string { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual cs_customers cs_customers { get; set; }

        public virtual edi_document_types edi_document_types { get; set; }

        public virtual edi_file_naming_schemes edi_file_naming_schemes { get; set; }
    }
}
