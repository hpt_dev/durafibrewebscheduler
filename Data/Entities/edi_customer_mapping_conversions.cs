namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_customer_mapping_conversions
    {
        [Key]
        public long customer_mapping_conversion_id { get; set; }

        public long customer_mapping_id { get; set; }

        [Required]
        public string document_value { get; set; }

        [Required]
        public string column_value { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual edi_customer_mappings edi_customer_mappings { get; set; }
    }
}
