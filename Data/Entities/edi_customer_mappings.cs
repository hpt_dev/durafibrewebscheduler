namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_customer_mappings
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public edi_customer_mappings()
        {
            edi_customer_mapping_conversions = new HashSet<edi_customer_mapping_conversions>();
        }

        [Key]
        public long customer_mapping_id { get; set; }

        public long customer_id { get; set; }

        public long document_type_id { get; set; }

        public bool is_incoming { get; set; }

        public bool is_outgoing { get; set; }

        [Required]
        public string field_level { get; set; }

        [Required]
        public string document_field { get; set; }

        [Column(TypeName = "xml")]
        public string mapping_xml { get; set; }

        public int table_id { get; set; }

        [Required]
        [StringLength(150)]
        public string table_column { get; set; }

        public bool custom_field { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual cs_customers cs_customers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_customer_mapping_conversions> edi_customer_mapping_conversions { get; set; }

        public virtual edi_document_types edi_document_types { get; set; }

        public virtual ps_tables ps_tables { get; set; }
    }
}
