namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_document_configs
    {
        [Key]
        public int document_config_id { get; set; }

        public int document_id { get; set; }

        [Required]
        public string config { get; set; }
    }
}
