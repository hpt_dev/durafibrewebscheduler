namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_document_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public edi_document_types()
        {
            edi_customer_file_formats = new HashSet<edi_customer_file_formats>();
            edi_customer_mappings = new HashSet<edi_customer_mappings>();
            edi_document_requests = new HashSet<edi_document_requests>();
            edi_parse_results = new HashSet<edi_parse_results>();
        }

        [Key]
        public long document_type_id { get; set; }

        public int schema_id { get; set; }

        [StringLength(50)]
        public string version { get; set; }

        [Required]
        [StringLength(255)]
        public string document_type { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_customer_file_formats> edi_customer_file_formats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_customer_mappings> edi_customer_mappings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_document_requests> edi_document_requests { get; set; }

        public virtual edi_schemas edi_schemas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_parse_results> edi_parse_results { get; set; }
    }
}
