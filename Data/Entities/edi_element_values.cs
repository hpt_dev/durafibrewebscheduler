namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_element_values
    {
        [Key]
        public int element_value_id { get; set; }

        [Required]
        [StringLength(10)]
        public string element { get; set; }

        public int? min { get; set; }

        public int? max { get; set; }

        [StringLength(50)]
        public string type { get; set; }

        public int? company_id { get; set; }
    }
}
