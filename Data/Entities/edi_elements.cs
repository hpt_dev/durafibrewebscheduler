namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_elements
    {
        [Key]
        public int edi_element_id { get; set; }

        public int? document_type_id { get; set; }

        public int element_no { get; set; }

        public int identifier_id { get; set; }

        public int min { get; set; }

        public int max { get; set; }

        public bool mandatory { get; set; }
    }
}
