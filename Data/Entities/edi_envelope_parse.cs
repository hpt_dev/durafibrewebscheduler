namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_envelope_parse
    {
        [Key]
        public int edi_envelope_parse_id { get; set; }

        public int? customer_id { get; set; }

        public int? document_type_id { get; set; }

        public int edi_envelope_id { get; set; }

        public int? segment_legnth { get; set; }

        public int? open_element_no { get; set; }

        public int? close_element_no { get; set; }

        [StringLength(10)]
        public string count_segment { get; set; }
    }
}
