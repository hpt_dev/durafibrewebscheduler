namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_envelopes
    {
        [Key]
        public int edi_envelope_id { get; set; }

        [Required]
        [StringLength(50)]
        public string edi_envelope_name { get; set; }

        [Required]
        [StringLength(10)]
        public string open_id { get; set; }

        [Required]
        [StringLength(10)]
        public string close_id { get; set; }

        public int control_open_element { get; set; }

        public int control_close_element { get; set; }
    }
}
