namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_error_logs
    {
        [Key]
        public int edi_error_log_id { get; set; }

        [Required]
        [StringLength(200)]
        public string edi_file_name { get; set; }

        public bool passed { get; set; }

        public bool rerun_file { get; set; }

        [StringLength(500)]
        public string edi_error_message { get; set; }

        [Required]
        [StringLength(50)]
        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        [StringLength(50)]
        public string last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        [StringLength(50)]
        public string deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public bool is_active { get; set; }
    }
}
