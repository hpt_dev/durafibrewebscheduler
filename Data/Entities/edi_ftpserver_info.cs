namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_ftpserver_info
    {
        [Key]
        public long ftpserver_id { get; set; }

        [Required]
        [StringLength(500)]
        public string server_url { get; set; }

        [StringLength(100)]
        public string server_pass { get; set; }

        [StringLength(250)]
        public string server_user { get; set; }

        [StringLength(5)]
        public string server_passive { get; set; }

        public long customer_id { get; set; }

        public bool? is_active { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual cs_customers cs_customers { get; set; }
    }
}
