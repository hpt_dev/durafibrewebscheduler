namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_generation_default_values
    {
        [Key]
        public int generation_default_id { get; set; }

        public int ID { get; set; }

        public int element_no { get; set; }

        [Required]
        public string value { get; set; }

        public int document_type_id { get; set; }
    }
}
