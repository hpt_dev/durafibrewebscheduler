namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_generation_results
    {
        [Key]
        public long generation_result_id { get; set; }

        public long document_request_id { get; set; }

        [StringLength(255)]
        public string file_name { get; set; }

        public string file_path { get; set; }

        [Required]
        [StringLength(255)]
        public string result_category { get; set; }

        [Required]
        public string message { get; set; }

        public bool passed { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual edi_document_requests edi_document_requests { get; set; }
    }
}
