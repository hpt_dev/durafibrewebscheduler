namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_mappings
    {
        [Key]
        public long edi_mapping_id { get; set; }

        public long customer_id { get; set; }

        [Required]
        [StringLength(50)]
        public string receiving_field_to { get; set; }

        [Required]
        [StringLength(50)]
        public string doc_number { get; set; }

        [Required]
        [StringLength(12)]
        public string edi_level { get; set; }

        [Required]
        [StringLength(50)]
        public string edi_identity { get; set; }

        public int? table_id { get; set; }

        public virtual cs_customers cs_customers { get; set; }

        public virtual ps_tables ps_tables { get; set; }
    }
}
