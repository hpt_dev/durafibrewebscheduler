namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_regex
    {
        [Key]
        public long regex_id { get; set; }

        [Required]
        [StringLength(50)]
        public string doc_number { get; set; }

        [Required]
        [StringLength(50)]
        public string segment { get; set; }

        [Required]
        [StringLength(200)]
        public string regex { get; set; }

        public bool mandatory_line { get; set; }

        public long? customer_id { get; set; }

        public virtual cs_customers cs_customers { get; set; }
    }
}
