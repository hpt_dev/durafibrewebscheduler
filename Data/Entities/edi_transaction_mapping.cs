namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_transaction_mapping
    {
        [Key]
        public int transaction_mapping_id { get; set; }

        [Required]
        [StringLength(50)]
        public string field_name { get; set; }

        public int segment_type { get; set; }

        public int element_no { get; set; }
    }
}
