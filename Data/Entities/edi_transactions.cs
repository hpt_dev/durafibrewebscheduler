namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_transactions
    {
        [Key]
        public int edi_transaction_id { get; set; }

        public string interchange_open { get; set; }

        public string functional_group_open { get; set; }

        public string transaction_set_open { get; set; }

        public string transaction_set_close { get; set; }

        public string functional_group_close { get; set; }

        public string interchange_close { get; set; }

        public int functional_group_count { get; set; }

        public int transaction_set_count { get; set; }

        public int transaction_count { get; set; }

        [StringLength(50)]
        public string error_code_id { get; set; }

        [StringLength(50)]
        public string error_segment_id { get; set; }

        public string error_segment { get; set; }

        [StringLength(50)]
        public string error_segment_no { get; set; }

        [StringLength(10)]
        public string error_element_no { get; set; }

        public DateTime? created_date { get; set; }
    }
}
