namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_x12_error_codes
    {
        [Key]
        public int error_code_id { get; set; }

        public int identifier_id { get; set; }

        public int element_no { get; set; }

        [Required]
        [StringLength(50)]
        public string code { get; set; }

        [Required]
        public string condition { get; set; }
    }
}
