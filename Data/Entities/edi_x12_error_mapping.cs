namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_x12_error_mapping
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int error_mapping_id { get; set; }

        public int identifier_id { get; set; }

        public int element_no { get; set; }

        public int mapping_id { get; set; }

        [Required]
        public string error_id { get; set; }
    }
}
