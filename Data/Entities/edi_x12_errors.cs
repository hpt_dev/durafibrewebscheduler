namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_x12_errors
    {
        [Key]
        public int error_id { get; set; }

        [Required]
        public string error_name { get; set; }

        [Required]
        public string error_codes { get; set; }
    }
}
