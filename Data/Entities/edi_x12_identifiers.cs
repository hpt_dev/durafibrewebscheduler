namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_x12_identifiers
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int identifier_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int document_type_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string identifier { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int num_elements { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool mandatory { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int disporder { get; set; }

        public bool? can_loop { get; set; }

        public int? parent_id { get; set; }

        public int? loop_id_element { get; set; }
    }
}
