namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class edi_x12mapping
    {
        [Key]
        public int x12mapping_id { get; set; }

        public int document_type_id { get; set; }

        public int customer_id { get; set; }

        public int identifier_id { get; set; }

        public int element_no { get; set; }

        public int qualifier_id { get; set; }

        public int table_id { get; set; }

        [Required]
        [StringLength(50)]
        public string table_column { get; set; }

        [StringLength(50)]
        public string type { get; set; }

        public bool? custom { get; set; }
    }
}
