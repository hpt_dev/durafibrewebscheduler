namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_consumed_inventory
    {
        [Key]
        public long consumed_inventory_id { get; set; }

        public long on_hand_inventory_id { get; set; }

        public long item_child_id { get; set; }

        [StringLength(50)]
        public string inventory_id { get; set; }

        [StringLength(50)]
        public string phoenix_unique_id { get; set; }

        [StringLength(50)]
        public string pallet_id { get; set; }

        [StringLength(50)]
        public string ref_no { get; set; }

        public long customer_id { get; set; }

        public decimal? stock_uom_1 { get; set; }

        public decimal? stock_uom_2 { get; set; }

        public decimal? stock_uom_weight { get; set; }

        public int location_id { get; set; }

        public decimal? total_cost { get; set; }

        public decimal? unit_cost { get; set; }

        [StringLength(12)]
        public string unit_cost_uom_id { get; set; }

        public bool? is_consigned { get; set; }

        public int? business_unit_id { get; set; }

        public int? status_id { get; set; }

        [Required]
        [StringLength(20)]
        public string allocated_wo { get; set; }

        [StringLength(20)]
        public string batch_id { get; set; }

        [StringLength(20)]
        public string bol_number { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public decimal? weight_pounds { get; set; }

        public decimal? length_feet { get; set; }

        public bool? is_partial { get; set; }

        public int item_category_id { get; set; }

        public int? od_range_id { get; set; }

        public int receiving_type_id { get; set; }

        public decimal? original_cost { get; set; }

        public decimal? holding_cost { get; set; }

        public decimal? original_weight { get; set; }

        public virtual cs_business_units cs_business_units { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }

        public virtual ic_on_hand_inventory ic_on_hand_inventory { get; set; }

        public virtual rv_receiving_types rv_receiving_types { get; set; }
    }
}
