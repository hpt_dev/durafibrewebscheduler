namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_customer_items
    {
        [Key]
        public long customer_item_id { get; set; }

        public long customer_id { get; set; }

        public long item_child_id { get; set; }

        [Required]
        [StringLength(50)]
        public string customer_item_number { get; set; }

        public bool is_primary { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public bool is_active { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual cs_customers cs_customers { get; set; }

        public virtual cs_customers cs_customers1 { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }
    }
}
