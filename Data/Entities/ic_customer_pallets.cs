namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_customer_pallets
    {
        [Required]
        [StringLength(50)]
        public string pallet_id { get; set; }

        [Required]
        [StringLength(50)]
        public string customers_pallet_id { get; set; }

        public decimal? pallet_weight { get; set; }

        public int ID { get; set; }

        public decimal tare_weight { get; set; }
    }
}
