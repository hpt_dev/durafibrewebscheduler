namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_dim_groups
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ic_dim_groups()
        {
            ic_item_master = new HashSet<ic_item_master>();
        }

        [Key]
        public int dim_group_id { get; set; }

        [Required]
        [StringLength(50)]
        public string dim_name { get; set; }

        public byte location_level_id { get; set; }

        public bool use_batch { get; set; }

        public bool use_pallet { get; set; }

        public bool use_serial { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_master> ic_item_master { get; set; }
    }
}
