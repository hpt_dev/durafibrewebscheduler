namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_inventory_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ic_inventory_types()
        {
            bom_produced_items = new HashSet<bom_produced_items>();
            bom_required_materials = new HashSet<bom_required_materials>();
            bom_route_produced_items = new HashSet<bom_route_produced_items>();
            bom_route_required_materials = new HashSet<bom_route_required_materials>();
            bom_templates = new HashSet<bom_templates>();
            ic_item_master = new HashSet<ic_item_master>();
            ic_physical_inventory_comparison_master = new HashSet<ic_physical_inventory_comparison_master>();
        }

        [Key]
        public int inventory_type_id { get; set; }

        [Required]
        [StringLength(20)]
        public string inventory_type { get; set; }

        public int uom1_decimal { get; set; }

        public int uom2_decimal { get; set; }

        public int uomweight_decimal { get; set; }

        public int stock_uom_1_label_id { get; set; }

        public int stock_uom_2_label_id { get; set; }

        public int stock_uom_weight_label_id { get; set; }

        [Required]
        [StringLength(12)]
        public string stock_uom_1 { get; set; }

        [Required]
        [StringLength(12)]
        public string stock_uom_2 { get; set; }

        [Required]
        [StringLength(12)]
        public string stock_uom_weight { get; set; }

        public bool backflush { get; set; }

        public bool can_receive { get; set; }

        public int dim_group_id { get; set; }

        [Required]
        [StringLength(4)]
        public string nmfc_class { get; set; }

        public bool is_consigned { get; set; }

        public long item_category_id { get; set; }

        public int reorder_field { get; set; }

        public long? grid_id { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_produced_items> bom_produced_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_required_materials> bom_required_materials { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_produced_items> bom_route_produced_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_required_materials> bom_route_required_materials { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_templates> bom_templates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_master> ic_item_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_comparison_master> ic_physical_inventory_comparison_master { get; set; }
    }
}
