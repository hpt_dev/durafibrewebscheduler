namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_item_children
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ic_item_children()
        {
            ap_po_children = new HashSet<ap_po_children>();
            bom_produced_items = new HashSet<bom_produced_items>();
            bom_required_materials = new HashSet<bom_required_materials>();
            bom_route_produced_items = new HashSet<bom_route_produced_items>();
            bom_route_required_materials = new HashSet<bom_route_required_materials>();
            bom_templates = new HashSet<bom_templates>();
            ic_consumed_inventory = new HashSet<ic_consumed_inventory>();
            ic_customer_items = new HashSet<ic_customer_items>();
            ic_item_children_X10 = new HashSet<ic_item_children_X10>();
            ic_item_images = new HashSet<ic_item_images>();
            ic_item_quantities = new HashSet<ic_item_quantities>();
            ic_physical_inventory_history = new HashSet<ic_physical_inventory_history>();
            lbl_item_mapping = new HashSet<lbl_item_mapping>();
            rv_pending_items = new HashSet<rv_pending_items>();
            wo_master = new HashSet<wo_master>();
        }

        [Key]
        public long item_child_id { get; set; }

        public long? item_master_id { get; set; }

        [StringLength(12)]
        public string unit_price_uom { get; set; }

        public decimal? last_unit_price { get; set; }

        [StringLength(1)]
        public string item_abc_classification { get; set; }

        public bool is_active { get; set; }

        public bool? is_temp { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public decimal? width { get; set; }

        public decimal? mwt { get; set; }

        public decimal? length { get; set; }

        [StringLength(20)]
        public string core { get; set; }

        public long? sheet_count { get; set; }

        [StringLength(30)]
        public string sku { get; set; }

        public long? carton_count { get; set; }

        public decimal? od { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ap_po_children> ap_po_children { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_produced_items> bom_produced_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_required_materials> bom_required_materials { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_produced_items> bom_route_produced_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_required_materials> bom_route_required_materials { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_templates> bom_templates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_consumed_inventory> ic_consumed_inventory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_customer_items> ic_customer_items { get; set; }

        public virtual ic_item_master ic_item_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_children_X10> ic_item_children_X10 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_images> ic_item_images { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_quantities> ic_item_quantities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_history> ic_physical_inventory_history { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<lbl_item_mapping> lbl_item_mapping { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<rv_pending_items> rv_pending_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_master> wo_master { get; set; }
    }
}
