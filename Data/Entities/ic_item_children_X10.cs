namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_item_children_X10
    {
        [Key]
        public long extend_data_id { get; set; }

        public long field_id { get; set; }

        public long row_id { get; set; }

        public string value { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }

        public virtual ps_table_extended_fields ps_table_extended_fields { get; set; }
    }
}
