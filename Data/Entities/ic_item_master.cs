namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_item_master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ic_item_master()
        {
            bom_produced_items = new HashSet<bom_produced_items>();
            bom_required_materials = new HashSet<bom_required_materials>();
            bom_route_produced_items = new HashSet<bom_route_produced_items>();
            bom_route_required_materials = new HashSet<bom_route_required_materials>();
            bom_templates = new HashSet<bom_templates>();
            ic_item_children = new HashSet<ic_item_children>();
            ic_physical_inventory_history = new HashSet<ic_physical_inventory_history>();
            lbl_item_mapping = new HashSet<lbl_item_mapping>();
            sf_bom_substitutions = new HashSet<sf_bom_substitutions>();
            sf_bom_substitutions1 = new HashSet<sf_bom_substitutions>();
        }

        [Key]
        public long item_master_id { get; set; }

        [Required]
        [StringLength(50)]
        public string item_number { get; set; }

        [Required]
        [StringLength(150)]
        public string item_description { get; set; }

        public long item_category_id { get; set; }

        [StringLength(12)]
        public string stock_uom_1 { get; set; }

        [StringLength(12)]
        public string stock_uom_2 { get; set; }

        [StringLength(12)]
        public string stock_uom_weight { get; set; }

        public bool backflush { get; set; }

        public int dim_group_id { get; set; }

        [StringLength(4)]
        public string nmfc_class { get; set; }

        public bool is_consigned { get; set; }

        public bool? bom_only { get; set; }

        public bool? is_searchable { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public int? reorder_field { get; set; }

        [StringLength(50)]
        public string family_grade { get; set; }

        [StringLength(50)]
        public string coating { get; set; }

        [StringLength(50)]
        public string finish { get; set; }

        public decimal? basis_weight { get; set; }

        public decimal? recycled_content { get; set; }

        [StringLength(50)]
        public string fda { get; set; }

        public int? brightness { get; set; }

        [StringLength(50)]
        public string color { get; set; }

        public decimal? opacity { get; set; }

        public decimal? ppi { get; set; }

        public decimal? caliper { get; set; }

        [StringLength(50)]
        public string finish_type { get; set; }

        [StringLength(50)]
        public string forest_cert { get; set; }

        public int? stiffness { get; set; }

        [StringLength(250)]
        public string trade_name { get; set; }

        [StringLength(50)]
        public string manufacturer { get; set; }

        public decimal? gsm { get; set; }

        [StringLength(50)]
        public string broke { get; set; }

        public decimal? md_stiffness { get; set; }

        public decimal? cd_stiffness { get; set; }

        public decimal? gloss { get; set; }

        [StringLength(50)]
        public string sheetfed_web { get; set; }

        [StringLength(50)]
        public string print_type { get; set; }

        public decimal? basic_length { get; set; }

        public decimal? basic_width { get; set; }

        public decimal? moisture { get; set; }

        [StringLength(50)]
        public string mill_item { get; set; }

        public decimal? basic_size { get; set; }

        [StringLength(50)]
        public string text_cover { get; set; }

        [StringLength(50)]
        public string grade_code { get; set; }

        public int? stock_uom_1_label_id { get; set; }

        public int? stock_uom_2_label_id { get; set; }

        public int? stock_uom_weight_label_id { get; set; }

        public int inventory_type_id { get; set; }

        public int? route_id { get; set; }

        public long? bom_template_type_id { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_produced_items> bom_produced_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_required_materials> bom_required_materials { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_produced_items> bom_route_produced_items { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_required_materials> bom_route_required_materials { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_templates> bom_templates { get; set; }

        public virtual ic_dim_groups ic_dim_groups { get; set; }

        public virtual ic_inventory_types ic_inventory_types { get; set; }

        public virtual ic_item_categories ic_item_categories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_children> ic_item_children { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_history> ic_physical_inventory_history { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<lbl_item_mapping> lbl_item_mapping { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_bom_substitutions> sf_bom_substitutions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_bom_substitutions> sf_bom_substitutions1 { get; set; }
    }
}
