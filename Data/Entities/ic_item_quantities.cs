namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_item_quantities
    {
        [Key]
        public long item_quantity_id { get; set; }

        public long? item_child_id { get; set; }

        public int? building_id { get; set; }

        public decimal? on_hand_qty_1 { get; set; }

        public decimal? on_hand_qty_2 { get; set; }

        public decimal? on_hand_qty_weight { get; set; }

        public decimal? allocated_qty_1 { get; set; }

        public decimal? allocated_qty_2 { get; set; }

        public decimal? allocated_qty_weight { get; set; }

        public decimal? on_order_qty_1 { get; set; }

        public decimal? on_order_qty_2 { get; set; }

        public decimal? on_order_qty_weight { get; set; }

        public decimal? min_qty { get; set; }

        public decimal? max_qty { get; set; }

        public decimal? warning_qty { get; set; }

        public decimal? in_transit_qty_1 { get; set; }

        public decimal? in_transit_qty_2 { get; set; }

        public decimal? in_transit_qty_weight { get; set; }

        public decimal? on_hold_qty_1 { get; set; }

        public decimal? on_hold_qty_2 { get; set; }

        public decimal? on_hold_qty_weight { get; set; }

        public decimal? on_quality_hold_qty_1 { get; set; }

        public decimal? on_quality_hold_qty_2 { get; set; }

        public decimal? on_quality_hold_qty_weight { get; set; }

        public decimal? available_qty_1 { get; set; }

        public decimal? available_qty_2 { get; set; }

        public decimal? available_qty_weight { get; set; }

        public virtual cs_buildings cs_buildings { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }
    }
}
