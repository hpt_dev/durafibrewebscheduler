namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_item_statuses
    {
        [Key]
        public int item_status_id { get; set; }

        [Required]
        [StringLength(30)]
        public string item_status { get; set; }

        public bool is_active { get; set; }

        public bool status_visibility { get; set; }
    }
}
