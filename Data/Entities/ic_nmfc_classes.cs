namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_nmfc_classes
    {
        [Key]
        [Column("class")]
        [StringLength(4)]
        public string _class { get; set; }

        [StringLength(4)]
        public string density { get; set; }

        public decimal? max_value_per_lb { get; set; }

        public bool is_active { get; set; }

        public byte[] description { get; set; }
    }
}
