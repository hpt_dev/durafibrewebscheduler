namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_on_hand_quality_holds
    {
        [Key]
        public long on_hand_quality_hold_id { get; set; }

        public long? on_hand_inventory_id { get; set; }

        [Required]
        [StringLength(20)]
        public string inventory_id { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string hold_reason { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        [Column(TypeName = "text")]
        public string handle_reason { get; set; }

        public long? handled_by { get; set; }

        public DateTime? handled_date { get; set; }

        public virtual ic_on_hand_inventory ic_on_hand_inventory { get; set; }
    }
}
