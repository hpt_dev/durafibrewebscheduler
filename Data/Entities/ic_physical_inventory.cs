namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_physical_inventory
    {
        [Key]
        public long physical_inventory_id { get; set; }

        public int physical_inventory_type_id { get; set; }

        public int physical_inventory_status_id { get; set; }

        public Guid physical_inventory_group { get; set; }

        public int location_id { get; set; }

        public DateTime created_date { get; set; }

        public DateTime? inventory_date { get; set; }

        public DateTime? approved_date { get; set; }

        public DateTime date_to_run_inventory { get; set; }

        public DateTime? startdate { get; set; }

        public DateTime? enddate { get; set; }

        public long? created_by { get; set; }

        public long? performed_by_id { get; set; }

        public long? approved_by_id { get; set; }

        public virtual cs_locations cs_locations { get; set; }

        public virtual ic_physical_inventory_status ic_physical_inventory_status { get; set; }
    }
}
