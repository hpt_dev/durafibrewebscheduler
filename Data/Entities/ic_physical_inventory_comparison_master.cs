namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_physical_inventory_comparison_master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ic_physical_inventory_comparison_master()
        {
            ic_physical_inventory_comparison_files = new HashSet<ic_physical_inventory_comparison_files>();
            ic_physical_inventory_comparison_headcount = new HashSet<ic_physical_inventory_comparison_headcount>();
            ic_physical_inventory_comparison_scanner = new HashSet<ic_physical_inventory_comparison_scanner>();
        }

        [Key]
        public long master_id { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long customer_id { get; set; }

        public long building_id { get; set; }

        public int inventory_type_id { get; set; }

        public bool is_active { get; set; }

        public virtual cs_customers cs_customers { get; set; }

        public virtual ic_inventory_types ic_inventory_types { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_comparison_files> ic_physical_inventory_comparison_files { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_comparison_headcount> ic_physical_inventory_comparison_headcount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_comparison_scanner> ic_physical_inventory_comparison_scanner { get; set; }
    }
}
