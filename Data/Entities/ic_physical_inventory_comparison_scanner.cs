namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_physical_inventory_comparison_scanner
    {
        [Key]
        public long pk_id { get; set; }

        public long master_id { get; set; }

        [Required]
        [StringLength(50)]
        public string inventory_id { get; set; }

        [Required]
        [StringLength(30)]
        public string location { get; set; }

        public virtual ic_physical_inventory_comparison_master ic_physical_inventory_comparison_master { get; set; }
    }
}
