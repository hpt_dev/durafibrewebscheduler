namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_physical_inventory_history
    {
        [Key]
        public long physical_inventory_history_id { get; set; }

        public long user_id { get; set; }

        public int? physical_inventory_type_id { get; set; }

        public int? building_id { get; set; }

        public int? location_id { get; set; }

        [StringLength(5000)]
        public string inventoryids { get; set; }

        public long? item_child_id { get; set; }

        public long? item_master_id { get; set; }

        public long? customer_id { get; set; }

        public DateTime? date_to_run_inventory { get; set; }

        public Guid? physical_inventory_group { get; set; }

        public int? no_items_to_be_inventoried { get; set; }

        public int? no_inventory_jobs_created { get; set; }

        public DateTime? created_date { get; set; }

        public bool is_active { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual cs_buildings cs_buildings { get; set; }

        public virtual cs_customers cs_customers { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }

        public virtual ic_item_master ic_item_master { get; set; }
    }
}
