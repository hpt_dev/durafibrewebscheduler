namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_physical_inventory_item
    {
        [Key]
        public long physical_inventory_item_id { get; set; }

        public int physical_inventory_id { get; set; }

        [Required]
        [StringLength(50)]
        public string inventory_id { get; set; }

        public long? item_child_id { get; set; }

        public int? expected_location_id { get; set; }

        public int? actual_location_id { get; set; }

        public long physical_inventory_item_status_id { get; set; }

        public long? on_hand_inventory_id { get; set; }

        public DateTime? created_date { get; set; }

        public virtual ic_on_hand_inventory ic_on_hand_inventory { get; set; }

        public virtual ic_physical_inventory_item_status ic_physical_inventory_item_status { get; set; }
    }
}
