namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_physical_inventory_item_status
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ic_physical_inventory_item_status()
        {
            ic_physical_inventory_item = new HashSet<ic_physical_inventory_item>();
        }

        [Key]
        public long physical_inventory_item_status_id { get; set; }

        [Required]
        [StringLength(20)]
        public string physical_inventory_item_status { get; set; }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_physical_inventory_item> ic_physical_inventory_item { get; set; }
    }
}
