namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_physical_inventory_type
    {
        [Key]
        public int physical_inventory_type_id { get; set; }

        [Required]
        [StringLength(20)]
        public string physical_inventory_type { get; set; }
    }
}
