namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_quality_hold_reasons
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ic_quality_hold_reasons()
        {
            ic_quality_holds = new HashSet<ic_quality_holds>();
        }

        [Key]
        public long quality_hold_reason_id { get; set; }

        public long quality_hold_type_id { get; set; }

        [Required]
        public string quality_hold_reason { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long last_modified_by { get; set; }

        public bool is_active { get; set; }

        public virtual ic_quality_hold_types ic_quality_hold_types { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_quality_holds> ic_quality_holds { get; set; }
    }
}
