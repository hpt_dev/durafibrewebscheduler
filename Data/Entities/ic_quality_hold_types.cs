namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_quality_hold_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ic_quality_hold_types()
        {
            ic_quality_hold_reasons = new HashSet<ic_quality_hold_reasons>();
        }

        [Key]
        public long quality_hold_type_id { get; set; }

        [Required]
        [StringLength(50)]
        public string quality_hold_type { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long last_modified_by { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_quality_hold_reasons> ic_quality_hold_reasons { get; set; }
    }
}
