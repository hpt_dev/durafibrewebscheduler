namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_quality_holds
    {
        [Key]
        public long quality_hold_id { get; set; }

        public long? on_hand_inventory_id { get; set; }

        [StringLength(150)]
        public string claim_number { get; set; }

        public long quality_hold_reason_id { get; set; }

        public DateTime? customer_notification_date { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long last_modified_by { get; set; }

        public virtual ic_on_hand_inventory ic_on_hand_inventory { get; set; }

        public virtual ic_quality_hold_reasons ic_quality_hold_reasons { get; set; }
    }
}
