namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_unit_allocation_history
    {
        [Key]
        public long unit_allocation_history_id { get; set; }

        public long? unit_allocation_id { get; set; }

        [Required]
        [StringLength(20)]
        public string work_order_no { get; set; }

        [Required]
        [StringLength(50)]
        public string inventory_id { get; set; }

        public long? on_hand_inventory_id { get; set; }

        [Required]
        [StringLength(500)]
        public string description { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }
    }
}
