namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ic_unit_allocations
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ic_unit_allocations()
        {
            bom_route_allocations = new HashSet<bom_route_allocations>();
            wo_side_by_sides = new HashSet<wo_side_by_sides>();
        }

        [Key]
        public long unit_allocation_id { get; set; }

        [Required]
        [StringLength(20)]
        public string work_order_no { get; set; }

        public long on_hand_inventory_id { get; set; }

        public decimal? allocated_uom_1 { get; set; }

        public decimal? allocated_uom_2 { get; set; }

        public decimal? allocated_uom_weight { get; set; }

        public int no_outs { get; set; }

        public int hard_allocation { get; set; }

        public bool is_active { get; set; }

        public long? hold_type_id { get; set; }

        public long? hold_reason_id { get; set; }

        public DateTime? hold_notification_date { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_allocations> bom_route_allocations { get; set; }

        public virtual ic_on_hand_inventory ic_on_hand_inventory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_side_by_sides> wo_side_by_sides { get; set; }
    }
}
