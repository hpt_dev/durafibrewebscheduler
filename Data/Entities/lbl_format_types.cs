namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_format_types
    {
        [Key]
        public int format_types_id { get; set; }

        [Required]
        [StringLength(50)]
        public string format_name { get; set; }

        [Required]
        [StringLength(200)]
        public string format_description { get; set; }
    }
}
