namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_item_mapping
    {
        [Key]
        public long label_mapping_id { get; set; }

        public long label_item_id { get; set; }

        public long label_type_id { get; set; }

        public long? item_master_id { get; set; }

        public long? item_child_id { get; set; }

        public long? on_hand_inventory_id { get; set; }

        public long? customer_id { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }

        public virtual ic_item_master ic_item_master { get; set; }

        public virtual ic_on_hand_inventory ic_on_hand_inventory { get; set; }

        public virtual lbl_label_type lbl_label_type { get; set; }
    }
}
