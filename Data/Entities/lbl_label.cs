namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_label
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public lbl_label()
        {
            lbl_label_printer_types = new HashSet<lbl_label_printer_types>();
            lbl_printjob = new HashSet<lbl_printjob>();
        }

        [Key]
        public int label_id { get; set; }

        public long? label_type_id { get; set; }

        [StringLength(50)]
        public string label_name { get; set; }

        public string bartenderformat { get; set; }

        [StringLength(100)]
        public string label_source { get; set; }

        public int? default_no_labels { get; set; }

        public int? format_type_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<lbl_label_printer_types> lbl_label_printer_types { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<lbl_printjob> lbl_printjob { get; set; }
    }
}
