namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_label_printer_types
    {
        [Key]
        public int printer_labels_type_id { get; set; }

        public int label_id { get; set; }

        public int printer_type_id { get; set; }

        public virtual lbl_label lbl_label { get; set; }

        public virtual lbl_printer_types lbl_printer_types { get; set; }
    }
}
