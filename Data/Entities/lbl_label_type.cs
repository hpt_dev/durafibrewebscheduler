namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_label_type
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public lbl_label_type()
        {
            lbl_item_mapping = new HashSet<lbl_item_mapping>();
        }

        [Key]
        public long label_type_id { get; set; }

        public long? parent_label_type_id { get; set; }

        [Required]
        [StringLength(25)]
        public string TYPE_NAME { get; set; }

        [StringLength(200)]
        public string type_description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<lbl_item_mapping> lbl_item_mapping { get; set; }
    }
}
