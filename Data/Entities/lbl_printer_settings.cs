namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_printer_settings
    {
        [Key]
        public int pinter_settings_id { get; set; }

        [Required]
        [StringLength(50)]
        public string setting_name { get; set; }

        [Required]
        [StringLength(80)]
        public string VALUE { get; set; }
    }
}
