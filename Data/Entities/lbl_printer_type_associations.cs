namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_printer_type_associations
    {
        [Key]
        public int printer_type_association_id { get; set; }

        public int printer_type_id { get; set; }

        public int printer_id { get; set; }

        public virtual lbl_printer_types lbl_printer_types { get; set; }

        public virtual lbl_printers lbl_printers { get; set; }
    }
}
