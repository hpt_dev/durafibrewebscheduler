namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_printer_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public lbl_printer_types()
        {
            lbl_label_printer_types = new HashSet<lbl_label_printer_types>();
            lbl_printer_type_associations = new HashSet<lbl_printer_type_associations>();
        }

        [Key]
        public int printer_type_id { get; set; }

        [Required]
        [StringLength(25)]
        public string type_name { get; set; }

        [Required]
        [StringLength(100)]
        public string type_description { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<lbl_label_printer_types> lbl_label_printer_types { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<lbl_printer_type_associations> lbl_printer_type_associations { get; set; }
    }
}
