namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_printers
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public lbl_printers()
        {
            ad_work_center_printer_associations = new HashSet<ad_work_center_printer_associations>();
            lbl_printer_type_associations = new HashSet<lbl_printer_type_associations>();
        }

        [Key]
        public int printer_id { get; set; }

        public int? work_center_id { get; set; }

        [StringLength(50)]
        public string printer_name { get; set; }

        [StringLength(100)]
        public string printer_description { get; set; }

        [StringLength(50)]
        public string printer_ip { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_work_center_printer_associations> ad_work_center_printer_associations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<lbl_printer_type_associations> lbl_printer_type_associations { get; set; }
    }
}
