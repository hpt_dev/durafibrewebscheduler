namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_printjob
    {
        [Key]
        public long labeljobid { get; set; }

        [Required]
        [StringLength(100)]
        public string printer_id { get; set; }

        public DateTime? date_printed { get; set; }

        public long? printed_by_id { get; set; }

        public DateTime create_date { get; set; }

        public long created_by_id { get; set; }

        public int label_id { get; set; }

        public string label_data_file_text { get; set; }

        [Column(TypeName = "xml")]
        [Required]
        public string PARAMETERS { get; set; }

        public virtual lbl_label lbl_label { get; set; }
    }
}
