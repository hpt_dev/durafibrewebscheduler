namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lbl_temp_print_list
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long labeljobid { get; set; }
    }
}
