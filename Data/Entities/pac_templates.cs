namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pac_templates
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pac_templates()
        {
            so_children_packaging = new HashSet<so_children_packaging>();
        }

        [Key]
        public long pac_template_id { get; set; }

        public int work_center_group_id { get; set; }

        [Required]
        [StringLength(50)]
        public string setting_category { get; set; }

        [Required]
        [StringLength(50)]
        public string field_name { get; set; }

        public byte field_type_id { get; set; }

        [Required]
        [StringLength(50)]
        public string default_value { get; set; }

        public int text_id { get; set; }

        public string acceptable_values { get; set; }

        public bool no_edit { get; set; }

        public int disporder { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ps_field_types ps_field_types { get; set; }

        public virtual sf_work_center_groups sf_work_center_groups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children_packaging> so_children_packaging { get; set; }
    }
}
