namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_ae_uom_labels
    {
        [Key]
        public long ae_uom_labels_id { get; set; }

        [Required]
        [StringLength(50)]
        public string unit_of_measure { get; set; }

        public int uom_label_id { get; set; }

        public virtual ps_unit_of_measure_labels ps_unit_of_measure_labels { get; set; }
    }
}
