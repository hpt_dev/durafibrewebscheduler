namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_controls
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ps_controls()
        {
            ad_security_controls = new HashSet<ad_security_controls>();
        }

        [Key]
        public long control_id { get; set; }

        public long screen_id { get; set; }

        [Required]
        [StringLength(50)]
        public string control_name { get; set; }

        public long control_type_id { get; set; }

        public int? text_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_security_controls> ad_security_controls { get; set; }

        public virtual ps_control_type ps_control_type { get; set; }

        public virtual ps_screens ps_screens { get; set; }
    }
}
