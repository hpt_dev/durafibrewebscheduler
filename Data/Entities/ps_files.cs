namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_files
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ps_files()
        {
            cs_customer_files = new HashSet<cs_customer_files>();
        }

        [Key]
        public long file_id { get; set; }

        [Required]
        [StringLength(300)]
        public string file_name { get; set; }

        [Required]
        public byte[] file_data { get; set; }

        public int file_type_id { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_files> cs_customer_files { get; set; }

        public virtual ps_file_types ps_file_types { get; set; }
    }
}
