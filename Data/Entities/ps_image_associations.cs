namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_image_associations
    {
        [Key]
        public long image_association_id { get; set; }

        public int table_id { get; set; }

        [Required]
        [StringLength(50)]
        public string field_name { get; set; }

        [Required]
        [StringLength(100)]
        public string primary_id { get; set; }

        public long image_id { get; set; }

        public int? image_order { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ps_images ps_images { get; set; }

        public virtual ps_tables ps_tables { get; set; }
    }
}
