namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_image_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ps_image_types()
        {
            ps_images = new HashSet<ps_images>();
        }

        [Key]
        public int image_type_id { get; set; }

        [Required]
        [StringLength(20)]
        public string image_type { get; set; }

        [Required]
        [StringLength(20)]
        public string image_type_extension { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_images> ps_images { get; set; }
    }
}
