namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_modules
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ps_modules()
        {
            ad_security_modules = new HashSet<ad_security_modules>();
            ps_screens = new HashSet<ps_screens>();
        }

        [Key]
        public long module_id { get; set; }

        [Required]
        [StringLength(50)]
        public string module_name { get; set; }

        public bool is_active { get; set; }

        public int? display_order { get; set; }

        [StringLength(100)]
        public string image_string { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_security_modules> ad_security_modules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_screens> ps_screens { get; set; }
    }
}
