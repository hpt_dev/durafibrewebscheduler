namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_screens
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ps_screens()
        {
            ad_security_screens = new HashSet<ad_security_screens>();
            ps_controls = new HashSet<ps_controls>();
        }

        [Key]
        public long screen_id { get; set; }

        public long module_id { get; set; }

        [Required]
        [StringLength(50)]
        public string screen_name { get; set; }

        public int? text_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_security_screens> ad_security_screens { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_controls> ps_controls { get; set; }

        public virtual ps_modules ps_modules { get; set; }
    }
}
