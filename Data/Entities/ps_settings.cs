namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_settings
    {
        [Key]
        public long setting_id { get; set; }

        [Required]
        [StringLength(50)]
        public string setting_category { get; set; }

        [Required]
        [StringLength(50)]
        public string setting_name { get; set; }

        [Required]
        [StringLength(200)]
        public string setting_value { get; set; }

        public byte field_type_id { get; set; }

        [StringLength(500)]
        public string comments { get; set; }

        public virtual ps_field_types ps_field_types { get; set; }
    }
}
