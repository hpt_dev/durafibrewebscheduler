namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_table_extended_fields
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ps_table_extended_fields()
        {
            ic_item_children_X10 = new HashSet<ic_item_children_X10>();
        }

        [Key]
        public long extended_field_id { get; set; }

        public long table_id { get; set; }

        public int field_order { get; set; }

        [Required]
        [StringLength(50)]
        public string field_name { get; set; }

        public int data_type_id { get; set; }

        public int? size { get; set; }

        public int? precision { get; set; }

        [StringLength(200)]
        public string default_value { get; set; }

        public string regex_validation { get; set; }

        [StringLength(200)]
        public string regex_validation_message { get; set; }

        public DateTime create_date { get; set; }

        public long created_by_id { get; set; }

        public DateTime last_modified_date { get; set; }

        public long last_modified_by_id { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_item_children_X10> ic_item_children_X10 { get; set; }
    }
}
