namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_tables
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ps_tables()
        {
            cs_custom_fields = new HashSet<cs_custom_fields>();
            cs_grid_filter_fields = new HashSet<cs_grid_filter_fields>();
            edi_customer_mappings = new HashSet<edi_customer_mappings>();
            edi_mappings = new HashSet<edi_mappings>();
            ps_image_associations = new HashSet<ps_image_associations>();
        }

        [Key]
        public int table_id { get; set; }

        [Required]
        [StringLength(50)]
        public string table_name { get; set; }

        [Required]
        [StringLength(50)]
        public string display_name { get; set; }

        public bool can_add { get; set; }

        [StringLength(50)]
        public string datawarehouse_name { get; set; }

        public bool is_extended { get; set; }

        public bool is_view { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_custom_fields> cs_custom_fields { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_grid_filter_fields> cs_grid_filter_fields { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_customer_mappings> edi_customer_mappings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<edi_mappings> edi_mappings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_image_associations> ps_image_associations { get; set; }
    }
}
