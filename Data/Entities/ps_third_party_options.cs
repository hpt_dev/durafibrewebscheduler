namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_third_party_options
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ps_third_party_options()
        {
            cs_customer_third_party_settings = new HashSet<cs_customer_third_party_settings>();
            so_children_service_settings = new HashSet<so_children_service_settings>();
        }

        [Key]
        public long third_party_option_id { get; set; }

        public long third_party_service_id { get; set; }

        [Required]
        public string option_name { get; set; }

        public string acceptable_values { get; set; }

        public byte field_type_id { get; set; }

        public int disorder { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_customer_third_party_settings> cs_customer_third_party_settings { get; set; }

        public virtual ps_field_types ps_field_types { get; set; }

        public virtual ps_third_party_services ps_third_party_services { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children_service_settings> so_children_service_settings { get; set; }
    }
}
