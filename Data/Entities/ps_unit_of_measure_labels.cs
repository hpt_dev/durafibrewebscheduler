namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_unit_of_measure_labels
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ps_unit_of_measure_labels()
        {
            ps_ae_uom_labels = new HashSet<ps_ae_uom_labels>();
        }

        [Key]
        public int uom_label_id { get; set; }

        public int text_id { get; set; }

        public bool is_active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ps_ae_uom_labels> ps_ae_uom_labels { get; set; }
    }
}
