namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ps_unit_of_measures
    {
        [Key]
        [StringLength(50)]
        public string unit_of_measure { get; set; }

        public bool? weight_uom { get; set; }

        [StringLength(20)]
        public string uom_type { get; set; }

        [Required]
        [StringLength(20)]
        public string display_name { get; set; }

        public decimal? conversion { get; set; }

        public bool can_order_by { get; set; }

        public bool is_active { get; set; }
    }
}
