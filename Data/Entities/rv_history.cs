namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class rv_history
    {
        [Key]
        public long receiving_history_id { get; set; }

        public bool is_edi { get; set; }

        [StringLength(50)]
        public string bol_number { get; set; }

        public long? customer_id { get; set; }

        public int expected_count { get; set; }

        public int? actual_count { get; set; }

        public long received_started_by_id { get; set; }

        public DateTime received_start_date { get; set; }

        public long? received_completed_by_id { get; set; }

        public DateTime? received_complete_date { get; set; }

        public virtual cs_customers cs_customers { get; set; }
    }
}
