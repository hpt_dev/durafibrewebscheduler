namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class rv_pending_items
    {
        [Key]
        public long pending_item_id { get; set; }

        public long item_child_id { get; set; }

        [Required]
        [StringLength(50)]
        public string inventory_id { get; set; }

        [StringLength(50)]
        public string ref_no { get; set; }

        public long customer_id { get; set; }

        public decimal? stock_uom_1 { get; set; }

        [StringLength(5)]
        public string stock_uom_1_label { get; set; }

        public decimal? stock_uom_2 { get; set; }

        [StringLength(5)]
        public string stock_uom_2_label { get; set; }

        public decimal stock_uom_weight { get; set; }

        [StringLength(5)]
        public string stock_uom_weight_label { get; set; }

        [Required]
        [StringLength(50)]
        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        [Required]
        [StringLength(50)]
        public string bol_number { get; set; }

        public long? building_id { get; set; }

        public bool received_edi { get; set; }

        public long? vendor_id { get; set; }

        public bool is_approved { get; set; }

        public int? od_range_id { get; set; }

        public decimal total_cost { get; set; }

        public decimal unit_cost { get; set; }

        [Required]
        [StringLength(12)]
        public string unit_cost_uom { get; set; }

        public virtual cs_customers cs_customers { get; set; }

        public virtual cs_customers cs_customers1 { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }

        public virtual ic_od_ranges ic_od_ranges { get; set; }
    }
}
