namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class rv_receiving_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public rv_receiving_types()
        {
            ic_consumed_inventory = new HashSet<ic_consumed_inventory>();
            ic_on_hand_inventory = new HashSet<ic_on_hand_inventory>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int receiving_type_id { get; set; }

        [Required]
        [StringLength(20)]
        public string receiving_type_name { get; set; }

        [Required]
        [StringLength(200)]
        public string receiving_type_description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_consumed_inventory> ic_consumed_inventory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ic_on_hand_inventory> ic_on_hand_inventory { get; set; }
    }
}
