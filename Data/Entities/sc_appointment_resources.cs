namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_appointment_resources
    {
        [Key]
        public long appointment_resource_id { get; set; }

        public long? appointment_id { get; set; }

        public long? resource_id { get; set; }

        public bool? many_to_many_workaround { get; set; }

        public virtual sc_appointments sc_appointments { get; set; }

        public virtual sc_resources sc_resources { get; set; }
    }
}
