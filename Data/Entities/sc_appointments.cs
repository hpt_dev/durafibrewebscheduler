namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_appointments
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sc_appointments()
        {
            sc_appointment_resources = new HashSet<sc_appointment_resources>();
            sc_calendar_appointments = new HashSet<sc_calendar_appointments>();
        }

        [Key]
        public long appointment_id { get; set; }

        [StringLength(100)]
        public string subject { get; set; }

        [StringLength(500)]
        public string body { get; set; }

        public DateTime start { get; set; }

        public DateTime end { get; set; }

        public bool is_all_day_event { get; set; }

        [StringLength(100)]
        public string reccurence_pattern { get; set; }

        [StringLength(100)]
        public string time_zone_string { get; set; }

        public int importance { get; set; }

        public int? time_marker_id { get; set; }

        public int? category_id { get; set; }

        public bool? is_active { get; set; }

        public Guid? unique_id { get; set; }

        public long? created_by { get; set; }
        public long? last_modified_by { get; set; }
        public long? calendar_id { get; set; }
        public int? work_center_group_id { get; set; }
        public int? work_center_id { get; set; }
        public int? avaliable { get; set; }
        public string color { get; set; }
        public string exempt_dates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_appointment_resources> sc_appointment_resources { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_calendar_appointments> sc_calendar_appointments { get; set; }
    }
}
