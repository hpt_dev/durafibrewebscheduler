namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_bom_schedule
    {
        [Key]
        public long schedule_id { get; set; }

        public long? entry_type_id { get; set; }

        [StringLength(1000)]
        public string entry_display { get; set; }

        public long? route_step_id { get; set; }

        public long? work_center_id { get; set; }

        public DateTime? est_start_time { get; set; }

        public DateTime? est_end_time { get; set; }

        public decimal? duration { get; set; }

        public DateTime? actual_start_time { get; set; }

        public DateTime? actual_end_time { get; set; }

        public long? schedule_status_id { get; set; }

        public int? position { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }
    }
}
