namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_calendar_appointments
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sc_calendar_appointments()
        {
            sc_exception_occurrences = new HashSet<sc_exception_occurrences>();
        }

        [Key]
        public long calendar_appointment_id { get; set; }

        public long calendar_id { get; set; }

        public long appointment_id { get; set; }

        public bool? use_exceptions { get; set; }

        public virtual sc_appointments sc_appointments { get; set; }

        public virtual sc_calendars sc_calendars { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_exception_occurrences> sc_exception_occurrences { get; set; }
    }
}
