namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_calendar_exceptions
    {
        [Key]
        public long calendar_exception_id { get; set; }

        public long calendar_id { get; set; }

        public long exception_id { get; set; }

        public virtual sc_calendars sc_calendars { get; set; }

        public virtual sc_exceptions sc_exceptions { get; set; }
    }
}
