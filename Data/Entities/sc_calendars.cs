namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_calendars
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sc_calendars()
        {
            sc_calendar_appointments = new HashSet<sc_calendar_appointments>();
            sc_calendar_exceptions = new HashSet<sc_calendar_exceptions>();
        }

        [Key]
        public long calendar_id { get; set; }

        [StringLength(50)]
        public string calendar_name { get; set; }

        public long? work_center_id { get; set; }

        public bool? is_active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_calendar_appointments> sc_calendar_appointments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_calendar_exceptions> sc_calendar_exceptions { get; set; }
    }
}
