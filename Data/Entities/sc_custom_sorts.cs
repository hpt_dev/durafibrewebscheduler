namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_custom_sorts
    {
        [Key]
        public long custom_sort_id { get; set; }

        [Required]
        [StringLength(50)]
        public string field_name { get; set; }

        public long table_id { get; set; }

        [Required]
        [StringLength(100)]
        public string value { get; set; }

        public long work_center_id { get; set; }

        public long sort_rank { get; set; }

        [Required]
        [StringLength(10)]
        public string sort_direction { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public bool is_active { get; set; }
    }
}
