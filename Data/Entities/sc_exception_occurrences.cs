namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_exception_occurrences
    {
        [Key]
        public long exception_occurrence_id { get; set; }

        public long? exception_id { get; set; }

        public DateTime exception_date { get; set; }

        public bool? Use_Exception { get; set; }

        public long? calendar_appointment_id { get; set; }

        public virtual sc_calendar_appointments sc_calendar_appointments { get; set; }

        public virtual sc_exceptions sc_exceptions { get; set; }
    }
}
