namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_exceptions
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sc_exceptions()
        {
            sc_calendar_exceptions = new HashSet<sc_calendar_exceptions>();
            sc_exception_occurrences = new HashSet<sc_exception_occurrences>();
            sc_exception_resources = new HashSet<sc_exception_resources>();
        }

        [Key]
        public long exception_id { get; set; }

        [StringLength(100)]
        public string subject { get; set; }

        [StringLength(500)]
        public string body { get; set; }

        public DateTime start { get; set; }

        public DateTime end { get; set; }

        public bool is_all_day_event { get; set; }

        [StringLength(100)]
        public string time_zone_string { get; set; }

        public int importance { get; set; }

        public int? time_marker_id { get; set; }

        public int? category_id { get; set; }

        public bool? is_active { get; set; }

        public Guid? unique_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_calendar_exceptions> sc_calendar_exceptions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_exception_occurrences> sc_exception_occurrences { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_exception_resources> sc_exception_resources { get; set; }
    }
}
