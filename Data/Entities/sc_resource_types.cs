namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_resource_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sc_resource_types()
        {
            sc_resources = new HashSet<sc_resources>();
        }

        [Key]
        public long resource_type_id { get; set; }

        [Required]
        [StringLength(100)]
        public string resource_name { get; set; }

        [Required]
        [StringLength(100)]
        public string display_name { get; set; }

        public bool? allow_multiple_selection { get; set; }

        [StringLength(100)]
        public string brush_name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_resources> sc_resources { get; set; }
    }
}
