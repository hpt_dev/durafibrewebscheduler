namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_resources
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sc_resources()
        {
            sc_appointment_resources = new HashSet<sc_appointment_resources>();
            sc_exception_resources = new HashSet<sc_exception_resources>();
        }

        [Key]
        public long resource_id { get; set; }

        public long? resource_type_id { get; set; }

        [StringLength(100)]
        public string resource_name { get; set; }

        [StringLength(100)]
        public string display_name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_appointment_resources> sc_appointment_resources { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sc_exception_resources> sc_exception_resources { get; set; }

        public virtual sc_resource_types sc_resource_types { get; set; }
    }
}
