namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_schedule_columns
    {
        [Key]
        public long sched_column_id { get; set; }

        [StringLength(500)]
        public string grid_name { get; set; }

        [StringLength(1000)]
        public string header { get; set; }

        [StringLength(1000)]
        public string field_name { get; set; }

        public int? column_type { get; set; }

        public bool? is_calculated { get; set; }

        [StringLength(8000)]
        public string calculation { get; set; }

        public bool? is_visible { get; set; }

        public bool? is_active { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public bool? is_readonly { get; set; }

        public int? displayorder { get; set; }

        public double? round_to { get; set; }

        public bool? round_ceiling { get; set; }
    }
}
