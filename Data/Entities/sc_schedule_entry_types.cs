namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_schedule_entry_types
    {
        [Key]
        public long entry_type_id { get; set; }

        [StringLength(500)]
        public string entry_type_name { get; set; }
    }
}
