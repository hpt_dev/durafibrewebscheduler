namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_schedule_statuses
    {
        [Key]
        public long schedule_status_id { get; set; }

        [StringLength(500)]
        public string status_name { get; set; }
    }
}
