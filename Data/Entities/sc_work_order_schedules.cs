namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sc_work_order_schedules
    {
        [Key]
        public long schedule_workorder_id { get; set; }

        [StringLength(50)]
        public string work_order_no { get; set; }

        public long? work_center_id { get; set; }

        public int? position { get; set; }

        public long? total_minutes { get; set; }

        public DateTime? start_date { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }
    }
}
