namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sca_dynamic_condition
    {
        [Key]
        public int condition_id { get; set; }

        public int field_id { get; set; }

        public string condition { get; set; }

        public bool is_active { get; set; }

        public DateTime created_date { get; set; }

        public int created_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public int last_modified_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public int? deleted_by { get; set; }

        public virtual sca_dynamic_fields sca_dynamic_fields { get; set; }
    }
}
