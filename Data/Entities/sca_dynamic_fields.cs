namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sca_dynamic_fields
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sca_dynamic_fields()
        {
            sca_dynamic_condition = new HashSet<sca_dynamic_condition>();
        }

        [Key]
        public int field_id { get; set; }

        public int screen_id { get; set; }

        public int container_id { get; set; }

        [Required]
        [StringLength(50)]
        public string control_name { get; set; }

        [StringLength(50)]
        public string control_text { get; set; }

        public int control_type_id { get; set; }

        public int control_source_type_id { get; set; }

        public string control_source { get; set; }

        public int display_row { get; set; }

        public int display_column { get; set; }

        public int display_column_span { get; set; }

        [StringLength(50)]
        public string datamember { get; set; }

        public string regex_validation { get; set; }

        public string regex_validation_msg { get; set; }

        [StringLength(50)]
        public string text_id { get; set; }

        public bool? is_read_only { get; set; }

        public bool is_active { get; set; }

        public DateTime created_date { get; set; }

        public int created_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public int last_modified_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public int? deleted_by { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sca_dynamic_condition> sca_dynamic_condition { get; set; }

        public virtual sca_dynamic_form_containers sca_dynamic_form_containers { get; set; }
    }
}
