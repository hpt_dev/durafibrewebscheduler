namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sca_dynamic_form
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sca_dynamic_form()
        {
            sca_dynamic_form_containers = new HashSet<sca_dynamic_form_containers>();
        }

        [Key]
        public int screen_id { get; set; }

        [Required]
        [StringLength(50)]
        public string screen_name { get; set; }

        public bool is_active { get; set; }

        public int margin { get; set; }

        public int control_height { get; set; }

        public DateTime created_date { get; set; }

        public int created_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public int last_modified_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public int? deleted_by { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sca_dynamic_form_containers> sca_dynamic_form_containers { get; set; }
    }
}
