namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sca_main_menu
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(15)]
        public string Caption { get; set; }

        public int OrderBy { get; set; }

        [Required]
        [StringLength(50)]
        public string Value { get; set; }

        public bool Visible { get; set; }

        [StringLength(100)]
        public string ApplicationName { get; set; }
    }
}
