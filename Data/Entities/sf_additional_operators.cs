namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_additional_operators
    {
        [Key]
        [Column(Order = 0)]
        public long additional_operator_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string shift_guid { get; set; }

        [Key]
        [Column(Order = 2, TypeName = "smalldatetime")]
        public DateTime start_time { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? stop_time { get; set; }
    }
}
