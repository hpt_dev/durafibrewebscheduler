namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_ae_work_center_groupings
    {
        [Key]
        public int ae_work_center_grouping_id { get; set; }

        public int work_center_id { get; set; }

        public int work_center_group_id { get; set; }

        public virtual sf_work_centers sf_work_centers { get; set; }
        public virtual sf_work_center_groups sf_work_center_groups { get; set; }
    }
}
