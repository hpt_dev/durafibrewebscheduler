namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_custom_accumulator_values
    {
        [Key]
        [Column(Order = 0)]
        public long custom_accumulator_value_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long custom_accumulator_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int node_id { get; set; }

        [Key]
        [Column(Order = 3)]
        public double hold_value { get; set; }

        [Key]
        [Column(Order = 4)]
        public double last_value { get; set; }
    }
}
