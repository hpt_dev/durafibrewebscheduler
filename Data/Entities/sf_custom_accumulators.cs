namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_custom_accumulators
    {
        [Key]
        [Column(Order = 0)]
        public long custom_accumulator_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short plc_tag_set_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(20)]
        public string accumulator_name { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int accumulator_index { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool is_simple { get; set; }

        [Key]
        [Column(Order = 5)]
        public bool use_hold { get; set; }

        [StringLength(50)]
        public string start_accumulator { get; set; }

        [StringLength(50)]
        public string stop_accumulator { get; set; }
    }
}
