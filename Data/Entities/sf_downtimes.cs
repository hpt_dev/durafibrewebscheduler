namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_downtimes
    {
        [Key]
        public long downtime_id { get; set; }

        public int? parent_id { get; set; }

        [StringLength(50)]
        public string downtime_display { get; set; }

        public int? disp_order { get; set; }

        public int? node_id { get; set; }

        public virtual sf_nodes sf_nodes { get; set; }
    }
}
