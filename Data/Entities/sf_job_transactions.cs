namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_job_transactions
    {
        [Key]
        public long job_transaction_id { get; set; }

        [StringLength(100)]
        public string source { get; set; }

        [StringLength(20)]
        public string work_order_no { get; set; }

        public int? work_center_id { get; set; }

        public long? process_id { get; set; }

        [StringLength(50)]
        public string process_name { get; set; }

        public DateTime? process_timestamp { get; set; }

        [StringLength(25)]
        public string node { get; set; }

        public double? logon_01_data_nbr { get; set; }

        public double? logon_02_data_nbr { get; set; }

        public double? logon_03_data_nbr { get; set; }

        [StringLength(50)]
        public string logon_01_data_txt { get; set; }

        public string logon_02_data_txt { get; set; }

        public string logon_03_data_txt { get; set; }

        public double? entry_18_data_nbr { get; set; }

        public double? entry_19_data_nbr { get; set; }

        public double? entry_20_data_nbr { get; set; }

        public double? entry_21_data_nbr { get; set; }

        public double? entry_22_data_nbr { get; set; }

        public double? entry_23_data_nbr { get; set; }

        public string entry_18_data_txt { get; set; }

        public string entry_19_data_txt { get; set; }

        public string entry_20_data_txt { get; set; }

        public string entry_21_data_txt { get; set; }

        public string entry_22_data_txt { get; set; }

        public string entry_23_data_txt { get; set; }

        public string entry_13_value { get; set; }

        public string order_results_01_txt { get; set; }

        public string order_results_02_txt { get; set; }

        public string order_results_03_txt { get; set; }

        public string order_results_04_txt { get; set; }

        public string order_results_05_txt { get; set; }

        public string order_results_06_txt { get; set; }

        public double? order_results_01_nbr { get; set; }

        public double? order_results_02_nbr { get; set; }

        public double? order_results_03_nbr { get; set; }

        public double? order_results_04_nbr { get; set; }

        public double? order_results_05_nbr { get; set; }

        public double? order_results_06_nbr { get; set; }

        public string active_grp { get; set; }

        public double? theo_time { get; set; }

        public double? actual_time { get; set; }

        public string process_object { get; set; }

        public double? shift_theo_vb_tt { get; set; }

        public double? shift_actual_vb_tt { get; set; }

        public DateTime? process_timestamp_start { get; set; }

        public float? process_ptp { get; set; }

        [StringLength(50)]
        public string logon_04_value { get; set; }

        public double? generic_timer_01 { get; set; }

        public double? generic_timer_02 { get; set; }

        public double? generic_timer_03 { get; set; }

        public double? set_incrementor { get; set; }

        public string events_downtime_level_2 { get; set; }

        public double? crew2_1_data_num { get; set; }

        public double? crew2_2_data_num { get; set; }

        public double? crew2_3_data_num { get; set; }

        public double? crew2_4_data_num { get; set; }

        public string crew2_1_data_text { get; set; }

        public string crew2_2_data_text { get; set; }

        public string crew2_3_data_text { get; set; }

        public string crew2_4_data_text { get; set; }

        public double? actual_count_number { get; set; }

        public double? actual_count_number2 { get; set; }

        public double? good_tot { get; set; }

        public double? bad_tot { get; set; }

        public short? logoff_1_value { get; set; }

        public double? generic_timer_04 { get; set; }

        public double? generic_timer_05 { get; set; }

        public double? one_1_data_num { get; set; }

        public double? one_2_data_num { get; set; }

        public string one_1_data_text { get; set; }

        public string one_2_data_text { get; set; }

        public double? order_actual_vb_tt { get; set; }

        public double? order_theo_vb_tt { get; set; }

        public double? group_actual_vb_tt { get; set; }

        public double? group_theo_vb_tt { get; set; }

        public double? set_actual_vb_tt { get; set; }

        public double? set_theo_vb_tt { get; set; }

        public int? parent_id { get; set; }

        public double? entry_24_data_num { get; set; }

        public string entry_24_data_text { get; set; }

        public double? extra_01_data_num { get; set; }

        public double? extra_02_data_num { get; set; }

        public double? extra_03_data_num { get; set; }

        public double? extra_04_data_num { get; set; }

        public double? extra_05_data_num { get; set; }

        public double? extra_06_data_num { get; set; }

        public string extra_01_data_text { get; set; }

        public string extra_02_data_text { get; set; }

        public string extra_03_data_text { get; set; }

        public string extra_04_data_text { get; set; }

        public string extra_05_data_text { get; set; }

        public string extra_06_data_text { get; set; }

        public string logon_04_data_txt { get; set; }

        public double? logon_04_data_nbr { get; set; }

        public double? good_tot_diff { get; set; }

        public double? bad_tot_diff { get; set; }

        [StringLength(10)]
        public string edited { get; set; }
    }
}
