namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_machine_work_orders
    {
        [Key]
        public long machine_work_order_id { get; set; }

        public Guid work_order_guid { get; set; }

        public Guid shift_guid { get; set; }

        [Required]
        [StringLength(20)]
        public string work_order { get; set; }

        public decimal? good_total { get; set; }

        public decimal? bad_total { get; set; }

        public decimal? order_actual_vb_tt { get; set; }

        public decimal? order_theo_vb_tt { get; set; }

        public DateTime start_time { get; set; }

        public DateTime? stop_time { get; set; }
    }
}
