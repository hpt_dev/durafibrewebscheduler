namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_node_groups
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte node_group_id { get; set; }

        public byte group_id { get; set; }

        public short node_id { get; set; }

        public virtual sf_groups sf_groups { get; set; }
    }
}
