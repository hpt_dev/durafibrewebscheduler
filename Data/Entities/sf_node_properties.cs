namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_node_properties
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sf_node_properties()
        {
            sf_node_settings = new HashSet<sf_node_settings>();
        }

        [Key]
        public long node_property_id { get; set; }

        public byte field_type_id { get; set; }

        public long translation_text_id { get; set; }

        public byte property_category_id { get; set; }

        public long? min_value { get; set; }

        public long? max_value { get; set; }

        public byte decimal_places { get; set; }

        public string drop_down_text { get; set; }

        public byte? node_property_disporder { get; set; }

        public virtual ps_field_types ps_field_types { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_node_settings> sf_node_settings { get; set; }
    }
}
