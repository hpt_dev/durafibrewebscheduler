namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_node_settings
    {
        [Key]
        public long node_setting_id { get; set; }

        public long node_property_id { get; set; }

        public short node_id { get; set; }

        [Required]
        [StringLength(50)]
        public string property_setting { get; set; }

        public virtual sf_node_properties sf_node_properties { get; set; }
    }
}
