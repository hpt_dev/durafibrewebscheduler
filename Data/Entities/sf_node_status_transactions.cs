namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_node_status_transactions
    {
        [Key]
        public long node_status_trans_id { get; set; }

        public long? node_id { get; set; }

        public long? status_id { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }
    }
}
