namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_node_statuses
    {
        [Key]
        public long node_status_id { get; set; }

        [StringLength(1)]
        public string node_status { get; set; }

        [StringLength(20)]
        public string status_description { get; set; }
    }
}
