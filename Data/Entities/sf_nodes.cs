namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_nodes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sf_nodes()
        {
            sf_downtimes = new HashSet<sf_downtimes>();
            sf_shift_details_temp = new HashSet<sf_shift_details_temp>();
        }

        [Key]
        public int node_id { get; set; }

        public int work_center_id { get; set; }

        [Required]
        [StringLength(30)]
        public string node_name { get; set; }

        [StringLength(30)]
        public string alt_node_name { get; set; }

        [Required]
        [StringLength(50)]
        public string node_desc { get; set; }

        public short node_disporder { get; set; }

        public bool node_visibility { get; set; }

        public short vector_set_id { get; set; }

        public short property_set_id { get; set; }

        public short theoretical_set_id { get; set; }

        public short plc_tag_set_id { get; set; }

        public short script_set_id { get; set; }

        public short process_set_id { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }

        public int? machine_type_id { get; set; }

        public int? machine_access_license_id { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_downtimes> sf_downtimes { get; set; }

        public virtual sf_machine_types sf_machine_types { get; set; }

        public virtual sf_work_centers sf_work_centers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_shift_details_temp> sf_shift_details_temp { get; set; }
    }
}
