namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_opc_fields
    {
        [Key]
        [Column(Order = 0)]
        public short opc_field_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(30)]
        public string opc_field { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool read_only { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short opc_order { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short plc_tag_set_id { get; set; }

        [Key]
        [Column(Order = 5)]
        public byte accumulator_index { get; set; }

        public bool? use_hold { get; set; }
    }
}
