namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_opc_values
    {
        [Key]
        public short opc_value_id { get; set; }

        public short opc_field_id { get; set; }

        public short node_id { get; set; }

        public double? hold_value { get; set; }

        public double? last_value { get; set; }
    }
}
