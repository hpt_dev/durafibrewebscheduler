namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_operator_shifts
    {
        [Key]
        public long operator_shift_id { get; set; }

        public Guid shift_guid { get; set; }

        public short node_id { get; set; }

        [Required]
        [StringLength(10)]
        public string USER_ID { get; set; }

        public DateTime start_time { get; set; }

        public DateTime? stop_time { get; set; }

        public byte? shift_id { get; set; }

        public decimal? shift_actual_vb_tt { get; set; }

        public decimal? shift_theo_vb_tt { get; set; }

        [StringLength(50)]
        public string shift_01 { get; set; }

        [StringLength(50)]
        public string shift_02 { get; set; }

        [StringLength(50)]
        public string shift_03 { get; set; }

        [StringLength(50)]
        public string shift_04 { get; set; }

        [StringLength(50)]
        public string shift_05 { get; set; }

        public virtual sf_shifts sf_shifts { get; set; }
    }
}
