namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_processes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sf_processes()
        {
            sf_processes_subdowntime_sets = new HashSet<sf_processes_subdowntime_sets>();
            sf_theoreticals = new HashSet<sf_theoreticals>();
        }

        [Key]
        public short process_id { get; set; }

        [Required]
        [StringLength(50)]
        public string process { get; set; }

        public byte proc_cat_id { get; set; }

        public bool process_visibility { get; set; }

        public short process_disporder { get; set; }

        public int? process_exclude_actual { get; set; }

        public bool is_active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_processes_subdowntime_sets> sf_processes_subdowntime_sets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_theoreticals> sf_theoreticals { get; set; }
    }
}
