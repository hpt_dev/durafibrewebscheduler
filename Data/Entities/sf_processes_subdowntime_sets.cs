namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_processes_subdowntime_sets
    {
        [Key]
        public long process_subdt_set_id { get; set; }

        public short process_set_id { get; set; }

        public short process_id { get; set; }

        public short subdowntime_id { get; set; }

        public bool edit_to { get; set; }

        public bool edit_from { get; set; }

        public bool ptp { get; set; }

        public bool static_theo { get; set; }

        public int time_limit { get; set; }

        public bool accumulator { get; set; }

        public virtual sf_processes sf_processes { get; set; }

        public virtual sf_subdowntimes sf_subdowntimes { get; set; }
    }
}
