namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_sets
    {
        [Key]
        public long set_id { get; set; }

        public Guid set_guid { get; set; }

        public Guid work_order_guid { get; set; }

        public DateTime start_time { get; set; }

        public DateTime? stop_time { get; set; }

        public decimal? good_total { get; set; }

        public decimal? bad_total { get; set; }

        [StringLength(50)]
        public string set_01 { get; set; }

        [StringLength(50)]
        public string set_02 { get; set; }

        [StringLength(50)]
        public string set_03 { get; set; }

        [StringLength(50)]
        public string set_04 { get; set; }

        [StringLength(50)]
        public string set_05 { get; set; }
    }
}
