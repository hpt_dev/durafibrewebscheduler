namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_shift_details_temp
    {
        [Key]
        public long shift_detail_id { get; set; }

        public DateTime? log_time { get; set; }

        public int? node_id { get; set; }

        public int? user_id { get; set; }

        [StringLength(20)]
        public string work_order_no { get; set; }

        [StringLength(50)]
        public string process_name { get; set; }

        [StringLength(50)]
        public string action_name { get; set; }

        public int? duration { get; set; }

        public int? quantity { get; set; }

        [StringLength(50)]
        public string field_01_name { get; set; }

        [StringLength(50)]
        public string field_01_value { get; set; }

        [StringLength(50)]
        public string field_02_name { get; set; }

        [StringLength(50)]
        public string field_02_value { get; set; }

        [StringLength(50)]
        public string field_03_name { get; set; }

        [StringLength(50)]
        public string field_03_value { get; set; }

        [StringLength(150)]
        public string comments { get; set; }

        public virtual sf_nodes sf_nodes { get; set; }
    }
}
