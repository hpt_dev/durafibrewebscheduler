namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_shifts
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sf_shifts()
        {
            sf_operator_shifts = new HashSet<sf_operator_shifts>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte shift_id { get; set; }

        [Required]
        [StringLength(20)]
        public string shift_name { get; set; }

        public long? translation_text_id { get; set; }

        public byte shift_disporder { get; set; }

        public bool is_active { get; set; }

        public short? node_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_operator_shifts> sf_operator_shifts { get; set; }
    }
}
