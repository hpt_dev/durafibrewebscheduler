namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_subdowntimes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sf_subdowntimes()
        {
            sf_processes_subdowntime_sets = new HashSet<sf_processes_subdowntime_sets>();
        }

        [Key]
        public short subdowntime_id { get; set; }

        [Required]
        [StringLength(50)]
        public string subdowntime { get; set; }

        public bool is_active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_processes_subdowntime_sets> sf_processes_subdowntime_sets { get; set; }
    }
}
