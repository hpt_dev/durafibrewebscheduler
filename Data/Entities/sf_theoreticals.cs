namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_theoreticals
    {
        [Key]
        public long theoretical_id { get; set; }

        public short theoretical_set_id { get; set; }

        [StringLength(50)]
        public string tag { get; set; }

        [StringLength(30)]
        public string qualifier_01 { get; set; }

        [StringLength(30)]
        public string qualifier_02 { get; set; }

        [StringLength(30)]
        public string qualifier_03 { get; set; }

        [StringLength(30)]
        public string qualifier_04 { get; set; }

        [StringLength(30)]
        public string qualifier_05 { get; set; }

        [StringLength(30)]
        public string qualifier_06 { get; set; }

        [StringLength(30)]
        public string qualifier_07 { get; set; }

        [StringLength(30)]
        public string qualifier_08 { get; set; }

        [StringLength(30)]
        public string qualifier_09 { get; set; }

        [StringLength(30)]
        public string qualifier_10 { get; set; }

        public decimal? theoretical_from { get; set; }

        public decimal? theoretical_to { get; set; }

        public decimal? value { get; set; }

        public decimal? theoretical_rate { get; set; }

        public short? process_id { get; set; }

        public int? version { get; set; }

        public virtual sf_processes sf_processes { get; set; }
    }
}
