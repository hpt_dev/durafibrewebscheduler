namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_time_records
    {
        [Key]
        public long time_record_id { get; set; }

        public DateTime? start_date { get; set; }

        public DateTime? end_date { get; set; }

        public long? shift_id { get; set; }

        public long? process_id { get; set; }

        public long? work_center_id { get; set; }

        public long? downtime_id { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        [StringLength(50)]
        public string work_order { get; set; }
    }
}
