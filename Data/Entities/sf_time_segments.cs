namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_time_segments
    {
        [Key]
        public long time_segment_id { get; set; }

        public Guid time_segment_guid { get; set; }

        public Guid? shift_guid { get; set; }

        public Guid? work_order_guid { get; set; }

        public Guid? set_guid { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime entered_timestamp { get; set; }

        public short node_id { get; set; }

        public long? parent_id { get; set; }

        [StringLength(50)]
        public string process_name { get; set; }

        [StringLength(50)]
        public string process_object { get; set; }

        [StringLength(50)]
        public string active_group { get; set; }

        [StringLength(50)]
        public string order_results_01 { get; set; }

        [StringLength(50)]
        public string order_results_02 { get; set; }

        [StringLength(50)]
        public string order_results_03 { get; set; }

        [StringLength(50)]
        public string order_results_04 { get; set; }

        [StringLength(50)]
        public string order_results_05 { get; set; }

        [StringLength(50)]
        public string order_results_06 { get; set; }

        [StringLength(50)]
        public string crew2_1 { get; set; }

        [StringLength(50)]
        public string crew2_2 { get; set; }

        [StringLength(50)]
        public string crew2_3 { get; set; }

        [StringLength(50)]
        public string crew2_4 { get; set; }

        [StringLength(50)]
        public string one_1 { get; set; }

        [StringLength(50)]
        public string one_2 { get; set; }

        public decimal? generic_timer_01 { get; set; }

        public decimal? generic_timer_02 { get; set; }

        public decimal? generic_timer_03 { get; set; }

        public decimal? generic_timer_04 { get; set; }

        public decimal? generic_timer_05 { get; set; }

        public long? set_incrementor { get; set; }

        [StringLength(50)]
        public string subdowntime { get; set; }

        public decimal? actual_count_number { get; set; }

        public decimal? actual_count_number2 { get; set; }

        public decimal? good_total { get; set; }

        public decimal? bad_total { get; set; }

        public decimal? good_total_diff { get; set; }

        public decimal? bad_total_diff { get; set; }

        public decimal? shift_actual_vb_tt { get; set; }

        public decimal? shift_theo_vb_tt { get; set; }

        public decimal? order_actual_vb_tt { get; set; }

        public decimal? order_theo_vb_tt { get; set; }

        public long? theo_time { get; set; }

        public long? actual_time { get; set; }

        [StringLength(50)]
        public string extra_01 { get; set; }

        [StringLength(50)]
        public string extra_02 { get; set; }

        [StringLength(50)]
        public string extra_03 { get; set; }

        [StringLength(50)]
        public string extra_04 { get; set; }

        [StringLength(50)]
        public string extra_05 { get; set; }

        [StringLength(50)]
        public string extra_06 { get; set; }

        [StringLength(50)]
        public string extra_07 { get; set; }

        [StringLength(50)]
        public string extra_08 { get; set; }

        [StringLength(50)]
        public string extra_09 { get; set; }

        [StringLength(50)]
        public string extra_10 { get; set; }

        public string counters { get; set; }
    }
}
