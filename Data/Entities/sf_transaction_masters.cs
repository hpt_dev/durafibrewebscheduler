namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_transaction_masters
    {
        [Key]
        public long pk_id { get; set; }

        public long transaction_id { get; set; }

        [Required]
        [StringLength(50)]
        public string master_id { get; set; }

        public virtual sf_transactions sf_transactions { get; set; }
    }
}
