namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_transactions
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sf_transactions()
        {
            sf_transaction_masters = new HashSet<sf_transaction_masters>();
        }

        [Key]
        public long transaction_id { get; set; }

        [StringLength(50)]
        public string inventory_id { get; set; }

        public long? on_hand_inventory_id { get; set; }

        public long? item_child_id { get; set; }

        public long? work_center_id { get; set; }

        [StringLength(30)]
        public string work_order { get; set; }

        public int? operation_number { get; set; }

        public int? status_id { get; set; }

        [StringLength(50)]
        public string ref_no { get; set; }

        [StringLength(50)]
        public string pallet_id { get; set; }

        public int? set_number { get; set; }

        public int? waste_id { get; set; }

        [StringLength(10)]
        public string transaction_type { get; set; }

        public decimal? stock_uom_1 { get; set; }

        public decimal? stock_uom_2 { get; set; }

        public decimal? stock_uom_weight { get; set; }

        [StringLength(10)]
        public string shift { get; set; }

        [StringLength(500)]
        public string master_ids { get; set; }

        public long? location_id { get; set; }

        [Column(TypeName = "text")]
        public string user_remark { get; set; }

        public long? customer_id { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public bool? has_error { get; set; }

        [StringLength(150)]
        public string description { get; set; }

        [StringLength(3)]
        public string source { get; set; }

        public decimal? total_cost { get; set; }

        public bool? reject { get; set; }

        public bool? rework { get; set; }

        public DateTime? shift_date { get; set; }

        [StringLength(20)]
        public string node { get; set; }

        public decimal? weight_pounds { get; set; }

        public decimal? length_feet { get; set; }

        public int splice_count { get; set; }

        public int defect_count { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_transaction_masters> sf_transaction_masters { get; set; }
    }
}
