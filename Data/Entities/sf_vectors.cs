namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_vectors
    {
        [Key]
        public short vector_id { get; set; }

        [Required]
        [StringLength(50)]
        public string vector_name { get; set; }

        public short vector_set_id { get; set; }

        public short from_process_id { get; set; }

        public short current_process_id { get; set; }

        public short to_process_id { get; set; }

        public byte sequence_number { get; set; }

        public short form_id { get; set; }

        public short before_script_id { get; set; }

        public short after_script_id { get; set; }

        public bool is_global { get; set; }
    }
}
