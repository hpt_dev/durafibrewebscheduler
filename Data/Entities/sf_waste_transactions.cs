namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_waste_transactions
    {
        [Key]
        public long waste_transaction_id { get; set; }

        public long waste_id { get; set; }

        public long? on_hand_inventory_id { get; set; }

        [StringLength(50)]
        public string inventory_id { get; set; }

        public decimal? stock_uom_1 { get; set; }

        [StringLength(12)]
        public string stock_uom_1_id { get; set; }

        public decimal? stock_uom_2 { get; set; }

        [StringLength(12)]
        public string stock_uom_2_id { get; set; }

        public decimal stock_uom_weight { get; set; }

        [Required]
        [StringLength(12)]
        public string stock_uom_weight_id { get; set; }

        [StringLength(20)]
        public string node { get; set; }

        [StringLength(50)]
        public string work_order { get; set; }

        [StringLength(200)]
        public string user_remark { get; set; }

        [Required]
        [StringLength(10)]
        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        public decimal? lbs { get; set; }

        public decimal? ft { get; set; }

        public virtual ic_on_hand_inventory ic_on_hand_inventory { get; set; }
    }
}
