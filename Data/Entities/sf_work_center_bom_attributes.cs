namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_work_center_bom_attributes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sf_work_center_bom_attributes()
        {
            wo_step_attributes = new HashSet<wo_step_attributes>();
        }

        [Key]
        public long bom_wc_attribute_id { get; set; }

        public int work_center_group_id { get; set; }

        public int custom_field_id { get; set; }

        public long bom_attribute_type_id { get; set; }

        public bool required_at_oe { get; set; }

        public bool required_at_sched { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual bom_step_attribute_types bom_step_attribute_types { get; set; }

        public virtual cs_custom_fields cs_custom_fields { get; set; }

        public virtual sf_work_center_groups sf_work_center_groups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_step_attributes> wo_step_attributes { get; set; }
    }
}
