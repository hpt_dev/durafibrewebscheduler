namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sf_work_centers
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sf_work_centers()
        {
            ad_joined_work_centers = new HashSet<ad_joined_work_centers>();
            ad_user_work_center_associations = new HashSet<ad_user_work_center_associations>();
            bom_route_steps = new HashSet<bom_route_steps>();
            sf_ae_work_center_groupings = new HashSet<sf_ae_work_center_groupings>();
            sf_nodes = new HashSet<sf_nodes>();
        }

        [Key]
        public int work_center_id { get; set; }

        public int building_id { get; set; }

        [Required]
        [StringLength(30)]
        public string work_center_name { get; set; }

        [StringLength(50)]
        public string work_center_desc { get; set; }

        public short work_center_disporder { get; set; }

        public bool work_center_visibility { get; set; }

        public bool machine_based { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal labor_rate { get; set; }

        public decimal labor_units { get; set; }

        public bool node_scheduling { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public DateTime? deleted_date { get; set; }

        public long? deleted_by { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal? variable_oh { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal? fixed_oh { get; set; }

        public decimal? overtime_factor { get; set; }

        public decimal? doubletime_factor { get; set; }

        [StringLength(50)]
        public string run_rate_measurement { get; set; }

        [StringLength(50)]
        public string run_rate_time { get; set; }

        public decimal? run_rate { get; set; }

        public int disporder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_joined_work_centers> ad_joined_work_centers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ad_user_work_center_associations> ad_user_work_center_associations { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_steps> bom_route_steps { get; set; }

        public virtual cs_buildings cs_buildings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_ae_work_center_groupings> sf_ae_work_center_groupings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sf_nodes> sf_nodes { get; set; }
    }
}
