namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sh_carriers
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sh_carriers()
        {
            cs_carrier_contacts = new HashSet<cs_carrier_contacts>();
            cs_carrier_notes = new HashSet<cs_carrier_notes>();
            sh_freight_loads = new HashSet<sh_freight_loads>();
        }

        [Key]
        public long carrier_id { get; set; }

        [Required]
        [StringLength(50)]
        public string carrier_name { get; set; }

        [StringLength(10)]
        public string phone { get; set; }

        [StringLength(200)]
        public string email { get; set; }

        [StringLength(10)]
        public string fax { get; set; }

        [StringLength(10)]
        public string dun { get; set; }

        public long address_id { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        public virtual cs_addresses cs_addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_carrier_contacts> cs_carrier_contacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_carrier_notes> cs_carrier_notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_freight_loads> sh_freight_loads { get; set; }
    }
}
