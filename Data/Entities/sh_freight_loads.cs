namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sh_freight_loads
    {
        [Key]
        public long freight_load_id { get; set; }

        public long carrier_id { get; set; }

        [Required]
        [StringLength(20)]
        public string carrier_PO { get; set; }

        public decimal cost { get; set; }

        public long est_total_wt_on_truck { get; set; }

        [StringLength(20)]
        public string trailer_no { get; set; }

        [StringLength(20)]
        public string seal_no { get; set; }

        [Column(TypeName = "text")]
        public string delivery_instructions { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        [StringLength(50)]
        public string loaded_by_name { get; set; }

        [StringLength(50)]
        public string counted_by_name { get; set; }

        public int trailer_load_type_id { get; set; }

        public int trailer_freight_type_id { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual sh_carriers sh_carriers { get; set; }
    }
}
