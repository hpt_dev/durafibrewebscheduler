namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sh_shipment_appoval
    {
        [Key]
        public long ship_approval_id { get; set; }

        public decimal shipped_weight { get; set; }

        [Required]
        [StringLength(20)]
        public string load_number { get; set; }

        [Required]
        [StringLength(500)]
        public string reason { get; set; }

        public long approved_by { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }
    }
}
