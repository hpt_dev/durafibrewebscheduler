namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sh_shipments
    {
        [Key]
        public long shipment_id { get; set; }

        public long freight_load_id { get; set; }

        public long so_children_id { get; set; }

        public decimal? admin_cost { get; set; }

        public decimal? markup_percent { get; set; }

        [StringLength(20)]
        public string load_number { get; set; }

        public decimal? weight { get; set; }

        [Column(TypeName = "text")]
        public string notes { get; set; }

        public DateTime? shipment_date { get; set; }

        [StringLength(20)]
        public string packingslip_no { get; set; }

        [StringLength(20)]
        public string bol_no { get; set; }

        public bool? is_shipped { get; set; }

        public int? stop_no { get; set; }

        public int shipment_status_id { get; set; }

        public int? building_id { get; set; }

        public int? work_order_units { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual sh_statuses sh_statuses { get; set; }

        public virtual so_children so_children { get; set; }
    }
}
