namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sh_shipped_lot_pallets
    {
        [Key]
        public long shipped_lot_pallets { get; set; }

        [StringLength(50)]
        public string pallet_id { get; set; }

        [StringLength(50)]
        public string lot_no { get; set; }
    }
}
