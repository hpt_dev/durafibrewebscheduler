namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sh_trailer_freight_types
    {
        [Key]
        public int trailer_freight_type_id { get; set; }

        [Required]
        [StringLength(50)]
        public string trailer_freight_type { get; set; }

        [Required]
        [StringLength(50)]
        public string trailer_freight_description { get; set; }
    }
}
