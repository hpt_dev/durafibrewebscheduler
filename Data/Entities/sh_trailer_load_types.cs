namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sh_trailer_load_types
    {
        [Key]
        public int trailer_load_type_id { get; set; }

        [Required]
        [StringLength(50)]
        public string trailer_load_type { get; set; }

        [Required]
        [StringLength(50)]
        public string trailer_load_description { get; set; }
    }
}
