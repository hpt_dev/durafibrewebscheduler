namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sh_transit_days
    {
        [Key]
        public long transit_days_id { get; set; }

        public int building_id { get; set; }

        public long address_id { get; set; }

        public long transit_days { get; set; }

        public virtual cs_addresses cs_addresses { get; set; }

        public virtual cs_buildings cs_buildings { get; set; }
    }
}
