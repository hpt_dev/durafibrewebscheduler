namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_additional_charge_categories
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public so_additional_charge_categories()
        {
            so_additional_charge_categories1 = new HashSet<so_additional_charge_categories>();
            so_additional_charges = new HashSet<so_additional_charges>();
            so_charges = new HashSet<so_charges>();
        }

        [Key]
        public long additional_charge_category_id { get; set; }

        public long? additional_charge_category_id_rk { get; set; }

        [Required]
        [StringLength(250)]
        public string category_name { get; set; }

        public bool is_special { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_additional_charge_categories> so_additional_charge_categories1 { get; set; }

        public virtual so_additional_charge_categories so_additional_charge_categories2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_additional_charges> so_additional_charges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_charges> so_charges { get; set; }
    }
}
