namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_additional_charges
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public so_additional_charges()
        {
            so_associated_charges = new HashSet<so_associated_charges>();
        }

        [Key]
        public long additional_charge_id { get; set; }

        public long additional_charge_category_id { get; set; }

        [Required]
        [StringLength(250)]
        public string description { get; set; }

        public decimal default_cost { get; set; }

        [Required]
        [StringLength(20)]
        public string qty_uom { get; set; }

        public bool use_qty { get; set; }

        public bool tax_exempt { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual so_additional_charge_categories so_additional_charge_categories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_associated_charges> so_associated_charges { get; set; }
    }
}
