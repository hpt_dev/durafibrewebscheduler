namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_associated_charges
    {
        [Key]
        public long associated_charge_id { get; set; }

        [Required]
        [StringLength(50)]
        public string setting_category { get; set; }

        [Required]
        [StringLength(50)]
        public string field_name { get; set; }

        public long additional_charge_id { get; set; }

        public bool is_qty { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        public virtual so_additional_charges so_additional_charges { get; set; }
    }
}
