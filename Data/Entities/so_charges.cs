namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_charges
    {
        [Key]
        public long charge_id { get; set; }

        public long so_children_id { get; set; }

        public long additional_charge_category_id { get; set; }

        public long additional_charge_id { get; set; }

        [Required]
        [StringLength(250)]
        public string description { get; set; }

        public decimal sale_price { get; set; }

        public decimal charge_qty { get; set; }

        [Required]
        [StringLength(12)]
        public string charge_uom { get; set; }

        public bool tax_exempt { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        public virtual so_additional_charge_categories so_additional_charge_categories { get; set; }

        public virtual so_children so_children { get; set; }
    }
}
