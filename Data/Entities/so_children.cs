namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_children
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public so_children()
        {
            sh_shipments = new HashSet<sh_shipments>();
            so_charges = new HashSet<so_charges>();
            so_children_images = new HashSet<so_children_images>();
            so_children_service_settings = new HashSet<so_children_service_settings>();
        }

        [Key]
        public long so_children_id { get; set; }

        public long sales_order_id { get; set; }

        [StringLength(20)]
        public string work_order_no { get; set; }

        public int? line_no { get; set; }

        public DateTime due_date { get; set; }

        public long? blanket_release_qty { get; set; }

        public int? blanket_release_interval { get; set; }

        public int? blanket_release_interval_uom { get; set; }

        public DateTime? last_release_date { get; set; }

        public DateTime? first_release_date { get; set; }

        public bool? blanket_order { get; set; }

        public decimal? sale_price { get; set; }

        public bool? is_blindship { get; set; }

        public long? shipto_customer_id { get; set; }

        public DateTime? shipping_est_date { get; set; }

        public DateTime? last_shipping_date { get; set; }

        public DateTime? shipping_est_delivery_date { get; set; }

        public long? shipto_address_id { get; set; }

        [StringLength(20)]
        public string shipto_PO { get; set; }

        public int? shipment_type_id { get; set; }

        public int? shipment_payment_id { get; set; }

        public int? freight_terms_id { get; set; }

        [Column(TypeName = "text")]
        public string ship_notes { get; set; }

        public long? shipping_zone_id { get; set; }

        public decimal? shipping_weight { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public decimal? ship_qty { get; set; }

        [StringLength(12)]
        public string ship_qty_uom { get; set; }

        public decimal? markup_percent { get; set; }

        public decimal? discount_percent { get; set; }

        public decimal? tare_weight { get; set; }

        [StringLength(12)]
        public string sale_price_uom { get; set; }

        [StringLength(500)]
        public string line_item { get; set; }

        public decimal tax_percent { get; set; }

        public bool? tax_exempt { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ap_delivery_terms ap_delivery_terms { get; set; }

        public virtual ap_payment_terms ap_payment_terms { get; set; }

        public virtual ap_ship_types ap_ship_types { get; set; }

        public virtual cs_addresses cs_addresses { get; set; }

        public virtual cs_customers cs_customers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sh_shipments> sh_shipments { get; set; }

        public virtual sh_zones sh_zones { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_charges> so_charges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children_images> so_children_images { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children_service_settings> so_children_service_settings { get; set; }

        public virtual so_sales_orders so_sales_orders { get; set; }
    }
}
