namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_children_images
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long so_children_image_id { get; set; }

        public long so_children_id { get; set; }

        public long image_id { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public int? image_order { get; set; }

        public virtual ps_images ps_images { get; set; }

        public virtual so_children so_children { get; set; }
    }
}
