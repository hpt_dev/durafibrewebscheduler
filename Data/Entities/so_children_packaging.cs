namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_children_packaging
    {
        [Key]
        public long child_packaging_id { get; set; }

        public long so_children_id { get; set; }

        public long pac_template_id { get; set; }

        [Required]
        [StringLength(50)]
        public string value { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public virtual pac_templates pac_templates { get; set; }
    }
}
