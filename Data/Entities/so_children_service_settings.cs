namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_children_service_settings
    {
        [Key]
        public long child_service_setting_id { get; set; }

        public long third_party_option_id { get; set; }

        public long so_children_id { get; set; }

        [Required]
        public string value { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        public virtual ps_third_party_options ps_third_party_options { get; set; }

        public virtual so_children so_children { get; set; }
    }
}
