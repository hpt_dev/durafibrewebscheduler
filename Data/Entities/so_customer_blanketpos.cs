namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_customer_blanketpos
    {
        [Key]
        public long customer_blanket_id { get; set; }

        public long customer_id { get; set; }

        [Required]
        [StringLength(50)]
        public string blanket_po { get; set; }

        public decimal amount { get; set; }

        [Required]
        [StringLength(200)]
        public string po_description { get; set; }

        public virtual cs_customers cs_customers { get; set; }
    }
}
