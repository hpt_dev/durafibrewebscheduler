namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_order_categories
    {
        [Key]
        public long order_category_id { get; set; }

        [StringLength(50)]
        public string category_name { get; set; }
    }
}
