namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_reps_territories
    {
        [Key]
        public long reps_territory_id { get; set; }

        public long sales_rep_id { get; set; }

        public long territory_id { get; set; }

        public virtual so_sales_reps so_sales_reps { get; set; }

        public virtual so_territories so_territories { get; set; }
    }
}
