namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class so_sales_orders
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public so_sales_orders()
        {
            so_children = new HashSet<so_children>();
        }

        [Key]
        public long sales_order_id { get; set; }

        [Required]
        [StringLength(20)]
        public string sales_order_no { get; set; }

        public long customer_id { get; set; }

        public long? customer_address_id { get; set; }

        [Required]
        [StringLength(20)]
        public string customer_PO { get; set; }

        public bool blanket_order { get; set; }

        public bool blind_ship { get; set; }

        public DateTime order_date { get; set; }

        public int so_status_id { get; set; }

        public int building_id { get; set; }

        public decimal? markup_percent { get; set; }

        public decimal? discount_percent { get; set; }

        [StringLength(50)]
        public string priority { get; set; }

        [StringLength(20)]
        public string blanket_PO { get; set; }

        public long? sales_rep { get; set; }

        public bool is_invoiced { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? account_children_id { get; set; }

        public int? payment_type_id { get; set; }

        [StringLength(20)]
        public string ref_no { get; set; }

        public long order_category_id { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual cs_buildings cs_buildings { get; set; }

        public virtual cs_customers cs_customers { get; set; }

        public virtual so_accounts_children so_accounts_children { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<so_children> so_children { get; set; }

        public virtual so_statuses so_statuses { get; set; }
    }
}
