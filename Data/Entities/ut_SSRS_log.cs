namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ut_SSRS_log
    {
        [Key]
        public long SSRS_log_id { get; set; }

        [Required]
        [StringLength(8000)]
        public string request_text { get; set; }

        [Required]
        [StringLength(8000)]
        public string results_text { get; set; }

        public long user_id { get; set; }

        public DateTime request_datetime { get; set; }

        public virtual ad_users ad_users { get; set; }
    }
}
