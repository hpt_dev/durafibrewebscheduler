namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_approval_statuses
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public wo_approval_statuses()
        {
            wo_approval_templates = new HashSet<wo_approval_templates>();
        }

        [Key]
        public long approval_status_id { get; set; }

        [Required]
        [StringLength(3)]
        public string status { get; set; }

        [Required]
        [StringLength(100)]
        public string status_description { get; set; }

        public int status_order { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_templates> wo_approval_templates { get; set; }
    }
}
