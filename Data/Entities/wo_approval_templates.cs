namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_approval_templates
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public wo_approval_templates()
        {
            wo_approval_groups = new HashSet<wo_approval_groups>();
            wo_approval_settings = new HashSet<wo_approval_settings>();
        }

        [Key]
        public long approval_template_id { get; set; }

        public long approval_status_id { get; set; }

        [Required]
        [StringLength(250)]
        public string approval_name { get; set; }

        public int? text_id { get; set; }

        public byte field_type_id { get; set; }

        public bool use_group { get; set; }

        public int? work_center_group_id { get; set; }

        [Required]
        [StringLength(150)]
        public string default_value { get; set; }

        [Required]
        public string acceptable_values { get; set; }

        public bool no_edit { get; set; }

        public bool required_for_sc { get; set; }

        public bool ignore_subassemblies { get; set; }

        public int disporder { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        public virtual ps_field_types ps_field_types { get; set; }

        public virtual sf_work_center_groups sf_work_center_groups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_groups> wo_approval_groups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_settings> wo_approval_settings { get; set; }

        public virtual wo_approval_statuses wo_approval_statuses { get; set; }
    }
}
