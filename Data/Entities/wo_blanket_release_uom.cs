namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_blanket_release_uom
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int blanket_release_uom_id { get; set; }

        [Required]
        [StringLength(20)]
        public string blanket_release_uom { get; set; }
    }
}
