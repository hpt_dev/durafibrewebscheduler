namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_comments
    {
        [Key]
        public long comment_id { get; set; }

        public long wo_master_id { get; set; }

        public string comment { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public virtual wo_master wo_master { get; set; }
    }
}
