namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_job_classes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public wo_job_classes()
        {
            wo_master = new HashSet<wo_master>();
            wo_type_job_classes = new HashSet<wo_type_job_classes>();
        }

        [Key]
        public int job_class_id { get; set; }

        [Required]
        [StringLength(50)]
        public string job_class { get; set; }

        public bool? use_side_by_side { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_master> wo_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_type_job_classes> wo_type_job_classes { get; set; }
    }
}
