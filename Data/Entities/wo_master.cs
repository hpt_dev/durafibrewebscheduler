namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public wo_master()
        {
            bom_route_steps = new HashSet<bom_route_steps>();
            bom_subassembly_hierarchies = new HashSet<bom_subassembly_hierarchies>();
            bom_subassembly_hierarchies1 = new HashSet<bom_subassembly_hierarchies>();
            wo_approval_settings = new HashSet<wo_approval_settings>();
            wo_comments = new HashSet<wo_comments>();
            wo_notes = new HashSet<wo_notes>();
        }

        [Key]
        public long wo_master_id { get; set; }

        [Required]
        [StringLength(20)]
        public string work_order_no { get; set; }

        public long item_child_id { get; set; }

        public DateTime due_date { get; set; }

        public DateTime? close_date { get; set; }

        public int lead_time { get; set; }

        public long? template_id { get; set; }

        public int? overrun_percent { get; set; }

        public int? underrun_percent { get; set; }

        public decimal order_qty { get; set; }

        public long? actual_qty { get; set; }

        [StringLength(12)]
        public string order_qty_uom { get; set; }

        public decimal? scrap_percent { get; set; }

        [Column(TypeName = "text")]
        public string mfg_notes { get; set; }

        public int mfg_building_id { get; set; }

        [Column(TypeName = "text")]
        public string label_marks { get; set; }

        public int order_type_id { get; set; }

        public int job_class_id { get; set; }

        public int? label_style { get; set; }

        public int wo_status_id { get; set; }

        public long? packaging_spec_id { get; set; }

        public long created_by { get; set; }

        public DateTime? created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public bool? fully_allocated { get; set; }

        public int? no_outs { get; set; }

        public int? no_webs_up { get; set; }

        [StringLength(20)]
        public string ss_number { get; set; }

        public bool? splices_ok { get; set; }

        [StringLength(4)]
        public string coating_in { get; set; }

        public int? split_id { get; set; }

        public decimal? od { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_route_steps> bom_route_steps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_subassembly_hierarchies> bom_subassembly_hierarchies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bom_subassembly_hierarchies> bom_subassembly_hierarchies1 { get; set; }

        public virtual bom_templates bom_templates { get; set; }

        public virtual cs_buildings cs_buildings { get; set; }

        public virtual ic_item_children ic_item_children { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_approval_settings> wo_approval_settings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_comments> wo_comments { get; set; }

        public virtual wo_job_classes wo_job_classes { get; set; }

        public virtual wo_order_types wo_order_types { get; set; }

        public virtual wo_statuses wo_statuses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_notes> wo_notes { get; set; }
    }
}
