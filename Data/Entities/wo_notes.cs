namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_notes
    {
        [Key]
        public long wo_note_id { get; set; }

        public long wo_master_id { get; set; }

        public DateTime time_entered { get; set; }

        public long user_id { get; set; }

        public long? wc_number { get; set; }

        [Column(TypeName = "text")]
        public string wo_note { get; set; }

        public long? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual wo_master wo_master { get; set; }
    }
}
