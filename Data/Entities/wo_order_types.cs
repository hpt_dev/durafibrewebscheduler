namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_order_types
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public wo_order_types()
        {
            wo_master = new HashSet<wo_master>();
            wo_type_job_classes = new HashSet<wo_type_job_classes>();
        }

        [Key]
        public int order_type_id { get; set; }

        [Required]
        [StringLength(50)]
        public string order_type { get; set; }

        public int? text_id { get; set; }

        public int? category_nibble { get; set; }

        public bool? ss_allowed { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_master> wo_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_type_job_classes> wo_type_job_classes { get; set; }
    }
}
