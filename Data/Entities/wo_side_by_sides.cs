namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_side_by_sides
    {
        [Key]
        public long side_by_side_id { get; set; }

        public long unit_allocation_id { get; set; }

        [Required]
        [StringLength(20)]
        public string work_order_no { get; set; }

        public int position { get; set; }

        public decimal width { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long? last_modified_by { get; set; }

        public DateTime? last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual ad_users ad_users { get; set; }

        public virtual ad_users ad_users1 { get; set; }

        public virtual ad_users ad_users2 { get; set; }

        public virtual ic_unit_allocations ic_unit_allocations { get; set; }
    }
}
