namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_statuses
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public wo_statuses()
        {
            wo_master = new HashSet<wo_master>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int wo_status_id { get; set; }

        [Required]
        [StringLength(3)]
        public string status { get; set; }

        [Required]
        [StringLength(25)]
        public string status_description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<wo_master> wo_master { get; set; }
    }
}
