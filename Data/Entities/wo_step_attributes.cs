namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_step_attributes
    {
        [Key]
        public long wo_step_attribute_id { get; set; }

        public long route_step_id { get; set; }

        public long bom_wc_attribute_id { get; set; }

        public string custom_field_value { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public DateTime created_date { get; set; }

        public long last_modified_by { get; set; }

        public DateTime last_modified_date { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_date { get; set; }

        public virtual bom_route_steps bom_route_steps { get; set; }

        public virtual sf_work_center_bom_attributes sf_work_center_bom_attributes { get; set; }
    }
}
