namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class wo_type_job_classes
    {
        [Key]
        public long wo_type_job_class_id { get; set; }

        public int? order_type_id { get; set; }

        public int? job_class_id { get; set; }

        public virtual wo_job_classes wo_job_classes { get; set; }

        public virtual wo_order_types wo_order_types { get; set; }
    }
}
