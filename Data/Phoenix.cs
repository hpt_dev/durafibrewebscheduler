

namespace Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Data;
    using System.Data.SqlClient;
    using System.Configuration;
    using System.Reflection;

    public partial class Phoenix : DbContext
    {
        //public Phoenix()
        //    : base()
        //{
        //    Database.SetInitializer<Phoenix>(null);  // not sure if this is a good idea im guessing its good since there are no migrations
        //                                             //Database.Connection.ConnectionString = "data source=25.86.198.54; initial catalog=phoenix;persist security info=True;user id=sa;password=Operon@1;MultipleActiveResultSets=True;";
        //    var connection = System.Configuration.ConfigurationManager.ConnectionStrings["Phoneix"].ConnectionString;
           
        //    Database.Connection.ConnectionString = "data source=10.0.1.56; initial catalog=phoenix_test;persist security info=True;user id=sa;password=Sun53t!@#;MultipleActiveResultSets =True;";
            
        //}
        public Phoenix(string connection)
           : base()
        {
            Database.SetInitializer<Phoenix>(null);  
            Database.Connection.ConnectionString = connection;
            Database.CommandTimeout = 0;
        }
        public override int SaveChanges()
        {
            
            return base.SaveChanges();
        }


        public async Task<List<T>> getData<T> (string sql) where T : class, new()
        {
            var ret = new List<T>();
            try
            {
                DataTable dt = new DataTable();
                using (SqlConnection con = new SqlConnection(Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.CommandTimeout = 0;
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        dt = await GetDataAsync(da);
                        List<string> columNames = new List<string>();
                        foreach (DataColumn col in dt.Columns) { 
                            columNames.Add(col.ColumnName);
                        }
                        foreach (var row in dt.AsEnumerable())
                        {                            
                            T obj = new T();
                            foreach (var prop in obj.GetType().GetProperties().Where(x => columNames.Contains(x.Name) ).ToList())
                            {
                                try
                                {
                                    PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                                    if(row[prop.Name].ToString() != ""  )
                                        //propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                                        propertyInfo.SetValue(obj, row[prop.Name]) ;
                                }
                                catch (Exception e)
                                {
                                    if(e.Message != "Object cannot be cast from DBNull to other types.")
                                        continue;
                                }
                            }

                            ret.Add(obj);
                        }
                        dt.Dispose();
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                return ret;
            }
        }



        public async Task<List<Dictionary<string, object>>> getData(string sql)
        {
            try
            {
                DataTable dt = new DataTable();
                using (SqlConnection con = new SqlConnection(Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.CommandTimeout = 0;
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        dt = await GetDataAsync(da);
                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        Dictionary<string, object> row;
                        foreach (DataRow dr in dt.Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                string key = col.ColumnName;
                                for (var j = 1; row.ContainsKey(key); j++)
                                    key = col.ColumnName + "_" + j;

                                row.Add(key, dr[col]);
                            }
                            rows.Add(row);
                        }
                        dt.Dispose();
                        return rows;
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<Dictionary<string, object>>();
                throw;
            }
        }
        private Task<DataTable> GetDataAsync(SqlDataAdapter dataAdapter)
        {
            var tcs = new TaskCompletionSource<DataTable>();
            var dt = new DataTable();
            try
            {
                dataAdapter.Fill(dt);
                tcs.TrySetResult(dt);
            }
            catch (Exception ex)
            {
                tcs.SetException(ex);
            }
            return tcs.Task;

        }
       
        public virtual DbSet<ad_page_security> ad_page_security { get; set; }
        public virtual DbSet<C_message_config> C_message_config { get; set; }
        public virtual DbSet<C_message_counters> C_message_counters { get; set; }
        public virtual DbSet<C_message_exp_dates> C_message_exp_dates { get; set; }
        public virtual DbSet<C_message_fields> C_message_fields { get; set; }
        public virtual DbSet<C_message_shifts> C_message_shifts { get; set; }
        public virtual DbSet<C_sp_param_tracking> C_sp_param_tracking { get; set; }
        public virtual DbSet<ad_application_message_type> ad_application_message_type { get; set; }
        public virtual DbSet<ad_application_messages> ad_application_messages { get; set; }
        public virtual DbSet<ad_audit> ad_audit { get; set; }
        public virtual DbSet<ad_comment_types> ad_comment_types { get; set; }
        public virtual DbSet<ad_comments> ad_comments { get; set; }
        public virtual DbSet<ad_joined_work_centers> ad_joined_work_centers { get; set; }
        public virtual DbSet<ad_languages> ad_languages { get; set; }
        public virtual DbSet<ad_predefined_comments> ad_predefined_comments { get; set; }
        public virtual DbSet<ad_security_controls> ad_security_controls { get; set; }
        public virtual DbSet<ad_security_groups> ad_security_groups { get; set; }
        public virtual DbSet<ad_security_modules> ad_security_modules { get; set; }
        public virtual DbSet<ad_security_screens> ad_security_screens { get; set; }
        public virtual DbSet<ad_translations> ad_translations { get; set; }
        public virtual DbSet<ad_user_buildings> ad_user_buildings { get; set; }
        public virtual DbSet<ad_user_work_center_associations> ad_user_work_center_associations { get; set; }
        public virtual DbSet<ad_users> ad_users { get; set; }
        public virtual DbSet<ad_work_center_printer_associations> ad_work_center_printer_associations { get; set; }
        public virtual DbSet<ap_delivery_terms> ap_delivery_terms { get; set; }
        public virtual DbSet<ap_payment_terms> ap_payment_terms { get; set; }
        public virtual DbSet<ap_po_children> ap_po_children { get; set; }
        public virtual DbSet<ap_po_master> ap_po_master { get; set; }
        public virtual DbSet<ap_ship_types> ap_ship_types { get; set; }
        public virtual DbSet<ar_invoices> ar_invoices { get; set; }
        public virtual DbSet<ar_payment_types> ar_payment_types { get; set; }
        public virtual DbSet<ar_payments_received> ar_payments_received { get; set; }
        public virtual DbSet<bom_produced_items> bom_produced_items { get; set; }
        public virtual DbSet<bom_required_materials> bom_required_materials { get; set; }
        public virtual DbSet<bom_route_allocations> bom_route_allocations { get; set; }
        public virtual DbSet<bom_route_item_transactions> bom_route_item_transactions { get; set; }
        public virtual DbSet<bom_route_material_transactions> bom_route_material_transactions { get; set; }
        public virtual DbSet<bom_route_produced_items> bom_route_produced_items { get; set; }
        public virtual DbSet<bom_route_required_materials> bom_route_required_materials { get; set; }
        public virtual DbSet<bom_route_statuses> bom_route_statuses { get; set; }
        public virtual DbSet<bom_route_steps> bom_route_steps { get; set; }
        public virtual DbSet<bom_step_attribute_types> bom_step_attribute_types { get; set; }
        public virtual DbSet<bom_step_options> bom_step_options { get; set; }
        public virtual DbSet<bom_step_rules> bom_step_rules { get; set; }
        public virtual DbSet<bom_steps> bom_steps { get; set; }
        public virtual DbSet<bom_subassemblies> bom_subassemblies { get; set; }
        public virtual DbSet<bom_subassembly_hierarchies> bom_subassembly_hierarchies { get; set; }
        public virtual DbSet<bom_templates> bom_templates { get; set; }
        public virtual DbSet<bom_transaction_options> bom_transaction_options { get; set; }
        public virtual DbSet<cs_address_types> cs_address_types { get; set; }
        public virtual DbSet<cs_addresses> cs_addresses { get; set; }
        public virtual DbSet<cs_building_associations> cs_building_associations { get; set; }
        public virtual DbSet<cs_buildings> cs_buildings { get; set; }
        public virtual DbSet<cs_business_units> cs_business_units { get; set; }
        public virtual DbSet<cs_carrier_contacts> cs_carrier_contacts { get; set; }
        public virtual DbSet<cs_carrier_notes> cs_carrier_notes { get; set; }
        public virtual DbSet<cs_companies> cs_companies { get; set; }
        public virtual DbSet<cs_countries> cs_countries { get; set; }
        public virtual DbSet<cs_credit_holds> cs_credit_holds { get; set; }
        public virtual DbSet<cs_custom_field_formulas> cs_custom_field_formulas { get; set; }
        public virtual DbSet<cs_custom_field_properties> cs_custom_field_properties { get; set; }
        public virtual DbSet<cs_custom_field_rules> cs_custom_field_rules { get; set; }
        public virtual DbSet<cs_custom_field_values> cs_custom_field_values { get; set; }
        public virtual DbSet<cs_custom_fields> cs_custom_fields { get; set; }
        public virtual DbSet<cs_customer_addresses> cs_customer_addresses { get; set; }
        public virtual DbSet<cs_customer_association> cs_customer_association { get; set; }
        public virtual DbSet<cs_customer_contacts> cs_customer_contacts { get; set; }
        public virtual DbSet<cs_customer_edis> cs_customer_edis { get; set; }
        public virtual DbSet<cs_customer_files> cs_customer_files { get; set; }
        public virtual DbSet<cs_customer_manufacturing_notes> cs_customer_manufacturing_notes { get; set; }
        public virtual DbSet<cs_customer_notes> cs_customer_notes { get; set; }
        public virtual DbSet<cs_customer_ranks> cs_customer_ranks { get; set; }
        public virtual DbSet<cs_customer_third_party_settings> cs_customer_third_party_settings { get; set; }
        public virtual DbSet<cs_customer_work_group_notes> cs_customer_work_group_notes { get; set; }
        public virtual DbSet<cs_customers> cs_customers { get; set; }
        public virtual DbSet<cs_global_counters> cs_global_counters { get; set; }
        public virtual DbSet<cs_grid_colors> cs_grid_colors { get; set; }
        public virtual DbSet<cs_grid_condition_types> cs_grid_condition_types { get; set; }
        public virtual DbSet<cs_grid_conditionals> cs_grid_conditionals { get; set; }
        public virtual DbSet<cs_grid_control_types> cs_grid_control_types { get; set; }
        public virtual DbSet<cs_grid_fields> cs_grid_fields { get; set; }
        public virtual DbSet<cs_grid_filter_fields> cs_grid_filter_fields { get; set; }
        public virtual DbSet<cs_grid_user_colors> cs_grid_user_colors { get; set; }
        public virtual DbSet<cs_grid_user_fields> cs_grid_user_fields { get; set; }
        public virtual DbSet<cs_grid_user_filter_fields> cs_grid_user_filter_fields { get; set; }
        public virtual DbSet<cs_grids> cs_grids { get; set; }
        public virtual DbSet<cs_location_types> cs_location_types { get; set; }
        public virtual DbSet<cs_locations> cs_locations { get; set; }
        public virtual DbSet<cs_manufacturing_note_types> cs_manufacturing_note_types { get; set; }
        public virtual DbSet<cs_quality_holds> cs_quality_holds { get; set; }
        public virtual DbSet<cs_rules> cs_rules { get; set; }
        public virtual DbSet<cs_zipcodes> cs_zipcodes { get; set; }
        public virtual DbSet<edi_customer_file_formats> edi_customer_file_formats { get; set; }
        public virtual DbSet<edi_customer_mapping_conversions> edi_customer_mapping_conversions { get; set; }
        public virtual DbSet<edi_customer_mappings> edi_customer_mappings { get; set; }
        public virtual DbSet<edi_document_configs> edi_document_configs { get; set; }
        public virtual DbSet<edi_document_requests> edi_document_requests { get; set; }
        public virtual DbSet<edi_document_types> edi_document_types { get; set; }
        public virtual DbSet<edi_element_values> edi_element_values { get; set; }
        public virtual DbSet<edi_elements> edi_elements { get; set; }
        public virtual DbSet<edi_envelope_parse> edi_envelope_parse { get; set; }
        public virtual DbSet<edi_envelopes> edi_envelopes { get; set; }
        public virtual DbSet<edi_error_logs> edi_error_logs { get; set; }
        public virtual DbSet<edi_file_naming_schemes> edi_file_naming_schemes { get; set; }
        public virtual DbSet<edi_ftpserver_info> edi_ftpserver_info { get; set; }
        public virtual DbSet<edi_generation_default_values> edi_generation_default_values { get; set; }
        public virtual DbSet<edi_generation_results> edi_generation_results { get; set; }
        public virtual DbSet<edi_mappings> edi_mappings { get; set; }
        public virtual DbSet<edi_parse_results> edi_parse_results { get; set; }
        public virtual DbSet<edi_regex> edi_regex { get; set; }
        public virtual DbSet<edi_schemas> edi_schemas { get; set; }
        public virtual DbSet<edi_transaction_mapping> edi_transaction_mapping { get; set; }
        public virtual DbSet<edi_transactions> edi_transactions { get; set; }
        public virtual DbSet<edi_x12_error_codes> edi_x12_error_codes { get; set; }
        public virtual DbSet<edi_x12_error_mapping> edi_x12_error_mapping { get; set; }
        public virtual DbSet<edi_x12_errors> edi_x12_errors { get; set; }
        public virtual DbSet<edi_x12mapping> edi_x12mapping { get; set; }
        public virtual DbSet<ic_consumed_inventory> ic_consumed_inventory { get; set; }
        public virtual DbSet<ic_customer_items> ic_customer_items { get; set; }
        public virtual DbSet<ic_customer_pallets> ic_customer_pallets { get; set; }
        public virtual DbSet<ic_dim_groups> ic_dim_groups { get; set; }
        public virtual DbSet<ic_inventory_history> ic_inventory_history { get; set; }
        public virtual DbSet<ic_inventory_images> ic_inventory_images { get; set; }
        public virtual DbSet<ic_inventory_types> ic_inventory_types { get; set; }
        public virtual DbSet<ic_item_categories> ic_item_categories { get; set; }
        public virtual DbSet<ic_item_children> ic_item_children { get; set; }
        public virtual DbSet<ic_item_children_X10> ic_item_children_X10 { get; set; }
        public virtual DbSet<ic_item_images> ic_item_images { get; set; }
        public virtual DbSet<ic_item_master> ic_item_master { get; set; }
        public virtual DbSet<ic_item_quantities> ic_item_quantities { get; set; }
        public virtual DbSet<ic_item_statuses> ic_item_statuses { get; set; }
        public virtual DbSet<ic_nmfc_classes> ic_nmfc_classes { get; set; }
        public virtual DbSet<ic_od_ranges> ic_od_ranges { get; set; }
        public virtual DbSet<ic_on_hand_inventory> ic_on_hand_inventory { get; set; }
        public virtual DbSet<ic_on_hand_quality_holds> ic_on_hand_quality_holds { get; set; }
        public virtual DbSet<ic_physical_inventory> ic_physical_inventory { get; set; }
        public virtual DbSet<ic_physical_inventory_comparison_files> ic_physical_inventory_comparison_files { get; set; }
        public virtual DbSet<ic_physical_inventory_comparison_headcount> ic_physical_inventory_comparison_headcount { get; set; }
        public virtual DbSet<ic_physical_inventory_comparison_master> ic_physical_inventory_comparison_master { get; set; }
        public virtual DbSet<ic_physical_inventory_comparison_scanner> ic_physical_inventory_comparison_scanner { get; set; }
        public virtual DbSet<ic_physical_inventory_history> ic_physical_inventory_history { get; set; }
        public virtual DbSet<ic_physical_inventory_item> ic_physical_inventory_item { get; set; }
        public virtual DbSet<ic_physical_inventory_item_status> ic_physical_inventory_item_status { get; set; }
        public virtual DbSet<ic_physical_inventory_status> ic_physical_inventory_status { get; set; }
        public virtual DbSet<ic_physical_inventory_type> ic_physical_inventory_type { get; set; }
        public virtual DbSet<ic_quality_hold_reasons> ic_quality_hold_reasons { get; set; }
        public virtual DbSet<ic_quality_hold_types> ic_quality_hold_types { get; set; }
        public virtual DbSet<ic_quality_holds> ic_quality_holds { get; set; }
        public virtual DbSet<ic_unit_allocation_history> ic_unit_allocation_history { get; set; }
        public virtual DbSet<ic_unit_allocations> ic_unit_allocations { get; set; }
        public virtual DbSet<lbl_format_types> lbl_format_types { get; set; }
        public virtual DbSet<lbl_item_mapping> lbl_item_mapping { get; set; }
        public virtual DbSet<lbl_label> lbl_label { get; set; }
        public virtual DbSet<lbl_label_printer_types> lbl_label_printer_types { get; set; }
        public virtual DbSet<lbl_label_type> lbl_label_type { get; set; }
        public virtual DbSet<lbl_printer_settings> lbl_printer_settings { get; set; }
        public virtual DbSet<lbl_printer_type_associations> lbl_printer_type_associations { get; set; }
        public virtual DbSet<lbl_printer_types> lbl_printer_types { get; set; }
        public virtual DbSet<lbl_printers> lbl_printers { get; set; }
        public virtual DbSet<lbl_printjob> lbl_printjob { get; set; }
        public virtual DbSet<MSpeer_conflictdetectionconfigrequest> MSpeer_conflictdetectionconfigrequest { get; set; }
        public virtual DbSet<MSpeer_lsns> MSpeer_lsns { get; set; }
        public virtual DbSet<pac_templates> pac_templates { get; set; }
        public virtual DbSet<ps_ae_uom_labels> ps_ae_uom_labels { get; set; }
        public virtual DbSet<ps_control_type> ps_control_type { get; set; }
        public virtual DbSet<ps_controls> ps_controls { get; set; }
        public virtual DbSet<ps_field_types> ps_field_types { get; set; }
        public virtual DbSet<ps_file_types> ps_file_types { get; set; }
        public virtual DbSet<ps_files> ps_files { get; set; }
        public virtual DbSet<ps_image_associations> ps_image_associations { get; set; }
        public virtual DbSet<ps_image_types> ps_image_types { get; set; }
        public virtual DbSet<ps_images> ps_images { get; set; }
        public virtual DbSet<ps_modules> ps_modules { get; set; }
        public virtual DbSet<ps_screens> ps_screens { get; set; }
        public virtual DbSet<ps_settings> ps_settings { get; set; }
        public virtual DbSet<ps_table_extended_fields> ps_table_extended_fields { get; set; }
        public virtual DbSet<ps_tables> ps_tables { get; set; }
        public virtual DbSet<ps_third_party_options> ps_third_party_options { get; set; }
        public virtual DbSet<ps_third_party_services> ps_third_party_services { get; set; }
        public virtual DbSet<ps_unit_of_measure_labels> ps_unit_of_measure_labels { get; set; }
        public virtual DbSet<ps_unit_of_measures> ps_unit_of_measures { get; set; }
        public virtual DbSet<rv_history> rv_history { get; set; }
        public virtual DbSet<rv_pending_items> rv_pending_items { get; set; }
        public virtual DbSet<rv_receiving_types> rv_receiving_types { get; set; }
        public virtual DbSet<sc_appointment_resources> sc_appointment_resources { get; set; }
        public virtual DbSet<sc_appointments> sc_appointments { get; set; }
        public virtual DbSet<sc_calendar_appointments> sc_calendar_appointments { get; set; }
        public virtual DbSet<sc_calendar_exceptions> sc_calendar_exceptions { get; set; }
        public virtual DbSet<sc_calendars> sc_calendars { get; set; }
        public virtual DbSet<sc_categories> sc_categories { get; set; }
        public virtual DbSet<sc_custom_sorts> sc_custom_sorts { get; set; }
        public virtual DbSet<sc_exception_occurrences> sc_exception_occurrences { get; set; }
        public virtual DbSet<sc_exception_resources> sc_exception_resources { get; set; }
        public virtual DbSet<sc_exceptions> sc_exceptions { get; set; }
        public virtual DbSet<sc_resource_types> sc_resource_types { get; set; }
        public virtual DbSet<sc_resources> sc_resources { get; set; }
        public virtual DbSet<sc_sorts> sc_sorts { get; set; }
        public virtual DbSet<sc_time_markers> sc_time_markers { get; set; }
        public virtual DbSet<sc_work_order_schedules> sc_work_order_schedules { get; set; }
        public virtual DbSet<sca_dynamic_condition> sca_dynamic_condition { get; set; }
        public virtual DbSet<sca_dynamic_fields> sca_dynamic_fields { get; set; }
        public virtual DbSet<sca_dynamic_form> sca_dynamic_form { get; set; }
        public virtual DbSet<sca_dynamic_form_containers> sca_dynamic_form_containers { get; set; }
        public virtual DbSet<sca_main_menu> sca_main_menu { get; set; }
        public virtual DbSet<sf_ae_work_center_groupings> sf_ae_work_center_groupings { get; set; }
        public virtual DbSet<sf_bom_substitutions> sf_bom_substitutions { get; set; }
        public virtual DbSet<sf_downtimes> sf_downtimes { get; set; }
        public virtual DbSet<sf_groups> sf_groups { get; set; }
        public virtual DbSet<sf_job_transactions> sf_job_transactions { get; set; }
        public virtual DbSet<sf_machine_types> sf_machine_types { get; set; }
        public virtual DbSet<sf_machine_work_orders> sf_machine_work_orders { get; set; }
        public virtual DbSet<sf_node_groups> sf_node_groups { get; set; }
        public virtual DbSet<sf_node_properties> sf_node_properties { get; set; }
        public virtual DbSet<sf_node_settings> sf_node_settings { get; set; }
        public virtual DbSet<sf_node_status_transactions> sf_node_status_transactions { get; set; }
        public virtual DbSet<sf_node_statuses> sf_node_statuses { get; set; }
        public virtual DbSet<sf_nodes> sf_nodes { get; set; }
        public virtual DbSet<sf_opc_values> sf_opc_values { get; set; }
        public virtual DbSet<sf_operator_shifts> sf_operator_shifts { get; set; }
        public virtual DbSet<sf_processes> sf_processes { get; set; }
        public virtual DbSet<sf_processes_subdowntime_sets> sf_processes_subdowntime_sets { get; set; }
        public virtual DbSet<sf_sets> sf_sets { get; set; }
        public virtual DbSet<sf_shift_details_temp> sf_shift_details_temp { get; set; }
        public virtual DbSet<sf_shifts> sf_shifts { get; set; }
        public virtual DbSet<sf_subdowntimes> sf_subdowntimes { get; set; }
        public virtual DbSet<sf_theoreticals> sf_theoreticals { get; set; }
        public virtual DbSet<sf_time_records> sf_time_records { get; set; }
        public virtual DbSet<sf_time_segments> sf_time_segments { get; set; }
        public virtual DbSet<sf_transaction_masters> sf_transaction_masters { get; set; }
        public virtual DbSet<sf_transactions> sf_transactions { get; set; }
        public virtual DbSet<sf_vectors> sf_vectors { get; set; }
        public virtual DbSet<sf_waste_transactions> sf_waste_transactions { get; set; }
        public virtual DbSet<sf_waste_types> sf_waste_types { get; set; }
        public virtual DbSet<sf_work_center_bom_attributes> sf_work_center_bom_attributes { get; set; }
        public virtual DbSet<sf_work_center_groups> sf_work_center_groups { get; set; }
        public virtual DbSet<sf_work_centers> sf_work_centers { get; set; }
        public virtual DbSet<sh_carriers> sh_carriers { get; set; }
        public virtual DbSet<sh_freight_loads> sh_freight_loads { get; set; }
        public virtual DbSet<sh_shipment_appoval> sh_shipment_appoval { get; set; }
        public virtual DbSet<sh_shipments> sh_shipments { get; set; }
        public virtual DbSet<sh_shipped_lot_pallets> sh_shipped_lot_pallets { get; set; }
        public virtual DbSet<sh_statuses> sh_statuses { get; set; }
        public virtual DbSet<sh_trailer_freight_types> sh_trailer_freight_types { get; set; }
        public virtual DbSet<sh_trailer_load_types> sh_trailer_load_types { get; set; }
        public virtual DbSet<sh_transit_days> sh_transit_days { get; set; }
        public virtual DbSet<sh_zones> sh_zones { get; set; }
        public virtual DbSet<so_accounts> so_accounts { get; set; }
        public virtual DbSet<so_accounts_children> so_accounts_children { get; set; }
        public virtual DbSet<so_additional_charge_categories> so_additional_charge_categories { get; set; }
        public virtual DbSet<so_additional_charges> so_additional_charges { get; set; }
        public virtual DbSet<so_associated_charges> so_associated_charges { get; set; }
        public virtual DbSet<so_charges> so_charges { get; set; }
        public virtual DbSet<so_children> so_children { get; set; }
        public virtual DbSet<so_children_images> so_children_images { get; set; }
        public virtual DbSet<so_children_packaging> so_children_packaging { get; set; }
        public virtual DbSet<so_children_service_settings> so_children_service_settings { get; set; }
        public virtual DbSet<so_customer_blanketpos> so_customer_blanketpos { get; set; }
        public virtual DbSet<so_reps_territories> so_reps_territories { get; set; }
        public virtual DbSet<so_sales_orders> so_sales_orders { get; set; }
        public virtual DbSet<so_sales_reps> so_sales_reps { get; set; }
        public virtual DbSet<so_statuses> so_statuses { get; set; }
        public virtual DbSet<so_territories> so_territories { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<sysreplserver> sysreplservers { get; set; }
        public virtual DbSet<ut_SSRS_log> ut_SSRS_log { get; set; }
        public virtual DbSet<wo_approval_groups> wo_approval_groups { get; set; }
        public virtual DbSet<wo_approval_settings> wo_approval_settings { get; set; }
        public virtual DbSet<wo_approval_statuses> wo_approval_statuses { get; set; }
        public virtual DbSet<wo_approval_templates> wo_approval_templates { get; set; }
        public virtual DbSet<wo_blanket_release_uom> wo_blanket_release_uom { get; set; }
        public virtual DbSet<wo_comments> wo_comments { get; set; }
        public virtual DbSet<wo_job_classes> wo_job_classes { get; set; }
        public virtual DbSet<wo_master> wo_master { get; set; }
        public virtual DbSet<wo_notes> wo_notes { get; set; }
        public virtual DbSet<wo_order_types> wo_order_types { get; set; }
        public virtual DbSet<wo_side_by_sides> wo_side_by_sides { get; set; }
        public virtual DbSet<wo_statuses> wo_statuses { get; set; }
        public virtual DbSet<wo_step_attributes> wo_step_attributes { get; set; }
        public virtual DbSet<wo_type_job_classes> wo_type_job_classes { get; set; }
        public virtual DbSet<C_MWV_subcontract_orders> C_MWV_subcontract_orders { get; set; }
        public virtual DbSet<bom_rules> bom_rules { get; set; }
        public virtual DbSet<cs_global_counter_objects> cs_global_counter_objects { get; set; }
        public virtual DbSet<edi_documents> edi_documents { get; set; }
        public virtual DbSet<edi_x12_identifiers> edi_x12_identifiers { get; set; }
        public virtual DbSet<edi_x12_qualifiers> edi_x12_qualifiers { get; set; }
        public virtual DbSet<lbl_temp_print_list> lbl_temp_print_list { get; set; }
        public virtual DbSet<MSpeer_conflictdetectionconfigresponse> MSpeer_conflictdetectionconfigresponse { get; set; }
        public virtual DbSet<MSpeer_originatorid_history> MSpeer_originatorid_history { get; set; }
        public virtual DbSet<MSpeer_request> MSpeer_request { get; set; }
        public virtual DbSet<MSpeer_response> MSpeer_response { get; set; }
        public virtual DbSet<MSpeer_topologyrequest> MSpeer_topologyrequest { get; set; }
        public virtual DbSet<MSpeer_topologyresponse> MSpeer_topologyresponse { get; set; }
        public virtual DbSet<MSpub_identity_range> MSpub_identity_range { get; set; }
        public virtual DbSet<sc_bom_schedule> sc_bom_schedule { get; set; }
        public virtual DbSet<sc_schedule_columns> sc_schedule_columns { get; set; }
        public virtual DbSet<sc_schedule_entry_types> sc_schedule_entry_types { get; set; }
        public virtual DbSet<sc_schedule_statuses> sc_schedule_statuses { get; set; }
        public virtual DbSet<sf_additional_operators> sf_additional_operators { get; set; }
        public virtual DbSet<sf_custom_accumulator_info> sf_custom_accumulator_info { get; set; }
        public virtual DbSet<sf_custom_accumulator_values> sf_custom_accumulator_values { get; set; }
        public virtual DbSet<sf_custom_accumulators> sf_custom_accumulators { get; set; }
        public virtual DbSet<sf_opc_fields> sf_opc_fields { get; set; }
        public virtual DbSet<so_order_categories> so_order_categories { get; set; }
        public virtual DbSet<sysarticlecolumn> sysarticlecolumns { get; set; }
        public virtual DbSet<sysarticle> sysarticles { get; set; }
        public virtual DbSet<sysarticleupdate> sysarticleupdates { get; set; }
        public virtual DbSet<syspublication> syspublications { get; set; }
        public virtual DbSet<sysschemaarticle> sysschemaarticles { get; set; }
        public virtual DbSet<syssubscription> syssubscriptions { get; set; }
        public virtual DbSet<systranschema> systranschemas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<C_message_config>()
                .Property(e => e.Language)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_config>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_config>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_config>()
                .Property(e => e.PixelMode)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_config>()
                .Property(e => e.Orientation)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_fields>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_fields>()
                .Property(e => e.DateType)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_fields>()
                .Property(e => e.Format)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_fields>()
                .Property(e => e.Language)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_fields>()
                .Property(e => e.Text)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_fields>()
                .Property(e => e.Orientation)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_fields>()
                .Property(e => e.DateDelimiter)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_fields>()
                .Property(e => e.TimeDelimiter)
                .IsUnicode(false);

            modelBuilder.Entity<C_message_shifts>()
                .Property(e => e.Text)
                .IsUnicode(false);

            modelBuilder.Entity<C_sp_param_tracking>()
                .Property(e => e.sp_name)
                .IsUnicode(false);

            modelBuilder.Entity<C_sp_param_tracking>()
                .Property(e => e.parm_name)
                .IsUnicode(false);

            modelBuilder.Entity<C_sp_param_tracking>()
                .Property(e => e.parm_value)
                .IsUnicode(false);

            modelBuilder.Entity<ad_application_message_type>()
                .Property(e => e.message_type)
                .IsUnicode(false);

            modelBuilder.Entity<ad_application_message_type>()
                .HasMany(e => e.ad_application_messages)
                .WithRequired(e => e.ad_application_message_type)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_application_messages>()
                .Property(e => e.application_id)
                .IsUnicode(false);

            modelBuilder.Entity<ad_application_messages>()
                .Property(e => e.message)
                .IsUnicode(false);

            modelBuilder.Entity<ad_audit>()
                .Property(e => e.app_name)
                .IsUnicode(false);

            modelBuilder.Entity<ad_audit>()
                .Property(e => e.node_name)
                .IsUnicode(false);

            modelBuilder.Entity<ad_audit>()
                .Property(e => e.user_name)
                .IsUnicode(false);

            modelBuilder.Entity<ad_audit>()
                .Property(e => e.table_name)
                .IsUnicode(false);

            modelBuilder.Entity<ad_audit>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<ad_audit>()
                .Property(e => e.old_value)
                .IsUnicode(false);

            modelBuilder.Entity<ad_audit>()
                .Property(e => e.new_value)
                .IsUnicode(false);

            modelBuilder.Entity<ad_comment_types>()
                .Property(e => e.comment_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<ad_comment_types>()
                .HasMany(e => e.ad_comments)
                .WithRequired(e => e.ad_comment_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_comment_types>()
                .HasMany(e => e.ad_predefined_comments)
                .WithRequired(e => e.ad_comment_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_comments>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<ad_comments>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<ad_languages>()
                .Property(e => e.language)
                .IsUnicode(false);

            modelBuilder.Entity<ad_languages>()
                .HasMany(e => e.ad_translations)
                .WithRequired(e => e.ad_languages)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_languages>()
                .HasMany(e => e.ad_users)
                .WithRequired(e => e.ad_languages)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_languages>()
                .HasMany(e => e.cs_countries)
                .WithRequired(e => e.ad_languages)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_predefined_comments>()
                .Property(e => e.predefined_comment_description)
                .IsUnicode(false);

            modelBuilder.Entity<ad_security_groups>()
                .Property(e => e.group_name)
                .IsUnicode(false);

            modelBuilder.Entity<ad_security_groups>()
                .HasMany(e => e.ad_security_modules)
                .WithRequired(e => e.ad_security_groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_security_groups>()
                .HasMany(e => e.wo_approval_groups)
                .WithRequired(e => e.ad_security_groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_security_groups>()
                .HasMany(e => e.ad_users3)
                .WithMany(e => e.ad_security_groups3)
                .Map(m => m.ToTable("ad_user_groups").MapLeftKey("security_group_id").MapRightKey("user_id"));

            modelBuilder.Entity<ad_security_modules>()
                .HasMany(e => e.ad_security_screens)
                .WithRequired(e => e.ad_security_modules)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_security_screens>()
                .HasMany(e => e.ad_security_controls)
                .WithRequired(e => e.ad_security_screens)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_translations>()
                .Property(e => e.translation)
                .IsUnicode(false);

            modelBuilder.Entity<ad_users>()
                .Property(e => e.alt_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<ad_users>()
                .Property(e => e.first_name)
                .IsUnicode(false);

            modelBuilder.Entity<ad_users>()
                .Property(e => e.last_name)
                .IsUnicode(false);

            modelBuilder.Entity<ad_users>()
                .Property(e => e.USER_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<ad_users>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<ad_users>()
                .Property(e => e.user_email)
                .IsUnicode(false);

            modelBuilder.Entity<ad_users>()
                .Property(e => e.version_stamp)
                .IsFixedLength();

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ad_security_groups)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ad_security_groups1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ad_security_groups2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ad_user_buildings)
                .WithRequired(e => e.ad_users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ad_user_work_center_associations)
                .WithRequired(e => e.ad_users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ad_users1)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ad_users11)
                .WithRequired(e => e.ad_users3)
                .HasForeignKey(e => e.created_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ad_users12)
                .WithRequired(e => e.ad_users4)
                .HasForeignKey(e => e.last_modified_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ap_po_children)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ap_po_children1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ap_po_master)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ap_po_master1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_addresses)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_addresses1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_addresses2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_buildings)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_buildings1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_buildings2)
                .WithRequired(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_companies)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_companies1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_companies2)
                .WithRequired(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_custom_field_values)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_custom_field_values1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_custom_field_values2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_custom_fields)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_custom_fields1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_custom_fields2)
                .WithRequired(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customer_contacts)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customer_contacts1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customer_contacts2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customer_manufacturing_notes)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customer_manufacturing_notes1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customer_notes)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customer_third_party_settings)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customer_third_party_settings1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customer_third_party_settings2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customers)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customers1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_customers2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_grid_conditionals)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_grid_conditionals1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_grid_conditionals2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_locations)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_locations1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.cs_locations2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_customer_items)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_customer_items1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_dim_groups)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_dim_groups1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_item_categories)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_item_categories1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_item_children)
                .WithOptional(e => e.ad_users)
                .HasForeignKey(e => e.created_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_item_children1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_item_children2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_item_master)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_item_master1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_item_master2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ic_physical_inventory_history)
                .WithRequired(e => e.ad_users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.pac_templates)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.pac_templates1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ps_third_party_options)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ps_third_party_options1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ps_third_party_options2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ps_third_party_services)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ps_third_party_services1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ps_third_party_services2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sf_bom_substitutions)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sf_bom_substitutions1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sf_bom_substitutions2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sf_nodes)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sf_nodes1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sf_nodes2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sf_work_centers)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sf_work_centers1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sf_work_centers2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sh_carriers)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sh_carriers1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sh_carriers2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sh_freight_loads)
                .WithOptional(e => e.ad_users)
                .HasForeignKey(e => e.created_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sh_freight_loads1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sh_shipments)
                .WithOptional(e => e.ad_users)
                .HasForeignKey(e => e.created_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.sh_shipments1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_associated_charges)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_associated_charges1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_associated_charges2)
                .WithRequired(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_charges)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_charges1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_charges2)
                .WithRequired(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_children)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_children1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_children_service_settings)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_children_service_settings1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_children_service_settings2)
                .WithRequired(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_sales_orders)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.so_sales_orders1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.ut_SSRS_log)
                .WithRequired(e => e.ad_users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_groups)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_groups1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_groups2)
                .WithRequired(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_settings)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_settings1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_settings2)
                .WithRequired(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_statuses)
                .WithOptional(e => e.ad_users)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_statuses1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_statuses2)
                .WithRequired(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_templates)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_templates1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_approval_templates2)
                .WithRequired(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_master)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_master1)
                .WithRequired(e => e.ad_users1)
                .HasForeignKey(e => e.last_modified_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_notes)
                .WithRequired(e => e.ad_users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_side_by_sides)
                .WithRequired(e => e.ad_users)
                .HasForeignKey(e => e.created_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_side_by_sides1)
                .WithOptional(e => e.ad_users1)
                .HasForeignKey(e => e.deleted_by);

            modelBuilder.Entity<ad_users>()
                .HasMany(e => e.wo_side_by_sides2)
                .WithOptional(e => e.ad_users2)
                .HasForeignKey(e => e.last_modified_by);

            modelBuilder.Entity<ap_delivery_terms>()
                .Property(e => e.delivery_term)
                .IsUnicode(false);

            modelBuilder.Entity<ap_delivery_terms>()
                .HasMany(e => e.ap_po_master)
                .WithRequired(e => e.ap_delivery_terms)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ap_delivery_terms>()
                .HasMany(e => e.so_children)
                .WithOptional(e => e.ap_delivery_terms)
                .HasForeignKey(e => e.freight_terms_id);

            modelBuilder.Entity<ap_payment_terms>()
                .Property(e => e.payment_term)
                .IsUnicode(false);

            modelBuilder.Entity<ap_payment_terms>()
                .HasMany(e => e.ap_po_master)
                .WithRequired(e => e.ap_payment_terms)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ap_payment_terms>()
                .HasMany(e => e.cs_customers)
                .WithRequired(e => e.ap_payment_terms)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ap_payment_terms>()
                .HasMany(e => e.so_children)
                .WithOptional(e => e.ap_payment_terms)
                .HasForeignKey(e => e.shipment_payment_id);

            modelBuilder.Entity<ap_po_children>()
                .Property(e => e.order_qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ap_po_children>()
                .Property(e => e.order_uom)
                .IsUnicode(false);

            modelBuilder.Entity<ap_po_children>()
                .Property(e => e.stock_uom_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ap_po_children>()
                .Property(e => e.stock_uom_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ap_po_children>()
                .Property(e => e.stock_uom_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ap_po_children>()
                .Property(e => e.discount)
                .HasPrecision(22, 2);

            modelBuilder.Entity<ap_po_master>()
                .Property(e => e.po_number)
                .IsUnicode(false);

            modelBuilder.Entity<ap_po_master>()
                .Property(e => e.work_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<ap_po_master>()
                .Property(e => e.tax_rate)
                .HasPrecision(6, 2);

            modelBuilder.Entity<ap_po_master>()
                .Property(e => e.subtotal)
                .HasPrecision(22, 2);

            modelBuilder.Entity<ap_po_master>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<ap_po_master>()
                .Property(e => e.total_tax)
                .HasPrecision(22, 2);

            modelBuilder.Entity<ap_po_master>()
                .Property(e => e.total_discount)
                .HasPrecision(22, 2);

            modelBuilder.Entity<ap_po_master>()
                .HasMany(e => e.ap_po_children)
                .WithRequired(e => e.ap_po_master)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ap_ship_types>()
                .Property(e => e.ship_type)
                .IsUnicode(false);

            modelBuilder.Entity<ap_ship_types>()
                .HasMany(e => e.cs_customers)
                .WithRequired(e => e.ap_ship_types)
                .HasForeignKey(e => e.delivery_term_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ap_ship_types>()
                .HasMany(e => e.so_children)
                .WithOptional(e => e.ap_ship_types)
                .HasForeignKey(e => e.shipment_type_id);

            modelBuilder.Entity<ar_invoices>()
                .Property(e => e.invoice_no)
                .IsUnicode(false);

            modelBuilder.Entity<ar_invoices>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<ar_invoices>()
                .HasMany(e => e.ar_payments_received)
                .WithRequired(e => e.ar_invoices)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ar_payment_types>()
                .Property(e => e.payment_type)
                .IsUnicode(false);

            modelBuilder.Entity<ar_payment_types>()
                .HasMany(e => e.ar_payments_received)
                .WithRequired(e => e.ar_payment_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ar_payments_received>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<ar_payments_received>()
                .Property(e => e.invoice_no)
                .IsUnicode(false);

            modelBuilder.Entity<bom_produced_items>()
                .Property(e => e.generic_type_item_name)
                .IsUnicode(false);

            modelBuilder.Entity<bom_produced_items>()
                .Property(e => e.quantity)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_produced_items>()
                .Property(e => e.uom)
                .IsUnicode(false);

            modelBuilder.Entity<bom_produced_items>()
                .HasMany(e => e.bom_required_materials)
                .WithOptional(e => e.bom_produced_items)
                .HasForeignKey(e => e.based_on_produced_item_id);

            modelBuilder.Entity<bom_produced_items>()
                .HasMany(e => e.bom_route_produced_items)
                .WithRequired(e => e.bom_produced_items)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_required_materials>()
                .Property(e => e.generic_type_item_name)
                .IsUnicode(false);

            modelBuilder.Entity<bom_required_materials>()
                .Property(e => e.quantity)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_required_materials>()
                .Property(e => e.uom)
                .IsUnicode(false);

            modelBuilder.Entity<bom_required_materials>()
                .Property(e => e.scrap)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_required_materials>()
                .Property(e => e.cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<bom_required_materials>()
                .HasMany(e => e.bom_route_required_materials)
                .WithRequired(e => e.bom_required_materials)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_required_materials>()
                .HasMany(e => e.bom_subassemblies)
                .WithRequired(e => e.bom_required_materials)
                .HasForeignKey(e => e.parent_required_material_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_route_item_transactions>()
                .Property(e => e.qty_completed_change)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_item_transactions>()
                .Property(e => e.qty_scrap_change)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_material_transactions>()
                .Property(e => e.qty_on_hand_change)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_material_transactions>()
                .Property(e => e.qty_completed_change)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_material_transactions>()
                .Property(e => e.qty_scrap_change)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_produced_items>()
                .Property(e => e.generic_type_item_name)
                .IsUnicode(false);

            modelBuilder.Entity<bom_route_produced_items>()
                .Property(e => e.ordered_uom)
                .IsUnicode(false);

            modelBuilder.Entity<bom_route_produced_items>()
                .Property(e => e.quantity_ordered)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_produced_items>()
                .Property(e => e.quantity_completed)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_produced_items>()
                .Property(e => e.quantity_scrapped)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_produced_items>()
                .Property(e => e.transaction_option)
                .IsUnicode(false);

            modelBuilder.Entity<bom_route_produced_items>()
                .HasMany(e => e.bom_route_item_transactions)
                .WithRequired(e => e.bom_route_produced_items)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_route_required_materials>()
                .Property(e => e.generic_type_item_name)
                .IsUnicode(false);

            modelBuilder.Entity<bom_route_required_materials>()
                .Property(e => e.ordered_uom)
                .IsUnicode(false);

            modelBuilder.Entity<bom_route_required_materials>()
                .Property(e => e.expected_scrap)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_required_materials>()
                .Property(e => e.quantity_ordered)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_required_materials>()
                .Property(e => e.quantity_on_hand)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_required_materials>()
                .Property(e => e.quantity_completed)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_required_materials>()
                .Property(e => e.quantity_scrapped)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_route_required_materials>()
                .HasMany(e => e.bom_route_material_transactions)
                .WithRequired(e => e.bom_route_required_materials)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_route_required_materials>()
                .HasMany(e => e.bom_subassembly_hierarchies)
                .WithRequired(e => e.bom_route_required_materials)
                .HasForeignKey(e => e.parent_route_required_material_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_route_statuses>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<bom_route_statuses>()
                .Property(e => e.status_description)
                .IsUnicode(false);

            modelBuilder.Entity<bom_route_statuses>()
                .HasMany(e => e.bom_route_steps)
                .WithRequired(e => e.bom_route_statuses)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_route_steps>()
                .Property(e => e.step_note)
                .IsUnicode(false);

            modelBuilder.Entity<bom_route_steps>()
                .HasMany(e => e.bom_route_produced_items)
                .WithRequired(e => e.bom_route_steps)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_route_steps>()
                .HasMany(e => e.bom_route_required_materials)
                .WithRequired(e => e.bom_route_steps)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_route_steps>()
                .HasMany(e => e.bom_subassembly_hierarchies)
                .WithRequired(e => e.bom_route_steps)
                .HasForeignKey(e => e.parent_route_step_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_route_steps>()
                .HasMany(e => e.wo_step_attributes)
                .WithRequired(e => e.bom_route_steps)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_step_attribute_types>()
                .Property(e => e.attribute_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<bom_step_attribute_types>()
                .HasMany(e => e.sf_work_center_bom_attributes)
                .WithRequired(e => e.bom_step_attribute_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_step_options>()
                .Property(e => e.display_as)
                .IsUnicode(false);

            modelBuilder.Entity<bom_step_options>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<bom_step_options>()
                .HasMany(e => e.bom_steps)
                .WithRequired(e => e.bom_step_options)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_step_rules>()
                .Property(e => e.pattern)
                .IsUnicode(false);

            modelBuilder.Entity<bom_step_rules>()
                .Property(e => e.step)
                .IsUnicode(false);

            modelBuilder.Entity<bom_step_rules>()
                .Property(e => e.selector)
                .IsUnicode(false);

            modelBuilder.Entity<bom_step_rules>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<bom_step_rules>()
                .Property(e => e.filter)
                .IsUnicode(false);

            modelBuilder.Entity<bom_step_rules>()
                .Property(e => e.selected_work_centers)
                .IsUnicode(false);

            modelBuilder.Entity<bom_step_rules>()
                .Property(e => e.work_center_selector)
                .IsUnicode(false);

            modelBuilder.Entity<bom_steps>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<bom_steps>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<bom_steps>()
                .HasMany(e => e.bom_produced_items)
                .WithRequired(e => e.bom_steps)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_steps>()
                .HasMany(e => e.bom_required_materials)
                .WithRequired(e => e.bom_steps)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_steps>()
                .HasMany(e => e.bom_route_steps)
                .WithRequired(e => e.bom_steps)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_templates>()
                .Property(e => e.bom_name)
                .IsUnicode(false);

            modelBuilder.Entity<bom_templates>()
                .Property(e => e.bom_description)
                .IsUnicode(false);

            modelBuilder.Entity<bom_templates>()
                .Property(e => e.bom_code)
                .IsUnicode(false);

            modelBuilder.Entity<bom_templates>()
                .Property(e => e.generic_type_item_name)
                .IsUnicode(false);

            modelBuilder.Entity<bom_templates>()
                .Property(e => e.quantity)
                .HasPrecision(18, 4);

            modelBuilder.Entity<bom_templates>()
                .Property(e => e.uom)
                .IsUnicode(false);

            modelBuilder.Entity<bom_templates>()
                .HasMany(e => e.bom_steps)
                .WithRequired(e => e.bom_templates)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_templates>()
                .HasMany(e => e.bom_subassemblies)
                .WithRequired(e => e.bom_templates)
                .HasForeignKey(e => e.child_template_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<bom_templates>()
                .HasMany(e => e.bom_templates1)
                .WithOptional(e => e.bom_templates2)
                .HasForeignKey(e => e.inherits_from_template_id);

            modelBuilder.Entity<bom_transaction_options>()
                .Property(e => e.transaction_option)
                .IsUnicode(false);

            modelBuilder.Entity<bom_transaction_options>()
                .Property(e => e.display)
                .IsUnicode(false);

            modelBuilder.Entity<bom_transaction_options>()
                .HasMany(e => e.bom_produced_items)
                .WithRequired(e => e.bom_transaction_options)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_address_types>()
                .Property(e => e.address_type)
                .IsUnicode(false);

            modelBuilder.Entity<cs_address_types>()
                .HasMany(e => e.cs_customer_addresses)
                .WithRequired(e => e.cs_address_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_addresses>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<cs_addresses>()
                .Property(e => e.address_2)
                .IsUnicode(false);

            modelBuilder.Entity<cs_addresses>()
                .Property(e => e.address_3)
                .IsUnicode(false);

            modelBuilder.Entity<cs_addresses>()
                .Property(e => e.attention)
                .IsUnicode(false);

            modelBuilder.Entity<cs_addresses>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<cs_addresses>()
                .Property(e => e.state)
                .IsUnicode(false);

            modelBuilder.Entity<cs_addresses>()
                .Property(e => e.zip_code)
                .IsUnicode(false);

            modelBuilder.Entity<cs_addresses>()
                .Property(e => e.zip_code_suffix)
                .IsUnicode(false);

            modelBuilder.Entity<cs_addresses>()
                .HasMany(e => e.cs_buildings)
                .WithOptional(e => e.cs_addresses)
                .HasForeignKey(e => e.building_address_id);

            modelBuilder.Entity<cs_addresses>()
                .HasMany(e => e.cs_companies)
                .WithOptional(e => e.cs_addresses)
                .HasForeignKey(e => e.company_address_id);

            modelBuilder.Entity<cs_addresses>()
                .HasMany(e => e.cs_customer_addresses)
                .WithRequired(e => e.cs_addresses)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_addresses>()
                .HasMany(e => e.cs_customer_manufacturing_notes)
                .WithRequired(e => e.cs_addresses)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_addresses>()
                .HasMany(e => e.cs_customer_work_group_notes)
                .WithRequired(e => e.cs_addresses)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_addresses>()
                .HasMany(e => e.sh_carriers)
                .WithRequired(e => e.cs_addresses)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_addresses>()
                .HasMany(e => e.sh_transit_days)
                .WithRequired(e => e.cs_addresses)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_addresses>()
                .HasMany(e => e.so_children)
                .WithOptional(e => e.cs_addresses)
                .HasForeignKey(e => e.shipto_address_id);

            modelBuilder.Entity<cs_buildings>()
                .Property(e => e.building_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_buildings>()
                .Property(e => e.building_desc)
                .IsUnicode(false);

            modelBuilder.Entity<cs_buildings>()
                .HasMany(e => e.cs_locations)
                .WithRequired(e => e.cs_buildings)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_buildings>()
                .HasMany(e => e.sf_work_centers)
                .WithRequired(e => e.cs_buildings)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_buildings>()
                .HasMany(e => e.sh_transit_days)
                .WithRequired(e => e.cs_buildings)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_buildings>()
                .HasMany(e => e.so_sales_orders)
                .WithRequired(e => e.cs_buildings)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_buildings>()
                .HasMany(e => e.wo_master)
                .WithRequired(e => e.cs_buildings)
                .HasForeignKey(e => e.mfg_building_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_business_units>()
                .Property(e => e.business_unit)
                .IsUnicode(false);

            modelBuilder.Entity<cs_business_units>()
                .Property(e => e.gl_acct_id)
                .IsUnicode(false);

            modelBuilder.Entity<cs_carrier_contacts>()
                .Property(e => e.first_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_carrier_contacts>()
                .Property(e => e.last_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_carrier_contacts>()
                .Property(e => e.work_phone)
                .IsUnicode(false);

            modelBuilder.Entity<cs_carrier_contacts>()
                .Property(e => e.mobile_phone)
                .IsUnicode(false);

            modelBuilder.Entity<cs_carrier_contacts>()
                .Property(e => e.fax_phone)
                .IsUnicode(false);

            modelBuilder.Entity<cs_carrier_contacts>()
                .Property(e => e.contact_email)
                .IsUnicode(false);

            modelBuilder.Entity<cs_carrier_contacts>()
                .Property(e => e.contact_title)
                .IsUnicode(false);

            modelBuilder.Entity<cs_carrier_contacts>()
                .Property(e => e.contact_dept)
                .IsUnicode(false);

            modelBuilder.Entity<cs_carrier_notes>()
                .Property(e => e.carrier_note)
                .IsUnicode(false);

            modelBuilder.Entity<cs_companies>()
                .Property(e => e.company_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_companies>()
                .Property(e => e.company_desc)
                .IsUnicode(false);

            modelBuilder.Entity<cs_companies>()
                .Property(e => e.dun)
                .IsUnicode(false);

            modelBuilder.Entity<cs_companies>()
                .HasMany(e => e.cs_buildings)
                .WithRequired(e => e.cs_companies)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_countries>()
                .Property(e => e.country_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_countries>()
                .Property(e => e.country_abbr)
                .IsUnicode(false);

            modelBuilder.Entity<cs_countries>()
                .Property(e => e.postcode_scheme)
                .IsUnicode(false);

            modelBuilder.Entity<cs_countries>()
                .Property(e => e.postcode_example)
                .IsUnicode(false);

            modelBuilder.Entity<cs_credit_holds>()
                .Property(e => e.credit_hold)
                .IsUnicode(false);

            modelBuilder.Entity<cs_credit_holds>()
                .HasMany(e => e.cs_customers)
                .WithRequired(e => e.cs_credit_holds)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_custom_field_formulas>()
                .Property(e => e.formula_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_custom_field_formulas>()
                .Property(e => e.formula_expr)
                .IsUnicode(false);

            modelBuilder.Entity<cs_custom_field_properties>()
                .Property(e => e.property_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_custom_field_properties>()
                .Property(e => e.property_type)
                .IsUnicode(false);

            modelBuilder.Entity<cs_custom_field_properties>()
                .Property(e => e.property_value)
                .IsUnicode(false);

            modelBuilder.Entity<cs_custom_field_rules>()
                .Property(e => e.object_field_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_custom_field_rules>()
                .Property(e => e.object_field_value)
                .IsUnicode(false);

            modelBuilder.Entity<cs_custom_field_values>()
                .Property(e => e.field_value)
                .IsUnicode(false);

            modelBuilder.Entity<cs_custom_fields>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_custom_fields>()
                .HasMany(e => e.cs_custom_field_properties)
                .WithRequired(e => e.cs_custom_fields)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_custom_fields>()
                .HasMany(e => e.cs_custom_field_values)
                .WithRequired(e => e.cs_custom_fields)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_custom_fields>()
                .HasMany(e => e.sf_work_center_bom_attributes)
                .WithRequired(e => e.cs_custom_fields)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customer_association>()
                .Property(e => e.alt_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_contacts>()
                .Property(e => e.first_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_contacts>()
                .Property(e => e.last_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_contacts>()
                .Property(e => e.work_phone)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_contacts>()
                .Property(e => e.mobile_phone)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_contacts>()
                .Property(e => e.fax_phone)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_contacts>()
                .Property(e => e.contact_email)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_contacts>()
                .Property(e => e.contact_title)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_contacts>()
                .Property(e => e.contact_dept)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_edis>()
                .Property(e => e.file_desc)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_files>()
                .Property(e => e.file_note)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_manufacturing_notes>()
                .Property(e => e.customer_manufacturing_note)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_notes>()
                .Property(e => e.customer_note)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_ranks>()
                .Property(e => e.customer_rank)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_ranks>()
                .HasMany(e => e.cs_customers)
                .WithRequired(e => e.cs_customer_ranks)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customer_third_party_settings>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customer_work_group_notes>()
                .Property(e => e.customer_work_group_note)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.customer_code)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.customer_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.credit_limit)
                .HasPrecision(18, 4);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.markup_percent)
                .HasPrecision(5, 2);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.admin_fee)
                .HasPrecision(18, 4);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.tax_id)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.dun)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.ftp_link)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.ftp_username)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.ftp_password)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.fax)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.discount_percent)
                .HasPrecision(5, 2);

            modelBuilder.Entity<cs_customers>()
                .Property(e => e.tax_percent)
                .HasPrecision(5, 4);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.ap_po_master)
                .WithRequired(e => e.cs_customers)
                .HasForeignKey(e => e.bill_to_customer_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.cs_customer_addresses)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.cs_customer_association)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.cs_customer_contacts)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.cs_customer_edis)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.cs_customer_files)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.cs_customer_manufacturing_notes)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.cs_customer_notes)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.cs_customer_third_party_settings)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.cs_customers1)
                .WithRequired(e => e.cs_customers2)
                .HasForeignKey(e => e.parent_id);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.edi_customer_file_formats)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.edi_customer_mappings)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.edi_ftpserver_info)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.edi_mappings)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.ic_customer_items)
                .WithRequired(e => e.cs_customers)
                .HasForeignKey(e => e.customer_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.ic_customer_items1)
                .WithRequired(e => e.cs_customers1)
                .HasForeignKey(e => e.customer_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.ic_physical_inventory_comparison_master)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.rv_pending_items)
                .WithRequired(e => e.cs_customers)
                .HasForeignKey(e => e.customer_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.rv_pending_items1)
                .WithOptional(e => e.cs_customers1)
                .HasForeignKey(e => e.vendor_id);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.so_children)
                .WithOptional(e => e.cs_customers)
                .HasForeignKey(e => e.shipto_customer_id);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.so_customer_blanketpos)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_customers>()
                .HasMany(e => e.so_sales_orders)
                .WithRequired(e => e.cs_customers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_global_counters>()
                .Property(e => e.counter_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_global_counters>()
                .Property(e => e.counter_prefix)
                .IsUnicode(false);

            modelBuilder.Entity<cs_global_counters>()
                .Property(e => e.counter_suffix)
                .IsUnicode(false);

            modelBuilder.Entity<cs_global_counters>()
                .Property(e => e.counter_number)
                .IsUnicode(false);

            modelBuilder.Entity<cs_global_counters>()
                .Property(e => e.lpad_character)
                .IsUnicode(false);

            modelBuilder.Entity<cs_global_counters>()
                .Property(e => e.counter_structure)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_colors>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_colors>()
                .Property(e => e.argb_color)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_condition_types>()
                .Property(e => e.condition_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_condition_types>()
                .HasMany(e => e.cs_grid_filter_fields)
                .WithRequired(e => e.cs_grid_condition_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_grid_conditionals>()
                .Property(e => e.conditional_value)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_conditionals>()
                .Property(e => e.match_regex)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_conditionals>()
                .Property(e => e.replacement)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_control_types>()
                .Property(e => e.control_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_control_types>()
                .HasMany(e => e.cs_grid_filter_fields)
                .WithRequired(e => e.cs_grid_control_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_grid_fields>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_fields>()
                .Property(e => e.grid_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_fields>()
                .Property(e => e.custom_column_sql)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_fields>()
                .Property(e => e.format_string)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_fields>()
                .HasMany(e => e.cs_grid_colors)
                .WithRequired(e => e.cs_grid_fields)
                .WillCascadeOnDelete(false);
                                    

            modelBuilder.Entity<cs_grid_filter_fields>()
                .Property(e => e.control_source)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_filter_fields>()
                .Property(e => e.validation)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_filter_fields>()
                .Property(e => e.validation_text)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_filter_fields>()
                .Property(e => e.default_value)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_filter_fields>()
                .Property(e => e.input_mask)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_filter_fields>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_filter_fields>()
                .Property(e => e.custom_filter)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_filter_fields>()
                .HasMany(e => e.cs_grid_conditionals)
                .WithRequired(e => e.cs_grid_filter_fields)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_grid_user_colors>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_user_colors>()
                .Property(e => e.argb_color)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grid_user_filter_fields>()
                .Property(e => e.default_value)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grids>()
                .Property(e => e.grid_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grids>()
                .Property(e => e.base_query_text)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grids>()
                .Property(e => e.base_sort_text)
                .IsUnicode(false);

            modelBuilder.Entity<cs_grids>()
                .HasMany(e => e.cs_grid_colors)
                .WithRequired(e => e.cs_grids)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_grids>()
                .HasMany(e => e.cs_grid_conditionals)
                .WithRequired(e => e.cs_grids)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_grids>()
                .HasMany(e => e.cs_grid_filter_fields)
                .WithRequired(e => e.cs_grids)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_grids>()
                .HasMany(e => e.cs_grid_fields)
                .WithRequired(e => e.cs_grids)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<cs_location_types>()
                .Property(e => e.location_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_location_types>()
                .Property(e => e.location_type_description)
                .IsUnicode(false);

            modelBuilder.Entity<cs_location_types>()
                .HasMany(e => e.cs_locations)
                .WithRequired(e => e.cs_location_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_locations>()
                .Property(e => e.location_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_locations>()
                .Property(e => e.location_desc)
                .IsUnicode(false);

            modelBuilder.Entity<cs_locations>()
                .HasMany(e => e.ic_physical_inventory)
                .WithRequired(e => e.cs_locations)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_manufacturing_note_types>()
                .Property(e => e.manufacturing_note_type)
                .IsUnicode(false);

            modelBuilder.Entity<cs_manufacturing_note_types>()
                .Property(e => e.manufacturing_note_type_description)
                .IsUnicode(false);

            modelBuilder.Entity<cs_manufacturing_note_types>()
                .HasMany(e => e.cs_customer_manufacturing_notes)
                .WithRequired(e => e.cs_manufacturing_note_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_quality_holds>()
                .Property(e => e.quality_hold)
                .IsUnicode(false);

            modelBuilder.Entity<cs_quality_holds>()
                .HasMany(e => e.cs_customers)
                .WithRequired(e => e.cs_quality_holds)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs_rules>()
                .Property(e => e.rule_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_rules>()
                .Property(e => e.rule_field_name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_rules>()
                .Property(e => e.rule_value)
                .IsUnicode(false);

            modelBuilder.Entity<cs_rules>()
                .Property(e => e.comments)
                .IsUnicode(false);

            modelBuilder.Entity<edi_customer_file_formats>()
                .Property(e => e.format_string)
                .IsUnicode(false);

            modelBuilder.Entity<edi_customer_mapping_conversions>()
                .Property(e => e.document_value)
                .IsUnicode(false);

            modelBuilder.Entity<edi_customer_mapping_conversions>()
                .Property(e => e.column_value)
                .IsUnicode(false);

            modelBuilder.Entity<edi_customer_mappings>()
                .Property(e => e.field_level)
                .IsUnicode(false);

            modelBuilder.Entity<edi_customer_mappings>()
                .Property(e => e.document_field)
                .IsUnicode(false);

            modelBuilder.Entity<edi_customer_mappings>()
                .Property(e => e.table_column)
                .IsUnicode(false);

            modelBuilder.Entity<edi_customer_mappings>()
                .HasMany(e => e.edi_customer_mapping_conversions)
                .WithRequired(e => e.edi_customer_mappings)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<edi_document_configs>()
                .Property(e => e.config)
                .IsUnicode(false);

            modelBuilder.Entity<edi_document_requests>()
                .HasMany(e => e.edi_generation_results)
                .WithRequired(e => e.edi_document_requests)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<edi_document_types>()
                .Property(e => e.version)
                .IsUnicode(false);

            modelBuilder.Entity<edi_document_types>()
                .Property(e => e.document_type)
                .IsUnicode(false);

            modelBuilder.Entity<edi_document_types>()
                .HasMany(e => e.edi_customer_file_formats)
                .WithRequired(e => e.edi_document_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<edi_document_types>()
                .HasMany(e => e.edi_customer_mappings)
                .WithRequired(e => e.edi_document_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<edi_document_types>()
                .HasMany(e => e.edi_document_requests)
                .WithRequired(e => e.edi_document_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<edi_document_types>()
                .HasMany(e => e.edi_parse_results)
                .WithRequired(e => e.edi_document_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<edi_element_values>()
                .Property(e => e.element)
                .IsUnicode(false);

            modelBuilder.Entity<edi_element_values>()
                .Property(e => e.type)
                .IsUnicode(false);

            modelBuilder.Entity<edi_envelope_parse>()
                .Property(e => e.count_segment)
                .IsUnicode(false);

            modelBuilder.Entity<edi_envelopes>()
                .Property(e => e.edi_envelope_name)
                .IsUnicode(false);

            modelBuilder.Entity<edi_envelopes>()
                .Property(e => e.open_id)
                .IsUnicode(false);

            modelBuilder.Entity<edi_envelopes>()
                .Property(e => e.close_id)
                .IsUnicode(false);

            modelBuilder.Entity<edi_error_logs>()
                .Property(e => e.edi_file_name)
                .IsUnicode(false);

            modelBuilder.Entity<edi_error_logs>()
                .Property(e => e.edi_error_message)
                .IsUnicode(false);

            modelBuilder.Entity<edi_error_logs>()
                .Property(e => e.created_by)
                .IsUnicode(false);

            modelBuilder.Entity<edi_error_logs>()
                .Property(e => e.last_modified_by)
                .IsUnicode(false);

            modelBuilder.Entity<edi_error_logs>()
                .Property(e => e.deleted_by)
                .IsUnicode(false);

            modelBuilder.Entity<edi_file_naming_schemes>()
                .Property(e => e.scheme_type)
                .IsUnicode(false);

            modelBuilder.Entity<edi_file_naming_schemes>()
                .HasMany(e => e.edi_customer_file_formats)
                .WithRequired(e => e.edi_file_naming_schemes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<edi_ftpserver_info>()
                .Property(e => e.server_url)
                .IsUnicode(false);

            modelBuilder.Entity<edi_ftpserver_info>()
                .Property(e => e.server_pass)
                .IsUnicode(false);

            modelBuilder.Entity<edi_ftpserver_info>()
                .Property(e => e.server_user)
                .IsUnicode(false);

            modelBuilder.Entity<edi_ftpserver_info>()
                .Property(e => e.server_passive)
                .IsUnicode(false);

            modelBuilder.Entity<edi_generation_default_values>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<edi_generation_results>()
                .Property(e => e.file_name)
                .IsUnicode(false);

            modelBuilder.Entity<edi_generation_results>()
                .Property(e => e.file_path)
                .IsUnicode(false);

            modelBuilder.Entity<edi_generation_results>()
                .Property(e => e.result_category)
                .IsUnicode(false);

            modelBuilder.Entity<edi_generation_results>()
                .Property(e => e.message)
                .IsUnicode(false);

            modelBuilder.Entity<edi_mappings>()
                .Property(e => e.receiving_field_to)
                .IsUnicode(false);

            modelBuilder.Entity<edi_mappings>()
                .Property(e => e.doc_number)
                .IsUnicode(false);

            modelBuilder.Entity<edi_mappings>()
                .Property(e => e.edi_level)
                .IsUnicode(false);

            modelBuilder.Entity<edi_mappings>()
                .Property(e => e.edi_identity)
                .IsUnicode(false);

            modelBuilder.Entity<edi_parse_results>()
                .Property(e => e.file_name)
                .IsUnicode(false);

            modelBuilder.Entity<edi_parse_results>()
                .Property(e => e.file_path)
                .IsUnicode(false);

            modelBuilder.Entity<edi_parse_results>()
                .Property(e => e.result_category)
                .IsUnicode(false);

            modelBuilder.Entity<edi_parse_results>()
                .Property(e => e.message)
                .IsUnicode(false);

            modelBuilder.Entity<edi_regex>()
                .Property(e => e.doc_number)
                .IsUnicode(false);

            modelBuilder.Entity<edi_regex>()
                .Property(e => e.segment)
                .IsUnicode(false);

            modelBuilder.Entity<edi_regex>()
                .Property(e => e.regex)
                .IsUnicode(false);

            modelBuilder.Entity<edi_schemas>()
                .Property(e => e.schema_name)
                .IsUnicode(false);

            modelBuilder.Entity<edi_schemas>()
                .HasMany(e => e.edi_document_types)
                .WithRequired(e => e.edi_schemas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<edi_transaction_mapping>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.interchange_open)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.functional_group_open)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.transaction_set_open)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.transaction_set_close)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.functional_group_close)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.interchange_close)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.error_code_id)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.error_segment_id)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.error_segment)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.error_segment_no)
                .IsUnicode(false);

            modelBuilder.Entity<edi_transactions>()
                .Property(e => e.error_element_no)
                .IsFixedLength();

            modelBuilder.Entity<edi_x12_error_codes>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<edi_x12_error_codes>()
                .Property(e => e.condition)
                .IsUnicode(false);

            modelBuilder.Entity<edi_x12_error_mapping>()
                .Property(e => e.error_id)
                .IsUnicode(false);

            modelBuilder.Entity<edi_x12_errors>()
                .Property(e => e.error_name)
                .IsUnicode(false);

            modelBuilder.Entity<edi_x12_errors>()
                .Property(e => e.error_codes)
                .IsUnicode(false);

            modelBuilder.Entity<edi_x12mapping>()
                .Property(e => e.table_column)
                .IsUnicode(false);

            modelBuilder.Entity<edi_x12mapping>()
                .Property(e => e.type)
                .IsUnicode(false);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.phoenix_unique_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.pallet_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.ref_no)
                .IsUnicode(false);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.stock_uom_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.stock_uom_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.stock_uom_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.total_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.unit_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.unit_cost_uom_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.allocated_wo)
                .IsUnicode(false);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.batch_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.bol_number)
                .IsUnicode(false);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.weight_pounds)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.length_feet)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.original_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.holding_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_consumed_inventory>()
                .Property(e => e.original_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_customer_items>()
                .Property(e => e.customer_item_number)
                .IsUnicode(false);

            modelBuilder.Entity<ic_customer_pallets>()
                .Property(e => e.pallet_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_customer_pallets>()
                .Property(e => e.customers_pallet_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_customer_pallets>()
                .Property(e => e.pallet_weight)
                .HasPrecision(22, 4);

            modelBuilder.Entity<ic_customer_pallets>()
                .Property(e => e.tare_weight)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ic_dim_groups>()
                .Property(e => e.dim_name)
                .IsUnicode(false);

            modelBuilder.Entity<ic_dim_groups>()
                .HasMany(e => e.ic_item_master)
                .WithRequired(e => e.ic_dim_groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_inventory_history>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_inventory_history>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<ic_inventory_images>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_inventory_types>()
                .Property(e => e.inventory_type)
                .IsUnicode(false);

            modelBuilder.Entity<ic_inventory_types>()
                .Property(e => e.stock_uom_1)
                .IsUnicode(false);

            modelBuilder.Entity<ic_inventory_types>()
                .Property(e => e.stock_uom_2)
                .IsUnicode(false);

            modelBuilder.Entity<ic_inventory_types>()
                .Property(e => e.stock_uom_weight)
                .IsUnicode(false);

            modelBuilder.Entity<ic_inventory_types>()
                .Property(e => e.nmfc_class)
                .IsUnicode(false);

            modelBuilder.Entity<ic_inventory_types>()
                .HasMany(e => e.bom_produced_items)
                .WithRequired(e => e.ic_inventory_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_inventory_types>()
                .HasMany(e => e.bom_required_materials)
                .WithRequired(e => e.ic_inventory_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_inventory_types>()
                .HasMany(e => e.bom_route_produced_items)
                .WithRequired(e => e.ic_inventory_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_inventory_types>()
                .HasMany(e => e.bom_route_required_materials)
                .WithRequired(e => e.ic_inventory_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_inventory_types>()
                .HasMany(e => e.bom_templates)
                .WithRequired(e => e.ic_inventory_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_inventory_types>()
                .HasMany(e => e.ic_item_master)
                .WithRequired(e => e.ic_inventory_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_inventory_types>()
                .HasMany(e => e.ic_physical_inventory_comparison_master)
                .WithRequired(e => e.ic_inventory_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_categories>()
                .Property(e => e.item_category)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_categories>()
                .HasMany(e => e.ic_item_master)
                .WithRequired(e => e.ic_item_categories)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_children>()
                .Property(e => e.unit_price_uom)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_children>()
                .Property(e => e.last_unit_price)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_children>()
                .Property(e => e.item_abc_classification)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_children>()
                .Property(e => e.width)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_children>()
                .Property(e => e.mwt)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_children>()
                .Property(e => e.length)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_children>()
                .Property(e => e.core)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_children>()
                .Property(e => e.sku)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_children>()
                .Property(e => e.od)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_children>()
                .HasMany(e => e.ap_po_children)
                .WithRequired(e => e.ic_item_children)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_children>()
                .HasMany(e => e.ic_consumed_inventory)
                .WithRequired(e => e.ic_item_children)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_children>()
                .HasMany(e => e.ic_customer_items)
                .WithRequired(e => e.ic_item_children)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_children>()
                .HasMany(e => e.ic_item_children_X10)
                .WithRequired(e => e.ic_item_children)
                .HasForeignKey(e => e.row_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_children>()
                .HasMany(e => e.ic_item_images)
                .WithRequired(e => e.ic_item_children)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_children>()
                .HasMany(e => e.rv_pending_items)
                .WithRequired(e => e.ic_item_children)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_children>()
                .HasMany(e => e.wo_master)
                .WithRequired(e => e.ic_item_children)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_children_X10>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.item_number)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.item_description)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.stock_uom_1)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.stock_uom_2)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.stock_uom_weight)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.nmfc_class)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.family_grade)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.coating)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.finish)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.basis_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.recycled_content)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.fda)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.color)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.opacity)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.ppi)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.caliper)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.finish_type)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.forest_cert)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.trade_name)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.manufacturer)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.gsm)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.broke)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.md_stiffness)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.cd_stiffness)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.gloss)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.sheetfed_web)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.print_type)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.basic_length)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.basic_width)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.moisture)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.mill_item)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.basic_size)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.text_cover)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .Property(e => e.grade_code)
                .IsUnicode(false);

            modelBuilder.Entity<ic_item_master>()
                .HasMany(e => e.bom_route_produced_items)
                .WithRequired(e => e.ic_item_master)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_master>()
                .HasMany(e => e.bom_route_required_materials)
                .WithRequired(e => e.ic_item_master)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_master>()
                .HasMany(e => e.sf_bom_substitutions)
                .WithRequired(e => e.ic_item_master)
                .HasForeignKey(e => e.item_master_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_master>()
                .HasMany(e => e.sf_bom_substitutions1)
                .WithRequired(e => e.ic_item_master1)
                .HasForeignKey(e => e.substitution_item_master_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_hand_qty_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_hand_qty_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_hand_qty_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.allocated_qty_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.allocated_qty_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.allocated_qty_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_order_qty_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_order_qty_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_order_qty_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.min_qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.max_qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.warning_qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.in_transit_qty_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.in_transit_qty_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.in_transit_qty_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_hold_qty_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_hold_qty_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_hold_qty_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_quality_hold_qty_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_quality_hold_qty_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.on_quality_hold_qty_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.available_qty_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.available_qty_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_quantities>()
                .Property(e => e.available_qty_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_item_statuses>()
                .Property(e => e.item_status)
                .IsUnicode(false);

            modelBuilder.Entity<ic_nmfc_classes>()
                .Property(e => e._class)
                .IsUnicode(false);

            modelBuilder.Entity<ic_nmfc_classes>()
                .Property(e => e.density)
                .IsUnicode(false);

            modelBuilder.Entity<ic_nmfc_classes>()
                .Property(e => e.max_value_per_lb)
                .HasPrecision(5, 2);

            modelBuilder.Entity<ic_od_ranges>()
                .Property(e => e.range_label)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.phoenix_unique_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.pallet_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.ref_no)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.stock_uom_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.stock_uom_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.stock_uom_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.total_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.unit_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.unit_cost_uom_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.allocated_wo)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.batch_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.bol_number)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.weight_pounds)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.length_feet)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.original_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.holding_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.original_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.tare_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.original_uom1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .Property(e => e.original_uom2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .HasMany(e => e.bom_route_allocations)
                .WithRequired(e => e.ic_on_hand_inventory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .HasMany(e => e.ic_consumed_inventory)
                .WithRequired(e => e.ic_on_hand_inventory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_on_hand_inventory>()
                .HasMany(e => e.ic_unit_allocations)
                .WithRequired(e => e.ic_on_hand_inventory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_on_hand_quality_holds>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_quality_holds>()
                .Property(e => e.hold_reason)
                .IsUnicode(false);

            modelBuilder.Entity<ic_on_hand_quality_holds>()
                .Property(e => e.handle_reason)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_comparison_files>()
                .Property(e => e.source)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_comparison_files>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_comparison_files>()
                .Property(e => e.stock_uom_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_physical_inventory_comparison_files>()
                .Property(e => e.stock_uom_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_physical_inventory_comparison_files>()
                .Property(e => e.stock_uom_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_physical_inventory_comparison_headcount>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_comparison_master>()
                .HasMany(e => e.ic_physical_inventory_comparison_files)
                .WithRequired(e => e.ic_physical_inventory_comparison_master)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_physical_inventory_comparison_master>()
                .HasMany(e => e.ic_physical_inventory_comparison_headcount)
                .WithRequired(e => e.ic_physical_inventory_comparison_master)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_physical_inventory_comparison_master>()
                .HasMany(e => e.ic_physical_inventory_comparison_scanner)
                .WithRequired(e => e.ic_physical_inventory_comparison_master)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_physical_inventory_comparison_scanner>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_comparison_scanner>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_history>()
                .Property(e => e.inventoryids)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_item>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_item_status>()
                .Property(e => e.physical_inventory_item_status)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_item_status>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_item_status>()
                .HasMany(e => e.ic_physical_inventory_item)
                .WithRequired(e => e.ic_physical_inventory_item_status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_physical_inventory_status>()
                .Property(e => e.physical_inventory_status)
                .IsUnicode(false);

            modelBuilder.Entity<ic_physical_inventory_status>()
                .HasMany(e => e.ic_physical_inventory)
                .WithRequired(e => e.ic_physical_inventory_status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_physical_inventory_type>()
                .Property(e => e.physical_inventory_type)
                .IsUnicode(false);

            modelBuilder.Entity<ic_quality_hold_reasons>()
                .Property(e => e.quality_hold_reason)
                .IsUnicode(false);

            modelBuilder.Entity<ic_quality_hold_reasons>()
                .HasMany(e => e.ic_quality_holds)
                .WithRequired(e => e.ic_quality_hold_reasons)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_quality_hold_types>()
                .Property(e => e.quality_hold_type)
                .IsUnicode(false);

            modelBuilder.Entity<ic_quality_hold_types>()
                .HasMany(e => e.ic_quality_hold_reasons)
                .WithRequired(e => e.ic_quality_hold_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ic_quality_holds>()
                .Property(e => e.claim_number)
                .IsUnicode(false);

            modelBuilder.Entity<ic_unit_allocation_history>()
                .Property(e => e.work_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<ic_unit_allocation_history>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<ic_unit_allocation_history>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<ic_unit_allocations>()
                .Property(e => e.work_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<ic_unit_allocations>()
                .Property(e => e.allocated_uom_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_unit_allocations>()
                .Property(e => e.allocated_uom_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_unit_allocations>()
                .Property(e => e.allocated_uom_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<ic_unit_allocations>()
                .HasMany(e => e.wo_side_by_sides)
                .WithRequired(e => e.ic_unit_allocations)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<lbl_format_types>()
                .Property(e => e.format_name)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_format_types>()
                .Property(e => e.format_description)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_label>()
                .Property(e => e.label_name)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_label>()
                .Property(e => e.bartenderformat)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_label>()
                .Property(e => e.label_source)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_label>()
                .HasMany(e => e.lbl_label_printer_types)
                .WithRequired(e => e.lbl_label)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<lbl_label>()
                .HasMany(e => e.lbl_printjob)
                .WithRequired(e => e.lbl_label)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<lbl_label_type>()
                .Property(e => e.TYPE_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_label_type>()
                .Property(e => e.type_description)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_label_type>()
                .HasMany(e => e.lbl_item_mapping)
                .WithRequired(e => e.lbl_label_type)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<lbl_printer_settings>()
                .Property(e => e.setting_name)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_printer_settings>()
                .Property(e => e.VALUE)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_printer_types>()
                .Property(e => e.type_name)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_printer_types>()
                .Property(e => e.type_description)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_printer_types>()
                .HasMany(e => e.lbl_label_printer_types)
                .WithRequired(e => e.lbl_printer_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<lbl_printer_types>()
                .HasMany(e => e.lbl_printer_type_associations)
                .WithRequired(e => e.lbl_printer_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<lbl_printers>()
                .Property(e => e.printer_name)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_printers>()
                .Property(e => e.printer_description)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_printers>()
                .Property(e => e.printer_ip)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_printers>()
                .HasMany(e => e.ad_work_center_printer_associations)
                .WithRequired(e => e.lbl_printers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<lbl_printers>()
                .HasMany(e => e.lbl_printer_type_associations)
                .WithRequired(e => e.lbl_printers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<lbl_printjob>()
                .Property(e => e.printer_id)
                .IsUnicode(false);

            modelBuilder.Entity<lbl_printjob>()
                .Property(e => e.label_data_file_text)
                .IsUnicode(false);

            modelBuilder.Entity<pac_templates>()
                .Property(e => e.setting_category)
                .IsUnicode(false);

            modelBuilder.Entity<pac_templates>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<pac_templates>()
                .Property(e => e.default_value)
                .IsUnicode(false);

            modelBuilder.Entity<pac_templates>()
                .Property(e => e.acceptable_values)
                .IsUnicode(false);

            modelBuilder.Entity<pac_templates>()
                .HasMany(e => e.so_children_packaging)
                .WithRequired(e => e.pac_templates)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_ae_uom_labels>()
                .Property(e => e.unit_of_measure)
                .IsUnicode(false);

            modelBuilder.Entity<ps_control_type>()
                .Property(e => e.control_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_control_type>()
                .HasMany(e => e.ps_controls)
                .WithRequired(e => e.ps_control_type)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_controls>()
                .Property(e => e.control_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_controls>()
                .HasMany(e => e.ad_security_controls)
                .WithRequired(e => e.ps_controls)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_field_types>()
                .Property(e => e.field_type)
                .IsUnicode(false);

            modelBuilder.Entity<ps_field_types>()
                .HasMany(e => e.cs_custom_fields)
                .WithRequired(e => e.ps_field_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_field_types>()
                .HasMany(e => e.pac_templates)
                .WithRequired(e => e.ps_field_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_field_types>()
                .HasMany(e => e.ps_settings)
                .WithRequired(e => e.ps_field_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_field_types>()
                .HasMany(e => e.ps_third_party_options)
                .WithRequired(e => e.ps_field_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_field_types>()
                .HasMany(e => e.sf_node_properties)
                .WithRequired(e => e.ps_field_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_field_types>()
                .HasMany(e => e.wo_approval_templates)
                .WithRequired(e => e.ps_field_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_file_types>()
                .Property(e => e.file_type)
                .IsUnicode(false);

            modelBuilder.Entity<ps_file_types>()
                .HasMany(e => e.ps_files)
                .WithRequired(e => e.ps_file_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_files>()
                .Property(e => e.file_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_files>()
                .HasMany(e => e.cs_customer_files)
                .WithRequired(e => e.ps_files)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_image_associations>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_image_associations>()
                .Property(e => e.primary_id)
                .IsUnicode(false);

            modelBuilder.Entity<ps_image_types>()
                .Property(e => e.image_type)
                .IsUnicode(false);

            modelBuilder.Entity<ps_image_types>()
                .Property(e => e.image_type_extension)
                .IsUnicode(false);

            modelBuilder.Entity<ps_image_types>()
                .HasMany(e => e.ps_images)
                .WithRequired(e => e.ps_image_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_images>()
                .Property(e => e.image_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_images>()
                .HasMany(e => e.ic_inventory_images)
                .WithRequired(e => e.ps_images)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_images>()
                .HasMany(e => e.ic_item_images)
                .WithRequired(e => e.ps_images)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_images>()
                .HasMany(e => e.ps_image_associations)
                .WithRequired(e => e.ps_images)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_images>()
                .HasMany(e => e.so_children_images)
                .WithRequired(e => e.ps_images)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_modules>()
                .Property(e => e.module_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_modules>()
                .Property(e => e.image_string)
                .IsUnicode(false);

            modelBuilder.Entity<ps_modules>()
                .HasMany(e => e.ad_security_modules)
                .WithRequired(e => e.ps_modules)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_modules>()
                .HasMany(e => e.ps_screens)
                .WithRequired(e => e.ps_modules)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_screens>()
                .Property(e => e.screen_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_screens>()
                .HasMany(e => e.ad_security_screens)
                .WithRequired(e => e.ps_screens)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_screens>()
                .HasMany(e => e.ps_controls)
                .WithRequired(e => e.ps_screens)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_settings>()
                .Property(e => e.setting_category)
                .IsUnicode(false);

            modelBuilder.Entity<ps_settings>()
                .Property(e => e.setting_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_settings>()
                .Property(e => e.setting_value)
                .IsUnicode(false);

            modelBuilder.Entity<ps_settings>()
                .Property(e => e.comments)
                .IsUnicode(false);

            modelBuilder.Entity<ps_table_extended_fields>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_table_extended_fields>()
                .Property(e => e.default_value)
                .IsUnicode(false);

            modelBuilder.Entity<ps_table_extended_fields>()
                .Property(e => e.regex_validation)
                .IsUnicode(false);

            modelBuilder.Entity<ps_table_extended_fields>()
                .Property(e => e.regex_validation_message)
                .IsUnicode(false);

            modelBuilder.Entity<ps_table_extended_fields>()
                .HasMany(e => e.ic_item_children_X10)
                .WithRequired(e => e.ps_table_extended_fields)
                .HasForeignKey(e => e.field_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_tables>()
                .Property(e => e.table_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_tables>()
                .Property(e => e.display_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_tables>()
                .Property(e => e.datawarehouse_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_tables>()
                .HasMany(e => e.cs_custom_fields)
                .WithRequired(e => e.ps_tables)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_tables>()
                .HasMany(e => e.cs_grid_filter_fields)
                .WithRequired(e => e.ps_tables)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_tables>()
                .HasMany(e => e.edi_customer_mappings)
                .WithRequired(e => e.ps_tables)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_tables>()
                .HasMany(e => e.ps_image_associations)
                .WithRequired(e => e.ps_tables)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_third_party_options>()
                .Property(e => e.option_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_third_party_options>()
                .Property(e => e.acceptable_values)
                .IsUnicode(false);

            modelBuilder.Entity<ps_third_party_options>()
                .HasMany(e => e.cs_customer_third_party_settings)
                .WithRequired(e => e.ps_third_party_options)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_third_party_options>()
                .HasMany(e => e.so_children_service_settings)
                .WithRequired(e => e.ps_third_party_options)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_third_party_services>()
                .Property(e => e.service_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_third_party_services>()
                .HasMany(e => e.ps_third_party_options)
                .WithRequired(e => e.ps_third_party_services)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_unit_of_measure_labels>()
                .HasMany(e => e.ps_ae_uom_labels)
                .WithRequired(e => e.ps_unit_of_measure_labels)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ps_unit_of_measures>()
                .Property(e => e.unit_of_measure)
                .IsUnicode(false);

            modelBuilder.Entity<ps_unit_of_measures>()
                .Property(e => e.uom_type)
                .IsUnicode(false);

            modelBuilder.Entity<ps_unit_of_measures>()
                .Property(e => e.display_name)
                .IsUnicode(false);

            modelBuilder.Entity<ps_unit_of_measures>()
                .Property(e => e.conversion)
                .HasPrecision(38, 19);

            modelBuilder.Entity<rv_history>()
                .Property(e => e.bol_number)
                .IsUnicode(false);

            modelBuilder.Entity<rv_pending_items>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<rv_pending_items>()
                .Property(e => e.ref_no)
                .IsUnicode(false);

            modelBuilder.Entity<rv_pending_items>()
                .Property(e => e.stock_uom_1_label)
                .IsUnicode(false);

            modelBuilder.Entity<rv_pending_items>()
                .Property(e => e.stock_uom_2_label)
                .IsUnicode(false);

            modelBuilder.Entity<rv_pending_items>()
                .Property(e => e.stock_uom_weight_label)
                .IsUnicode(false);

            modelBuilder.Entity<rv_pending_items>()
                .Property(e => e.created_by)
                .IsUnicode(false);

            modelBuilder.Entity<rv_pending_items>()
                .Property(e => e.bol_number)
                .IsUnicode(false);

            modelBuilder.Entity<rv_pending_items>()
                .Property(e => e.total_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<rv_pending_items>()
                .Property(e => e.unit_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<rv_pending_items>()
                .Property(e => e.unit_cost_uom)
                .IsUnicode(false);

            modelBuilder.Entity<rv_receiving_types>()
                .Property(e => e.receiving_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<rv_receiving_types>()
                .Property(e => e.receiving_type_description)
                .IsUnicode(false);

            modelBuilder.Entity<rv_receiving_types>()
                .HasMany(e => e.ic_consumed_inventory)
                .WithRequired(e => e.rv_receiving_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<rv_receiving_types>()
                .HasMany(e => e.ic_on_hand_inventory)
                .WithRequired(e => e.rv_receiving_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sc_appointments>()
                .Property(e => e.reccurence_pattern)
                .IsUnicode(false);

            modelBuilder.Entity<sc_appointments>()
                .Property(e => e.time_zone_string)
                .IsUnicode(false);

            modelBuilder.Entity<sc_appointments>()
                .HasMany(e => e.sc_calendar_appointments)
                .WithRequired(e => e.sc_appointments)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sc_calendars>()
                .Property(e => e.calendar_name)
                .IsUnicode(false);

            modelBuilder.Entity<sc_calendars>()
                .HasMany(e => e.sc_calendar_appointments)
                .WithRequired(e => e.sc_calendars)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sc_calendars>()
                .HasMany(e => e.sc_calendar_exceptions)
                .WithRequired(e => e.sc_calendars)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sc_categories>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<sc_categories>()
                .Property(e => e.display_name)
                .IsUnicode(false);

            modelBuilder.Entity<sc_categories>()
                .Property(e => e.brush_name)
                .IsUnicode(false);

            modelBuilder.Entity<sc_custom_sorts>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<sc_custom_sorts>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<sc_custom_sorts>()
                .Property(e => e.sort_direction)
                .IsUnicode(false);

            modelBuilder.Entity<sc_exceptions>()
                .Property(e => e.time_zone_string)
                .IsUnicode(false);

            modelBuilder.Entity<sc_exceptions>()
                .HasMany(e => e.sc_calendar_exceptions)
                .WithRequired(e => e.sc_exceptions)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sc_resource_types>()
                .Property(e => e.brush_name)
                .IsUnicode(false);

            modelBuilder.Entity<sc_sorts>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<sc_sorts>()
                .Property(e => e.sort_direction)
                .IsUnicode(false);

            modelBuilder.Entity<sc_work_order_schedules>()
                .Property(e => e.work_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_condition>()
                .Property(e => e.condition)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_fields>()
                .Property(e => e.control_name)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_fields>()
                .Property(e => e.control_text)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_fields>()
                .Property(e => e.control_source)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_fields>()
                .Property(e => e.datamember)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_fields>()
                .Property(e => e.regex_validation)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_fields>()
                .Property(e => e.regex_validation_msg)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_fields>()
                .Property(e => e.text_id)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_fields>()
                .HasMany(e => e.sca_dynamic_condition)
                .WithRequired(e => e.sca_dynamic_fields)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sca_dynamic_form>()
                .Property(e => e.screen_name)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_form>()
                .HasMany(e => e.sca_dynamic_form_containers)
                .WithRequired(e => e.sca_dynamic_form)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sca_dynamic_form_containers>()
                .Property(e => e.container_name)
                .IsUnicode(false);

            modelBuilder.Entity<sca_dynamic_form_containers>()
                .HasMany(e => e.sca_dynamic_fields)
                .WithRequired(e => e.sca_dynamic_form_containers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sca_main_menu>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<sca_main_menu>()
                .Property(e => e.Caption)
                .IsUnicode(false);

            modelBuilder.Entity<sca_main_menu>()
                .Property(e => e.Value)
                .IsUnicode(false);

            modelBuilder.Entity<sca_main_menu>()
                .Property(e => e.ApplicationName)
                .IsUnicode(false);

            modelBuilder.Entity<sf_downtimes>()
                .Property(e => e.downtime_display)
                .IsUnicode(false);

            modelBuilder.Entity<sf_groups>()
                .HasMany(e => e.sf_node_groups)
                .WithRequired(e => e.sf_groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.source)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.work_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.process_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.node)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.logon_01_data_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.logon_02_data_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.logon_03_data_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.entry_18_data_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.entry_19_data_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.entry_20_data_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.entry_21_data_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.entry_22_data_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.entry_23_data_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.entry_13_value)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.order_results_01_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.order_results_02_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.order_results_03_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.order_results_04_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.order_results_05_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.order_results_06_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.active_grp)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.process_object)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.logon_04_value)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.events_downtime_level_2)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.crew2_1_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.crew2_2_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.crew2_3_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.crew2_4_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.one_1_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.one_2_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.entry_24_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.extra_01_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.extra_02_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.extra_03_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.extra_04_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.extra_05_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.extra_06_data_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.logon_04_data_txt)
                .IsUnicode(false);

            modelBuilder.Entity<sf_job_transactions>()
                .Property(e => e.edited)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<sf_machine_types>()
                .Property(e => e.machine_type)
                .IsUnicode(false);

            modelBuilder.Entity<sf_machine_work_orders>()
                .Property(e => e.work_order)
                .IsUnicode(false);

            modelBuilder.Entity<sf_machine_work_orders>()
                .Property(e => e.good_total)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_machine_work_orders>()
                .Property(e => e.bad_total)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_machine_work_orders>()
                .Property(e => e.order_actual_vb_tt)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_machine_work_orders>()
                .Property(e => e.order_theo_vb_tt)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_node_properties>()
                .Property(e => e.drop_down_text)
                .IsUnicode(false);

            modelBuilder.Entity<sf_node_properties>()
                .HasMany(e => e.sf_node_settings)
                .WithRequired(e => e.sf_node_properties)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_node_settings>()
                .Property(e => e.property_setting)
                .IsUnicode(false);

            modelBuilder.Entity<sf_node_statuses>()
                .Property(e => e.node_status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<sf_node_statuses>()
                .Property(e => e.status_description)
                .IsUnicode(false);

            modelBuilder.Entity<sf_nodes>()
                .Property(e => e.node_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_nodes>()
                .Property(e => e.alt_node_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_nodes>()
                .Property(e => e.node_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sf_operator_shifts>()
                .Property(e => e.USER_ID)
                .IsUnicode(false);

            modelBuilder.Entity<sf_operator_shifts>()
                .Property(e => e.shift_actual_vb_tt)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_operator_shifts>()
                .Property(e => e.shift_theo_vb_tt)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_operator_shifts>()
                .Property(e => e.shift_01)
                .IsUnicode(false);

            modelBuilder.Entity<sf_operator_shifts>()
                .Property(e => e.shift_02)
                .IsUnicode(false);

            modelBuilder.Entity<sf_operator_shifts>()
                .Property(e => e.shift_03)
                .IsUnicode(false);

            modelBuilder.Entity<sf_operator_shifts>()
                .Property(e => e.shift_04)
                .IsUnicode(false);

            modelBuilder.Entity<sf_operator_shifts>()
                .Property(e => e.shift_05)
                .IsUnicode(false);

            modelBuilder.Entity<sf_processes>()
                .Property(e => e.process)
                .IsUnicode(false);

            modelBuilder.Entity<sf_processes>()
                .HasMany(e => e.sf_processes_subdowntime_sets)
                .WithRequired(e => e.sf_processes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_sets>()
                .Property(e => e.good_total)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_sets>()
                .Property(e => e.bad_total)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_sets>()
                .Property(e => e.set_01)
                .IsUnicode(false);

            modelBuilder.Entity<sf_sets>()
                .Property(e => e.set_02)
                .IsUnicode(false);

            modelBuilder.Entity<sf_sets>()
                .Property(e => e.set_03)
                .IsUnicode(false);

            modelBuilder.Entity<sf_sets>()
                .Property(e => e.set_04)
                .IsUnicode(false);

            modelBuilder.Entity<sf_sets>()
                .Property(e => e.set_05)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shift_details_temp>()
                .Property(e => e.work_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shift_details_temp>()
                .Property(e => e.process_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shift_details_temp>()
                .Property(e => e.action_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shift_details_temp>()
                .Property(e => e.field_01_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shift_details_temp>()
                .Property(e => e.field_01_value)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shift_details_temp>()
                .Property(e => e.field_02_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shift_details_temp>()
                .Property(e => e.field_02_value)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shift_details_temp>()
                .Property(e => e.field_03_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shift_details_temp>()
                .Property(e => e.field_03_value)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shift_details_temp>()
                .Property(e => e.comments)
                .IsUnicode(false);

            modelBuilder.Entity<sf_shifts>()
                .Property(e => e.shift_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_subdowntimes>()
                .Property(e => e.subdowntime)
                .IsUnicode(false);

            modelBuilder.Entity<sf_subdowntimes>()
                .HasMany(e => e.sf_processes_subdowntime_sets)
                .WithRequired(e => e.sf_subdowntimes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.tag)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.qualifier_01)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.qualifier_02)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.qualifier_03)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.qualifier_04)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.qualifier_05)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.qualifier_06)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.qualifier_07)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.qualifier_08)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.qualifier_09)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.qualifier_10)
                .IsUnicode(false);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.theoretical_from)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.theoretical_to)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.value)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_theoreticals>()
                .Property(e => e.theoretical_rate)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_records>()
                .Property(e => e.work_order)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.process_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.process_object)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.active_group)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.order_results_01)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.order_results_02)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.order_results_03)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.order_results_04)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.order_results_05)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.order_results_06)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.crew2_1)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.crew2_2)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.crew2_3)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.crew2_4)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.one_1)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.one_2)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.generic_timer_01)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.generic_timer_02)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.generic_timer_03)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.generic_timer_04)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.generic_timer_05)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.subdowntime)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.actual_count_number)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.actual_count_number2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.good_total)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.bad_total)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.good_total_diff)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.bad_total_diff)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.shift_actual_vb_tt)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.shift_theo_vb_tt)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.order_actual_vb_tt)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.order_theo_vb_tt)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.extra_01)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.extra_02)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.extra_03)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.extra_04)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.extra_05)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.extra_06)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.extra_07)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.extra_08)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.extra_09)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.extra_10)
                .IsUnicode(false);

            modelBuilder.Entity<sf_time_segments>()
                .Property(e => e.counters)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transaction_masters>()
                .Property(e => e.master_id)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.work_order)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.ref_no)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.pallet_id)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.transaction_type)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.stock_uom_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.stock_uom_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.stock_uom_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.shift)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.master_ids)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.user_remark)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.source)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.total_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.node)
                .IsUnicode(false);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.weight_pounds)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_transactions>()
                .Property(e => e.length_feet)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_transactions>()
                .HasMany(e => e.sf_transaction_masters)
                .WithRequired(e => e.sf_transactions)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_vectors>()
                .Property(e => e.vector_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.inventory_id)
                .IsUnicode(false);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.stock_uom_1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.stock_uom_1_id)
                .IsUnicode(false);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.stock_uom_2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.stock_uom_2_id)
                .IsUnicode(false);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.stock_uom_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.stock_uom_weight_id)
                .IsUnicode(false);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.node)
                .IsUnicode(false);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.work_order)
                .IsUnicode(false);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.user_remark)
                .IsUnicode(false);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.created_by)
                .IsUnicode(false);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.lbs)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_waste_transactions>()
                .Property(e => e.ft)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_waste_types>()
                .Property(e => e.waste_type)
                .IsUnicode(false);

            modelBuilder.Entity<sf_work_center_bom_attributes>()
                .HasMany(e => e.wo_step_attributes)
                .WithRequired(e => e.sf_work_center_bom_attributes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_work_center_groups>()
                .Property(e => e.work_center_group_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_work_center_groups>()
                .HasMany(e => e.bom_route_steps)
                .WithRequired(e => e.sf_work_center_groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_work_center_groups>()
                .HasMany(e => e.bom_steps)
                .WithRequired(e => e.sf_work_center_groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_work_center_groups>()
                .HasMany(e => e.cs_customer_work_group_notes)
                .WithRequired(e => e.sf_work_center_groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_work_center_groups>()
                .HasMany(e => e.pac_templates)
                .WithRequired(e => e.sf_work_center_groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_work_center_groups>()
                .HasMany(e => e.sf_work_center_bom_attributes)
                .WithRequired(e => e.sf_work_center_groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.work_center_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.work_center_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.labor_rate)
                .HasPrecision(10, 4);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.labor_units)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.variable_oh)
                .HasPrecision(10, 4);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.fixed_oh)
                .HasPrecision(10, 4);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.overtime_factor)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.doubletime_factor)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.run_rate_measurement)
                .IsUnicode(false);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.run_rate_time)
                .IsUnicode(false);

            modelBuilder.Entity<sf_work_centers>()
                .Property(e => e.run_rate)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sf_work_centers>()
                .HasMany(e => e.ad_joined_work_centers)
                .WithRequired(e => e.sf_work_centers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_work_centers>()
                .HasMany(e => e.ad_user_work_center_associations)
                .WithRequired(e => e.sf_work_centers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_work_centers>()
                .HasMany(e => e.sf_ae_work_center_groupings)
                .WithRequired(e => e.sf_work_centers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sf_work_centers>()
                .HasMany(e => e.sf_nodes)
                .WithRequired(e => e.sf_work_centers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sh_carriers>()
                .Property(e => e.carrier_name)
                .IsUnicode(false);

            modelBuilder.Entity<sh_carriers>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<sh_carriers>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<sh_carriers>()
                .Property(e => e.fax)
                .IsUnicode(false);

            modelBuilder.Entity<sh_carriers>()
                .Property(e => e.dun)
                .IsUnicode(false);

            modelBuilder.Entity<sh_carriers>()
                .HasMany(e => e.cs_carrier_contacts)
                .WithRequired(e => e.sh_carriers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sh_carriers>()
                .HasMany(e => e.cs_carrier_notes)
                .WithRequired(e => e.sh_carriers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sh_carriers>()
                .HasMany(e => e.sh_freight_loads)
                .WithRequired(e => e.sh_carriers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sh_freight_loads>()
                .Property(e => e.carrier_PO)
                .IsUnicode(false);

            modelBuilder.Entity<sh_freight_loads>()
                .Property(e => e.cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sh_freight_loads>()
                .Property(e => e.trailer_no)
                .IsUnicode(false);

            modelBuilder.Entity<sh_freight_loads>()
                .Property(e => e.seal_no)
                .IsUnicode(false);

            modelBuilder.Entity<sh_freight_loads>()
                .Property(e => e.delivery_instructions)
                .IsUnicode(false);

            modelBuilder.Entity<sh_freight_loads>()
                .Property(e => e.loaded_by_name)
                .IsUnicode(false);

            modelBuilder.Entity<sh_freight_loads>()
                .Property(e => e.counted_by_name)
                .IsUnicode(false);

            modelBuilder.Entity<sh_shipment_appoval>()
                .Property(e => e.shipped_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sh_shipment_appoval>()
                .Property(e => e.load_number)
                .IsUnicode(false);

            modelBuilder.Entity<sh_shipment_appoval>()
                .Property(e => e.reason)
                .IsUnicode(false);

            modelBuilder.Entity<sh_shipments>()
                .Property(e => e.admin_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sh_shipments>()
                .Property(e => e.markup_percent)
                .HasPrecision(3, 2);

            modelBuilder.Entity<sh_shipments>()
                .Property(e => e.load_number)
                .IsUnicode(false);

            modelBuilder.Entity<sh_shipments>()
                .Property(e => e.weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<sh_shipments>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<sh_shipments>()
                .Property(e => e.packingslip_no)
                .IsUnicode(false);

            modelBuilder.Entity<sh_shipments>()
                .Property(e => e.bol_no)
                .IsUnicode(false);

            modelBuilder.Entity<sh_shipped_lot_pallets>()
                .Property(e => e.pallet_id)
                .IsUnicode(false);

            modelBuilder.Entity<sh_shipped_lot_pallets>()
                .Property(e => e.lot_no)
                .IsUnicode(false);

            modelBuilder.Entity<sh_statuses>()
                .Property(e => e.shipment_status)
                .IsUnicode(false);

            modelBuilder.Entity<sh_statuses>()
                .HasMany(e => e.sh_shipments)
                .WithRequired(e => e.sh_statuses)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sh_trailer_freight_types>()
                .Property(e => e.trailer_freight_type)
                .IsUnicode(false);

            modelBuilder.Entity<sh_trailer_freight_types>()
                .Property(e => e.trailer_freight_description)
                .IsUnicode(false);

            modelBuilder.Entity<sh_trailer_load_types>()
                .Property(e => e.trailer_load_type)
                .IsUnicode(false);

            modelBuilder.Entity<sh_trailer_load_types>()
                .Property(e => e.trailer_load_description)
                .IsUnicode(false);

            modelBuilder.Entity<sh_zones>()
                .Property(e => e.shipping_zone)
                .IsUnicode(false);

            modelBuilder.Entity<so_accounts>()
                .Property(e => e.account_name)
                .IsUnicode(false);

            modelBuilder.Entity<so_accounts>()
                .HasMany(e => e.so_accounts_children)
                .WithRequired(e => e.so_accounts)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_accounts_children>()
                .Property(e => e.child_name)
                .IsUnicode(false);

            modelBuilder.Entity<so_accounts_children>()
                .Property(e => e.gl_account)
                .IsUnicode(false);

            modelBuilder.Entity<so_additional_charge_categories>()
                .Property(e => e.category_name)
                .IsUnicode(false);

            modelBuilder.Entity<so_additional_charge_categories>()
                .HasMany(e => e.so_additional_charge_categories1)
                .WithOptional(e => e.so_additional_charge_categories2)
                .HasForeignKey(e => e.additional_charge_category_id_rk);

            modelBuilder.Entity<so_additional_charge_categories>()
                .HasMany(e => e.so_additional_charges)
                .WithRequired(e => e.so_additional_charge_categories)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_additional_charge_categories>()
                .HasMany(e => e.so_charges)
                .WithRequired(e => e.so_additional_charge_categories)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_additional_charges>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<so_additional_charges>()
                .Property(e => e.default_cost)
                .HasPrecision(18, 4);

            modelBuilder.Entity<so_additional_charges>()
                .Property(e => e.qty_uom)
                .IsUnicode(false);

            modelBuilder.Entity<so_additional_charges>()
                .HasMany(e => e.so_associated_charges)
                .WithRequired(e => e.so_additional_charges)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_associated_charges>()
                .Property(e => e.setting_category)
                .IsUnicode(false);

            modelBuilder.Entity<so_associated_charges>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<so_charges>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<so_charges>()
                .Property(e => e.sale_price)
                .HasPrecision(18, 4);

            modelBuilder.Entity<so_charges>()
                .Property(e => e.charge_qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<so_charges>()
                .Property(e => e.charge_uom)
                .IsUnicode(false);

            modelBuilder.Entity<so_children>()
                .Property(e => e.work_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<so_children>()
                .Property(e => e.sale_price)
                .HasPrecision(18, 4);

            modelBuilder.Entity<so_children>()
                .Property(e => e.shipto_PO)
                .IsUnicode(false);

            modelBuilder.Entity<so_children>()
                .Property(e => e.ship_notes)
                .IsUnicode(false);

            modelBuilder.Entity<so_children>()
                .Property(e => e.shipping_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<so_children>()
                .Property(e => e.ship_qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<so_children>()
                .Property(e => e.ship_qty_uom)
                .IsUnicode(false);

            modelBuilder.Entity<so_children>()
                .Property(e => e.markup_percent)
                .HasPrecision(5, 2);

            modelBuilder.Entity<so_children>()
                .Property(e => e.discount_percent)
                .HasPrecision(5, 2);

            modelBuilder.Entity<so_children>()
                .Property(e => e.tare_weight)
                .HasPrecision(18, 4);

            modelBuilder.Entity<so_children>()
                .Property(e => e.sale_price_uom)
                .IsUnicode(false);

            modelBuilder.Entity<so_children>()
                .Property(e => e.line_item)
                .IsUnicode(false);

            modelBuilder.Entity<so_children>()
                .Property(e => e.tax_percent)
                .HasPrecision(5, 4);

            modelBuilder.Entity<so_children>()
                .HasMany(e => e.sh_shipments)
                .WithRequired(e => e.so_children)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_children>()
                .HasMany(e => e.so_charges)
                .WithRequired(e => e.so_children)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_children>()
                .HasMany(e => e.so_children_images)
                .WithRequired(e => e.so_children)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_children>()
                .HasMany(e => e.so_children_service_settings)
                .WithRequired(e => e.so_children)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_children_packaging>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<so_children_service_settings>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<so_customer_blanketpos>()
                .Property(e => e.blanket_po)
                .IsUnicode(false);

            modelBuilder.Entity<so_customer_blanketpos>()
                .Property(e => e.amount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<so_customer_blanketpos>()
                .Property(e => e.po_description)
                .IsUnicode(false);

            modelBuilder.Entity<so_sales_orders>()
                .Property(e => e.sales_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<so_sales_orders>()
                .Property(e => e.customer_PO)
                .IsUnicode(false);

            modelBuilder.Entity<so_sales_orders>()
                .Property(e => e.markup_percent)
                .HasPrecision(5, 2);

            modelBuilder.Entity<so_sales_orders>()
                .Property(e => e.discount_percent)
                .HasPrecision(5, 2);

            modelBuilder.Entity<so_sales_orders>()
                .Property(e => e.priority)
                .IsUnicode(false);

            modelBuilder.Entity<so_sales_orders>()
                .Property(e => e.blanket_PO)
                .IsUnicode(false);

            modelBuilder.Entity<so_sales_orders>()
                .Property(e => e.ref_no)
                .IsUnicode(false);

            modelBuilder.Entity<so_sales_orders>()
                .HasMany(e => e.so_children)
                .WithRequired(e => e.so_sales_orders)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_sales_reps>()
                .Property(e => e.first_name)
                .IsUnicode(false);

            modelBuilder.Entity<so_sales_reps>()
                .Property(e => e.last_name)
                .IsUnicode(false);

            modelBuilder.Entity<so_sales_reps>()
                .HasMany(e => e.so_reps_territories)
                .WithRequired(e => e.so_sales_reps)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_statuses>()
                .Property(e => e.sales_order_status)
                .IsUnicode(false);

            modelBuilder.Entity<so_statuses>()
                .HasMany(e => e.so_sales_orders)
                .WithRequired(e => e.so_statuses)
                .HasForeignKey(e => e.so_status_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<so_territories>()
                .Property(e => e.territory_name)
                .IsUnicode(false);

            modelBuilder.Entity<so_territories>()
                .HasMany(e => e.so_reps_territories)
                .WithRequired(e => e.so_territories)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ut_SSRS_log>()
                .Property(e => e.request_text)
                .IsUnicode(false);

            modelBuilder.Entity<ut_SSRS_log>()
                .Property(e => e.results_text)
                .IsUnicode(false);

            modelBuilder.Entity<wo_approval_settings>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<wo_approval_statuses>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<wo_approval_statuses>()
                .Property(e => e.status_description)
                .IsUnicode(false);

            modelBuilder.Entity<wo_approval_statuses>()
                .HasMany(e => e.wo_approval_templates)
                .WithRequired(e => e.wo_approval_statuses)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_approval_templates>()
                .Property(e => e.approval_name)
                .IsUnicode(false);

            modelBuilder.Entity<wo_approval_templates>()
                .Property(e => e.default_value)
                .IsUnicode(false);

            modelBuilder.Entity<wo_approval_templates>()
                .Property(e => e.acceptable_values)
                .IsUnicode(false);

            modelBuilder.Entity<wo_approval_templates>()
                .HasMany(e => e.wo_approval_groups)
                .WithRequired(e => e.wo_approval_templates)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_approval_templates>()
                .HasMany(e => e.wo_approval_settings)
                .WithRequired(e => e.wo_approval_templates)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_blanket_release_uom>()
                .Property(e => e.blanket_release_uom)
                .IsUnicode(false);

            modelBuilder.Entity<wo_comments>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<wo_job_classes>()
                .Property(e => e.job_class)
                .IsUnicode(false);

            modelBuilder.Entity<wo_job_classes>()
                .HasMany(e => e.wo_master)
                .WithRequired(e => e.wo_job_classes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_master>()
                .Property(e => e.work_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<wo_master>()
                .Property(e => e.order_qty)
                .HasPrecision(18, 4);

            modelBuilder.Entity<wo_master>()
                .Property(e => e.order_qty_uom)
                .IsUnicode(false);

            modelBuilder.Entity<wo_master>()
                .Property(e => e.scrap_percent)
                .HasPrecision(3, 2);

            modelBuilder.Entity<wo_master>()
                .Property(e => e.mfg_notes)
                .IsUnicode(false);

            modelBuilder.Entity<wo_master>()
                .Property(e => e.label_marks)
                .IsUnicode(false);

            modelBuilder.Entity<wo_master>()
                .Property(e => e.ss_number)
                .IsUnicode(false);

            modelBuilder.Entity<wo_master>()
                .Property(e => e.coating_in)
                .IsUnicode(false);

            modelBuilder.Entity<wo_master>()
                .Property(e => e.od)
                .HasPrecision(18, 4);

            modelBuilder.Entity<wo_master>()
                .HasMany(e => e.bom_route_steps)
                .WithRequired(e => e.wo_master)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_master>()
                .HasMany(e => e.bom_subassembly_hierarchies)
                .WithRequired(e => e.wo_master)
                .HasForeignKey(e => e.child_wo_master_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_master>()
                .HasMany(e => e.bom_subassembly_hierarchies1)
                .WithRequired(e => e.wo_master1)
                .HasForeignKey(e => e.parent_wo_master_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_master>()
                .HasMany(e => e.wo_approval_settings)
                .WithRequired(e => e.wo_master)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_master>()
                .HasMany(e => e.wo_comments)
                .WithRequired(e => e.wo_master)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_master>()
                .HasMany(e => e.wo_notes)
                .WithRequired(e => e.wo_master)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_notes>()
                .Property(e => e.wo_note)
                .IsUnicode(false);

            modelBuilder.Entity<wo_order_types>()
                .Property(e => e.order_type)
                .IsUnicode(false);

            modelBuilder.Entity<wo_order_types>()
                .HasMany(e => e.wo_master)
                .WithRequired(e => e.wo_order_types)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_side_by_sides>()
                .Property(e => e.work_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<wo_side_by_sides>()
                .Property(e => e.width)
                .HasPrecision(18, 4);

            modelBuilder.Entity<wo_statuses>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<wo_statuses>()
                .Property(e => e.status_description)
                .IsUnicode(false);

            modelBuilder.Entity<wo_statuses>()
                .HasMany(e => e.wo_master)
                .WithRequired(e => e.wo_statuses)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<wo_step_attributes>()
                .Property(e => e.custom_field_value)
                .IsUnicode(false);

            modelBuilder.Entity<C_MWV_subcontract_orders>()
                .Property(e => e.customer_po)
                .IsUnicode(false);

            modelBuilder.Entity<C_MWV_subcontract_orders>()
                .Property(e => e.processed_filename)
                .IsUnicode(false);

            modelBuilder.Entity<bom_rules>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<bom_rules>()
                .Property(e => e.rule_filter)
                .IsUnicode(false);

            modelBuilder.Entity<bom_rules>()
                .Property(e => e.step_filter)
                .IsUnicode(false);

            modelBuilder.Entity<bom_rules>()
                .Property(e => e.source)
                .IsUnicode(false);

            modelBuilder.Entity<cs_global_counter_objects>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<cs_global_counter_objects>()
                .Property(e => e.counter_type)
                .IsUnicode(false);

            modelBuilder.Entity<cs_global_counter_objects>()
                .Property(e => e.counter_num_prefix)
                .IsUnicode(false);

            modelBuilder.Entity<edi_x12_identifiers>()
                .Property(e => e.identifier)
                .IsUnicode(false);

            modelBuilder.Entity<edi_x12_qualifiers>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<sc_bom_schedule>()
                .Property(e => e.entry_display)
                .IsUnicode(false);

            modelBuilder.Entity<sc_bom_schedule>()
                .Property(e => e.duration)
                .HasPrecision(10, 2);

            modelBuilder.Entity<sc_schedule_columns>()
                .Property(e => e.grid_name)
                .IsUnicode(false);

            modelBuilder.Entity<sc_schedule_columns>()
                .Property(e => e.header)
                .IsUnicode(false);

            modelBuilder.Entity<sc_schedule_columns>()
                .Property(e => e.field_name)
                .IsUnicode(false);

            modelBuilder.Entity<sc_schedule_columns>()
                .Property(e => e.calculation)
                .IsUnicode(false);

            modelBuilder.Entity<sc_schedule_entry_types>()
                .Property(e => e.entry_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<sc_schedule_statuses>()
                .Property(e => e.status_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_additional_operators>()
                .Property(e => e.shift_guid)
                .IsUnicode(false);

            modelBuilder.Entity<sf_custom_accumulators>()
                .Property(e => e.accumulator_name)
                .IsUnicode(false);

            modelBuilder.Entity<sf_custom_accumulators>()
                .Property(e => e.start_accumulator)
                .IsUnicode(false);

            modelBuilder.Entity<sf_custom_accumulators>()
                .Property(e => e.stop_accumulator)
                .IsUnicode(false);

            modelBuilder.Entity<sf_opc_fields>()
                .Property(e => e.opc_field)
                .IsUnicode(false);

            modelBuilder.Entity<so_order_categories>()
                .Property(e => e.category_name)
                .IsUnicode(false);

            modelBuilder.Entity<sysarticle>()
                .Property(e => e.schema_option)
                .IsFixedLength();

            modelBuilder.Entity<syspublication>()
                .Property(e => e.snapshot_jobid)
                .IsFixedLength();

            modelBuilder.Entity<syspublication>()
                .Property(e => e.min_autonosync_lsn)
                .IsFixedLength();

            modelBuilder.Entity<sysschemaarticle>()
                .Property(e => e.schema_option)
                .IsFixedLength();

            modelBuilder.Entity<syssubscription>()
                .Property(e => e.distribution_jobid)
                .IsFixedLength();

            modelBuilder.Entity<syssubscription>()
                .Property(e => e.timestamp)
                .IsFixedLength();

            modelBuilder.Entity<systranschema>()
                .Property(e => e.startlsn)
                .IsFixedLength();

            modelBuilder.Entity<systranschema>()
                .Property(e => e.endlsn)
                .IsFixedLength();
        }
    }
}
