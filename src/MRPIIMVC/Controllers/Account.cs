﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Security.Claims;
using Microsoft.AspNetCore.Http.Authentication;



// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MRPIIMVC.Controllers
{
    public class Account : BaseController
    {
        private Models.Account.UserManager userManager = new Models.Account.UserManager();
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        //POST: /Account/Login

       [HttpPost]
       [AllowAnonymous]
       [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = userManager.login(model.UserName, model.Password);
                if (user != null)
                {
                    user.IsAuthenticated = true;
                    //not sure if name is the right place to put serialized version but oh well. 
                    var Json = Newtonsoft.Json.JsonConvert.SerializeObject(user);
                    user.Name = Json;
                   
                    var userPrincipal = new ClaimsPrincipal(user);

                    await HttpContext.Authentication.SignInAsync("MyCookieAuthenticationScheme", userPrincipal,
                        new AuthenticationProperties
                        {
                            ExpiresUtc = DateTime.UtcNow.AddDays(30),
                            IsPersistent = true,
                            AllowRefresh = true
                        });
                    if (returnUrl != null)
                        return Redirect(".." + returnUrl);

                        return RedirectToAction("Index", "Home");
                    }

                }
            
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        public async Task<ActionResult> LogOff()
        {
            await HttpContext.Authentication.SignOutAsync("MyCookieAuthenticationScheme");
            return RedirectToAction("Login", "Account");
        }
        public async Task<ActionResult> LogOffPage(string returnUrl)
        {
            await HttpContext.Authentication.SignOutAsync("MyCookieAuthenticationScheme");
            return Redirect(".." + returnUrl);
        }

        public ActionResult CurrentUserInfo()
        {
            return new JsonResult(CurrentUser);
        }

        public class LoginModel
        {
            [Required]
            [Display(Name = "User name")]
            public string UserName { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
        }
    }
}
