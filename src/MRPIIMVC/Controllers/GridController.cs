﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Web.Mvc;
using System.IO;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MRPIIMVC.Controllers
{
    public class GridController :Controller
    {
        //brought in with base controller
        private readonly Data.Phoenix db;
        public GridController(Data.Phoenix dbContext)
        {
            db = dbContext;
        }
        // GET: /<controller>/
        public IActionResult EditGrid()
        {            
            return View();
        }
        
        public async Task<IActionResult> getGrid(string name)
        {
            var vm = new Models.Grid.EditGridViewModel();
            await vm.load(name);
            await vm.updateGridFilters();
            return new JsonResult(vm);                        
        }

        public async Task<IActionResult> getGridForViewing(string name)
        {
            var vm = new Models.Grid.EditGridViewModel();
            await vm.load(name);
            await vm.loadUserSetting();
            await vm.updateGridFilters();
            return new JsonResult(vm);
        }

        [HttpPost]
        public async Task<IActionResult> SaveGrid()
        {
            string json = "";
            using (var reader = new StreamReader(Request.Body))
            {
                json = reader.ReadToEnd();
            }
            try
            {
                var vm = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Grid.EditGridViewModel>(json);
                await vm.save();
                return new JsonResult(vm);
            }
            catch (Exception error)
            {
                Response.StatusCode = 400;
                return Content("error" + error.Message);

            }
        }

        [HttpPost]
        public async Task<IActionResult> updateGridFilters()
        {
            string json = "";
            using (var reader = new StreamReader(Request.Body))
            {              
                json = reader.ReadToEnd();
            }
            try
            {
                var vm = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Grid.EditGridViewModel>(json);
                await vm.updateGridFilters();
                return new JsonResult(vm);
            }
            catch (Exception error)
            {
                return Content("error" + error.Message);
            }
            
        }
        [HttpPost]
        public async Task<IActionResult> Search()
        {
            string json = "";
            using (var reader = new StreamReader(Request.Body))
            {
                json = reader.ReadToEnd();
            }
            try
            {
                var vm = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Grid.EditGridViewModel>(json);
                var data = await vm.search();
                return new JsonResult(data);
            }
            catch (Exception error)
            {
                Response.StatusCode = 400;
                return Content("error" + error.Message);

            }
        }

    }
}
