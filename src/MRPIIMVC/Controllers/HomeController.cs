﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;

namespace MRPIIMVC.Controllers
{
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            return View("~/Views/Schedule/Schedule.cshtml");

        }


    }
}
