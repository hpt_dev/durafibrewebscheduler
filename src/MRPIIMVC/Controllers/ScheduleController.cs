﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MRPIIMVC.Models;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Http.Authentication;
using MRPIIMVC.Helpers;
using MRPIIMVC.Models.Account;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MRPIIMVC.Controllers
{
    public class ScheduleController : BaseController
    {

        private string DetermineScheduleByEnvironment()
        {
            switch (hostingEnv.EnvironmentName.ToUpper())
            {
                case ("POPINK"):
                    {
                        return "PopInkSchedule";
                    }
                case ("PPC"):
                    {
                        return "PPCSchedule";
                    }
                case ("NORKOL"):
                    {
                        return "NorkolSchedule";
                    }
                default:
                    return "Schedule";
            }
        }

        private string DetermineReadOnlyScheduleByEnvironment()
        {
            switch (hostingEnv.EnvironmentName.ToUpper())
            {
                case ("POPINK"):
                    {
                        return "PopInkSchedule_ReadOnly";
                    }
                case ("PPC"):
                    {
                        return "PPCSchedule_ReadOnly";
                    }
                case ("NORKOL"):
                    {
                        return "NorkolSchedule_ReadOnly";
                    }
                default:
                    return "Schedule_ReadOnly";
            }
        }

        public IActionResult Schedule()
        {
            return View(DetermineScheduleByEnvironment());
        }

        public IActionResult ReadOnlySchedule()
        {
            return View(DetermineReadOnlyScheduleByEnvironment());
        }

        [ResponseCache(Duration = 60)]
        public async Task<ActionResult> ScheduleWithUser(long UserId)
        {
            try
            {
                await Logger.logInfo(Request.Path + Request.QueryString);
                // used from silver light set user-id by pram and hide base layout.           
                ViewBag.hideSharedLayout = true;
                var userManager = new MRPIIMVC.Models.Account.UserManager();
                var user = userManager.getUserById(UserId);
                var Json = Newtonsoft.Json.JsonConvert.SerializeObject(user);
                user.Name = Json;

                var userPrincipal = new ClaimsPrincipal(user);

                await HttpContext.Authentication.SignInAsync("MyCookieAuthenticationScheme", userPrincipal,
                    new AuthenticationProperties
                    {
                        ExpiresUtc = DateTime.UtcNow.AddDays(30),
                        IsPersistent = true,
                        AllowRefresh = true
                    });

                return View(DetermineScheduleByEnvironment());
            }
            catch
            {

                throw;
            }
        }

        [ResponseCache(Duration = 60)]
        public async Task<ActionResult> ReadOnlyScheduleWithUser(long UserId)
        {
            try
            {
                await Logger.logInfo(Request.Path + Request.QueryString);
                // used from silver light set user-id by pram and hide base layout.           
                ViewBag.hideSharedLayout = true;
                var userManager = new MRPIIMVC.Models.Account.UserManager();
                var user = userManager.getUserById(UserId);
                var Json = Newtonsoft.Json.JsonConvert.SerializeObject(user);
                user.Name = Json;

                var userPrincipal = new ClaimsPrincipal(user);

                await HttpContext.Authentication.SignInAsync("MyCookieAuthenticationScheme", userPrincipal,
                    new AuthenticationProperties
                    {
                        ExpiresUtc = DateTime.UtcNow.AddDays(30),
                        IsPersistent = true,
                        AllowRefresh = true
                    });

                return View(DetermineReadOnlyScheduleByEnvironment());
            }
            catch
            {

                throw;
            }
        }

        public IActionResult HolidayCalendar()
        {
            return View();
        }
        [ResponseCache(Duration = 60)]
        public async Task<ActionResult> HolidayCalendarWithUser(long UserId)
        {
            try
            {
                await Logger.logInfo(Request.Path + Request.QueryString);
                // used from silver light set user-id by pram and hide base layout.           
                ViewBag.hideSharedLayout = true;
                var userManager = new MRPIIMVC.Models.Account.UserManager();
                var user = userManager.getUserById(UserId);
                var Json = Newtonsoft.Json.JsonConvert.SerializeObject(user);
                user.Name = Json;

                var userPrincipal = new ClaimsPrincipal(user);

                await HttpContext.Authentication.SignInAsync("MyCookieAuthenticationScheme", userPrincipal,
                    new AuthenticationProperties
                    {
                        ExpiresUtc = DateTime.UtcNow.AddDays(30),
                        IsPersistent = true,
                        AllowRefresh = true
                    });

                return View("HolidayCalendar");
            }
            catch
            {
                throw;
            }
        }
        [ResponseCache(CacheProfileName = "Never")]
        public IActionResult getTasks()
        {
            var vm = new Models.Schedule.CalendarViewModel();
            return new JsonResult(vm.getAll());
        }
        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> UpdateTask(string userId)
        {
            try
            {
                var task = await ReadFromStream<Models.Schedule.CalendarViewModel.Appointment>();
                var vm = new Models.Schedule.CalendarViewModel();
                return new JsonResult(vm.saveAppointment(task, userId));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }
        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> DeleteTask()
        {
            try
            {
                var task = await ReadFromStream<Models.Schedule.CalendarViewModel.Appointment>();
                var vm = new Models.Schedule.CalendarViewModel();
                return new JsonResult(vm.deleteAppointment(task));
            }
            catch
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(false);
            }

        }






        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> scheduleDFEntry()
        {
            var task = await ReadFromStream<Models.Schedule.ScheduleViewModel.DFScheduleEntry>();
            var vm = new Models.Schedule.ScheduleViewModel();
            return new JsonResult(vm.scheduleDFEntry(task));
        }

        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> updateBlockTime()
        {
            var task = await ReadFromStream<dynamic>();
            var vm = new Models.Schedule.ScheduleViewModel();
            return new JsonResult(vm.updateBlockTime(task));
        }

        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> deleteBlockTime()
        {
            var task = await ReadFromStream<dynamic>();
            var vm = new Models.Schedule.ScheduleViewModel();
            return new JsonResult(vm.deleteBlockTime(task));
        }

        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> clearScheduleJam()
        {
            var vm = new Models.Schedule.ScheduleViewModel();
            return new JsonResult(vm.clearScheduleJam());
        }


        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> newBlockTime()
        {
            var task = await ReadFromStream<dynamic>();
            var vm = new Models.Schedule.ScheduleViewModel();
            return new JsonResult(vm.newBlockTime(task));
        }

        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> saveLeadDays()
        {
            var leadDays = await ReadFromStream<dynamic>();
            var vm = new Models.Schedule.ScheduleViewModel();
            return new JsonResult(vm.saveLeadDays(leadDays));
        }

        

        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> unscheduleDFEntry()
        {
            var task = await ReadFromStream<Models.Schedule.ScheduleViewModel.DFScheduleEntry>();
            var vm = new Models.Schedule.ScheduleViewModel();
            return new JsonResult(vm.unscheduleDFEntry(task));
        }

        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> moveDFEntry()
        {
            var task = await ReadFromStream<dynamic>();
            var vm = new Models.Schedule.ScheduleViewModel();
            return new JsonResult(vm.moveDFEntry(task));
        }

        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> updateFrozen()
        {
            var task = await ReadFromStream<dynamic>();
            var vm = new Models.Schedule.ScheduleViewModel();
            return new JsonResult(vm.updateFrozen(task));
        }

        [ResponseCache(CacheProfileName = "Never")]
        public IActionResult amtech(int machNo)
        {
            try
            {
                var vm = new Models.Schedule.ScheduleViewModel();
                return new JsonResult(vm.amtech(machNo));

            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        [ResponseCache(CacheProfileName = "Never")]
        public IActionResult scheduleGroup(int machNo, string idArrayString)
        {
            try
            {
                var vm = new Models.Schedule.ScheduleViewModel();
                return new JsonResult(vm.scheduleGroup(machNo, idArrayString));

            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        [ResponseCache(CacheProfileName = "Never")]
        public IActionResult saveLamDowntime(int minutes)
        {
            try
            {
                var vm = new Models.Schedule.ScheduleViewModel();
                return new JsonResult(vm.saveLamDowntime(minutes));

            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        [ResponseCache(CacheProfileName = "Never")]
        public IActionResult saveFoamLamDowntime(int minutes)
        {
            try
            {
                var vm = new Models.Schedule.ScheduleViewModel();
                return new JsonResult(vm.saveFoamLamDowntime(minutes));

            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        [ResponseCache(CacheProfileName = "Never")]
        public IActionResult unscheduleGroup(int machNo, string idArrayString)
        {
            try
            {
                var vm = new Models.Schedule.ScheduleViewModel();
                return new JsonResult(vm.unscheduleGroup(machNo, idArrayString));

            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        [ResponseCache(CacheProfileName = "Never")]
        public IActionResult saveConvertingDetails(int machNo, int rate, int setupTime)
        {
            try
            {
                var vm = new Models.Schedule.ScheduleViewModel();
                return new JsonResult(vm.saveConvertingDetails(machNo, rate, setupTime));

            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }


        [ResponseCache(CacheProfileName = "Never")]
        public void refresh()
        {
            var vm = new Models.Schedule.ScheduleViewModel();
            vm.refresh();
        }
        [ResponseCache(CacheProfileName = "Never")]
        public void amtechRefresh()
        {
            var vm = new Models.Schedule.ScheduleViewModel();
            vm.amtechRefresh();
        }

        [ResponseCache(CacheProfileName = "Never")]
        public void refreshSelMachine(int machNo)
        {
            var vm = new Models.Schedule.ScheduleViewModel();
            vm.refreshSelMachine(machNo);
        }

        [ResponseCache(CacheProfileName = "Never")]
        public void orderByDate(int machNo)
        {
            var vm = new Models.Schedule.ScheduleViewModel();
            vm.orderByDate(machNo);
        }

        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> checkUserAccess(long userID)
        {
            try
            {
                var vm = new Models.Account.UserManager();
                return new JsonResult(await vm.checkUserAccess(userID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> getSchedule(long buildingId = -1)
        {

            try
            {
                switch (hostingEnv.EnvironmentName.ToUpper())
                {
                    case "PopInk":
                        {
                            var vm = new Models.Schedule.ScheduleViewModel();
                            return new JsonResult(await vm.getSchedule(5));
                        }

                    default:
                        {
                            var vm = new Models.Schedule.ScheduleViewModel();
                            return new JsonResult(await vm.getSchedule(buildingId));
                        }
                }
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        [ResponseCache(CacheProfileName = "Never")]
        public async Task<IActionResult> getTimeBlockEdit(long blockID)
        {

            try
            {
                var vm = new Models.Schedule.ScheduleViewModel();
                return new JsonResult(await vm.getTimeBlockEdit(blockID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        public IActionResult GetCalendarOptions()
        {
            var vm = new Models.Schedule.CalendarViewModel();
            return new JsonResult(vm.getAppointmentTypes());
        }
        public IActionResult prePress()
        {
            ViewBag.hideSharedLayout = true;
            ViewBag.ReturnUrl = "/schedule/prepress";
            return View();
        }


        public async Task<IActionResult> getStepMaterialAvaliblity(long step)
        {
            try
            {
                var data = await db.getData($"EXEC wo_GetBOMRawMaterialAvailability {step}");
                return new JsonResult(data);

            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }




    }
}
