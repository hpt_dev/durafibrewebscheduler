﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MRPIIMVC.Controllers
{
    public class SecurityController : BaseController
    {
        public IActionResult getPageSecurity(string path)
        {
            var vm = new Models.Security.PageSecurity();
            return new JsonResult(vm.getVM(path));

        }
        public async Task<IActionResult> SaveSecurityPage()
        {
            var page = await ReadFromStream<Models.Security.PageSecurity.pageVM>();
            await page.save();
            return new JsonResult(page);
        }
    }
}
