using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MRPIIMVC.Models;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Http.Authentication;
using MRPIIMVC.Helpers;
using MRPIIMVC.Models.Account;

namespace MRPIIMVC.Controllers
{
    public class ShaftPatternController : BaseController
    {
        #region ADD/COPY
        public async Task<IActionResult> addShaftPattern(decimal masterWidth, decimal jobID, long routeStepID)
        {
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.addShaftPattern(masterWidth, jobID, routeStepID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }
        
        public async Task<IActionResult> copyJobPattern(long shaftPatternID, long jobID){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.copyJobPattern(shaftPatternID, jobID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        public async Task<IActionResult> addShaftPatternCut(long shaftPatternID, decimal cutWidth, string cutType, int cutOrder, long routeStepID, long jobID){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.addShaftPatternCut(shaftPatternID, cutWidth, cutType, cutOrder, routeStepID, jobID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }
        #endregion

        #region GET

        public async Task<IActionResult> getShaftPatternCuts(long patternID){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.getShaftPattern(patternID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

         public async Task<IActionResult> getJobPatterns(long jobID){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.getJobPatterns(jobID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        public async Task<IActionResult> getJobSimilarRouteSteps(long patternID, long routeStepID){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.getJobSimilarRouteSteps(patternID, routeStepID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }
        #endregion

        #region UPDATE

        public async Task<IActionResult> updateShaftPatternCut(long shaftPatternCutID, decimal cutWidth, string cutType, int cutOrder, long routeStepID, long jobID){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.updateShaftPatternCut(shaftPatternCutID, cutWidth, cutType, cutOrder, routeStepID, jobID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }   

        public async Task<IActionResult> updateJobStepQuantity(long jobID, decimal quantity){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.updateJobStepQuantity(jobID, quantity));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        public async Task<IActionResult> updateJobStepNote(long jobID, string note)
        {
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.updateJobStepNote(jobID, note));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        public async Task<IActionResult> updateStepWidth(long jobID, long routeStepID, decimal width){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.updateStepWidth(jobID, routeStepID, width));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }     

        public async Task<IActionResult> updateShaftPattern(long jobID, long shaftPatternID, bool isDefault, bool canChangeOnFloor, int numberOfSets){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.updateShaftPattern(jobID, shaftPatternID, isDefault, canChangeOnFloor, numberOfSets));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }


        #endregion

       #region DELETE

        public async Task<IActionResult> deleteJobPattern(long shaftPatternID, long jobID){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.deleteJobPattern(shaftPatternID, jobID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

         public async Task<IActionResult> deleteShaftPatternCut(long shaftPatternCutID){
            try
            {
                var vm = new Models.Schedule.ShaftPatterns.ShaftPatternsViewModel();
                return new JsonResult(await vm.deleteShaftPatternCut(shaftPatternCutID));
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 404;
                return new JsonResult(e);
            }
        }

        #endregion
    }
}
