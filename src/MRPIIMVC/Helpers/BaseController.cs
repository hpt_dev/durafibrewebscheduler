﻿using System;
using Data;
using System.IO;
using MRPIIMVC.Helpers;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Hosting;

namespace Microsoft.AspNetCore.Mvc
{
    public class BaseController : Controller
    {
        public MRPIIMVC.Models.Account.User CurrentUser
        {
            get
            {
                try
                {
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<MRPIIMVC.Models.Account.User>(System.Web.HttpContext.Current.User.Identity.Name);
                }
                catch (Exception)
                {
                    try // get user id from header referal page silverlight ajax methods without cookies
                    {
                        var refer = HttpContext.Request.Headers["Referer"][0];
                        refer = refer.Substring(refer.IndexOf('?')+1);
                        var prams = refer.Split('&');
                        var user = prams.FirstOrDefault(x => x.ToLower().StartsWith("userid"));
                        var id = user.Split('=')[1];
                        var um = new MRPIIMVC.Models.Account.UserManager();
                        return um.getUserById(long.Parse(id));
                    }
                    catch (Exception)
                    {
                        
                    }
                    return null;
                }
            }
        }

        public Data.Phoenix db
        {
            get
            {
                return (Phoenix)System.Web.HttpContext.Current.RequestServices.GetService(typeof(Phoenix));
            }
        }
        
        public static IHostingEnvironment hostingEnv
        {
            get
            {
                return (IHostingEnvironment)System.Web.HttpContext.Current.RequestServices.GetService(typeof(IHostingEnvironment));
            }
        }
        public async Task<T> ReadFromStream<T>()
        {
            string json = "";
            using (var reader = new StreamReader(Request.Body))
            {
                json = reader.ReadToEnd();
            }
            try
            {
                await Logger.log(Request.Path + Request.QueryString + ": " + json, Logger.MessageType.Information);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
            }
            catch
            {
                return default(T);
            }
        }
    }
}
