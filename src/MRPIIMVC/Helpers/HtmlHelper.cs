﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MRPIIMVC.Helpers
{
    public static class MyExtensionMethods
    {
        public static bool IsDebug(this IHtmlHelper htmlHelper)
        {
            #if DEBUG
                return true;
            #else
               return false;
            #endif
        }
    }
}
