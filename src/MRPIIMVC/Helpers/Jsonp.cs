﻿namespace System.Web.Mvc
{
    using System;
    using System.Text;
    using Threading.Tasks;
    using System.Web;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json.Converters;

    public class JsonpResult : ActionResult
    {

        public JsonpResult()
        {
        }
        public JsonpResult(object data)
        {
            Data = data;
        }
        public Encoding ContentEncoding  {get; set; }

        public string ContentType {get; set;}

        public object Data  { get;  set;}

        public string JsonCallback { get; set; }



        //public override void ExecuteResult(ControllerContext context)
        //{
        //    if (context == null)
        //    {
        //        throw new ArgumentNullException("context");
        //    }

        //    this.JsonCallback = context.HttpContext.Request["jsoncallback"];

        //    if (string.IsNullOrEmpty(this.JsonCallback))
        //        this.JsonCallback = context.HttpContext.Request["callback"];

        //    if (string.IsNullOrEmpty(this.JsonCallback))
        //        throw new ArgumentNullException("JsonCallback required for JSONP response.");

        //    HttpResponseBase response = context.HttpContext.Response;

        //    if (!String.IsNullOrEmpty(ContentType))
        //    {
        //        response.ContentType = ContentType;
        //    }
        //    else
        //    {
        //        response.ContentType = "application/json";
        //    }
        //    if (ContentEncoding != null)
        //    {
        //        response.ContentEncoding = ContentEncoding;
        //    }
        //    if (Data != null)
        //    {
        //        JavaScriptSerializer serializer = new JavaScriptSerializer();
        //        response.Write(string.Format("{0}({1});", this.JsonCallback, serializer.Serialize(Data)));
        //    }
        //}

        public async override Task ExecuteResultAsync(ActionContext context)
        {
            
            var a = context.HttpContext.Request;
            // context.HttpContext.Request
            context.HttpContext.Response.ContentType = "application/json";
            this.JsonCallback = context.HttpContext.Request.Query["callback"];
            if (string.IsNullOrEmpty(this.JsonCallback))
                throw new ArgumentNullException("JsonCallback required for JSONP response.");
            

                byte[] data = Encoding.UTF8.GetBytes(string.Format("{0}({1});", this.JsonCallback,  Newtonsoft.Json.JsonConvert.SerializeObject(Data)));
            await context.HttpContext.Response.Body.WriteAsync(data, 0, data.Length);
            
            
        }
    }

    //extension methods for the controller to allow jsonp.
    public static class ContollerExtensions
    {
        public static JsonpResult Jsonp(this Controller controller, object data)
        {
            JsonpResult result = new JsonpResult();
            result.Data = data;
            return result;
        }
    }
}