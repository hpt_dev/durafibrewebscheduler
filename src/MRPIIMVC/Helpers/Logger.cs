﻿using Data;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MRPIIMVC.Helpers
{
    public static class Logger
    {
        public enum MessageType
        {
            Exception = 1,
            LoginFalure = 2,
            TimeOut = 3,
            LogIn = 4,
            LogOut = 5,
            Information = 6,
            SQLErrror = 7,
            UIEvent = 8, 
            SQL_Information = 9,
            ObjectLifeCycle= 10
        }
        public static Data.Phoenix db
        {
            get
            {
                return (Phoenix)System.Web.HttpContext.Current.RequestServices.GetService(typeof(Phoenix));
            }
        }
        public static MRPIIMVC.Models.Account.User CurrentUser
        {
            get
            {
                try
                {
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<MRPIIMVC.Models.Account.User>(System.Web.HttpContext.Current.User.Identity.Name);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
        public static async Task<bool> log(string data, MessageType type)
        {
            long? id = null;
            if (CurrentUser != null)
                id = CurrentUser.id;
            return await log(data, type, id);            
        }

        public static async Task<bool> logInfo(string data)
        {
            return await log(data, MessageType.Information);
        }
        public static async Task<bool> logError(string data)
        {
            return await log(data, MessageType.Exception);
        }
        public static async Task<bool> log(string data, MessageType type, long? userId)
        {
            try
            {
                var message = db.ad_application_messages.Create();
                db.ad_application_messages.Add(message);
                message.application_id = "MRPII";
                message.message_type_id = (int)type;
                message.message = data;
                message.message_time = DateTime.Now;
                message.user_id = userId;
                await db.SaveChangesAsync();
                return true;
            }
            catch
            {           
                return false;
            }
        }
    }
}
