﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Hosting;

namespace MRPIIMVC.TagHelpers
{
    public class SecurityTagHelper : TagHelper
    {

        private Microsoft.AspNetCore.Hosting.IHostingEnvironment hostingEnv { get
            {
                return (IHostingEnvironment)System.Web.HttpContext.Current.RequestServices.GetService(typeof(IHostingEnvironment));
            } }
        public Models.Account.User CurrentUser
        {
            get
            {
                try
                {
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Account.User>(System.Web.HttpContext.Current.User.Identity.Name);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
        public string Tag { get; set; }
        public string Id { get; set; }   
        public string Page { get; set; }   
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            var cx = System.Web.HttpContext.Current;
            var path = Page == "" ? cx.Request.Path.ToString() : Page;
            var page = Models.Security.PageSecurity.getPage(path);
            if (page == null)
                return;
            var ele = page.secElements.FirstOrDefault(x => x.id == Id);
            if(ele == null)
            {
                ele = new Models.Security.PageSecurity.Page.SecurityElement();
                ele.id = Id;
                page.secElements.Add(ele);
            }
            var securityGroups = new List<long>();
            if (CurrentUser != null)
                securityGroups.AddRange(CurrentUser.securityGroups.Select(x => x.id).ToList());
            var matchingRule = ele.rules.Where(x => (x.companyies.Contains(hostingEnv.EnvironmentName) && x.match )
                                                  ||(!x.companyies.Contains(hostingEnv.EnvironmentName) && !x.match)
                                                  ||(x.securityGroups.Any(s=> securityGroups.Contains( s)) && x.match)
                                                  ||(!x.securityGroups.Any(s => securityGroups.Contains(s)) && !x.match)
            );
            var content = await output.GetChildContentAsync();
            ele.content = content.GetContent();
            if (matchingRule.Any(x => x.hide))
            {
                output.SuppressOutput();
                output.TagName = "Security";
            }
            else
            {
                var classes = output.Attributes.FirstOrDefault(a => a.Name == "class")?.Value;
                output.Attributes.SetAttribute("class", $"security {classes}");
                output.Attributes.SetAttribute("id", Id);
                output.TagName = Tag;
                if (Id == null)
                    output.PreContent.AppendHtml($"<span style=\"background-color:pink\" >Missing ID for {Tag} </span> ");
            }
           // output.Content.AppendHtml($"<{Tag}>{content.GetContent() }</{Tag}>");
            

           // return base.ProcessAsync(context, output);
        }

    }
}
