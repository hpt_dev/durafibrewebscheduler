﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace MRPIIMVC.Models.Account 
{
    public class User : IIdentity
    {
        public string Name { get; set; }
        public string AuthenticationType { get; set; }
        public bool IsAuthenticated { get; set; }
        //end IIdenty requirements

        public long id { get; set; }
        public string altUserId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }
        public long languageId { get; set; }
        public string email { get; set; }
        public bool isActive { get; set; }
        public int? buildingId { get; set; }
        public bool? isGlobal { get; set; }
        public List<Building> buildings { get; set; }
        public List<SecurityGroup> securityGroups { get; set; }

        

        public class Building
        {
            public string name { get; set; }
            public long id { get; set; }           
            public long parentBuildingId { get; set; }
        }
        public class SecurityGroup
        {
            public long id { get; set; }
            public string name { get; set; }
        }
    }
    
}
