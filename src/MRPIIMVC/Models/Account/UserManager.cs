﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using MRPIIMVC.Models;
using Data;
using System.Data.Entity;

namespace MRPIIMVC.Models.Account
{
    public class UserManager :ViewModel
    {
       
        public User login(string userName, string pass)
        {
            var user = new User();
            var hash = GenerateHash(pass);

            user = db.ad_users.Include(x => x.cs_buildings).Include(x => x.ad_security_groups).Where(x => x.is_active && x.USER_NAME.ToLower() == userName.ToLower() && x.password == hash).Select(n => new User {
                userName = n.USER_NAME,
                id = n.USER_ID,
                altUserId = n.alt_user_id,
                buildingId = n.building_id,
                buildings = n.cs_buildings.Select(b => new User.Building {
                    id = b.building_id,
                    name = b.building_name,
                    parentBuildingId = b.building_id_rk,
                }).ToList(),
                email = n.user_email,
                firstName = n.first_name,
                lastName = n.last_name,
                isGlobal = n.global_user,
                isActive = n.is_active,
                languageId = n.language_id,
                securityGroups = n.ad_security_groups.Select(g => new User.SecurityGroup {
                    id = g.security_group_id, 
                    name = g.group_name
                }).ToList()
           }).FirstOrDefault();


            return user;
        }

        private string GenerateHash(string password)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] tmpSource;
            byte[] tmpHash;
            StringBuilder sOutput = new StringBuilder();

            // Turn password into byte array
            tmpSource = ASCIIEncoding.ASCII.GetBytes(password);
            tmpHash = md5.ComputeHash(tmpSource);

            StringBuilder sOuput = new StringBuilder(tmpHash.Length);
            for (int i = 0; i < tmpHash.Length; i++)
            {
                // X2 formats to hexadecimal
                sOutput.Append(tmpHash[i].ToString("X2"));
            }
            return sOutput.ToString();
        }

        public User getUserById(long id)
        {
            var user = new User();
            
            user = db.ad_users.Include(x => x.cs_buildings).Include(x => x.ad_security_groups).Where(x => x.USER_ID == id).Select(n => new User
            {
                userName = n.USER_NAME,
                id = n.USER_ID,
                altUserId = n.alt_user_id,
                buildingId = n.building_id,
                buildings = n.cs_buildings.Select(b => new User.Building
                {
                    id = b.building_id,
                    name = b.building_name,
                    parentBuildingId = b.building_id_rk,
                }).ToList(),
                email = n.user_email,
                firstName = n.first_name,
                lastName = n.last_name,
                isGlobal = n.global_user,
                isActive = n.is_active,
                languageId = n.language_id,
                securityGroups = n.ad_security_groups.Select(g => new User.SecurityGroup
                {
                    id = g.security_group_id,
                    name = g.group_name
                }).ToList()
            }).FirstOrDefault();


            return user;
        }


        public async Task<bool> checkUserAccess(long id)
        {
            try
            {
                var ret = await db.getData($"exec ad_CheckUserAccess '{id}', {"Scheduling"}");
                
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
