﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Data;
using System;
using System.Text.RegularExpressions;

namespace MRPIIMVC.Models.Grid
{
    public class EditGridViewModel : ViewModel
    {
        public GridInfo gridInfo { get; set; }
        public List<GridColumn> gridColumns { get; set; }
        public List<GridFilter> gridFilters { get; set; }
        public List<GridFilter.ContitionType> filterConditions { get; set; }
        public List<GridColor> gridColors { get; set; }
        public List<GridUserColumn> gridUserColumns { get; set; }
        public List<GridUserFilter> gridUserFilters { get; set; }

        public async Task<bool> load(string name)
        {
            var grid = await db.cs_grids
                .Include(x => x.cs_grid_filter_fields)
                .Include(x => x.cs_grid_fields)
                .Include(x => x.cs_grid_filter_fields.Select(y => y.ps_tables))
                .Where(x => x.grid_name == name).FirstOrDefaultAsync();

            //var grid = db.cs_grids.Where(x => x.grid_name == name).FirstOrDefault();
            gridInfo = new GridInfo();
            gridInfo.id = grid.grid_id;
            gridInfo.name = grid.grid_name;
            gridInfo.isVisible = grid.is_visible;
            gridInfo.isActive = grid.is_active;
            gridInfo.isDistinct = grid.is_distinct;
            gridInfo.baseQueryText = grid.base_query_text;
            gridInfo.baseQuerySort = grid.base_sort_text;
            gridInfo.maxRows = grid.max_rows;
            gridInfo.pageSize = grid.page_size;
            gridInfo.usePaging = grid.use_paging;
            gridInfo.autoSelectFirstRow = grid.auto_select_first_row;
            //gridColumns = db.cs_grid_fields.Where(x => x.grid_id == grid.grid_id).Select(n => new GridColumn {
            gridColumns = grid.cs_grid_fields.Select(n => new GridColumn
            {
                id = n.grid_field_id,
                fieldName = n.field_name,
                customField = n.custom_field,
                textId = n.text_id,
                disporder = n.disporder,
                gridName = n.grid_name,
                tableId = n.table_id,
                isVisible = n.is_visible,
                isActive = n.is_active,
                gridId = n.grid_id,
                includeSummary = n.include_summary,
                customColumnSql = n.custom_column_sql,
                requiredByCode = n.required_by_code,
                groupBy = n.groupby,
                formatString = n.format_string
            }).ToList();
            //gridFilters = db.cs_grid_filter_fields.Where(x => x.grid_id == grid.grid_id).Select(n => new GridFilter {
            gridFilters = grid.cs_grid_filter_fields.Select(n => new GridFilter
            {
                id = n.grid_filter_field_id,
                gridId = n.grid_id,
                disporder = n.disporder,
                controlType = n.control_type_id,
                controlSource = n.control_source,
                validation = n.validation,
                validationText = n.validation_text,
                isRequired = n.is_required,
                defaultValue = n.default_value,
                inputMask = n.input_mask,
                conditionTypeId = n.condition_type_id,
                isVisible = n.is_visible,
                isActive = n.is_active,
                tableId = n.table_id,
                fieldName = n.field_name,
                customFilter = n.custom_filter,
                doNotGenerateWhere = n.do_not_generate_where,
                textId = n.text_id,
                table = n.ps_tables == null ? "" : n.ps_tables.table_name
            }).ToList();
            var t = gridColumns.Select(y => y.tableId).ToList();
            var tables = db.ps_tables.Where(x => t.Contains(x.table_id)).ToList();
            gridColumns.ForEach(x => x.table = tables.FirstOrDefault(y => y.table_id == x.tableId) == null ? "" : x.table = tables.FirstOrDefault(y => y.table_id == x.tableId).table_name);
            var textIds = gridColumns.Select(x => x.textId).ToList().Union(gridFilters.Select(x => x.textId).ToList()).ToList();
            var trans = db.ad_translations.Where(x => textIds.Contains(x.text_id)).ToList();
            foreach (var c in gridColumns)
            {
                c.text = trans.FirstOrDefault(x => x.text_id == c.textId) == null ? "" : trans.FirstOrDefault(x => x.text_id == c.textId).translation;
            }
            foreach (var f in gridFilters)
            {
                f.text = trans.FirstOrDefault(x => x.text_id == f.textId) == null ? "" : trans.FirstOrDefault(x => x.text_id == f.textId).translation;
                f.value = replaceTagWithValues(f.defaultValue);
            }
           

            filterConditions = await db.cs_grid_condition_types.Select(x => new GridFilter.ContitionType { name = x.condition_name, id = x.condition_type_id }).ToListAsync();

            gridColors = await db.cs_grid_colors.Where(x => x.is_active && x.grid_id == gridInfo.id).Select(n => new GridColor
            {
                columnId = n.grid_field_id,
                color = n.argb_color,
                matchOrder = n.match_order,
                userColor = false,
                value = n.value
            }).ToListAsync();            
            return true;
        }
        public async Task<bool> loadUserSetting()
        {
            gridColors.AddRange(db.cs_grid_user_colors.Where(x => x.grid_id == gridInfo.id && x.user_id == CurrentUser.id && x.is_active).Select(n => new GridColor
            {
                columnId = n.grid_field_id,
                color = n.argb_color,
                matchOrder = n.match_order,
                userColor = true,
                value = n.value
            }).ToList());
            gridUserColumns = await db.cs_grid_user_fields.Where(x => x.grid_id == gridInfo.id && x.user_id == CurrentUser.id && x.is_active).Select(n => new GridUserColumn {
                columnId = n.grid_field_id,
                displayOrder = n.disporder,
                isVisible = n.is_visible,
            }).ToListAsync();
            gridUserFilters = await db.cs_grid_user_filter_fields.Where(x => x.grid_id == gridInfo.id && x.user_id == CurrentUser.id).Select(n => new GridUserFilter {
               filterId = n.grid_filter_field_id, 
               conditionTypeId = n.condition_type_id, 
               defaultValue = n.default_value, 
               disporder = n.disporder, 
               isVisible = n.is_visible
            }).ToListAsync();
            foreach(var c in gridUserColumns)
            {
                var col = gridColumns.FirstOrDefault(x => x.id == c.columnId);
                if (col != null)
                {
                    col.isVisible = c.isVisible;
                    col.disporder = c.displayOrder;                   
                }
            }
            foreach (var f in gridUserFilters)
            {
                var filter = gridFilters.FirstOrDefault(x => x.id == f.filterId);
                if (filter != null)
                {
                    filter.isVisible = f.isVisible;
                    filter.disporder = f.disporder;
                    filter.defaultValue = f.defaultValue;
                    filter.conditionTypeId = f.conditionTypeId;
                }
            }
            return true;

        }
        public enum GridFilterControlType
        {
            Textbox,
            ComboBox,
            CheckBox,
            DatePicker
        }
        public class SearchResult
        {
            public string sql { get; set; }
            public List<Dictionary<string, object>> data { get; set; }
            public string error { get; set; }
        }

        public async Task<SearchResult> search()
        {
            var ret = new SearchResult();
            var qb = new OSMRP1.QueryBuilder.SelectQueryBuilder();
            qb.BaseQueryText = gridInfo.baseQueryText;
            qb.BaseSortText = gridInfo.baseQuerySort ?? "";
            qb.TopRecords = gridInfo.maxRows;
            qb.Distinct = gridInfo.isDistinct;
            try
            {
                foreach (var column in gridColumns.Where(x => x.isActive))
                {
                    if (string.IsNullOrEmpty(column.customColumnSql))
                    {
                        qb.AddColumn(new OSMRP1.QueryBuilder.Clauses.SelectColumn(column.table, column.fieldName, column.text));
                    }
                    else
                    {
                        OSMRP1.QueryBuilder.Clauses.SelectColumn newCol = new OSMRP1.QueryBuilder.Clauses.SelectColumn
                        {
                            CustomColumnSQL = column.customColumnSql,
                            Caption = column.text
                        };
                        qb.AddColumn(newCol);
                    }
                    if (column.groupBy)
                        qb.GroupBy(column.fieldName);
                }

                foreach (var filter in gridFilters.Where(x => x.isActive.Value))
                {
                    if (filter.value != null && filter.value.ToString() != "" && filter.doNotGenerateWhere == false)
                    {
                        if (filter.value != null && filter.value.ToString().Trim() != ""
                            && !((GridFilterControlType)filter.controlType == GridFilterControlType.ComboBox && filter.value.ToString() == "-1"))
                        {
                            if (!string.IsNullOrEmpty(filter.customFilter)) // .custom_filter_with_tags))
                            {
                                qb.AddWhere(new OSMRP1.QueryBuilder.Clauses.WhereClause(replaceTagWithValues( filter.customFilter), replaceTagWithValues(filter.value)));
                            }
                            else
                            {
                                qb.AddWhere(new OSMRP1.QueryBuilder.Clauses.WhereClause(filter.table + "." + filter.fieldName, (OSMRP1.QueryBuilder.Enums.eSQLComparisonType)filter.conditionTypeId, filter.value));
                            }
                        }
                    }
                }
                ret.sql = qb.BuildQuery();
            }
            catch 
            {
                ret.error = "Failed to build query";
                return ret;
            }
            try
            {
                ret.data = await db.getData(ret.sql);
            }
            catch (Exception)
            {
                ret.error = "Failed to search";
            }
            return ret;
        }
        public async Task<bool> save()
        {
            // get translations
            var texts = gridColumns.Select(x => x.text).ToList().Union(gridFilters.Select(x => x.text)).ToList();
            var translations = db.ad_translations.Where(x => texts.Contains(x.translation)).ToList();
            var missing = texts.Where(x => !translations.Any(y => y.translation == x) && x != "").ToList();
            foreach (var t in missing)
            {
                var tran = db.ad_translations.Create();
                tran.text_id = db.ad_translations.Max(x => x.text_id) + 1;
                tran.translation = t;
                tran.language_id = 1;
                db.ad_translations.Add(tran);
                db.SaveChanges();
            }
            if (missing.Count > 0)
            {
                translations = db.ad_translations.Where(x => texts.Contains(x.translation)).ToList();
            }
            // update grid
            var dbGrid = await db.cs_grids
                .Include(x => x.cs_grid_filter_fields)
                .Include(x => x.cs_grid_fields)
                .Include(x => x.cs_grid_filter_fields.Select(y => y.ps_tables))
                .Where(x => x.grid_name == gridInfo.name).FirstOrDefaultAsync();
            dbGrid.is_visible = gridInfo.isVisible;
            dbGrid.is_active = gridInfo.isActive;
            dbGrid.is_distinct = gridInfo.isDistinct;
            dbGrid.base_query_text = gridInfo.baseQueryText;
            dbGrid.base_sort_text = gridInfo.baseQuerySort;
            dbGrid.max_rows = gridInfo.maxRows;
            dbGrid.page_size = gridInfo.pageSize;
            dbGrid.use_paging = gridInfo.usePaging;
            dbGrid.auto_select_first_row = gridInfo.autoSelectFirstRow;
            foreach (var c in gridColumns)
            {
                var dbColumn = dbGrid.cs_grid_fields.FirstOrDefault(x => x.grid_field_id == c.id);
                if (dbColumn == null)
                {
                    dbColumn = db.cs_grid_fields.Create();
                    dbGrid.cs_grid_fields.Add(dbColumn);
                    dbColumn.grid_id = dbGrid.grid_id;
                    dbColumn.grid_name = dbGrid.grid_name;
                    dbColumn.created_by = 0;
                    dbColumn.created_date = System.DateTime.Now;
                    dbColumn.last_modified_by = 0;
                    dbColumn.last_modified_date = System.DateTime.Now;
                }
                dbColumn.custom_column_sql = c.customColumnSql;
                dbColumn.custom_field = c.customField;
                dbColumn.disporder = c.disporder;
                dbColumn.field_name = c.fieldName;
                dbColumn.format_string = c.formatString;
                dbColumn.groupby = c.groupBy;
                dbColumn.include_summary = c.includeSummary;
                dbColumn.is_active = c.isActive;
                dbColumn.is_visible = c.isVisible;
                dbColumn.required_by_code = c.requiredByCode;
                dbColumn.table_id = c.tableId;
                if (c.text != "")
                    dbColumn.text_id = translations.FirstOrDefault(x => x.translation == c.text).text_id;
            }
            foreach (var f in gridFilters)
            {
                var dbFilter = dbGrid.cs_grid_filter_fields.FirstOrDefault(x => x.grid_filter_field_id == f.id);
                if (dbFilter == null)
                {
                    dbFilter = db.cs_grid_filter_fields.Create();
                    dbGrid.cs_grid_filter_fields.Add(dbFilter);
                    dbFilter.grid_id = dbGrid.grid_id;
                    dbFilter.created_by = 0;
                    dbFilter.created_date = System.DateTime.Now;
                    dbFilter.last_modified_by = 0;
                    dbFilter.last_modified_date = System.DateTime.Now;
                }
                dbFilter.condition_type_id = f.conditionTypeId;
                dbFilter.control_source = f.controlSource;
                dbFilter.control_type_id = f.controlType;
                dbFilter.custom_filter = f.customFilter;
                dbFilter.default_value = f.defaultValue;
                dbFilter.disporder = f.disporder;
                dbFilter.do_not_generate_where = f.doNotGenerateWhere;
                dbFilter.field_name = f.fieldName;
                dbFilter.input_mask = f.inputMask;
                dbFilter.is_active = f.isActive;
                dbFilter.is_required = f.isRequired;
                dbFilter.is_visible = f.isVisible;
                dbFilter.table_id = f.tableId;
                if (f.text != "")
                    dbFilter.text_id = translations.FirstOrDefault(x => x.translation == f.text).text_id;
                dbFilter.validation = f.validation;
                dbFilter.validation_text = f.validationText;
            }

            try
            {
                db.SaveChanges();
            }
            catch
            {

                throw;
            }


            return true;
        }

        public async Task<bool> updateGridFilters()
        {
            var tasks = new List<Task>();
            foreach (var f in gridFilters.Where(x => x.isActive.Value))
            {
                if (f.controlSource != null && f.controlSource != "")
                {
                    tasks.Add(f.updateCollection(this));
                }
            }
            foreach (var t in tasks) await t;
            return true;
        }



        public class GridInfo
        {
            public long id { get; set; }
            public string name { get; set; }
            public bool isVisible { get; set; }
            public bool isActive { get; set; }
            public bool isDistinct { get; set; }
            public string baseQueryText { get; set; }
            public string baseQuerySort { get; set; }
            public int maxRows { get; set; }
            public int pageSize { get; set; }
            public bool usePaging { get; set; }
            public bool autoSelectFirstRow { get; set; }
        }
        public class GridColumn
        {
            public long id { get; set; }
            public string fieldName { get; set; }
            public bool customField { get; set; }
            public long textId { get; set; }
            public string text { get; set; }
            public int disporder { get; set; }
            public string gridName { get; set; }
            public int? tableId { get; set; }
            public bool isVisible { get; set; }
            public bool isActive { get; set; }
            public long? gridId { get; set; }
            public bool includeSummary { get; set; }
            public string customColumnSql { get; set; }
            public bool requiredByCode { get; set; }
            public bool groupBy { get; set; }
            public string formatString { get; set; }
            public string table { get; set; }
        }
        public class GridColor
        {
            public long columnId { get; set; }
            public string value { get; set; }
            public string color { get; set; }
            public bool userColor { get; set; }
            public int matchOrder { get; set; }
        }
        public class GridUserColumn
        {
            public long columnId { get; set; }
            public int displayOrder { get; set; }
            public bool isVisible { get; set; }
        }
        public class GridUserFilter
        {
            public long filterId { get; set; }
            public int disporder { get; set; }
            public int conditionTypeId { get; set; }
            public string defaultValue { get; set; }
            public bool isVisible { get; set; }
        }
        public class GridFilter
        {
            public long id { get; set; }
            public long gridId { get; set; }
            public long textId { get; set; }
            public string text { get; set; }
            public int disporder { get; set; }
            public int controlType { get; set; }
            public string controlSource { get; set; }
            public string validation { get; set; }
            public string validationText { get; set; }
            public bool isRequired { get; set; }
            public string defaultValue { get; set; }
            public string inputMask { get; set; }
            public int conditionTypeId { get; set; }
            public bool? isVisible { get; set; }
            public bool? isActive { get; set; }
            public int tableId { get; set; }
            public string fieldName { get; set; }
            public string customFilter { get; set; }
            public bool doNotGenerateWhere { get; set; }
            public string table { get; set; }
            public List<SourceCollection> sourceCollection { get; set; }
            public string value { get; set; }
            public async Task<bool> updateCollection(EditGridViewModel vm)
            {
                try
                {
                    var source = await db.getData(vm.replaceTagWithValues(controlSource));
                    if (value == null && source != null)
                    {
                        var a = source.FirstOrDefault();
                        if(a != null)
                            value = a["ID"].ToString();

                    }
                    var temp = Newtonsoft.Json.JsonConvert.SerializeObject(source);
                    sourceCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SourceCollection>>(temp);
                }
                catch
                {
                    value = "error";
                    //throw;
                }
                return true;
            }
            public class SourceCollection
            {
                public string Name { get; set; }
                public string ID { get; set; }
            }
            public class ContitionType
            {
                public string name { get; set; }
                public int id { get; set; }
            }
        }
        public string replaceTagWithValues(string input)
        {
            if (input == null)
                return null;
            string ret = input;
            string TAGREGEX = @"\{(?<type>Value|Caption|User|Param)(OR\[(?<OR>(.*?))\])?:(?<field>(.*?))\}";
            Regex regexTag = new Regex(TAGREGEX, RegexOptions.IgnoreCase);
            foreach (Match match in regexTag.Matches(ret))
            {
                if (match.Groups["type"].Success && match.Groups["field"].Success)
                {
                    var type = match.Groups["type"].ToString();
                    var field = match.Groups["field"].ToString();
                    if (type == "user" && field.ToLower() == "userid")
                        ret = ret.Replace(match.Value, CurrentUser == null ? "-5" : CurrentUser.id.ToString());
                    if (type == "user" && field.ToLower() == "buildingid")
                        ret = ret.Replace(match.Value, CurrentUser == null ? "-5" : CurrentUser.buildingId.ToString());
                    if (type == "user" && field.ToLower() == "globaluser")
                        ret = ret.Replace(match.Value, CurrentUser == null ? "0" : (CurrentUser.isGlobal == true ? "1" : "0"));
                    if (type == "value")
                    {
                        var f = gridFilters.FirstOrDefault(x => x.id.ToString() == field);
                        if (f == null)
                        {
                            var ore = match.Groups["or"].ToString();
                            ret = ret.Replace(match.Value, "");
                        }
                        else
                        {
                            ret = ret.Replace(match.Value, f.value);
                        }
                    }

                }

            }
            return ret;
        }
    }
}
