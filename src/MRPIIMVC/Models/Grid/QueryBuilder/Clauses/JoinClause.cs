using System;
using OSMRP1.QueryBuilder.Enums;

namespace OSMRP1.QueryBuilder.Clauses
{
    /// <summary>
    /// Represents a JOIN clause to be used with SELECT statements
    /// </summary>
    public struct JoinClause
    {
        public JoinType JoinType;
        public string FromTable;
        public string FromColumn;
        public eSQLComparisonType ComparisonOperator;
        public string ToTable;
        public string ToColumn;

        public JoinClause(JoinType join, string toTableName, string toColumnName, eSQLComparisonType @operator, string fromTableName, string fromColumnName)
        {
            JoinType = join;
            FromTable = fromTableName;
            FromColumn = fromColumnName;
            ComparisonOperator = @operator;
            ToTable = toTableName;
            ToColumn = toColumnName;
        }
    }
}