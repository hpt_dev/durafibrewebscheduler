﻿using System;

namespace OSMRP1.QueryBuilder.Clauses
{
    public class SelectColumn
    {
        public string TableName { get; set; }

        public string FieldName { get; set; }

        public string Caption { get; set; }

        public string CustomColumnSQL { get; set; }

        public SelectColumn()
        {
        }

        public SelectColumn(string fieldName)
            : this("", fieldName)
        {
        }

        public SelectColumn(string tableName, string fieldName)
        {
            TableName = tableName;
            FieldName = fieldName;
        }

        public SelectColumn(string tableName, string fieldName, string caption)
            : this(tableName, fieldName)
        {
            if (!String.IsNullOrWhiteSpace(caption))
            {
                Caption = caption;
            }
        }

        public override string ToString()
        {
            string fullString = "";

            if (!String.IsNullOrWhiteSpace(CustomColumnSQL))
            {
                fullString = CustomColumnSQL;
            }
            else
            {
                if (TableName != null && TableName.Trim() != "")
                {
                    fullString = String.Format("[{0}]", TableName);
                }

                if (FieldName != null && FieldName.Trim() != "")
                {
                    if (fullString.Trim() != "")
                    {
                        fullString = fullString + ".";
                    }
                    fullString = fullString + String.Format("[{0}]", FieldName);
                }
            }
            if (fullString != null && fullString.Trim() != "" && Caption != null && Caption.Trim() != "")
            {
                fullString = fullString + String.Format(" AS [{0}]", Caption);
            }

            return fullString;
        }
    }
}