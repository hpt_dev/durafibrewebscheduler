using System;
using OSMRP1.QueryBuilder.Enums;

namespace OSMRP1.QueryBuilder.Clauses
{
    /// <summary>
    /// Represents a TOP clause for SELECT statements
    /// </summary>
    public struct TopClause
    {
        public int Quantity;
        public TopUnit Unit;

        public TopClause(int nr)
        {
            Quantity = nr;
            Unit = TopUnit.Records;
        }

        public TopClause(int nr, TopUnit aUnit)
        {
            Quantity = nr;
            Unit = aUnit;
        }

        public override string ToString()
        {
            string clause = "";
            clause += "TOP " + Quantity;
            if (Unit == TopUnit.Percent)
            {
                clause += " PERCENT";
            }
            return clause;
        }
    }
}