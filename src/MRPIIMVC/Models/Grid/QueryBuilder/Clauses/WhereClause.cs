using System;
using System.Collections.Generic;
using OSMRP1.QueryBuilder.Enums;

namespace OSMRP1.QueryBuilder.Clauses
{
    public enum eWhereClauseSource
    {
        Standard,
        Custom
    }

    /// <summary>
    /// Represents a WHERE clause on 1 database column, containing 1 or more comparisons on
    /// that column, chained together by logic operators: eg (UserID=1 or UserID=2 or UserID>100)
    /// This can be achieved by doing this:
    /// WhereClause myWhereClause = new WhereClause("UserID", Comparison.Equals, 1);
    /// myWhereClause.AddClause(LogicOperator.Or, Comparison.Equals, 2);
    /// myWhereClause.AddClause(LogicOperator.Or, Comparison.GreaterThan, 100);
    /// </summary>
    public struct WhereClause
    {
        private string _fieldName;
        private eSQLComparisonType _comparisonOperator;
        private object _value;
        private string _customFilter;
        private eWhereClauseSource _source;

        internal struct SubClause
        {
            public LogicOperator LogicOperator;
            public eSQLComparisonType ComparisonOperator;
            public object Value;

            public SubClause(LogicOperator logic, eSQLComparisonType compareOperator, object compareValue)
            {
                LogicOperator = logic;
                ComparisonOperator = compareOperator;
                Value = compareValue;
            }
        }

        internal List<SubClause> SubClauses;	// Array of SubClause

        /// <summary>
        /// Gets/sets the name of the database column this WHERE clause should operate on
        /// </summary>
        public string FieldName
        {
            get { return _fieldName; }
            set { _fieldName = value; }
        }

        /// <summary>
        /// CustomFilter overrides the normal clause with straight text
        /// </summary>
        public string CustomFilter
        {
            get { return _customFilter; }
            set { _customFilter = value; }
        }

        /// <summary>
        /// Gets/sets the comparison method
        /// </summary>
        public eSQLComparisonType ComparisonOperator
        {
            get { return _comparisonOperator; }
            set { _comparisonOperator = value; }
        }

        /// <summary>
        /// Gets/set the Source
        /// </summary>
        public eWhereClauseSource Source
        {
            get { return _source; }
            set { _source = value; }
        }

        /// <summary>
        /// Gets/sets the value that was set for comparison
        /// </summary>
        public object Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public WhereClause(string customFilter, object firstCompareValue)
        {
            _source = eWhereClauseSource.Custom;
            _fieldName = "";
            _comparisonOperator = eSQLComparisonType.Equals;
            _value = firstCompareValue;
            _customFilter = customFilter;
            SubClauses = new List<SubClause>();
        }

        public WhereClause(string field, eSQLComparisonType firstCompareOperator, object firstCompareValue)
        {
            _source = eWhereClauseSource.Standard;
            _fieldName = field;
            _comparisonOperator = firstCompareOperator;
            _value = firstCompareValue;
            _customFilter = "";
            SubClauses = new List<SubClause>();
        }

        public void AddClause(LogicOperator logic, eSQLComparisonType compareOperator, object compareValue)
        {
            var newSubClause = new SubClause(logic, compareOperator, compareValue);
            SubClauses.Add(newSubClause);
        }
    }
}