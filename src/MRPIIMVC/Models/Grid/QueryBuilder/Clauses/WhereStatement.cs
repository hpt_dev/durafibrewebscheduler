using System;
using System.Collections.Generic;
using OSMRP1.QueryBuilder.Enums;

namespace OSMRP1.QueryBuilder.Clauses
{
    public class WhereStatement : List<List<WhereClause>>
    {
        // The list in this container will contain lists of clauses, and
        // forms a where statement all together!

        public int ClauseLevels
        {
            get { return Count; }
        }

        private void AssertLevelExistance(int level)
        {
            if (Count < level - 1)
            {
                throw new Exception("Level " + level + " not allowed because level " + (level - 1) + " does not exist.");
            }
            // Check if new level must be created
            else if (Count < level)
            {
                Add(new List<WhereClause>());
            }
        }

        public void Add(WhereClause clause)
        {
            Add(clause, 1);
        }

        public void Add(WhereClause clause, int level)
        {
            AddWhereClauseToLevel(clause, level);
        }

        public WhereClause Add(string field, eSQLComparisonType @operator, object compareValue)
        {
            return Add(field, @operator, compareValue, 1);
        }

        public WhereClause Add(Enum field, eSQLComparisonType @operator, object compareValue)
        {
            return Add(field.ToString(), @operator, compareValue, 1);
        }

        public WhereClause Add(string field, eSQLComparisonType @operator, object compareValue, int level)
        {
            var newWhereClause = new WhereClause(field, @operator, compareValue);
            AddWhereClauseToLevel(newWhereClause, level);
            return newWhereClause;
        }

        private void AddWhereClause(WhereClause clause)
        {
            AddWhereClauseToLevel(clause, 1);
        }

        private void AddWhereClauseToLevel(WhereClause clause, int level)
        {
            // Add the new clause to the array at the right level
            AssertLevelExistance(level);
            this[level - 1].Add(clause);
        }

        public string BuildWhereStatement()
        {
            object dummyCommand = null; // = DataAccess.UsedDbProviderFactory.CreateCommand();
            return BuildWhereStatement(false, ref dummyCommand);
        }

        public string BuildWhereStatement(bool useCommandObject, ref object usedDbCommand)
        {
            string result = "";
            int counter = 0;

            foreach (List<WhereClause> whereStatement in this) // Loop through all statement levels, OR them together
            {
                string levelWhere = "";
                foreach (WhereClause clause in whereStatement) // Loop through all conditions, AND them together
                {
                    string whereClause = "";

                    if (useCommandObject)
                    {
                        // Create a parameter
                        string parameterName = string.Format(
                            "@p{0}_{1}",
                            counter += 1,
                            clause.FieldName.Replace('.', '_')
                            );

                        //DbParameter parameter = usedDbCommand.CreateParameter();
                        //parameter.ParameterName = parameterName;
                        //parameter.Value = Clause.Value;
                        //usedDbCommand.Parameters.Add(parameter);

                        // Create a where clause using the parameter, instead of its value
                        whereClause += CreateComparisonClause(clause.FieldName, clause.ComparisonOperator, new SqlLiteral(parameterName));
                    }
                    else
                    {
                        if (clause.Source == eWhereClauseSource.Standard)
                        {
                            whereClause = CreateComparisonClause(clause.FieldName, clause.ComparisonOperator, clause.Value);
                        }
                        else
                        {
                            whereClause = clause.CustomFilter;
                        }
                    }

                    foreach (WhereClause.SubClause subWhereClause in clause.SubClauses)	// Loop through all sub-clauses, append them together with the specified logic operator
                    {
                        switch (subWhereClause.LogicOperator)
                        {
                            case LogicOperator.And:
                                whereClause += " AND "; break;
                            case LogicOperator.Or:
                                whereClause += " OR "; break;
                        }

                        if (useCommandObject)
                        {
                            // Create a parameter
                            string parameterName = string.Format(
                                "@p{0}_{1}",
                                counter += 1,
                                clause.FieldName.Replace('.', '_')
                                );

                            //DbParameter parameter = usedDbCommand.CreateParameter();
                            //parameter.ParameterName = parameterName;
                            //parameter.Value = SubWhereClause.Value;
                            //usedDbCommand.Parameters.Add(parameter);

                            // Create a where clause using the parameter, instead of its value
                            whereClause += CreateComparisonClause(clause.FieldName, subWhereClause.ComparisonOperator, new SqlLiteral(parameterName));

                        }
                        else
                        {
                            whereClause += CreateComparisonClause(clause.FieldName, subWhereClause.ComparisonOperator, subWhereClause.Value);

                        }
                    }

                    levelWhere += "(" + whereClause + ") AND ";
                }

                levelWhere = levelWhere.Substring(0, levelWhere.Length - 5); // Trim de last AND inserted by foreach loop
                if (whereStatement.Count > 1)
                {
                    result += " (" + levelWhere + ") ";
                }
                else
                {
                    result += " " + levelWhere + " ";
                }
                result += " OR";
            }

            result = result.Substring(0, result.Length - 2); // Trim de last OR inserted by foreach loop

            return result;
        }

        internal static string CreateComparisonClause(string fieldName, eSQLComparisonType comparisonOperator, object value)
        {
            string output = "";
            if (value != null && value != DBNull.Value)
            {
                switch (comparisonOperator)
                {
                    case eSQLComparisonType.Equals:
                        output = fieldName + " = " + FormatSQLValue(value); break;
                    case eSQLComparisonType.Not_Equals:
                        output = fieldName + " <> " + FormatSQLValue(value); break;
                    case eSQLComparisonType.Greater_Than:
                        output = fieldName + " > " + FormatSQLValue(value); break;
                    case eSQLComparisonType.Greater_Or_Equals:
                        output = fieldName + " >= " + FormatSQLValue(value); break;
                    case eSQLComparisonType.Less_Than:
                        output = fieldName + " < " + FormatSQLValue(value); break;
                    case eSQLComparisonType.Less_Or_Equals:
                        output = fieldName + " <= " + FormatSQLValue(value); break;
                    case eSQLComparisonType.Contains:
                        output = fieldName + " LIKE " + FormatSQLValue("%" + value.ToString() + "%"); break;
                    case eSQLComparisonType.Not_Contains:
                        output = fieldName + "NOT LIKE " + FormatSQLValue("%" + value.ToString() + "%"); break;
                    case eSQLComparisonType.Ends_With:
                        output = fieldName + " LIKE " + FormatSQLValue("%" + value.ToString()); break;
                    case eSQLComparisonType.Not_Ends_With:
                        output = fieldName + "NOT LIKE " + FormatSQLValue("%" + value.ToString()); break;
                    case eSQLComparisonType.Starts_With:
                        output = fieldName + " LIKE " + FormatSQLValue(value.ToString() + "%"); break;
                    case eSQLComparisonType.Not_Starts_With:
                        output = fieldName + "NOT " + FormatSQLValue(value.ToString() + "%"); break;
                    case eSQLComparisonType.In:
                        output = fieldName + " IN (" + FormatSQLValue(value) + ")"; break;
                }
            }
            else // value==null	|| value==DBNull.Value
            {
                if (comparisonOperator != eSQLComparisonType.Equals && comparisonOperator != eSQLComparisonType.Not_Equals)
                {
                    throw new Exception("Cannot use comparison operator " + comparisonOperator.ToString() + " for NULL values.");
                }
                else
                {
                    switch (comparisonOperator)
                    {
                        case eSQLComparisonType.Equals:
                            output = fieldName + " IS NULL"; break;
                        case eSQLComparisonType.Not_Equals:
                            output = "NOT " + fieldName + " IS NULL"; break;
                    }
                }
            }

            return output;
        }

        internal static string FormatSQLValue(object someValue)
        {
            string formattedValue = "";
            //				string StringType = Type.GetType("string").Name;
            //				string DateTimeType = Type.GetType("DateTime").Name;

            if (someValue == null)
            {
                formattedValue = "NULL";
            }
            else
            {
                switch (someValue.GetType().Name)
                {
                    case "String": formattedValue = "'" + ((string)someValue).Replace("'", "''") + "'"; break;
                    case "DateTime": formattedValue = "'" + ((DateTime)someValue).ToString("yyyy/MM/dd hh:mm:ss") + "'"; break;
                    case "DBNull": formattedValue = "NULL"; break;
                    case "Boolean": formattedValue = (bool)someValue ? "1" : "0"; break;
                    case "SqlLiteral": formattedValue = ((SqlLiteral)someValue).Value; break;
                    default: formattedValue = someValue.ToString(); break;
                }
            }

            return formattedValue;
        }

        /// <summary>
        /// This static method combines 2 where statements with each other to form a new statement
        /// </summary>
        /// <param name="statement1"></param>
        /// <param name="statement2"></param>
        /// <returns></returns>
        public static WhereStatement CombineStatements(WhereStatement statement1, WhereStatement statement2)
        {
            // statement1: {Level1}((Age<15 OR Age>=20) AND (strEmail LIKE 'e%') OR {Level2}(Age BETWEEN 15 AND 20))
            // Statement2: {Level1}((Name = 'Peter'))
            // Return statement: {Level1}((Age<15 or Age>=20) AND (strEmail like 'e%') AND (Name = 'Peter'))

            // Make a copy of statement1
            WhereStatement result = Copy(statement1);

            // Add all clauses of statement2 to result
            for (int i = 0; i < statement2.ClauseLevels; i++) // for each clause level in statement2
            {
                List<WhereClause> level = statement2[i];
                foreach (WhereClause clause in level) // for each clause in level i
                {
                    for (int j = 0; j < result.ClauseLevels; j++)  // for each level in result, add the clause
                    {
                        result.AddWhereClauseToLevel(clause, j);
                    }
                }
            }

            return result;
        }

        public static WhereStatement Copy(WhereStatement statement)
        {
            var result = new WhereStatement();
            int currentLevel = 0;
            foreach (List<WhereClause> level in statement)
            {
                currentLevel++;
                result.Add(new List<WhereClause>());
                foreach (WhereClause clause in statement[currentLevel - 1])
                {
                    var clauseCopy = new WhereClause(clause.FieldName, clause.ComparisonOperator, clause.Value);
                    foreach (WhereClause.SubClause subClause in clause.SubClauses)
                    {
                        var subClauseCopy = new WhereClause.SubClause(subClause.LogicOperator, subClause.ComparisonOperator, subClause.Value);
                        clauseCopy.SubClauses.Add(subClauseCopy);
                    }

                    result[currentLevel - 1].Add(clauseCopy);
                }
            }

            return result;
        }
    }
}