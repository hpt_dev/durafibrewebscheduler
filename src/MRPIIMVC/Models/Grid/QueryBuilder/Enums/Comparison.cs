using System;

namespace OSMRP1.QueryBuilder.Enums
{
    /// <summary>
    /// Represents comparison operators for WHERE, HAVING and JOIN clauses
    /// </summary>
    public enum eSQLComparisonType
    {
        Equals = 1,
        Not_Equals = 2,
        Greater_Than = 3,
        Greater_Or_Equals = 4,
        Less_Than = 5,
        Less_Or_Equals = 6,
        In = 7,
        Starts_With = 8,
        Ends_With = 9,
        Contains = 10,
        Not_Starts_With = 11,
        Not_Ends_With = 12,
        Not_Contains = 13
    }
}