using System;

namespace OSMRP1.QueryBuilder.Enums
{
    /// <summary>
    /// Represents a unit for TOP clauses in SELECT statements
    /// </summary>
    public enum TopUnit
    {
        Records,
        Percent
    }
}