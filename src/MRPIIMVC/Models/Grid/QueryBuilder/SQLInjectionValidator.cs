﻿using System;

namespace OSMRP1.QueryBuilder
{
    public static class SQLInjectionValidator
    {
        public static Boolean CheckForSQLInjection(string userInput)
        {
            bool isSQLInjection = false;
            string[] sqlCheckList = { "--",
                                       ";--",
                                       ";",
                                       "/*",
                                       "*/",
                                        "@@",
                                        "@",
                                        "char",
                                       "nchar",
                                       "varchar",
                                       "nvarchar",
                                       "alter",
                                       "begin",
                                       "cast",
                                       "create",
                                       "cursor",
                                       "declare",
                                       "delete",
                                       "drop",
                                       "end",
                                       "exec",
                                       "execute",
                                       "fetch",
                                       "insert",
                                       "kill",
                                       "select",
                                       "sys",
                                       "sysobjects",
                                       "syscolumns",
                                       "table",
                                       "update"
                                       };
            string checkString = userInput.Replace("'", "''");
            for (int i = 0; i <= sqlCheckList.Length - 1; i++)
            {
                if (checkString.IndexOf(sqlCheckList[i], StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    isSQLInjection = true;
                }
            }

            return isSQLInjection;
        }
    }
}