using System;
using System.Collections.Generic;
using OSMRP1.QueryBuilder.Clauses;
using OSMRP1.QueryBuilder.Enums;
using System.Linq;

namespace OSMRP1.QueryBuilder
{
    public class SelectQueryBuilder
    {
        protected bool _distinct = false;
        protected TopClause _topClause = new TopClause(100, TopUnit.Percent);
        protected List<SelectColumn> _columns = new List<SelectColumn>();
        private string _baseQueryText = "";
        private string _baseSortText = "";
        protected List<string> _tables = new List<string>();	// array of string
        protected List<JoinClause> _joins = new List<JoinClause>();	// array of JoinClause
        protected WhereStatement _whereStatement = new WhereStatement();
        protected List<OrderByClause> _orderByStatement = new List<OrderByClause>();	// array of OrderByClause
        protected List<string> _groupByColumns = new List<string>();		// array of string
        protected WhereStatement _havingStatement = new WhereStatement();

        internal WhereStatement WhereStatement
        {
            get { return _whereStatement; }
            set { _whereStatement = value; }
        }

        public SelectQueryBuilder()
        {
        }

        public bool Distinct
        {
            get { return _distinct; }
            set { _distinct = value; }
        }

        public int TopRecords
        {
            get { return _topClause.Quantity; }
            set
            {
                if (value == 0)
                {
                    _topClause.Quantity = 100;
                    _topClause.Unit = TopUnit.Percent;
                }
                else
                {
                    _topClause.Quantity = value;
                    _topClause.Unit = TopUnit.Records;
                }
            }
        }

        public TopClause TopClause
        {
            get { return _topClause; }
            set { _topClause = value; }
        }

        public SelectColumn[] Columns
        {
            get { return _columns.Count > 0 ? _columns.ToArray() : new SelectColumn[1] { new SelectColumn("*") }; }
        }

        public string BaseSortText
        {
            get { return _baseSortText; }
            set { _baseSortText = value; }
        }

        public string BaseQueryText
        {
            get { return _baseQueryText; }
            set { _baseQueryText = value; }
        }

        public string[] Tables
        {
            get { return _tables.ToArray(); }
        }

        public void SelectCount()
        {
            AddColumn("count(1)");
        }

        public void AddColumn(SelectColumn column)
        {
            _columns.Add(column);
        }

        public void AddColumns(SelectColumn[] columns)
        {
            foreach (SelectColumn column in columns)
            {
                AddColumn(column);
            }
        }

        public void AddColumn(string column)
        {
            AddColumn(new SelectColumn(column));
        }

        public void AddColumns(params string[] columns)
        {
            _columns.Clear();
            foreach (string column in columns)
            {
                AddColumn(new SelectColumn(column));
            }
        }

        public void Clear()
        {
            ClearTables();
            ClearColumns();
            ClearWhereClauses();
            ClearOrderByStatements();
            ClearHavingClauses();
            TopRecords = 0;
            _baseQueryText = "";
            _baseSortText = "";
            _groupByColumns.Clear();
        }

        public void ClearColumns()
        {
            _columns.Clear();
        }

        public void ClearTables()
        {
            _tables.Clear();
        }

        public void ClearWhereClauses()
        {
            _whereStatement.Clear();
        }

        public void ClearOrderByStatements()
        {
            _orderByStatement.Clear();
        }

        public void ClearHavingClauses()
        {
            _havingStatement.Clear();
        }

        public void AddTable(string table)
        {
            _tables.Add(table);
        }

        public void AddTables(params string[] tables)
        {
            foreach (string table in tables)
            {
                _tables.Add(table);
            }
        }

        public void AddJoin(JoinClause newJoin)
        {
            _joins.Add(newJoin);
        }

        public void AddJoin(JoinType join, string toTableName, string toColumnName, eSQLComparisonType @operator, string fromTableName, string fromColumnName)
        {
            var newJoin = new JoinClause(join, toTableName, toColumnName, @operator, fromTableName, fromColumnName);
            _joins.Add(newJoin);
        }

        public WhereStatement Where
        {
            get { return _whereStatement; }
            set { _whereStatement = value; }
        }

        public void AddWhere(WhereClause clause)
        {
            AddWhere(clause, 1);
        }

        public void AddWhere(WhereClause clause, int level)
        {
            _whereStatement.Add(clause, level);
        }

        public WhereClause AddWhere(string field, eSQLComparisonType @operator, object compareValue)
        {
            return AddWhere(field, @operator, compareValue, 1);
        }

        public WhereClause AddWhere(Enum field, eSQLComparisonType @operator, object compareValue)
        {
            return AddWhere(field.ToString(), @operator, compareValue, 1);
        }

        public WhereClause AddWhere(string field, eSQLComparisonType @operator, object compareValue, int level)
        {
            var newWhereClause = new WhereClause(field, @operator, compareValue);
            _whereStatement.Add(newWhereClause, level);
            return newWhereClause;
        }

        public void AddOrderBy(OrderByClause clause)
        {
            _orderByStatement.Add(clause);
        }

        public void AddOrderBy(Enum field, Sorting order)
        {
            AddOrderBy(field.ToString(), order);
        }

        public void AddOrderBy(string field, Sorting order)
        {
            var newOrderByClause = new OrderByClause(field, order);
            _orderByStatement.Add(newOrderByClause);
        }

        public void GroupBy(params string[] columns)
        {
            foreach (string column in columns)
            {
                _groupByColumns.Add(column);
            }
        }

        public WhereStatement Having
        {
            get { return _havingStatement; }
            set { _havingStatement = value; }
        }

        public void AddHaving(WhereClause clause)
        {
            AddHaving(clause, 1);
        }

        public void AddHaving(WhereClause clause, int level)
        {
            _havingStatement.Add(clause, level);
        }

        public WhereClause AddHaving(string field, eSQLComparisonType @operator, object compareValue)
        {
            return AddHaving(field, @operator, compareValue, 1);
        }

        public WhereClause AddHaving(Enum field, eSQLComparisonType @operator, object compareValue)
        {
            return AddHaving(field.ToString(), @operator, compareValue, 1);
        }

        public WhereClause AddHaving(string field, eSQLComparisonType @operator, object compareValue, int level)
        {
            var newWhereClause = new WhereClause(field, @operator, compareValue);
            _havingStatement.Add(newWhereClause, level);
            return newWhereClause;
        }

        /// <summary>
        /// Builds the select query
        /// </summary>
        /// <returns>Returns a string containing the query</returns>
        public string BuildQuery()
        {
            string query = "SELECT ";

            // Output Distinct
            query = AppendDistinct(query);

            // Output Top clause
            query = AppendTop(query);

            // Output column names
            query = AppendColumnNames(query);

            query = AppendFrom(query);

            // Output joins
            query = AppendJoins(query);

            // Output where statement
            query = AppendWhere(query);

            // Output GroupBy statement
            query = AppendGroupBy(query);

            // Output having statement
            query = AppendHaving(query);

            // Output OrderBy statement
            query = AppendOrderBy(query);

            return query;
        }

        /// <summary>
        /// Builds the select query modified to support paging
        /// </summary>
        /// <returns>Returns a string containing the query</returns>
        public string BuildPagingQuery(int page = 1, int pageSize = 50, List<Tuple<string, string>> sortCriteria = null, List<string> inGridFilters = null)
        {
            string query = "WITH cte AS (SELECT  ";

            query = AppendPagingOrderBy(sortCriteria, query);
            query += " ) AS intRow , * from (select ";
            // Output Distinct
            query = AppendDistinct(query);

            // Output Top clause
            query = AppendTop(query);

            // Output column names
            query = AppendColumnNames(query);

            query = AppendFrom(query);

            // Output joins
            query = AppendJoins(query);

            HandlePagingWhereClauses(inGridFilters);

            // Output where statement
            query = AppendWhere(query);

            // Output GroupBy statement
            query = AppendGroupBy(query);

            // Output having statement
            query = AppendHaving(query);

            query += " ) A) Select * from cte  ";

            query += "  WHERE intRow BETWEEN "+ page.ToString() + " * " + pageSize.ToString() + " +1 AND (" + page.ToString() +  " +1) * " + pageSize.ToString();

            return query;
        }

        private void HandlePagingWhereClauses(List<string> inGridFilters)
        {
            if (inGridFilters == null)
                return;

            string[] comp = "IsEqualTo,IsNotEqualTo,IsGreaterThan,IsGreaterThanOrEqualTo,isLessThan,IsLessThanOrEqualTo,Contains,StartsWith,EndsWith,NotSupported10,NotSupported12,NotSupported12,DoesNotCointain".Split(',');
            foreach (string f in inGridFilters)
            {
                for (var j = 0; j < comp.Length; j++)
                {
                    if (f.Contains(comp[j].Trim()))
                    {
                        string[] fieldValue = f.Split(comp, StringSplitOptions.RemoveEmptyEntries);
                        if (fieldValue.Length == 2)
                        {
                            SelectColumn column = _columns.FirstOrDefault(x => x.Caption == fieldValue[0].Trim());
                            if (column == null)
                                continue;

                            var compEnum = (eSQLComparisonType)Enum.GetValues(eSQLComparisonType.Contains.GetType()).GetValue(j);
                            List<WhereClause> whereList = _whereStatement.First();
                            WhereClause where = whereList.FirstOrDefault(x => x.FieldName == column.TableName + "." + column.FieldName);
                            if (where.Value == null)
                            {
                                _whereStatement.Add(column.TableName + "." + column.FieldName, compEnum, fieldValue[1].Trim());
                            }
                            else
                            {
                                where.SubClauses.Add(new WhereClause.SubClause(LogicOperator.Or, compEnum, fieldValue[1].Trim()));
                            }
                        }

                    }
                }
            }
        }

        private string AppendPagingOrderBy(List<Tuple<string, string>> sortCriteria, string query)
        {
            if (_orderByStatement.Count > 0 || _baseSortText.Trim() != "")
            {
                query += "ROW_NUMBER() OVER ( ORDER BY ";

                if (sortCriteria != null)
                {
                    if (sortCriteria.Count == 0)
                       query = SetDefaultOrderByClause(query);
                    else
                    {
                    foreach (Tuple<string, string> crit in sortCriteria)
                    {
                        SelectColumn column = _columns.FirstOrDefault(x => x.Caption == crit.Item1);
                        if (column == null)
                            continue;

                            string columnCrit = "[" + column.Caption + "]";
                        if (crit.Item2 == "Ascending")
                            query += columnCrit + " ASC" + ',';
                        else
                            query += columnCrit + " DESC" + ',';
                    }
                }
                        }

                else
                    query = SetDefaultOrderByClause(query);

                query = query.TrimEnd(','); // Trim the last AND inserted by foreach loop
                query += ' ';
            }

            return query;
        }

        private string SetDefaultOrderByClause(string query)
        {
            //Get the sort text that is set in the database
            // The else shouldn't be hit, but left it in as a fail over.
            // replace field name with column text
            var text = "";
            var sorts = _baseSortText.Split(',');
            foreach(var sort in sorts)
            {
                var s = sort.ToLower();
                var dir = "asc";
                if(s.Contains(" desc"))
                    dir = "desc";
                s = s.Replace("[", "").Replace("]", "").Replace(" asc", "").Replace(" desc", "").Trim();

                SelectColumn column = _columns.FirstOrDefault(x => (x.CustomColumnSQL ?? "").ToLower() == s || (x.TableName + "." + x.FieldName).ToLower() == s || x.Caption.ToLower() == s);
                if(column != null)
                    text = text + " A.[" + column.Caption + "] " + dir;
            }

            return _baseSortText.Trim() != "" ? query +=  text : query;
        }
        private string AppendHaving(string query)
        {
            if (_havingStatement.ClauseLevels > 0)
            {
                // Check if a Group By Clause was set
                if (_groupByColumns.Count == 0)
                {
                    throw new Exception("Having statement was set without Group By");
                }

                query += " HAVING " + _havingStatement.BuildWhereStatement();
            }

            return query;
        }

        private string AppendGroupBy(string query)
        {
            if (_groupByColumns.Count > 0)
            {
                query += " GROUP BY ";
                foreach (string column in _groupByColumns)
                {
                    query += column + ',';
                }

                query = query.TrimEnd(',');
                query += ' ';
            }

            return query;
        }

        private string AppendWhere(string query)
        {
            if (_whereStatement.ClauseLevels > 0)
            {
                query += " WHERE " + _whereStatement.BuildWhereStatement();
            }

            return query;
        }

        private string AppendJoins(string query)
        {
            if (_joins.Count > 0)
            {
                foreach (JoinClause clause in _joins)
                {
                    string joinString = "";
                    switch (clause.JoinType)
                    {
                        case JoinType.InnerJoin: joinString = "INNER JOIN"; break;
                        case JoinType.OuterJoin: joinString = "OUTER JOIN"; break;
                        case JoinType.LeftJoin: joinString = "LEFT JOIN"; break;
                        case JoinType.RightJoin: joinString = "RIGHT JOIN"; break;
                    }

                    joinString += " " + clause.ToTable + " ON ";
                    joinString += WhereStatement.CreateComparisonClause(clause.FromTable + '.' + clause.FromColumn, clause.ComparisonOperator, new SqlLiteral(clause.ToTable + '.' + clause.ToColumn));
                    query += joinString + ' ';
                }
            }

            return query;
        }

        private string AppendTop(string query)
        {
            if (!(_topClause.Quantity == 100 & _topClause.Unit == TopUnit.Percent))
            {
                query += "TOP " + _topClause.Quantity;
                if (_topClause.Unit == TopUnit.Percent)
                {
                    query += " PERCENT";
                }
                query += " ";
            }

            return query;
        }

        private string AppendDistinct(string query)
        {
            if (_distinct)
            {
                query += "DISTINCT ";
            }

            return query;
        }

        private string AppendColumnNames(string query)
        {
            if (_columns.Count == 0)
            {
                if (_tables.Count == 1)
                    query += _tables[0] + "."; // By default only select * from the table that was selected. If there are any joins, it is the responsibility of the user to select the needed columns.

                query += "*";
            }
            else
            {
                foreach (SelectColumn column in _columns)
                {
                    query += column.ToString() + ',';
                }

                query = query.TrimEnd(','); // Trim de last comma inserted by foreach loop
                query += ' ';
            }

            return query;
        }

        private string AppendFrom(string query)
        {
            if (_tables.Count > 0 || _baseQueryText != "")
            {
                query += " FROM ";

                query += _baseQueryText.Replace(" FROM ", " ");

                // Output table names
                if (_tables.Count > 0)
                {
                    foreach (string tableName in _tables)
                    {
                        query += tableName + ',';
                    }

                    query = query.TrimEnd(','); // Trim the last comma inserted by foreach loop
                    query += ' ';
                }
            }

            return query;
        }

        private string AppendOrderBy(string query)
        {
            if (_orderByStatement.Count > 0 || _baseSortText.Trim() != "")
            {
                query += " ORDER BY ";

                if (_baseSortText.Trim() != "")
                {
                    query += _baseSortText.Replace(" ORDER BY ", " ");
                }

                foreach (OrderByClause clause in _orderByStatement)
                {
                    string orderByClause = "";
                    switch (clause.SortOrder)
                    {
                        case Sorting.Ascending:
                            orderByClause = clause.FieldName + " ASC"; break;
                        case Sorting.Descending:
                            orderByClause = clause.FieldName + " DESC"; break;
                    }

                    query += orderByClause + ',';
                }

                query = query.TrimEnd(','); // Trim the last AND inserted by foreach loop
                query += ' ';
            }

            return query;
        }
    }
}