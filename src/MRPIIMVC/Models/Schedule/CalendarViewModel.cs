﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MRPIIMVC.Models.Schedule
{
    public class CalendarViewModel :ViewModel
    {
       public List<Appointment> getAll()
        {
            var ret = new List<Appointment>();

            ret = db.sc_appointments.Where(x => x.is_active == true).Select(n => new Appointment
            {
                TaskID = n.appointment_id,
                Title = n.subject,
                Description = n.body,
                Start = n.start,                
                End = n.end,
                IsAllDay = n.is_all_day_event,
                RecurrenceRule = n.reccurence_pattern,
                avaliable = n.avaliable,
                calendar_id = n.calendar_id, 
                work_center_group_id = n.work_center_group_id,
                work_center_id = n.work_center_id,
                color = n.color,
                pattern = n.reccurence_pattern,
                importance = n.importance, 
                exemptDates = n.exempt_dates
            }).ToList();
            return ret; 
        }
        public long saveAppointment(Appointment ap, string overRiderUserId)
        {
            var dbApp = db.sc_appointments.FirstOrDefault(x => x.appointment_id == ap.TaskID);
            if(dbApp == null)
            {
                long? userId = CurrentUser == null ? -5 : CurrentUser.id;
                if (overRiderUserId != null && overRiderUserId != "")
                {
                    var user = db.ad_users.FirstOrDefault(x => x.USER_ID.ToString() == overRiderUserId);
                    if (user != null)
                        userId = user.USER_ID;
                }

                dbApp = db.sc_appointments.Create();
                db.sc_appointments.Add(dbApp);
                dbApp.unique_id = System.Guid.NewGuid();
                dbApp.created_by = userId;
            }
            dbApp.start = ap.Start;
            dbApp.end = ap.End;
            dbApp.subject = ap.Title;
            dbApp.body = ap.Description;
            dbApp.color = ap.color;
            dbApp.calendar_id = ap.calendar_id;
            dbApp.work_center_id =ap.work_center_id;
            dbApp.work_center_group_id = ap.work_center_group_id;
            dbApp.avaliable = ap.avaliable;
            dbApp.reccurence_pattern = ap.pattern;
            dbApp.is_active = true;
            dbApp.importance = ap.importance;
            dbApp.exempt_dates = ap.exemptDates;


            db.SaveChanges();
          
          
            return dbApp.appointment_id;
        }
        public long deleteAppointment(Appointment ap)
        {
            var dbApp = db.sc_appointments.FirstOrDefault(x => x.appointment_id == ap.TaskID);
            if (dbApp != null)
            {
                long? userId = CurrentUser == null ? -5 : CurrentUser.id;
               
                dbApp.is_active = false;
                dbApp.last_modified_by = userId;
                db.SaveChanges();
            }
            return ap.TaskID;
        }
        public List<AppointmentType> getAppointmentTypes()
        {
            var ret = new List<AppointmentType>();
            var a = db.Database.SqlQuery<AppointmentType>(@"exec sc_getEventTypes").ToList();
            return a;
        }
        public class Appointment
        {
            public long TaskID { get; set; }
            public long OwnerID { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string StartTimezone { get; set; }
            public DateTime Start { get; set; }    
            public DateTime End { get; set; }
            public string EndTimezone { get; set; }
            public string RecurrenceRule { get; set; }
            public long RecurrenceID { get; set; }
            public string RecurrenceException { get; set; }
            public bool IsAllDay { get; set; }
            public long? calendar_id { get; set; }
            public int? work_center_group_id { get; set; }
            public int? work_center_id { get; set; }
            public int? avaliable { get; set; }
            public string color { get; set; }
            public string pattern { get; set; }
            public int importance { get; set; }
            public string exemptDates { get; set; }
        }
        public class AppointmentType
        {
            public string type { get; set; }
            public string calendar_name { get; set; }
            public long? calendar_id { get; set; }
            public int? work_center_group_id { get; set; }
            public int? work_center_id { get; set; }
        }
    }
    
}
