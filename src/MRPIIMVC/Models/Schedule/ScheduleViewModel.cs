﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace MRPIIMVC.Models.Schedule
{
    public class ScheduleViewModel : ViewModel
    {
        public List<ScheduleColumn> Columns { get; set; }

        public async Task<DFSchedule> getSchedule(long buildingId)
        {
            var schedule = new DFSchedule();
            
            
            var dtsql = $"SELECT cast(setting_value as int) as 'minutes' from service_settings where setting_name = 'Lam Downtime'";
            var dtresults = new List<DowntimeRecord>();
            dtresults = await db.getData<DowntimeRecord>(dtsql);
            schedule.LamDownMinutes = dtresults[0].minutes;
            //Columns = await getColumns();
            //schedule.LamDownMinutes = dt[0].setting_name;
            schedule.ScheduleMachines = await getWorkCenterGroups(buildingId);

            var sql = $"DF_GetLeadDays";
            schedule.LeadDays = await db.getData<LeadDayRecord>(sql);

            var fdtsql = $"SELECT cast(setting_value as int) as 'minutes' from service_settings where setting_name = 'Foam Lam Downtime'";
            var fdtresults = new List<DowntimeRecord>();
            fdtresults = await db.getData<DowntimeRecord>(fdtsql);
            schedule.FoamLamDownMinutes = fdtresults[0].minutes;

            return schedule;
        }

        public async Task<BlockTimeEntry> getTimeBlockEdit(long blockID)
        {
            var schedule = new DFSchedule();

            //Columns = await getColumns();

            var sqlList = new List<BlockTimeEntry>();
            var TimeBlockEdit = new BlockTimeEntry();
            var sql = $"DF_GetTimeBlockEdit {blockID}";
            sqlList = await db.getData<BlockTimeEntry>(sql);
            TimeBlockEdit = sqlList[0];

            Console.WriteLine(sql);
            return TimeBlockEdit;
        }

        public async Task<Boolean> scheduleGroup(int machNo, string idArrayString)
        {
            try
            {
                var sql = $"DF_ScheduleGroup {machNo}, '{idArrayString}'";
                Console.WriteLine(sql);
                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<Boolean> saveLamDowntime(int minutes)
        {
            try
            {
                var sql = $"UPDATE service_settings set setting_value = {minutes} where setting_name = 'Lam Downtime'";
                Console.WriteLine(sql);
                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<Boolean> saveFoamLamDowntime(int minutes)
        {
            try
            {
                var sql = $"UPDATE service_settings set setting_value = {minutes} where setting_name = 'Foam Lam Downtime'";
                Console.WriteLine(sql);
                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<Boolean> unscheduleGroup(int machNo, string idArrayString)
        {
            try
            {
                var sql = $"DF_UnscheduleGroup {machNo}, '{idArrayString}'";
                Console.WriteLine(sql);
                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<Boolean> saveConvertingDetails(int machNo, int rate, int setupTime)
        {
            try
            {
                var sql = $"DF_SaveConvertingDetails {machNo}, {rate}, {setupTime}";
                Console.WriteLine(sql);
                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<List<ScheduleColumn>> getColumns()
        {
            var sql = $"spSched_GetColumns";

            var columns = await db.getData<ScheduleColumn>(sql);

            return columns;
        }

        public async Task<List<ScheduleMachine>> getWorkCenterGroups(long? buildingId)
        {
            var sql = $"DF_GetScheduleMachines ";
            if (buildingId != null)
                sql += $" {buildingId}";


            var machines = await db.getData<sc_GetScheduleMachinesResults>(sql);
            var ret = new List<ScheduleMachine>();
            //var tasks = new List<Task>();

            foreach (var e in machines)
            {

                var sm = new ScheduleMachine { Name = e.machine_name, MachNO = e.machine_number, ScheduledEntries = new List<DFScheduleEntry>(), UnscheduledEntries = new List<DFScheduleEntry>() };
                await sm.loadScheduledEntriesForMachine();
                await sm.loadUnscheduledEntriesForMachine();
                await sm.loadHeldEntriesForMachine();
                await sm.loadHSFCEntriesForMachine();
                await sm.loadBlockTimesForMachine();
                await sm.loadStatusCountsForMachine();
                await sm.loadRateForMachine();
                await sm.loadSetupTimeForMachine();

                //tasks.Add(sm.loadOpenOrders(buildingId));
                ret.Add(sm);

            }
            //           foreach (var t in tasks)
            //               await t;
            return ret;
        }



        public async Task<BlockTimeEntry> updateBlockTime(dynamic te)
        {
            try
            {
                var startTime = te.startTime.ToString();
                var endTime = te.endTime.ToString();
                if (endTime.Length < 8) { endTime = endTime + ":00"; }
                if (startTime.Length < 8) { startTime = startTime + ":00"; }
                startTime = startTime.Replace("T", " ");
                endTime = endTime.Replace("T", " ");
                var sql = $"exec DF_UpdateTimeBlock {te.ID}, '{startTime}', '{endTime}', '{te.reason}', {te.machNo}";
                Console.WriteLine(sql);
                var BlockTimeList = await db.getData<BlockTimeEntry>(sql);
                var BlockTime = BlockTimeList[0];
                db.Database.ExecuteSqlCommand(sql);
                return BlockTime;
            }
            catch
            {
                return null;
            }
        }

        public async Task<BlockTimeEntry> deleteBlockTime(dynamic te)
        {
            try
            {
                var sql = $"exec DF_DeleteTimeBlock {te.ID}";
                Console.WriteLine(sql);
                var BlockTimeList = await db.getData<BlockTimeEntry>(sql);
                var BlockTime = BlockTimeList[0];
                db.Database.ExecuteSqlCommand(sql);
                return BlockTime;
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> clearScheduleJam()
        {
            try
            {
                var sql = $"exec DF_ClearSchedulerBlock";
                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {
                return true;
            }
        }

        public async Task<BlockTimeEntry> newBlockTime(dynamic te)
        {
            try
            {
                var sql = $"exec DF_NewTimeBlock '{te.startTime}', '{te.endTime}', '{te.reason}', {te.machNo}";
                Console.WriteLine(sql);
                var BlockTimeList = await db.getData<BlockTimeEntry>(sql);
                var BlockTime = BlockTimeList[0];
                db.Database.ExecuteSqlCommand(sql);
                return BlockTime;
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> saveLeadDays(dynamic ld)
        {
            try
            {

                foreach (dynamic day in ld.Children())
                {
                    var sql = $"exec DF_UpdateLeadDay {day.id}, {day.days}";
                    db.Database.ExecuteSqlCommand(sql);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool scheduleDFEntry(DFScheduleEntry se)
        {
            try
            {
                var sql = $"exec sf_insert_schedule_changes_new {se.JobNumber}, null, null, null, {se.OrderStatus}, null, null, null, {se.MachNo}";
                Console.WriteLine(sql);

                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool unscheduleDFEntry(DFScheduleEntry se)
        {
            try
            {
                var sql = $"exec sf_insert_schedule_changes_new {se.JobNumber}, null, null, {se.OrderStatus}, 'P', null, null, null, {se.MachNo}";
                Console.WriteLine(sql);

                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool moveDFEntry(dynamic se)
        {
            try
            {
                var sql = $"exec sf_move_order_direct_new {se.machNo}, {se.orderNo}, {se.newIndex}, {se.oldIndex}, '', null, 0, ''";
                Console.WriteLine(sql);

                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool updateFrozen(dynamic se)
        {
            try
            {
                var sql = $"update sf_schedule set frozen = '{se.frozen}' where work_order_id = {se.jobID}";
                Console.WriteLine(sql);

                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool adjustSchedule()
        {
            try
            {
                var sql = $"exec sc_AdjustSchedule {CurrentUser.id}";
                Console.WriteLine(sql);

                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {

                return false;
            }
        }

        public bool amtech(int machNo)
        {
            try
            {
                var sql = $"exec usp_op_populate_scheduler " + machNo.ToString();
                Console.WriteLine(sql);
                db.Database.ExecuteSqlCommand(sql);
                return true;
            }
            catch
            {

                return false;
            }
        }

        public async void refresh()
        {

            var sql = $"DF_GetScheduleMachines 5";


            var machines = await db.getData<sc_GetScheduleMachinesResults>(sql);
            var ret = new List<ScheduleMachine>();
            //var tasks = new List<Task>();

            foreach (var e in machines)
            {
                sql = $"exec sf_get_schedule_adj_new " + e.machine_number;
                Console.WriteLine(sql);
                db.Database.ExecuteSqlCommand(sql);
            }

        }

        public async void amtechRefresh()
        {

            var sql = $"DF_GetScheduleMachines 5";
            sc_GetScheduleMachinesResults zodiac = new sc_GetScheduleMachinesResults();
            zodiac.machine_name = "Zodiac";
            zodiac.machine_number = 1900;


            var machines = await db.getData<sc_GetScheduleMachinesResults>(sql);
            machines.Add(zodiac);
            var ret = new List<ScheduleMachine>();
            //var tasks = new List<Task>();

            foreach (var e in machines)
            {
                sql = $"exec amtech_refresh " + e.machine_number;
                Console.WriteLine(sql);
                db.Database.ExecuteSqlCommand(sql);
            }

        }

        public async void refreshSelMachine(int machNo)
        {

            var sql = $"exec sf_get_schedule_adj_new " + machNo;
            Console.WriteLine(sql);
            db.Database.ExecuteSqlCommand(sql);

        }

        public async void orderByDate(int machNo)
        {

            var sql = $"exec sf_reorder_schedule_by_date " + machNo;
            Console.WriteLine(sql);
            db.Database.ExecuteSqlCommand(sql);

        }



        public class DFSchedule
        {
            public bool IsDragging { get; set; }
            public List<LeadDayRecord> LeadDays { get; set; }
            public List<ScheduleMachine> ScheduleMachines { get; set; }
            public int LamDownMinutes { get; set; }
            public int FoamLamDownMinutes { get; set; }

        }

        public class LeadDayRecord
        {
            public int id { get; set; }
            public string title { get; set; }
            public string type { get; set; }
            public int days { get; set; }
        }

        public class RateRecord
        {
            public int rate { get; set; }
        }

        public class DowntimeRecord
        {
            public int minutes { get; set; }
        }

        public class SetupTimeRecord
        {
            public int setup_minutes { get; set; }
        }

        public class ScheduleColumn
        {
            public long id { get; set; }
            public string ColumnName { get; set; }
            public string ColumnType { get; set; }
            public bool IsEditable { get; set; }
        }



        public class ScheduleMachine
        {
            public string Name { get; set; }
            public long ID { get; set; }
            public long MachNO { get; set; }
            public string Desc { get; set; }
            public int DisplayOrder { get; set; }
            public int FirmCount { get; set; }
            public int PendingCount { get; set; }
            public int HeldCount { get; set; }
            public decimal? FirmHours { get; set; } = 0;
            public decimal? PendingHours { get; set; } = 0;
            public decimal? HeldHours { get; set; } = 0;

            public async Task<bool> loadStatusCountsForMachine()
            {
                var sql = $"sf_get_status_counts {MachNO}";
                List<HourStatusCount> counts = new List<HourStatusCount>();
                counts = await db.getData<HourStatusCount>(sql);
                foreach (HourStatusCount count in counts)
                {
                    if (count.order_status == Char.ToString('H'))
                    {
                        HeldCount = count.StatusCount;
                        HeldHours = count.TotalHours;
                    }
                    else if (count.order_status == Char.ToString('P'))
                    {
                        PendingCount = count.StatusCount;
                        PendingHours = count.TotalHours;
                    }
                    else if (count.order_status == Char.ToString('F'))
                    {
                        FirmCount = count.StatusCount;
                        FirmHours = count.TotalHours;
                    }
                }

                Console.WriteLine(sql);
                return true;
            }
            public async Task<bool> loadUnscheduledEntriesForMachine()
            {
                var sql = $"DF_GetUnscheduledOrdersByMachine {MachNO}";
                UnscheduledEntries = await db.getData<DFScheduleEntry>(sql);
                Console.WriteLine(sql);
                return true;
            }
            public async Task<bool> loadScheduledEntriesForMachine()
            {
                var sql = $"DF_GetScheduledOrdersByMachine {MachNO}";
                ScheduledEntries = await db.getData<DFScheduleEntry>(sql);
                Console.WriteLine(sql);
                return true;
            }
            public async Task<bool> loadHeldEntriesForMachine()
            {
                var sql = $"DF_GetHeldOrdersByMachine {MachNO}";
                HeldEntries = await db.getData<DFScheduleEntry>(sql);
                Console.WriteLine(sql);
                return true;
            }
            public async Task<bool> loadHSFCEntriesForMachine()
            {
                var sql = $"DF_GetUnscheduledHWFCOrdersByMachine {MachNO}";
                hsfcEntries = await db.getData<DFScheduleEntry>(sql);
                Console.WriteLine(sql);
                return true;
            }
            public async Task<bool> loadBlockTimesForMachine()
            {
                var sql = $"DF_GetBlockTimesByMachine {MachNO}";
                BlockTimes = await db.getData<BlockTimeEntry>(sql);
                Console.WriteLine(sql);
                return true;
            }
            public async Task<bool> loadRateForMachine()
            {
                if (MachNO == 500 || MachNO == 600)
                {
                    Rate = 0;
                }
                else
                {
                    var sql = $"DF_GetMachineRate {MachNO}";
                    var results = new List<RateRecord>();
                    results = await db.getData<RateRecord>(sql);
                    Rate = results[0].rate;
                    Console.WriteLine(sql);
                }

                return true;
            }
            public async Task<bool> loadSetupTimeForMachine()
            {
                if (MachNO == 500 || MachNO == 600)
                {
                    SetupTime = 0;
                }
                else
                {
                    var sql = $"DF_GetMachineSetupTime {MachNO}";
                    var results = new List<SetupTimeRecord>();
                    results = await db.getData<SetupTimeRecord>(sql);
                    SetupTime = results[0].setup_minutes;
                    Console.WriteLine(sql);
                }

                return true;
            }

            public List<DFScheduleEntry> ScheduledEntries { get; set; }
            public List<DFScheduleEntry> UnscheduledEntries { get; set; }
            public List<DFScheduleEntry> HeldEntries { get; set; }
            public List<DFScheduleEntry> hsfcEntries { get; set; }
            public List<BlockTimeEntry> BlockTimes { get; set; }
            public int SetupTime { get; set; }
            public int Rate { get; set; }
        }

        public class sc_GetWorkCenterGroupsResults
        {
            public string work_center_group_name { get; set; }
            public long work_center_group_id { get; set; }
            public long work_center_id { get; set; }
            public string work_center_name { get; set; }
        }
        public class sc_GetScheduleMachinesResults
        {
            public string machine_name { get; set; }
            public long machine_number { get; set; }
        }


        public class DFScheduleEntry
        {
            public long JobID { get; set; }
            public string OrderStatus { get; set; }
            public long ScheduleOrder { get; set; }
            public long JobNumber { get; set; }
            public string CustomerName { get; set; }
            public DateTime? EstimatedStartTime { get; set; }
            public DateTime? EstimatedEndTime { get; set; }
            public string LamEndTime { get; set; }
            public DateTime? DueDate { get; set; }
            public long QtyOrdered { get; set; }
            public decimal? LinealFeet { get; set; }
            public decimal? Width { get; set; }
            public string ItemDescription { get; set; }
            public int MachNo { get; set; }
            public bool isForte { get; set; }

            public double TotalRunHours { get; set; }
            public string NextOp { get; set; }
            public string Materials { get; set; }
            public string DescriptionSp { get; set; }
            public long Cuts { get; set; }
            public long CutsRemaining { get; set; }
            public int Outs { get; set; }
            public int StackHeight { get; set; }
            public decimal? Caliper { get; set; }
            public int NoPly { get; set; }
            public decimal? Length { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public int Priority { get; set; }
            public string CutToMark { get; set; }
            public string DesignNo { get; set; }
            public string PO { get; set; }
            public string PartNo { get; set; }
            public int LabelsPallet { get; set; }
            public bool Coated { get; set; }
            public bool Slits { get; set; }

            public decimal? RawWidth { get; set; }
            public decimal? ScheduleWidth { get; set; }
            public string Ply1 { get; set; }
            public string Ply2 { get; set; }
            public string Ply3 { get; set; }
            public string Ply4 { get; set; }
            public string Ply5 { get; set; }
            public string Ply6 { get; set; }
            public string JobType { get; set; }
            public int JobTypeID { get; set; }
            public DateTime? OrderDate { get; set; }
            public string DESCR { get; set; }
            public int Year { get; set; }
            public int Week { get; set; }
            public decimal? RunHours { get; set; }
            public decimal? DownHours { get; set; }
            public decimal? SetupHours { get; set; }
            public int ConvertingHours { get; set; }
            public DateTime? AdjustedDueDate { get; set; }
            public bool IsNew { get; set; }
            public bool Checked { get; set; } = false;
            public bool UpdateOrderMode { get; set; } = false;
            public long NewScheduleOrder { get; set; }
            public bool Frozen { get; set; }
            public string Stock { get; set; }


        }

        public class BlockTimeEntry
        {
            public long BlockID { get; set; }
            public long MachNo { get; set; }
            public decimal Hours { get; set; }
            public string Reason { get; set; }
            public DateTime? start { get; set; }
            public DateTime? end { get; set; }
            public string strtTm { get; set; }
            public string ndTm { get; set; }
            public string title { get; set; }
        }

        public class HourStatusCount
        {
            public string order_status { get; set; }
            public int StatusCount { get; set; }
            public decimal TotalHours { get; set; }
        }
    }
}

