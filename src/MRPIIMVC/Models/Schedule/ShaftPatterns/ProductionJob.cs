using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace MRPIIMVC.Models.Schedule.ShaftPatterns
{
    public class ProductionJob
    {
        public long JobID { get; set; }

        public string JobNumber { get; set; }

        public List<ShaftPattern> ShaftPatterns { get; set; }

        public List<RouteStep> Steps { get; set; }

        public int RoundWidthToDecimalPlaces { get; set; }
    }
}