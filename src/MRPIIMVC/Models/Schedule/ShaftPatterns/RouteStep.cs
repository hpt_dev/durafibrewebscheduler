using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace MRPIIMVC.Models.Schedule.ShaftPatterns
{
    public class RouteStep
    {
        public long RouteStepID { get; set; }

        public WorkOrder WorkOrder { get; set; }

        public long ParentJobID { get; set; }

        public string StepNumber { get; set; }

        public string StepDisplayNumber { get; set; }

        public decimal StepQuantity { get; set; }

        public string StepQuantityUOM { get; set; }

        public string StepQuantityDisplay
        {
            get
            {
                return String.Format("{0:#.##}", StepQuantity) + " " + StepQuantityUOM;
            }
        }

        public decimal StepOffWidth { get; set; }

        public bool HasWidthFromBOM { get; set; }

        public string StepOffWidthDisplay
        {
            get
            {
                return StepOffWidth.ToString() + WidthUnit;
            }
        }
        public string WidthUnit { get; set; }

        public bool OtherStepsHaveCuts { get; set; }

        public bool IsCurrentStep { get; set; }

        public string WorkCenterGroup { get; set; }

        public string RouteState
        {
            get
            {
                return (IsCurrentStep ? "current" : "other");
            }
        }

        public string StepNote { get; set; }
    }
}