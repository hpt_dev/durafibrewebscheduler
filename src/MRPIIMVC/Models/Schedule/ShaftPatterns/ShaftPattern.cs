using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace MRPIIMVC.Models.Schedule.ShaftPatterns
{
    public class ShaftPattern
    {
        public long ShaftPatternID { get; set; }
        public decimal MasterWidth { get; set; }
        public string MasterWidthDisplay 
        { 
            get 
            {
                return MasterWidth.ToString() + "\"";
            }
        }
        public bool IsAllocated { get; set; }

        public bool IsDefault { get; set; }

        public bool CanChangeOnFloor {get; set; }

        public string ProductionCutColors { get; set; }

        public decimal LeftTrim 
        {
            get 
            {
                ShaftPatternCut leftTrim = Cuts.FirstOrDefault(x => x.CutTypeDisplay == "Trim" && x.DisplayOrder == 1);
                return (leftTrim == null ? 0 : leftTrim.CutWidth);
            }
        }

        public decimal RightTrim 
        {
            get 
            {
                ShaftPatternCut rightTrim = Cuts.FirstOrDefault(x => x.CutTypeDisplay == "Trim" && x.DisplayOrder == 100);
                return (rightTrim == null ? 0 : rightTrim.CutWidth);
            }
        }

        public int NumberOfOuts 
        {
            get 
            {
                if (Cuts == null)
                {
                    return 0;
                }
                else 
                {
                    return Cuts.Where(x => x.CutTypeDisplay != "Scrap" && x.CutTypeDisplay != "Trim" && x.CutTypeDisplay != "Empty").Count();
                }
            }
        }

        public int NumberOfSets { get; set; }

        public decimal TotalScrapWidth 
        {
            get 
            {
                if (Cuts.Count <= 0)
                {
                    return 0;
                }
                else 
                {
                    return Cuts.Where(x => x.CutTypeDisplay == "Scrap").Sum(x => x.CutWidth);
                }
            }    

        }

        public decimal TotalStockWidth
        {
            get 
            {
                if (Cuts.Count <= 0)
                {
                    return 0;
                }
                else 
                {
                    return Cuts.Where(x => x.CutTypeDisplay == "Stock").Sum(x => x.CutWidth);
                }
            }    

        }

        public string ItemNumber { get; set; }

        public List<ShaftPatternCut> Cuts { get; set; }

        public List<RouteStep> SimilarRouteSteps { get; set; }

        public decimal RemainingWidth { 
            get 
            {
                if (Cuts == null)
                    return MasterWidth;

                if (Cuts.Count == 0)
                    return MasterWidth;

                if (Cuts.Where(x => x.CutTypeDisplay == "Empty").Sum(x => x.CutWidth) < 0)
                {
                    return 0;
                }
                else
                {
                    return ( Cuts.Where(x => x.CutTypeDisplay == "Empty").Sum(x => x.CutWidth));
                }
            }
        }

        public void FillShaft()
        {
            SynchronizeDisplayOrders();
            CalculateDisplayWidths();
        }

        public void SynchronizeDisplayOrders()
        {
            for(int x = 0; x < Cuts.Count; x++)
            {
                if (Cuts[x].DisplayOrder < 99)
                {
                    Cuts[x].DisplayOrder = x + 1;
                }
            }
        }

        public void CalculateDisplayWidths(){
            for(int x = 0; x < Cuts.Count; x++)
            {
                if (MasterWidth > 0) 
                {
                    Cuts[x].DisplayWidth = (Cuts[x].CutWidth / MasterWidth) * 100;
                }
                else {
                    Cuts[x].DisplayWidth = 0;
                }
            }       
        }

        public void SetCutProductionNumbers(long routeStepID){
            int productionNumber = 2;
            for(int x = 0; x < Cuts.Where(cut => cut.CutTypeDisplay == "Production").Count(); x++){
                if (Cuts[x].RouteStepID == routeStepID){
                    Cuts[x].ProductionNumber = "01" + productionNumber.ToString();
                }else{
                    Cuts[x].ProductionNumber = "01" + productionNumber.ToString();
                    productionNumber += 1;
                }
            }
        }
    }
}