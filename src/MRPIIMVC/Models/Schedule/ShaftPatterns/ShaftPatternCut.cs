using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace MRPIIMVC.Models.Schedule.ShaftPatterns
{
    public class ShaftPatternCut
    {
        public long ShaftPatternCutID { get; set; }
        public decimal CutWidth { get; set; }
        public string CutType { get; set; }
        public int DisplayOrder { get; set; }
        public decimal DisplayWidth {get; set;}
        public bool Hover { get; set; }
        public bool HasChanges { get; set; }
        public string WorkOrder { get; set; }
        public long RouteStepID { get; set; }
        public string ProductionNumber { get; set; }
        public bool CanDrag { get; set; }

        public string CutTypeDisplay 
        {
            get {
                return char.ToUpper(CutType[0]) + CutType.Substring(1);
            } 
        }

        public bool IsInDatabase {
            get {
                if (ShaftPatternCutID < 0)
                {
                    return false;
                };

                return true;
            }
        }
    }
}