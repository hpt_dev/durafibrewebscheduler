using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace MRPIIMVC.Models.Schedule.ShaftPatterns
{
    public class ShaftPatternsViewModel : ViewModel
    {
        private string roundToDecimalPlacesFormat = "";


        private void updateDecimalPlacesFormatString(int decimals){
            var formatString = "{0:0.";

            for(int x = 0; x < decimals; x++){
                formatString += "#";
            }
            formatString += "}";

            roundToDecimalPlacesFormat = formatString;

            Log(roundToDecimalPlacesFormat);
        }

        #region ADD/COPY
        public async Task<ShaftPattern> addShaftPattern(decimal masterWidth, decimal jobID, long routeStepID)
        {
            try
            {
                var showResults = 1; //This to use results to load the new pattern
                var sql = $"spPHX_AddJobShaftPattern {masterWidth}, {jobID}, {showResults}, {CurrentUser.id}, {routeStepID}";
                LogQuery(sql);

                var results = await db.getData(sql);
                var patternID = Convert.ToInt64(results[0]["ShaftPatternID"]);

                ShaftPattern shaftPattern = await getShaftPattern(patternID);

                return shaftPattern;
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return null;
            }
        }

        public async Task<long> copyJobPattern(long shaftPatternID, long jobID){
            try
            {
                var sql = $"spPHX_CopyJobShaftPattern {shaftPatternID}, {jobID}, {CurrentUser.id}";
                LogQuery(sql);

                var results = await db.getData(sql);
                
                if (results.Count > 0)
                {
                    return Convert.ToInt64(results[0]["ShaftPatternID"]);
                }
                else 
                {
                    return -1;
                }
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return -1;
            }
        }

        public async Task<ShaftPatternCut> addShaftPatternCut(long shaftPatternID, decimal cutWidth, string cutType, int cutOrder, long routeStepID, long jobID){
            try
            {
                if (await addUpdateRouteStepToJob(jobID, routeStepID))
                {
                    var sql = $"spPHX_AddShaftPatternCut {shaftPatternID}, {cutWidth}, '{cutType}', {cutOrder}, {routeStepID}, {CurrentUser.id}";
                    LogQuery(sql);

                    var results = await db.getData(sql);
                    if (results.Count > 0)
                    {
                        ShaftPatternCut cut = new ShaftPatternCut() {
                            ShaftPatternCutID = Convert.ToInt64(results[0]["ShaftPatternCutID"]),
                            RouteStepID = Convert.ToInt64(results[0]["RouteStepID"]),
                            CutType = cutType
                        };

                        return cut;       
                    }
                    else 
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return null;
            }
        }

        public async Task<bool> addUpdateRouteStepToJob(long jobID, long routeStepID){
            try
            {
                if (routeStepID > -1)
                {
                    var sql = $"spPHX_AddRouteStepToJob {jobID}, {routeStepID}, {CurrentUser.id}";
                    LogQuery(sql);

                    var results = await db.getData(sql);

                    return true;
                }
                else 
                {
                    return true;
                }
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return false;
            }                
        }
        #endregion

        #region GET

        public async Task<ShaftPattern> getShaftPattern(long patternID)
        {
            try
            {
                var sql = $"spPHX_GetShaftPatternCuts {patternID}";
                LogQuery(sql);

                var results = await db.getData(sql);
                
                if (roundToDecimalPlacesFormat == ""){
                    updateDecimalPlacesFormatString(3);
                }

                ShaftPattern patternReturn = new ShaftPattern(){
                    ShaftPatternID = Convert.ToInt64(results[0]["ShaftPatternID"]),
                    MasterWidth = Convert.ToDecimal(String.Format(roundToDecimalPlacesFormat, results[0]["MasterWidth"])),
                    Cuts = new List<ShaftPatternCut>(),
                    NumberOfSets = Convert.ToInt32(results[0]["NumberOfSets"]),
                    IsDefault = Convert.ToBoolean(results[0]["IsDefault"]),
                    CanChangeOnFloor = Convert.ToBoolean(results[0]["CanChangeOnFloor"]),
                    ItemNumber = results[0]["ItemNumber"].ToString()
                };

                for(int x = 0; x < results.Count; x ++){
                    patternReturn.Cuts.Add(new ShaftPatternCut(){
                        ShaftPatternCutID = Convert.ToInt64(results[x]["ShaftPatternCutID"]),
                        CutWidth = Convert.ToDecimal(String.Format(roundToDecimalPlacesFormat, results[x]["CutWidth"])),
                        CutType = Convert.ToString(results[x]["CutType"]).ToLower(),
                        DisplayOrder = Convert.ToInt32(results[x]["DisplayOrder"]),
                        Hover = false,
                        HasChanges = false,
                        CanDrag = true,
                        WorkOrder = results[x]["WorkOrder"].ToString(),
                        RouteStepID = Convert.ToInt64(results[x]["RouteStepID"])
                    });

                }

                patternReturn.FillShaft();  

                return patternReturn;
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return null;
            }
        }

        public async Task<ProductionJob> getJobPatterns(long jobID)
        {
            try
            {
                var sql = $"spPHX_GetJobPatterns {jobID}";
                LogQuery(sql);

                var results = await db.getData(sql);

                updateDecimalPlacesFormatString(Convert.ToInt32(results[0]["RoundWidthToDecimalPlaces"]));

                ProductionJob jobReturn = new ProductionJob(){
                    JobID = Convert.ToInt64(results[0]["JobID"]),
                    JobNumber = results[0]["JobNumber"].ToString(),
                    RoundWidthToDecimalPlaces = Convert.ToInt32(results[0]["RoundWidthToDecimalPlaces"]),
                    ShaftPatterns = new List<ShaftPattern>()
                };

                for(int x = 0; x < results.Count; x++){
                    ShaftPattern shaftPattern = await getShaftPattern(Convert.ToInt64(results[x]["ShaftPatternID"]));
                    shaftPattern.ItemNumber = results[x]["ItemNumber"].ToString();

                    jobReturn.ShaftPatterns.Add(shaftPattern);
                };

                jobReturn.Steps = await getJobRouteSteps(jobID);

                return jobReturn;
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return null;
            }
        }

        public async Task<List<RouteStep>> getJobSimilarRouteSteps(long patternID, long routeStepID){
            try
            {
                var sql = $"spPHX_GetJobSimilarRouteSteps {patternID}, {routeStepID}";
                LogQuery(sql);

                if (roundToDecimalPlacesFormat == ""){
                    updateDecimalPlacesFormatString(3);
                }

                var results = await db.getData(sql);

                List<RouteStep> routeSteps = new List<RouteStep>();

                for(int x = 0; x < results.Count; x++){
                        routeSteps.Add(new RouteStep() {
                            ParentJobID = Convert.ToInt64(results[x]["JobID"]),
                            RouteStepID = Convert.ToInt64(results[x]["RouteStepID"]),
                            StepNumber = results[x]["StepNumber"].ToString(),
                            StepDisplayNumber = results[x]["StepDisplayNumber"].ToString(),
                            StepQuantity = Convert.ToDecimal(results[x]["StepQuantity"]),
                            StepOffWidth = Convert.ToDecimal(String.Format(roundToDecimalPlacesFormat, results[x]["StepOffWidth"])),
                            WorkCenterGroup = results[x]["WorkCenterGroup"].ToString(),
                            WorkOrder = new WorkOrder(){
                                WorkOrderID = Convert.ToInt64(results[x]["WorkOrderID"]),
                                WorkOrderNumber = results[x]["WorkOrderNumber"].ToString(),
                                Customer = results[x]["Customer"].ToString(),
                                DueDate = Convert.ToDateTime(results[x]["DueDate"]),
                                FinishedQuantity = Convert.ToDecimal(results[x]["FinishedQty"]),
                                FinishedQuantityUOM = results[x]["FinishedQtyUOM"].ToString(),
                                Steps = new List<RouteStep>()
                            }
                        }
                    );
                }

                return routeSteps;
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return null;
            }
        }

         public async Task<List<RouteStep>> getJobRouteSteps(long jobID)
        {
            try
            {
                var sql = $"spPHX_GetJobRouteSteps {jobID}";
                LogQuery(sql);

                if (roundToDecimalPlacesFormat == ""){
                    updateDecimalPlacesFormatString(3);
                }

                var results = await db.getData(sql);

                List<RouteStep> routeSteps = new List<RouteStep>();

                for(int x = 0; x < results.Count; x++){
                        routeSteps.Add(new RouteStep() {
                            ParentJobID = Convert.ToInt64(results[x]["JobID"]),
                            RouteStepID = Convert.ToInt64(results[x]["RouteStepID"]),
                            StepNumber = results[x]["StepNumber"].ToString(),
                            StepDisplayNumber = results[x]["StepDisplayNumber"].ToString(),
                            StepQuantity = Convert.ToDecimal(results[x]["StepQuantity"]),
                            StepOffWidth = Convert.ToDecimal(String.Format(roundToDecimalPlacesFormat, results[x]["StepOffWidth"])),
                            WorkCenterGroup = results[x]["WorkCenterGroup"].ToString(),
                            HasWidthFromBOM = Convert.ToBoolean(results[x]["HasWidthFromBOM"]),
                            WidthUnit = "\"", //Hard Coded to Inches
                            OtherStepsHaveCuts = Convert.ToBoolean(results[x]["OtherStepsHaveCuts"]),
                            WorkOrder = new WorkOrder(){
                                WorkOrderID = Convert.ToInt64(results[x]["WorkOrderID"]),
                                WorkOrderNumber = results[x]["WorkOrderNumber"].ToString(),
                                Customer = results[x]["Customer"].ToString(),
                                DueDate = Convert.ToDateTime(results[x]["DueDate"]),
                                FinishedQuantity = Convert.ToDecimal(results[x]["FinishedQty"]),
                                FinishedQuantityUOM = results[x]["FinishedQtyUOM"].ToString(),
                                Steps = await getWorkOrderJobs(Convert.ToInt64(results[x]["WorkOrderID"]), jobID)
                            },
                            StepNote = results[x]["StepNotes"].ToString().Replace("\r", " ")
                        }
                    );
                }

                return routeSteps;
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return null;
            }
        }

        public async Task<List<RouteStep>> getWorkOrderJobs(long workOrderID, long currentJobID){
            try
            {
                var sql = $"spPHX_GetWorkOrderJobs {workOrderID}";
                LogQuery(sql);

                var results = await db.getData(sql);

                List<RouteStep> routeSteps = new List<RouteStep>();

                for(int x = 0; x < results.Count; x++){
                    routeSteps.Add(new RouteStep() {
                            ParentJobID = Convert.ToInt64(results[x]["JobID"]),
                            RouteStepID = Convert.ToInt64(results[x]["RouteStepID"]),
                            StepNumber = results[x]["StepNumber"].ToString(),
                            StepDisplayNumber = results[x]["StepDisplayNumber"].ToString(),
                            StepQuantity = 0,
                            StepOffWidth = 0,
                            WorkOrder = new WorkOrder(),
                            WorkCenterGroup = results[x]["WorkCenterGroup"].ToString(),
                            IsCurrentStep = (Convert.ToInt64(results[x]["JobID"]) == currentJobID)
                        }
                    );
                }

                return routeSteps;
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return null;
            }
        }
        #endregion

        #region UPDATE

        public async Task<bool> updateShaftPattern(long jobID, long shaftPatternID, bool isDefault, bool canChangeOnFloor, int numberOfSets)
        {
            try
            {
                var sql = $"spPHX_UpdateJobPattern {jobID}, {shaftPatternID}, {isDefault}, {canChangeOnFloor}, {numberOfSets}, {CurrentUser.id}";
                LogQuery(sql);

                var results = await db.getData(sql);

                if (results.Count > 0)
                {
                    return true;
                }
                else 
                {
                    return false;
                }
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return false;
            }
        }

        public async Task<bool> updateShaftPatternCut(long shaftPatternCutID, decimal cutWidth, string cutType, int cutOrder, long routeStepID, long jobID){
            try
            {
                var sql = $"spPHX_UpdateShaftPatternCut {shaftPatternCutID}, {cutWidth}, '{cutType}', {cutOrder}, {routeStepID}, {CurrentUser.id}";
                LogQuery(sql);

                var results = await db.getData(sql);

                if (results.Count > 0 && routeStepID > 0)
                {
                    if (await addUpdateRouteStepToJob(jobID, routeStepID))
                    {
                        return true;   
                    }
                    else
                    {
                        return false;
                    }
                }
                else 
                {
                    return false;
                }
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return false;
            }
        }

        #endregion
        
        #region DELETE

        public async Task<bool> deleteJobPattern(long shaftPatternID, long jobID){
            try
            {
                var sql = $"spPHX_DeleteJobShaftPattern {shaftPatternID}, {jobID}, {CurrentUser.id}";
                LogQuery(sql);

                var results = await db.getData(sql);

                if (results.Count > 0)
                {
                    return true;
                }
                else 
                {
                    return false;
                }
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return false;
            }
        }

        public async Task<string> deleteShaftPatternCut(long shaftPatternCutID)
        {

            try
            {
                var sql = $"spPHX_DeleteShaftPatternCut {shaftPatternCutID}, {CurrentUser.id}";
                LogQuery(sql);

                var results = await db.getData(sql);

                if (results[0]["ReloadMainScreen"].ToString() == "True"){
                    return "Main";
                }
                else if (results[0]["ReloadEditScreen"].ToString() == "True") {
                    return "Edit";
                }
                else 
                {
                    return "None";
                }
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return "None";
            }
        }

        public async Task<bool> updateStepWidth(long jobID, long routeStepID, decimal width)
        {
            try
            {
                var sql = $"spPHX_UpdatePatternStepWidth {jobID}, {routeStepID}, {width}, {CurrentUser.id}";
                LogQuery(sql);

                var results = await db.getData(sql);

                return true;
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return false;
            }
        }

        public async Task<bool> updateJobStepQuantity(long jobID, decimal quantity)
        {
            try
            {
                var sql = $"spPHX_UpdateJobStepQuantity {jobID}, {quantity}, {CurrentUser.id}";
                LogQuery(sql);

                var results = await db.getData(sql);

                return true;
            }
            catch(Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return false;
            }
        }

        public async Task<bool> updateJobStepNote(long jobID, string note)
        {
            try
            {
                var sql = $"spPHX_UpdateJobStepNotes {jobID}, '{note}', {CurrentUser.id}";
                LogQuery(sql);

                var results = await db.getData(sql);

                return true;
            }
            catch (Exception ex)
            {
                Log("FAIL: " + ex.Message);
                return false;
            }
        }

        #endregion

        #region Logging
        private void LogQuery(string query)
        {
            query = "Exec " + query;
            Console.WriteLine(query);
        }
        private void Log(string message)
        {
            Console.WriteLine(message);
        }
        #endregion
    }
}