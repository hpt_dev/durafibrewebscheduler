using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace MRPIIMVC.Models.Schedule.ShaftPatterns
{
    public class WorkOrder
    {
        public long WorkOrderID { get; set; }

        public string WorkOrderNumber { get; set; }

        public string Customer { get; set; }

        public DateTime DueDate { get; set; }

        public string DueDateDisplay {
            get {
                return DueDate.ToString("d");
            }
        }

        public decimal FinishedQuantity { get; set; }

        public string FinishedQuantityUOM { get; set; }

        public string FinishedQuantityDisplay 
        { 
            get 
            {
                return String.Format("{0:#.##}", FinishedQuantity) + " " + FinishedQuantityUOM;
            }
        }

        public List<RouteStep> Steps { get; set; }
    }
}