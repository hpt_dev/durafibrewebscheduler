﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace MRPIIMVC.Models.Security
{
    public class PageSecurity : ViewModel
    {

        public static List<Page> _cachedPages;
        public static List<Page> cachedPages { get { return _cachedPages; } }
        public static Page getPage(string path)
        {
            try //TODO move this out of the database and into the project file system
            {


                if (cachedPages == null)
                {
                    _cachedPages = new List<Page>();
                    var pages = db.ad_page_security.ToList();
                    foreach (var p in pages)
                    {
                        var pp = Newtonsoft.Json.JsonConvert.DeserializeObject<Page>(p.json);
                        _cachedPages.Add(pp);
                    }
                }
                var page = cachedPages.FirstOrDefault(x => x.path == path);
                if (page == null)
                {
                    page = new Page();
                    page.path = path;
                    cachedPages.Add(page);
                }

                return page;
            }
            catch (Exception)
            {

                return null;
            }
        }

        List<string> Company = new List<string>(new string[] { "SHS", "PopInk" });

        public class pageVM
        {
            public Page page { get; set; }
            public Page layoutPage { get; set; }
            public List<string> companies { get; set; }
            public List<SecurityGroup> securityGroups { get; set; }
            public async Task<bool> save()
            {
                if (page != null)
                    await page.save();
                if (layoutPage != null)
                    await layoutPage.save();
                return true;
            }
        }
        public pageVM getVM(string path)
        {
            var ret = new pageVM();
            ret.companies = Company;
            ret.securityGroups = db.ad_security_groups.Select(x => new SecurityGroup()
            {
                name = x.group_name,
                id = x.security_group_id,
            }).ToList();
            ret.page = cachedPages.FirstOrDefault(x => x.path != null && x.path.ToLower() == path.ToLower());
            ret.layoutPage = cachedPages.FirstOrDefault(x => x.path.ToLower() == "layout");
            return ret;
        }


        public class Page
        {
            public Page() { this.secElements = new List<SecurityElement>(); }

            public async Task<bool> save()
            {
                var dbPage = db.ad_page_security.FirstOrDefault(x => x.path == path);
                if (dbPage == null)
                {
                    dbPage = db.ad_page_security.Create();
                    dbPage.path = path;
                    db.ad_page_security.Add(dbPage);
                }
                dbPage.json = Newtonsoft.Json.JsonConvert.SerializeObject(this);
                await db.SaveChangesAsync();
                _cachedPages.Remove(_cachedPages.FirstOrDefault(x => x.path == path));
                _cachedPages.Add(this);

                return true;
            }

            public string path { get; set; }
            public List<SecurityElement> secElements;
            public class SecurityElement
            {
                public SecurityElement() { rules = new List<SecurityRule>(); }
                public string id { get; set; }
                public string content { get; set; }
                public List<SecurityRule> rules { get; set; }

                public class SecurityRule
                {
                    public List<string> companyies { get; set; }
                    public List<long> securityGroups { get; set; }
                    public bool match { get; set; }
                    public bool hide { get; set; }
                    public string replaceContent { get; set; }
                    public string orignalText { get; set; }
                }
            }
        }


        public class SecurityGroup
        {
            public string name { get; set; }
            public long id { get; set; }
        }
    }


}
