﻿using System;
using Data;
using System.Linq;
using System.Runtime.Serialization;
using Microsoft.AspNetCore.Hosting;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MRPIIMVC.Models
{
    
    public class ViewModel
    {
        [IgnoreDataMember]
        public Models.Account.User CurrentUser { get
            {
                try
                {
                    return  Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Account.User>(System.Web.HttpContext.Current.User.Identity.Name);
                }
                catch (Exception)
                {
                    try // get user id from header referal page silverlight ajax methods without cookies
                    {
                        var refer = System.Web.HttpContext.Current.Request.Headers["Referer"][0];
                        refer = refer.Substring(refer.IndexOf('?') + 1);
                        var prams = refer.Split('&');
                        var user = prams.FirstOrDefault(x => x.ToLower().StartsWith("userid"));
                        var id = user.Split('=')[1];
                        var um = new MRPIIMVC.Models.Account.UserManager();
                        return um.getUserById(long.Parse(id));
                    }
                    catch (Exception)
                    {

                    }
                    return null;
                }
            }
        }
        [IgnoreDataMember]
        public static Data.Phoenix db
        {
            get
            {
                return (Phoenix)System.Web.HttpContext.Current.RequestServices.GetService(typeof(Phoenix));
            }
        }
        [IgnoreDataMember]
        public static IHostingEnvironment hostingEnv
        {
            get
            {
                return (IHostingEnvironment)System.Web.HttpContext.Current.RequestServices.GetService(typeof(IHostingEnvironment));
            }
        }
    }
}
