﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Configuration;
using Data;
using System.IO;
using MRPIIMVC.Helpers;
using Microsoft.AspNetCore.DataProtection;

namespace MRPIIMVC
{
    public class Startup
    {      
        public Startup(IHostingEnvironment env)
        {           
            var path = Directory.GetCurrentDirectory();
            if (path.Contains("\\src\\test\\bin"))
                path = path.Substring(0,path.IndexOf("\\src\\test\\bin")) +"\\src\\test\\";

            var builder = new ConfigurationBuilder()
                .SetBasePath(path)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)                                
                .AddEnvironmentVariables();
                
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var rootFolder = Directory.GetCurrentDirectory();
            // Add framework services.
            var config = Configuration.GetSection("ConnectionStrings");
            var test = config.GetValue<string>("PhoenixTest");
            var connection = config.GetValue<string>("phoenix");

            var configEnvronments = new List<ConfigEnvironments>();
            Configuration.GetSection("Environments").Bind(configEnvronments) ;           
            var c = configEnvronments.FirstOrDefault(x => rootFolder.Contains(x.path));
            if (c != null)
                connection = c.phoenix;
            
            services.AddMvc( options => options.CacheProfiles.Add("Never",new Microsoft.AspNetCore.Mvc.CacheProfile() { Location = Microsoft.AspNetCore.Mvc.ResponseCacheLocation.None, NoStore = true }));
            services.AddScoped<Phoenix>(_ => new Phoenix(connection));
            services.AddScoped<Models.Account.User>(_ => System.Web.HttpContext.Current.User.Identity.Name == null ? new Models.Account.User() : Newtonsoft.Json.JsonConvert.DeserializeObject<MRPIIMVC.Models.Account.User>(System.Web.HttpContext.Current.User.Identity.Name));          
            services.AddSingleton<Microsoft.AspNetCore.Http.IHttpContextAccessor, Microsoft.AspNetCore.Http.HttpContextAccessor>();

           
           // !!! Requires App pool Load User Profile = true   !!!!
            services.AddDataProtection() // needed for updated kestrel                                           
                           .PersistKeysToRegistry(Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"SOFTWARE\MRPII\Keys", true));            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceProvider svp)
        {           
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            System.Web.HttpContext.Configure(app.ApplicationServices.
                 GetRequiredService<Microsoft.AspNetCore.Http.IHttpContextAccessor>());


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                app.UseDeveloperExceptionPage();
            }
            
            app.UseStaticFiles();
            app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                AccessDeniedPath = "/Account/Forbidden/",
                AuthenticationScheme = "MyCookieAuthenticationScheme",
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                LoginPath = "/Account/Login/"
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
    public class ConfigEnvironments
    {
        public string path { get; set; }
        public string phoenix { get; set; }
    }
}
