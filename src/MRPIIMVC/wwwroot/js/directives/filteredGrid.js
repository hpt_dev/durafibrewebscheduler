﻿function getGridTemplate(title, target, info) {
    return '<div class="row">'
        + '<div ng-if="filter.visible && !filter.hidden" class="col-xs-3" >'
        + '<div class="btn btn-default" ng-click="search()"><span class="glyphicon glyphicon-search"> </span> {{name}} </div>'
        + ' <div ng-repeat="f in vm.gridFilters | orderBy :\'disporder\'" >'
        + '     <div class="row" ng-if="f.controlType === 0 && f.isVisible">' //text box
        + '         <div class="col-xs-5" >{{f.text}}</div><select style="padding:0;" class="col-xs-2" ng-model="f.conditionTypeId" ng-options="+(x.id) as x.name for x in vm.filterConditions"></select><input class="col-xs-5" ng-model="f.value" ng=change="filterChanged();" />    '
        + '     </div>'
        + '     <div class="row" ng-if="f.controlType === 1 && f.isVisible">' // combo
        + '         <div class="col-xs-5" >{{f.text}}</div><select style="padding:0;" class="col-xs-2" ng-model="f.conditionTypeId" ng-options="+(x.id) as x.name for x in vm.filterConditions"></select> <select class="col-xs-5" ng-model="f.selected" ng-options="x.name for x in f.sourceCollection track by x.id" ng-change="filterChanged(); f.value = f.selected.id" ></select>    '
        + '     </div>'
        + '     <div class="row" ng-if="f.controlType === 2 && f.isVisible">'//check box
        + '         <div class="col-xs-5" >{{f.text}}</div><select style="padding:0;" class="col-xs-2" ng-model="f.conditionTypeId" ng-options="+(x.id) as x.name for x in vm.filterConditions"><input class="col-xs-5" ng-model="f.value" ng=change="filterChanged();" type="checkbox" />    '
        + '     </div>'
        + '     <div class="row" ng-if="f.controlType === 3 && f.isVisible">'// date
        + '           <div class="col-xs-5">{{f.text}}</div><select style="padding:0;" class="col-xs-2" ng-model="f.conditionTypeId" ng-options="+(x.id) as x.name for x in vm.filterConditions"><input class="col-xs-5" ng-model="f.value" ng=change="filterChanged();" />    '
        + '     </div>'
        +'  </div>'
        +' </div>'
        + '<div ng-class="getDataClass()" >'
        + '<div class="row">'
        + '   <span ng-if="filter.visible && !filter.hidden" class="glyphicon glyphicon-arrow-left" ng-click="filter.hidden=true"> </span>'
        + '   <span ng-if="filter.visible && filter.hidden" class="glyphicon glyphicon-arrow-right" ng-click="filter.hidden=false"> </span>'
        + ' <ul class="pagination" style="margin:0;"> <li ng-repeat="b in paging.buttons" ><a href="#" ng-click=b.click() >{{b.text}}</a></li></ul> Count: {{results && results.data ? results.data.length : 0}} '
        + '</div>'
        + '<div style="max-width:100%; overflow:auto; max-height:600px;" ><table class="table table-hover table-bordered table-responsive  ">'
        + '  <thead>'
        + '      <tr><th ng-repeat="c in vm.gridColumns | orderBy :\'disporder\'" ng-if="c.isVisible" >{{c.text}}</th></tr>'
        + '  </thead>'
        + '  <tbody>'
        + '      <tr ng-repeat="d in visibleData"  ng-style="getRowStyle(d)" ng-click="selectRow(d)" ><td ng-repeat="c in vm.gridColumns |orderBy :\'disporder\'" ng-if="c.isVisible" >{{getData(c,d)}}</td></tr>'
        + '  </tbody>'
        + '</table></div>'
        + '</div>'
        + '</div><pre>{{visibleData |json :5}}</pre> '
}




if (window.app != undefined) {
    app.directive('filteredGrid', ['$http', '$compile', '$sce', function ($http, $compile, $sce) {
        return {
            restrict: 'E',
            scope: { target: "@target", grid: "@name", opts: "@options" },
            transclude: true,
            //templateUrl: "../scripts/GridTemplate.html",
            template: getGridTemplate,


            link: function (scope, element, attrs) {
                scope.$parent.$watch(attrs.grid, function (value) { // allow preview to switch grids. 
                    scope.name = value;
                    if (!scope.name) return;
                    scope.load();
                }, true);
                scope.filter = { visible: true, hidden: false };

                scope.$parent.$watch(attrs.options, function (value) {
                    scope.options = value
                    if (scope.options.hideFilter)
                        scope.filter.visible = false;
                }, true);

                scope.load = function () {
                    $http.get(window.urlBase + "grid/getGridForViewing?name=" + scope.name)
                       .then(function mySuccess(response) {
                           scope.vm = response.data;
                           scope.paging.gridVm = scope.vm;
                           scope.results = {};
                           scope.selected = [];
                           for (var j = 0, filter; filter = scope.vm.gridFilters[j]; j++) {
                               if (filter.sourceCollection) {
                                   for (var k = 0, c; c = filter.sourceCollection[k]; k++) {
                                       if (c.id == filter.value)
                                           filter.selected = c;
                                   }
                               }
                           }
                           scope.options.gridVM = scope.vm;
                           scope.options.selected = scope.selected;
                           scope.options.results = scope.results;
                           if (scope.options && scope.options.ready && typeof (scope.options.ready) == "function")
                               scope.options.ready();


                       }, function myError(response) {
                           toastr.error("Failed to load VM", '')
                       });
                }

                scope.filterChanged = function () {
                    $http({
                        url: window.urlBase + "grid/updateGridFilters",
                        method: "POST",
                        data: scope.vm,
                        headers: { 'Content-Type': 'application/json' }
                    }).then(function mySuccess(response) {
                        scope.vm = response.data;
                        for (var j = 0, filter; filter = scope.vm.gridFilters[j]; j++) {
                            if (filter.sourceCollection) {
                                var found = false;
                                for (var k = 0, c; c = filter.sourceCollection[k]; k++) {
                                    if (c.id == filter.value) {
                                        filter.selected = c;
                                        found = true;
                                    }
                                }
                                if (!found && filter.sourceCollection[0]) {
                                    filter.selected = filter.sourceCollection[0];
                                    filter.value = filter.sourceCollection[0].id;
                                }
                            }
                        }
                    }, function myError(response) {
                        toastr.error("Failed to Update Grid Filters", '')
                    });
                }
                scope.search = function () {
                    $http({
                        url: window.urlBase + "grid/Search",
                        method: "POST",
                        data: scope.vm,
                        headers: { 'Content-Type': 'application/json' }
                    }).then(function mySuccess(response) {
                        scope.results = response.data;
                        scope.paging.parent = scope;
                        scope.paging.updateData(scope.results, scope);                        
                        scope.options.results = scope.results;
                        if (scope.options && scope.options.dataLoaded && typeof (scope.options.dataLoaded) == "function")
                            scope.options.dataLoaded();

                        if (scope.results.error)
                            toastr.error(scope.results.error, '')
                    }, function myError(response) {
                        toastr.error("Failed to Update Grid Filters", '')
                    });
                }
                scope.getData = function (c, d) {
                    return d[c.text];
                }
                scope.getRowStyle = function (d) {
                    var ret = {};
                    var match = false;
                    for (var j = 0, c; c = scope.vm.gridColors[j]; j++) {
                        var match = false;
                        for (var k = 0, column; column = scope.vm.gridColumns[k]; k++) {
                            if (column.id === c.columnId) {
                                match = column;
                                break;
                            }
                        }
                        if (match) {
                            if (d[match.text].toString().toLocaleLowerCase() == c.value.toLocaleLowerCase()) {
                                ret = { "background-color": "#" + c.color.substr(2) }
                            }
                        }
                    }
                    for (var j = 0, it; it = scope.selected[j]; j++) {
                        if (it == d) {
                            ret = { "background-color": "#FFA07A" }
                        }
                    }
                    return ret;
                }
                scope.selectRow = function (row) {
                    if (scope.options && scope.options.multiselect) {
                        var newSelected = [];
                        var found = false;
                        for (var j = 0, it; it = scope.selected[j]; j++) {
                            if (it == row)
                                found = true;
                            else
                                newSelected.push(it);
                        }
                        if (!found)
                            newSelected.push(row);
                        scope.selected = newSelected;
                    }
                    else
                        scope.selected = [row]
                    if (scope.options && scope.options.selectionChanged && typeof (scope.options.selectionChanged) == "function")
                        scope.options.selectionChanged();

                }
                scope.getFilterClass = function () {
                    if (!scope.filter.visible || !scope.filter.hidden)
                        return "invisible"
                    return "col-xs-3"
                }
                scope.getDataClass = function () {
                    if (!scope.filter.visible || scope.filter.hidden)
                        return "col-xs-12"
                    return "col-xs-9"

                }
               
                scope.name = attrs.grid
                scope.ele = element;

                
                scope.paging = new Paging(scope);
            }

        }
    }]);
}
 function Paging(s) {
    this.scope = s;
    this.gridVM= {},
    this.data= [],
    this.visibleData= [],
    this.buttons= [
        {
            text: "1", click: function () {
                this.parent.currentPage = this.page; this.parent.update();
            }, parent: this, page: 1
        },
        {
            text: "2", click: function () {
                this.parent.currentPage = this.page; this.parent.update();
            }, parent: this, page: 2
        }
    ]
    this.currentPage= 1,
    this.updateData= function (d) {
        this.data = d; this.update()
    }
    this.update= function () {
        visibleData = [];
        var pageSize = 10;
        for (var j = pageSize * (this.currentPage - 1) ; j < this.data.data.length && j < pageSize * this.currentPage ; j++) {
            visibleData.push(this.data.data[j]);
        }
        this.scope.visibleData = visibleData;
    }
}
(function ($) {
    $.tableToExcel = {
        version: "1.0",
        uri: 'data:application/vnd.ms-excel;base64,',
        template: '<html xmlns:v="urn:schemas-microsoft-com:vml"xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv=Content-Type content="text/html; charset=UTF-8"><meta name=ProgId content=Excel.Sheet><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Worksheet</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table border=1>{table}</table></body></html>',
        base64: function (s) { return window.btoa(unescape(encodeURIComponent(s))); },
        format: function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }); },
    };

    $.fn.extend({
        tableToExcel: function (options) {
            options = $.extend({ name: 'Worksheet' }, options);
            return $(this).each(function () {
                var ctx = { worksheet: options.name, table: $(this).html() };
                if (window.isIE9 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                {
                    txtArea1.document.open("txt/html", "replace");
                    txtArea1.document.write($.tableToExcel.format($.tableToExcel.template, ctx));
                    txtArea1.document.close();
                    txtArea1.focus();
                    sa = txtArea1.document.execCommand("SaveAs", true, options.name + ".xls");
                }
                else                 //other browser not tested on IE 11

                    window.location.href = $.tableToExcel.uri + $.tableToExcel.base64($.tableToExcel.format($.tableToExcel.template, ctx));
            });
        }
    });
}(window.jQuery));


function formatDates(data) {
    for (var j = 0, it; it = data[j]; j++) {
        if (it.START_TIME) {
            d = new Date(it.START_TIME)
            it.START_TIME = d.toLocaleDateString() + " " + d.toLocaleTimeString()
        }
        if (it.END_TIME) {
            d = new Date(it.END_TIME)
            it.END_TIME = d.toLocaleDateString() + " " + d.toLocaleTimeString()
        }
        if (it.DATE) {
            d = new Date(it.DATE)
            it.DATE = d.toLocaleDateString() + " " + d.toLocaleTimeString()
        }
    }
}