﻿
// because ie adds non ascii to toLocaleDateString
Date.prototype.toLocaleDateString = function () { return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear(); }

Date.prototype.shortDateTime = function () {
    return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear() + " "
        + (this.getHours() > 12 ? this.getHours() - 12 : this.getHours()) + ":" + (this.getMinutes() < 10 ? ("0" + this.getMinutes()) : this.getMinutes()) + " " + (this.getHours() > 11 ? "PM" : "AM")
}


var bind = Function.bind;
var unbind = bind.bind(bind);

function instantiate(constructor, args) {
    return new (unbind(constructor, null).apply(null, args));
}

/**Date = function (Date) {

    // copy date methods - this is a pain in the butt because they're mostly nonenumerable
    var names = Object.getOwnPropertyNames(Date);
    for (var i = 0; i < names.length; i++) {
        if (names[i] in MyDate) continue;
        var desc = Object.getOwnPropertyDescriptor(Date, names[i]);
        Object.defineProperty(MyDate, names[i], desc);
    }
    return MyDate;

    function MyDate() {
        // we only care about modifying the constructor if a datestring is passed in

        // hoping we dont need this in this project
        if (arguments.length === 1 && typeof (arguments[0]) === 'string') {
            // if you're adding other date transformations, add them here
            var dateReg = /\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d/g
            if (dateReg.test(arguments[0])) {               
                var ret = new Date();              
                var parts = arguments[0].split(/-|:|T/g)
                ret.setFullYear(parts[0])
                ret.setMonth(parts[1] - 1)
                ret.setDate(parts[2])
                ret.setHours(parts[3], parts[4], parts[5])
              
                return ret;
            }

            //ie  sql date is passes as "2017-02-07 08:16:54.000"
            var dateReg2 = /\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d.\d\d\d/g
            if (dateReg2.test(arguments[0])) {
                var d = arguments[0].split(/\s|\.|\-|\:/g)
                return new Date(d[0], d[1] - 1, d[2], d[3], d[4], d[5], d[6])
            }

            // match dates of format m-d-yyyy and convert them to cross-browser-friendly m/d/yyyy
            var mdyyyyDashRegex = /(\d{1,2})-(\d{1,2})-(\d{4})/g;
            arguments[0] = arguments[0].replace(mdyyyyDashRegex, function (match, p1, p2, p3) {
                return p1 + "/" + p2 + "/" + p3;
            });
        }
        // call the original Date constructor with whatever arguments are passed in here
        var date = instantiate(Date, arguments);

        MyDate.prototype = Date.prototype;
        return date;
    }
}(Date);*/