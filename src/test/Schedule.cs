﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using MRPIIMVC.Controllers;
using Microsoft.AspNetCore.TestHost;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using MRPIIMVC;
using Data;
using System.IO;

namespace test
{
    public class Schedule
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        private Data.Phoenix db;
            
        public Schedule()
        {
            var path = Directory.GetCurrentDirectory();
            if (path.Contains("\\src\\test\\bin"))
                path = path.Substring(0, path.IndexOf("\\src\\test\\bin")) + "\\src\\MRPIIMVC\\";

            _server = new TestServer(new WebHostBuilder()
                .UseContentRoot(path)
                .UseStartup<Startup>());
            _client = _server.CreateClient();
            db = (Phoenix)_server.Host.Services.GetService(typeof(Data.Phoenix));
            // controller = new ScheduleController();
        }
        [Fact]
        public async void has_sc_appointments()
        {            
            var response = await _client.GetAsync("/schedule/getTasks");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var results = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MRPIIMVC.Models.Schedule.CalendarViewModel.Appointment>>(responseString);
            Assert.NotEmpty(results);
        }
        [Fact]
        public async void has_calendarOptions()
        {
            var a =await  db.getData<MRPIIMVC.Models.Schedule.CalendarViewModel.AppointmentType>(@"exec sc_getEventTypes");
            
            Assert.NotEmpty(a);
        }
        [Fact]
        public async void canPassUserId()
        {
            _client.DefaultRequestHeaders.Add("Referer", "asdf?userid=0");
                                    
            var response = await _client.GetAsync("/account/CurrentUserInfo");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var user = Newtonsoft.Json.JsonConvert.DeserializeObject<MRPIIMVC.Models.Account.User>(responseString);
            Assert.Equal(0, user.id);
        }
        [Fact]
        public async void canReadViewPage()
        {
            _client.DefaultRequestHeaders.Add("Referer", "asdf?userid=0");

            var response = await _client.GetAsync("/home/about");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var a = 1;
        }
    }
}
